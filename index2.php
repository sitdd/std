<?php session_start();
if($_SESSION['CODIGO_TRABAJADOR']!=""){
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <title>SISTEMA INTEGRAL DE TRAMITE DOCUMENTARIO</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    <link href="cInterfaseUsuario_SITD/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="cInterfaseUsuario_SITD/css/mdb.min.css" rel="stylesheet">
    <link href="cInterfaseUsuario_SITD/css/main.css" rel="stylesheet">
</head>

<body>

<header>
    <header>

        <div class="firstmenu">

            <!--Navbar-->
            <nav class="navbar navbar-expand-lg navbar-dark colorfondo fixed-top clearfix" id="navegacion">

                <a class="navbar-brand" href="main.php" class="shortcut"><b>SITDD</b></a>

                <!-- Siempre presente -->

                <div class="navbar-nav ml-auto">
                    <div class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-transform: capitalize">
                            <img class="img-profile-menu" src="cInterfaseUsuario_SITD/images/foto.jpg" alt="<?php echo  $RsNom[cNombresTrabajador] ?>">&nbsp;<?php echo strtolower($RsNom[cNombresTrabajador]); ?></a>
                        <div class="dropdown-menu dropdown-menu-right " aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="cInterfaseUsuario_SITD/logout.php" class="shortcut"><i class="fas fa-sign-out-alt"></i>&nbsp;Salida</a>
                        </div>
                    </div>
                </div>
                <!-- fin Siempre presente -->
            </nav>
            <!--/.Navbar-->


        </div>

        <div class="secondmenu">
            <!--Navbar-->
            <nav class="navbar navbar-expand-lg navbar-dark colorfondo fixed-top" id="navegacion">

                <!-- Navbar brand -->
                <a class="navbar-brand" href="main.php" class="shortcut"><b>SITDD</b></a>

                <!-- Collapse button -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- Collapsible content -->
                <div class="collapse navbar-collapse" id="navbarNav" style="text-align: left!important;">

                    <!-- Links -->
                    <div class="navbar-nav ml-auto">

                        <!-- Siempre presente -->
                        <div class="nav-item dropdown">
                            <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-transform: capitalize"><img class="img-profile-menu" src="images/foto.jpg" alt="<?php echo  $RsNom[cNombresTrabajador] ?>">&nbsp;<?php echo strtolower($RsNom[cNombresTrabajador]); ?><span class="dropdown-toggle d-inline"></span></a>
                            <div class="dropdown-menu dropdown-menu-right " aria-labelledby="navbarDropdownMenuLink">
                                <p class="dropdown-header droptitulo" style="text-transform: capitalize "><?php echo strtolower($RsNom[cNombresTrabajador]); ?><p>
                                    <a class="dropdown-item" href="cInterfaseUsuario_SITD/logout.php" class="shortcut"><i class="fas fa-sign-out-alt"></i>&nbsp;Salida</a>
                            </div>
                        </div>
                        <!-- fin Siempre presente -->

                    </div>
                    <!-- Links -->


                </div>
                <!-- Collapsible content -->

            </nav>
            <!--/.Navbar-->
        </div>

    </header>

</header>

<main class="mx-lg-5">

        <div class="container">

            <div class="row justify-content-center">

                        <?php include_once("conexion/conexion.php"); ?>
                        <?php
                        $sqlTem="select iCodPerfilUsuario,(select cDescPerfil from Tra_M_Perfil where iCodPerfil=o.iCodPerfil) as iCodPerfil,(select cNomOficina from Tra_M_Oficinas where iCodOficina=o.iCodOficina) as iCodOficina 
                        from Tra_M_Perfil_Ususario o where iCodTrabajador='".$_SESSION['CODIGO_TRABAJADOR']."'";
                        $rsTem=mssql_query($sqlTem,$cnx);
                        while ($RsTem=MsSQL_fetch_array($rsTem)){

                            ?>

                            <form class="col-9 col-sm-5 col-md-4 col-lg-3 m-2" method='POST' action='cInterfaseUsuario_SITD/login_next.php' name='Datos'>

                                <input type='hidden' value=' <? echo $_SESSION['CODIGO_TRABAJADOR'] ?>' name='id_usuario'>
                                    <div>

                                    <div class='card'>

                                        <div class='card-header text-center'>
                                            <?php echo $RsTem['iCodPerfil'] ?>
                                        </div>

                                        <div class='card-body'>

                                            <input type='hidden' name='perfil' value='<?php echo $RsTem["iCodPerfilUsuario"] ?>'>

                                            <h5 class='card-title text-center'><a><?php echo $RsTem["iCodOficina"] ?></a></h5>

                                            <div class='row justify-content-center'>
                                                <input name='Submit' type='submiT' onClick='loguear2()' class='btn btn-black' value='INGRESAR'/>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </form>

                        <?php ;
                        }

                        ?>

            </div>

        </div>

    <?php
    switch ($_GET["alter"]) {
        case 2:
            $observacion = "<h5><font color=white><b>SALIDA</b><br>Ud. ha salido correctamente del sistema.</font></h5>";
            $notificacion="
                                <br>
                                <div class='rectangle'>
                                <div class='notification-text'>
                                <font color='white'>$observacion</font>
                                </div>
                                </div>
                            ";
            break;
        case 3:
            $observacion = "<h5><font color=white><b>ERROR</b> - datos vacios<br>\"ingrese correctamente\"</font></h5>";
            $notificacion="
                                <br>
                                <div class='rectangle'>
                                <div class='notification-text'>
                                <font color='white'>$observacion</font>
                                </div>
                                </div>
                            ";
            break;
        case 4:
            $observacion = "<h5><font color=white>ERROR... <b>Clave incorrecta</b> o <br>es <b>Cuenta Incorrecta</b></font></h5>";
            $notificacion="
                                <br>
                                <div class='rectangle'>
                                <div class='notification-text'>
                                <font color='white'>$observacion</font>
                                </div>
                                </div>
                            ";
            break;
        case 5:
            $observacion = "<h5><font color=white>ERROR... <b>Usuario no autorizado</b></font></h5>";
            $notificacion="
                                <br>
                                <div class='rectangle'>
                                <div class='notification-text'>
                                <font color='white'>$observacion</font>
                                </div>
                                </div>
                            ";
            break;
    };
    ?>

    <?php echo $notificacion;?>
</main>

<!--Footer-->
<footer class="page-footer fixed-bottom blue darken-4 text-center">
    SITDD-APCI
</footer>
<!--/.Footer-->


    <script language="JavaScript" type="text/javascript">
      if (document.Datos) {
        document.Datos('usuario').focus();
      }
    </script>
    <!-- JQuery -->
    <script type="text/javascript" src="cInterfaseUsuario_SITD/js_select/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="cInterfaseUsuario_SITD/js_select/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="cInterfaseUsuario_SITD/js_select/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="cInterfaseUsuario_SITD/js_select/mdb.min.js"></script>

<script type="text/javascript">
    function loguear2() {
        document.Datos.submit();
    }
</script>

<?php
include_once("conexion/conexion.php");
$sqlUsr = "SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$_SESSION[CODIGO_TRABAJADOR]'";
$rsUsr  = mssql_query($sqlUsr,$cnx);
$RsUsr  = mssql_fetch_array($rsUsr);

$hora= date('H')-1;
if($hora<12){
    $saludo = "Buenos dias";
}elseif ($hora<7){
    $saludo = "Buenas tardes";
}else{
    $saludo = "Buenas noches";
}
?>


<script>
    Command: toastr["info"]("Bienvenido al Sistema de Tramite Documentario"," <?php echo $saludo. " ". utf8_encode($RsUsr[cNombresTrabajador]);?> ")

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": false,
        "showDuration": 300,
        "hideDuration": 1000,
        "timeOut": 5000,
        "extendedTimeOut": 1000,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

</script>


</body>

</html>
    <?php
}else{
    header("Location: index.php?alter=5");
}
?>