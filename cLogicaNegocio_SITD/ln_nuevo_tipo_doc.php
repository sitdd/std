<?php

/* * ************************************************************************************
  NOMBRE DEL PROGRAMA: ln_nuevo_tipo_doc.php
  SISTEMA: SISTEMA INTEGRAL DE TR�MITE DOCUMENTARIO
  OBJETIVO: Enlazar la Capa de Datos de la Tabla Maestra de Tipo de Documentos
  -> Crear Registro de Tipo de Documento
  PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL


  CONTROL DE VERSIONES:
  Ver      Autor             Fecha        Descripci�n
  ------------------------------------------------------------------------
  1.0   APCI       03/08/2018   Creaci�n del programa.

  ------------------------------------------------------------------------
 * *************************************************************************************** */
require_once("../conexion/conexion.php");
require_once("../cAccesoBaseDato_SITD/ad_nuevo_tipo_doc.php");
?>