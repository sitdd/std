<?php

/* * ************************************************************************************
  NOMBRE DEL PROGRAMA: ln_actualiza_corre_interno.php
  SISTEMA: SISTEMA INTEGRAL DE TRÁMITE DOCUMENTARIO
  OBJETIVO: Enlazar la Capa de Datos de la Tabla Maestra de Correlativos
  -> Actualizar Registro de Correlativos
  PROPIETARIO: AGENCIA PERUANA DE COOPERACIÓN INTERNACIONAL


  CONTROL DE VERSIONES:
  Ver      Autor             Fecha        Descripción
  ------------------------------------------------------------------------
  1.0   APCI       03/08/2018   Creación del programa.

  ------------------------------------------------------------------------
 * *************************************************************************************** */
require_once("../conexion/conexion.php");
require_once("../cAccesoBaseDato_SITD/ad_actualiza_correlativo.php");
?>
