<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: consultaInternoGeneral.php
SISTEMA: SISTEMA  DE TRÁMITE DOCUMENTARIO DIGITAL
OBJETIVO: Consulta de los Documentos Internos Generales
PROPIETARIO: AGENCIA PERUANA DE COOPERACIÓN INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver      Autor             Fecha        Descripción
------------------------------------------------------------------------
1.0   APCI       03/08/2018   Creación del programa.
 
------------------------------------------------------------------------
*****************************************************************************************/
?>
<?
session_start();
If($_SESSION['CODIGO_TRABAJADOR']!=""){
include_once("../conexion/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<script type="text/javascript" language="javascript" src="includes/lytebox.js"></script>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
<link type="text/css" rel="stylesheet" href="css/dhtmlgoodies_calendar.css" media="screen"/>
<script type="text/javascript" src="scripts/dhtmlgoodies_calendar.js"></script>
<script Language="JavaScript">

function Buscar()
{
  document.frmConsultaEntrada.action="<?=$PHP_SELF?>";
  document.frmConsultaEntrada.submit();
}

//--></script>
</head>
<body>

	<?include("includes/menu.php");?>



<!--Main layout-->
 <main class="mx-lg-5">
     <div class="container-fluid">
          <!--Grid row-->
         <div class="row wow fadeIn">
              <!--Grid column-->
             <div class="col-md-12 mb-12">
                  <!--Card-->
                 <div class="card">
                      <!-- Card header -->
                     <div class="card-header text-center ">
                         >>
                     </div>
                      <!--Card content-->
                     <div class="card-body">

<div class="AreaTitulo">Consulta >> Doc. Internos Generales</div>




							<form name="frmConsultaEntrada" method="GET" action="consultaInternoGeneral.php">
						<tr>
							<td width="110" >N&ordm; Documento:</td>
							<td width="390" align="left"><input type="txt" name="cCodificacion" value="<?=$_GET[cCodificacion]?>" size="28" class="FormPropertReg form-control"></td>
							<td width="110" >Desde:</td>
							<td align="left">

									<td>
    <?
	if(trim($_REQUEST[fHasta])==""){$fecfin = date("d-m-Y");}  else { $fecfin = $_REQUEST[fHasta]; }
	if(trim($_REQUEST[fDesde])==""){$fecini = date("01-m-Y");} else { $fecini = $_REQUEST[fDesde]; }
	?>                 
                                    <input type="text" readonly name="fDesde" value="<?=$fecini?>" style="width:75px" class="FormPropertReg form-control"></td><td><div class="boton" style="width:24px;height:20px"><a href="javascript:;" onclick="displayCalendar(document.forms[0].fDesde,'dd-mm-yyyy',this,false)"><img src="images/icon_calendar.png" width="22" height="20" border="0"></a></div></td>
									<td width="20"></td>
									<td >Hasta:&nbsp;<input type="text" readonly name="fHasta" value="<?=$fecfin?>" style="width:75px" class="FormPropertReg form-control"></td><td><div class="boton" style="width:24px;height:20px"><a href="javascript:;" onclick="displayCalendar(document.forms[0].fHasta,'dd-mm-yyyy',this,false)"><img src="images/icon_calendar.png" width="22" height="20" border="0"></a></div></td>
									</tr></table>
							</td>
						</tr>
						<tr>
							<td width="110" >Tipo Documento:</td>
							<td width="390" align="left"><select name="cCodTipoDoc" class="FormPropertReg form-control" style="width:260px" />
									<option value="">Seleccione:</option>
									<?
									$sqlTipo="SELECT * FROM Tra_M_Tipo_Documento WHERE nFlgInterno=1 And cCodTipoDoc!=45 ";
									$sqlTipo.="ORDER BY cDescTipoDoc ASC";
          				            $rsTipo=mssql_query($sqlTipo,$cnx);
          				while ($RsTipo=MsSQL_fetch_array($rsTipo)){
          					if($RsTipo["cCodTipoDoc"]==$_GET[cCodTipoDoc]){
          						$selecTipo="selected";
          					}Else{
          						$selecTipo="";
          					}
          				echo "<option value=".$RsTipo["cCodTipoDoc"]." ".$selecTipo.">".$RsTipo["cDescTipoDoc"]."</option>";
          				}
          				mssql_free_result($rsTipo);
									?>
									</select></td>
							<td width="110" >Asunto:</td>
							<td align="left"><input type="txt" name="cAsunto" value="<?=$_GET[cAsunto]?>" size="65" class="FormPropertReg form-control">
							</td>
						</tr>
						<tr>
							<td width="110" >Enviado:</td>
							<td width="390" align="left">
						      SI<input type="checkbox" name="SI" value="1" <?if($_GET[SI]==1) echo "checked"?> />
							   &nbsp;&nbsp;&nbsp;
	                          NO<input type="checkbox" name="NO" value="1" <?if($_GET[NO]==1) echo "checked"?> />
                               </td>
							<td width="110" >Observaciones:</td>
							<td align="left" class="CellFormRegOnly"><input type="txt" name="cObservaciones" value="<?=$_GET[cObservaciones]?>" size="65" class="FormPropertReg form-control">                    
						  </td>
						</tr>
												<tr>
						  <td >Oficina Origen:</td>
						  <td align="left">
                  	<select name="iCodOficinaOri" class="FormPropertReg form-control" style="width:360px" />
     	            <option value="">Seleccione:</option>
	              <? 
	                 $sqlOfi="SP_OFICINA_LISTA_COMBO"; 
                     $rsOfi=mssql_query($sqlOfi,$cnx);
	                 while ($RsOfi=MsSQL_fetch_array($rsOfi)){
	  	             if($RsOfi["iCodOficina"]==$_GET[iCodOficinaOri]){
												$selecClas="selected";
          	         }Else{
          		      		$selecClas="";
                     }
                   	 echo "<option value=".$RsOfi["iCodOficina"]." ".$selecClas.">".$RsOfi["cNomOficina"]."</option>";
                     }
                     mssql_free_result($rsOfi);
                  ?>
            </select></td>
						  <td >Oficina Destino:</td>
						  <td align="left">
                          	<select name="iCodOficinaDes" class="FormPropertReg form-control" style="width:360px"  />
     	            <option value="">Seleccione:</option>
	              <? 
	                 $sqlOfi="SP_OFICINA_LISTA_COMBO "; 
                     $rsOfi=mssql_query($sqlOfi,$cnx);
	                 while ($RsOfi=MsSQL_fetch_array($rsOfi)){
	  	             if($RsOfi["iCodOficina"]==$_GET[iCodOficinaDes]){
												$selecClas="selected";
          	         }Else{
          		      		$selecClas="";
                     }
                   	 echo "<option value=".$RsOfi["iCodOficina"]." ".$selecClas.">".$RsOfi["cNomOficina"]."</option>";
                     }
                     mssql_free_result($rsOfi);
                  ?>
            </select></td>
						  </tr>
						<tr>
                         
							<td colspan="2" align="left">
                            <table width="400" border="0" align="left">
                           <tr>
                            <td align="left">
                              Descargar &nbsp; <img src="images/icon_download.png" width="16" height="16" border="0" > &nbsp; &nbsp;
	                          | &nbsp; &nbsp;  Editar &nbsp; <i class="fas fa-edit"></i>&nbsp;&nbsp;&nbsp; </td>
                           </tr>
                          </table>
                          </td>
                          <td colspan="2" align="right">
							<button class="btn btn-primary" onclick="Buscar();" onMouseOver="this.style.cursor='hand'"> <b>Buscar</b> <img src="images/icon_buscar.png" width="17" height="17" border="0"> </button>
							&nbsp;
                           <button class="btn btn-primary" onclick="window.open('<?=$PHP_SELF?>', '_self');" onMouseOver="this.style.cursor='hand'"> <b>Restablecer</b> <img src="images/icon_clear.png" width="17" height="17" border="0"> </button>
                             &nbsp;
			    <? // ordenamiento
                if($_GET[campo]==""){ $campo="Fecha"; }Else{ $campo=$_GET[campo]; }
                if($_GET[orden]==""){ $orden="DESC"; }Else{ $orden=$_GET[orden]; } ?>
				<button class="btn btn-primary" onclick="window.open('consultaInternoGeneral_xls.php?fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodificacion=<?=$_GET[cCodificacion]?>&SI=<?=$_GET[SI]?>&NO=<?=$_GET[NO]?>&cObservaciones=<?=$_GET[cObservaciones]?>&cAsunto=<?=$_GET[cAsunto]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&iCodOficinaOri=<?=$_GET['iCodOficinaOri']?>&iCodOficinaDes=<?=$_GET['iCodOficinaDes']?>&traRep=<?=$_SESSION['CODIGO_TRABAJADOR']?>&orden=<?=$orden?>&campo=<?=$campo?>', '_blank');" onMouseOver="this.style.cursor='hand'"> <b>a Excel</b> <img src="images/icon_excel.png" width="17" height="17" border="0"> </button>
							&nbsp;
							<button class="btn btn-primary" onclick="window.open('consultaInternoGeneral_pdf.php?fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodificacion=<?=$_GET[cCodificacion]?>&SI=<?=$_GET[SI]?>&NO=<?=$_GET[NO]?>&cObservaciones=<?=$_GET[cObservaciones]?>&cAsunto=<?=$_GET[cAsunto]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&iCodOficinaOri=<?=$_GET['iCodOficinaOri']?>&iCodOficinaDes=<?=$_GET['iCodOficinaDes']?>&orden=<?=$orden?>&campo=<?=$campo?>', '_blank');" onMouseOver="this.style.cursor='hand'"> <b>a Pdf</b> <img src="images/icon_pdf.png" width="17" height="17" border="0"> </button>
							
							</td>
						</tr>
							</form>

</form>



<?
function paginar($actual, $total, $por_pagina, $enlace, $maxpags=0) {
$total_paginas = ceil($total/$por_pagina);
$anterior = $actual - 1;
$posterior = $actual + 1;
$minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
$maximo = $maxpags ? min($total_paginas, $actual+floor($maxpags/2)): $total_paginas;
if ($actual>1)
$texto = "<a href=\"$enlace$anterior\">�</a> ";
else
$texto = "<b>�</b> ";
if ($minimo!=1) $texto.= "... ";
for ($i=$minimo; $i<$actual; $i++)
$texto .= "<a href=\"$enlace$i\">$i</a> ";
$texto .= "<b>$actual</b> ";
for ($i=$actual+1; $i<=$maximo; $i++)
$texto .= "<a href=\"$enlace$i\">$i</a> ";
if ($maximo!=$total_paginas) $texto.= "... ";
if ($actual<$total_paginas)
$texto .= "<a href=\"$enlace$posterior\">�</a>";
else
$texto .= "<b>�</b>";
return $texto;
}


if (!isset($pag)) $pag = 1; // Por defecto, pagina 1
$tampag = 20;
$reg1 = ($pag-1) * $tampag;

//invertir orden
if($orden=="ASC") $cambio="DESC";
if($orden=="DESC") $cambio="ASC";

	if ($fecini!=''){$fecini=date("Ymd", strtotime($fecini));}
    if( $fecfin!=''){
    $fecfin=date("Y-m-d", strtotime($fecfin));
	function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    $date_r = getdate(strtotime($date));
    $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
    return $date_result;
				}
	$fecfin=dateadd($fecfin,1,0,0,0,0,0); // + 1 dia
	}
	$sql.= " SP_CONSULTA_INTERNO_GENERAL '$fecini', '$fecfin',  '$_GET[SI]', '$_GET[NO]',  '%$_GET[cCodificacion]%', '%$_GET[cAsunto]%',  '%$_GET[cObservaciones]%', '$_GET[cCodTipoDoc]' ,'$_GET[iCodOficinaOri]','$_GET[iCodOficinaDes]', '$campo', '$orden' ";
    $rs=mssql_query($sql,$cnx);
	$total = MsSQL_num_rows($rs);
   //echo $sql;

?>
<br>
<table width="1000" border="0" cellpadding="3" cellspacing="3" align="center">
<tr>
 <td width="88" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=Fecha&orden=<?=$cambio?>&cCodificacion=<?=$_GET[cCodificacion]?>&fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cAsunto=<?=$_GET[cAsunto]?>&cReferencia=<?=$_GET[cReferencia]?>&SI=<?=$_GET[SI]?>&NO=<?=$_GET[NO]?>&iCodOficinaOri=<?=$_GET[iCodOficinaOri]?>&iCodOficinaDes=<?=$_GET[iCodOficinaDes]?>" class="Estilo1" style=" text-decoration:<?if($campo=="fFecRegistro"){ echo "underline"; }Else{ echo "none";}?>">Fecha</a></td>
 <td width="138" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=Oficina&orden=<?=$cambio?>&cNomOficina=<?=$_GET[cNomOficina]?>&cCodificacion=<?=$_GET[cCodificacion]?>&fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cAsunto=<?=$_GET[cAsunto]?>&cReferencia=<?=$_GET[cReferencia]?>&SI=<?=$_GET[SI]?>&NO=<?=$_GET[NO]?>&iCodOficinaOri=<?=$_GET[iCodOficinaOri]?>&iCodOficinaDes=<?=$_GET[iCodOficinaDes]?>" class="Estilo1" style=" text-decoration:<?if($campo=="Oficina"){ echo "underline"; }Else{ echo "none";}?>">Oficina Origen</a></td>
	<td width="154" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=Documento&orden=<?=$cambio?>&cDescTipoDoc=<?=$_GET[cDescTipoDoc]?>&cCodificacion=<?=$_GET[cCodificacion]?>&fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cAsunto=<?=$_GET[cAsunto]?>&cReferencia=<?=$_GET[cReferencia]?>&SI=<?=$_GET[SI]?>&NO=<?=$_GET[NO]?>&iCodOficinaOri=<?=$_GET[iCodOficinaOri]?>&iCodOficinaDes=<?=$_GET[iCodOficinaDes]?>" class="Estilo1" style=" text-decoration:<?if($campo=="Documento"){ echo "underline"; }Else{ echo "none";}?>">Tipo de Documento</a></td>
   <td width="396" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=Asunto&orden=<?=$cambio?>&cAsunto=<?=$_GET[cAsunto]?>&cCodificacion=<?=$_GET[cCodificacion]?>&fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cReferencia=<?=$_GET[cReferencia]?>&SI=<?=$_GET[SI]?>&NO=<?=$_GET[NO]?>&iCodOficinaOri=<?=$_GET[iCodOficinaOri]?>&iCodOficinaDes=<?=$_GET[iCodOficinaDes]?>" class="Estilo1" style=" text-decoration:<?if($campo=="Asunto"){ echo "underline"; }Else{ echo "none";}?>">Asunto</a></td>
	<td width="105" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?cCodificacion=<?=$_GET[cCodificacion]?>&fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cAsunto=<?=$_GET[cAsunto]?>&cReferencia=<?=$_GET[cReferencia]?>&SI=<?=$_GET[SI]?>&NO=<?=$_GET[NO]?>&iCodOficinaOri=<?=$_GET[iCodOficinaOri]?>&iCodOficinaDes=<?=$_GET[iCodOficinaDes]?>" class="Estilo1" style=" text-decoration:<?if($campo=="cNomOficina"){ echo "underline"; }Else{ echo "none";}?>">Oficina Destino</a></td>
  <td width="62" class="headCellColum">Opciones</td>
	</tr>
<?
if($fecini=="" && $fecfin=="" && $_GET[cCodTipoDoc]=="" && $_GET[cAsunto]=="" && $_GET[cCodificacion]=="" && $_GET[cReferencia]=="" && $_GET[SI]=="" && $_GET[NO]=="" && $_GET[iCodOficinaOri]=="" && $_GET[iCodOficinaDes]=="" ){	
  $sqlin= "SP_CONSULTA_INTERNO_GENERAL_LISTA '$_GET[iCodOficinaOri]','$_GET[iCodOficinaDes]'";
  $rsin=mssql_query($sqlin,$cnx);
$numrows=MsSQL_num_rows($rsin);
 }
else{
$numrows=MsSQL_num_rows($rs);	
} 
if($numrows==0){ 
		echo "NO SE ENCONTRARON REGISTROS<br>";
		echo "TOTAL DE REGISTROS : ".$numrows;
}else{
         echo "TOTAL DE REGISTROS : ".$numrows;
for ($i=$reg1; $i<min($reg1+$tampag, $total); $i++) {
mssql_data_seek($rs, $i);
$Rs=MsSQL_fetch_array($rs);
//while ($Rs=MsSQL_fetch_array($rs))
        		if ($color == "#DDEDFF"){
			  			$color = "#F9F9F9";
	    			}else{
			  			$color = "#DDEDFF";
	    			}
	    			if ($color == ""){
			  			$color = "#F9F9F9";
	    			}	
?>

 <tr bgcolor="<?=$color?>" onMouseOver="this.style.backgroundColor='#BFDEFF'" OnMouseOut="this.style.backgroundColor='<?=$color?>'" >
    <td valign="top" align="center">
    	<?
    	echo "<div style=color:#727272>".date("d-m-Y", strtotime($Rs[fFecRegistro]))."</div>";
    	echo "<div style=color:#727272;font-size:10px>".date("G:i", strtotime($Rs[fFecRegistro]))."</div>";
		$sqlTra="SELECT cApellidosTrabajador,cNombresTrabajador FROM Tra_M_Trabajadores WHERE iCodTrabajador='$Rs[iCodTrabajadorRegistro]'";
			$rsTra=mssql_query($sqlTra,$cnx);
			$RsTra=MsSQL_fetch_array($rsTra);
			echo "<div style=color:#808080;>".$RsTra[cNombresTrabajador]." ".$RsTra[cApellidosTrabajador]."</div>";	 
			?>
		</td>
    <td valign="top" align="left"><? echo $Rs[cNomOficina];
	echo "<div style=color:#808080;>".$Rs[cNombresTrabajador]." ".$Rs[cApellidosTrabajador]."</div>";	
	?></td>
    <td valign="top" align="left">
	    <?	
			echo $Rs[cDescTipoDoc];
			echo "<br>";
			echo "<a style=\"color:#0067CE\" href=\"registroOficinaDetalles.php?iCodTramite=".$Rs[iCodTramite]."\" rel=\"lyteframe\" title=\"Detalle del TRÁMITE\" rev=\"width: 970px; height: 450px; scrolling: auto; border:no\">";
			echo $Rs[cCodificacion];
			echo "</a>";
			?>
		</td>
        
    <td valign="top" align="left"><?=$Rs[cAsunto]?></td> 
     <td valign="top" align="left">
	        <? $sqlDes= " SELECT TOP 1 Tra_M_Tramite.iCodTramite,cNombresTrabajador,cApellidosTrabajador,cNomOficina FROM Tra_M_Tramite "; 
               $sqlDes.= " LEFT OUTER JOIN Tra_M_Tramite_Movimientos on Tra_M_Tramite.icodtramite=Tra_M_Tramite_Movimientos.icodtramite ";
			   $sqlDes.= " LEFT OUTER JOIN Tra_M_Trabajadores on Tra_M_Tramite_Movimientos.iCodTrabajadorDerivar=Tra_M_Trabajadores.iCodTrabajador, Tra_M_Oficinas ";
               $sqlDes.= " WHERE Tra_M_Oficinas.iCodOficina=Tra_M_Tramite_Movimientos.iCodOficinaDerivar AND Tra_M_Tramite.nFlgTipoDoc=2 AND Tra_M_Tramite.nFlgClaseDoc=1 ";
			   $sqlDes.= " AND Tra_M_Tramite.iCodTramite='$Rs[iCodTramite]' ORDER BY iCodMovimiento ASC";
			   $rsDes=mssql_query($sqlDes,$cnx);
			    $RsDes=MsSQL_fetch_array($rsDes); 
			    echo $RsDes[cNomOficina]; 
				echo "<div style=color:#727272>".$RsDes[cNombresTrabajador]." ".$RsDes[cApellidosTrabajador]."</div>"; 
				 
			   ?></td>
     <td valign="top" width="62">

    		<?php
            	$tramitePDF   = mssql_query("SELECT * FROM Tra_M_Tramite WHERE iCodTramite='$Rs[iCodTramite]'",$cnx);
  				$RsTramitePDF = mssql_fetch_object($tramitePDF);
				
				if ($RsTramitePDF->descripcion != NULL AND $RsTramitePDF->descripcion!=' ') {
            ?>
            <a href="registroInternoDocumento_pdf.php?iCodTramite=<?php echo $RsTramitePDF->iCodTramite;?>" target="_blank" title="Documento Electrónico">
            	<img src="images/1471041812_pdf.png" border="0" height="17" width="17">
            </a>
            <?php } ?>
    		<?php
    			$sqlDw = "SELECT TOP 1 * FROM Tra_M_Tramite_Digitales WHERE iCodTramite='$Rs[iCodTramite]'";
      			$rsDw  = mssql_query($sqlDw,$cnx);
      			
      			if (mssql_num_rows($rsDw) > 0){
      				$RsDw = mssql_fetch_array($rsDw);
      				
      				if($RsDw["cNombreNuevo"]!=""){
				 			if (file_exists("../cAlmacenArchivos/".trim($Rs1[nombre_archivo]))){
								echo "<a href=\"download.php?direccion=../cAlmacenArchivos/&file=".trim($RsDw["cNombreNuevo"])."\"><img src=images/icon_download.png border=0 width=16 height=16 alt=\"".trim($RsDw["cNombreNuevo"])."\"></a>";
							}
						}
      		}Else{
      			echo "<img src=images/space.gif width=16 height=16>";
      		}
    			echo "<a href=\"registroOficinaEdit.php?iCodTramite=".$Rs[iCodTramite]."&URI=".$_SERVER['REQUEST_URI']."\"><img src=\"images/icon_edit.png\" width=\"16\" height=\"16\" alt=\"Editar Documento\" border=\"0\"></a>";
    			?>		</td>
        
</tr>
  
<? 
  }
  }?> 
<tr>
		<td colspan="8" align="center">
         <? echo paginar($pag, $total, $tampag, "consultaInternoGeneral.php?cCodificacion=".$_GET[cCodificacion]."&fDesde=".$_GET[fDesde]."&fHasta=".$_GET[fHasta]."&cCodTipoDoc=".$_GET[cCodTipoDoc]."&cAsunto=".$_GET[cAsunto]."&cReferencia=".$_GET[cReferencia]."&SI=".$_GET[SI]."&NO=".$_GET[NO]."&iCodOficinaOri=".$_GET[iCodOficinaOri]."&iCodOficinaDes=".$_GET[iCodOficinaDes]."&pag=");?>
         </td>
		</tr>
</table>
 	  </tr>
		</table>  
					</div>
                 </div>
             </div>
         </div>
     </div>
 </main>


<?include("includes/userinfo.php");?> <?include("includes/pie.php");?>


<map name="Map" id="Map"><area shape="rect" coords="1,4,19,15" href="#" /></map>
<map name="Map2" id="Map2"><area shape="rect" coords="0,5,15,13" href="#" /></map></body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>