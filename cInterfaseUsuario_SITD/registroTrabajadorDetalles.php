<? header('Content-Type: text/html; charset=UTF-8');
session_start();
If($_SESSION['CODIGO_TRABAJADOR']!=""){
include_once("../conexion/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv=Content-Type content=text/html; charset=utf-8>
<title>SITDD</title>

<link type="text/css" rel="stylesheet" href="css/detalle.css" media="screen" />
<script language="javascript" type="text/javascript">
    function muestra(nombrediv) {
        if(document.getElementById(nombrediv).style.display == '') {
                document.getElementById(nombrediv).style.display = 'none';
        } else {
                document.getElementById(nombrediv).style.display = '';
        }
    }
</script>
</head>
<body>
		<?
		$rs=mssql_query("SELECT * FROM Tra_M_Tramite WHERE iCodTramite='$_GET[iCodTramite]'",$cnx);
		$Rs=MsSQL_fetch_array($rs);
		?>

<!--Main layout-->
 <main class="mx-lg-5">
     <div class="container-fluid">
          <!--Grid row-->
         <div class="row wow fadeIn">
              <!--Grid column-->
             <div class="col-md-12 mb-12">
                  <!--Card-->
                 <div class="card">
                      <!-- Card header -->
                     <div class="card-header text-center ">
                         >>
                     </div>
                      <!--Card content-->
                     <div class="card-body">

<div class="AreaTitulo">DETALLE DE DOCUMENTO TRABAJADOR</div>
<table cellpadding="0" cellspacing="0" border="0" width="910">
<tr>
<td class="FondoFormRegistro">
		
		<table width="880" border="0" align="center">
		<tr>
		<td>
				<fieldset id="tfa_GeneralDoc" class="fieldset">
				<legend class="legend"><a href="javascript:;" onClick="muestra('zonaGeneral')" class="LnkZonas">Datos Generales <img src="images/icon_expand.png" width="16" height="13" border="0"></a></legend>
		    <div id="zonaGeneral">
		    <table border="0" width="860">
		    <tr>
		        <td width="130" >Tipo de Documento:&nbsp;</td>
		        <td width="300">
		        		<? 
			          $sqlTipDoc="SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$Rs[cCodTipoDoc]'";
			          $rsTipDoc=mssql_query($sqlTipDoc,$cnx);
			          $RsTipDoc=MsSQL_fetch_array($rsTipDoc);
			          echo $RsTipDoc[cDescTipoDoc];
		            ?>
		        </td>
		        <td width="130" >Fecha:&nbsp;</td>
		        <td>
		        	<span><?=date("d-m-Y G:i:s", strtotime(substr($Rs[fFecRegistro], 0, -6)))/*date("d-m-Y", strtotime($Rs[fFecRegistro]))*/?></span>
        			<span style=font-size:10px><?/*=date("h:i A", strtotime($Rs[fFecRegistro]))*/?></span>
		        </td>
		    </tr> 
		    
		    <tr>
		        <td width="130" >N&ordm; Documento:&nbsp;</td>
		        <td style="text-transform:uppercase"><?=$Rs[cCodificacion]?></td>
		        <td width="130" >Observaciones:</td>
	        <td style="text-transform:uppercase"><?=$Rs[cObservaciones]?></td>
		    </tr>
	    
		    <tr>
		        <td width="130"  valign="top">Asunto:&nbsp;</td>
		        <td width="300"><?=$Rs[cAsunto]?></td>
		        <td width="130"  valign="top">Digital:</td>
		        <td width="300">
		        <?php
	            	$tramitePDF   = mssql_query("SELECT * FROM Tra_M_Tramite WHERE iCodTramite='$_GET[iCodTramite]'",$cnx);
	  				$RsTramitePDF = mssql_fetch_object($tramitePDF);
					
					if ($RsTramitePDF->descripcion != NULL AND $RsTramitePDF->descripcion != ' ') {
	            ?>
	            <a href="registroInternoDocumento_pdf.php?iCodTramite=<?php echo $RsTramitePDF->iCodTramite;?>" target="_blank" title="Documento Electrónico">
	            	<img src="images/1471041812_pdf.png" border="0" height="17" width="17">
	            </a>
	            <?php } ?>
		        <?php
					$sqlDw="SELECT TOP 1 * FROM Tra_M_Tramite_Digitales WHERE iCodTramite='$_GET[iCodTramite]'";
      				$rsDw=mssql_query($sqlDw,$cnx);
      				if(MsSQL_num_rows($rsDw)>0){
      					$RsDw=MsSQL_fetch_array($rsDw);
      						if($RsDw["cNombreNuevo"]!=""){
				 						if (file_exists("../cAlmacenArchivos/".trim($Rs1[nombre_archivo]))){
											echo "<a href=\"download.php?direccion=../cAlmacenArchivos/&file=".trim($RsDw["cNombreNuevo"])."\"><img src=images/icon_download.png border=0 width=16 height=16 alt=\"".trim($RsDw["cNombreNuevo"])."\"></a>";
										}
									}
      					}Else{
      						echo "<img src=images/space.gif width=16 height=16>";
      					}
								?></td>
		    </tr>	    
		    </table>
		  	</div>
		  	<img src="images/space.gif" width="0" height="0">
				</fieldset>
		</td>
		</tr>
		
    <tr>
		<td>  
		  	<fieldset id="tfa_FlujoOfi" class="fieldset">
		  	<legend class="legend"><a href="javascript:;" onClick="muestra('zonaOficina')" class="LnkZonas">Flujo <img src="images/icon_expand.png" width="16" height="13" border="0"></a></legend>
		    <div id="zonaOficina">
		    <table border="0" align="center" width="860">
		    <tr>
		       <td class="headCellColum" width="200">Trabajador Emisor</td>
		       <td class="headCellColum" width="400">Trabajador Receptor</td>
		       <td class="headCellColum" width="120">Fecha Derivo</td>
		       <td class="headCellColum" width="100">Estado</td>
		    </tr>
		   	<? 
		   	$sqlM="SELECT * FROM Tra_M_Tramite_Movimientos  WHERE (iCodTramite='$Rs[iCodTramite]' OR iCodTramiteRel='$Rs[iCodTramite]') ORDER BY iCodMovimiento ASC";
		   	$rsM=mssql_query($sqlM,$cnx);
		   	//echo $sqlM;
		    while ($RsM=MsSQL_fetch_array($rsM)){
		      	if ($color == "#CEE7FF"){
			  			$color = "#F9F9F9";
		  			}else{
			  			$color = "#CEE7FF";
		  			}
		  			if ($color == ""){
			  			$color = "#F9F9F9";
		  			}	
				?>
				<tr bgcolor="<?=$color?>">
				<td valign="top">
						 <? 		    		 
					  $sqlTrab="SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsM[iCodTrabajadorRegistro]'";
					  $rsTrab=mssql_query($sqlTrab,$cnx);
					  $RsTrab=MsSQL_fetch_array($rsTrab);
					  echo $RsTrab[cNombresTrabajador]." ".$RsTrab[cApellidosTrabajador]." ";
					  ?>		     	
				</td>		    	
				<td valign="top" align="left">
						<?
					  $sqlTraD="SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsM[iCodTrabajadorDelegado]'";
					  $rsTraD=mssql_query($sqlTraD,$cnx);
					  $RsTraD=MsSQL_fetch_array($rsTraD);
					  echo $RsTraD[cNombresTrabajador]." ".$RsTraD[cApellidosTrabajador]." ";
						?>
				</td>
				<td valign="top">
						
				</td>

				<td valign="top" align="center">
							<?
								switch ($RsM[nEstadoMovimiento]) {
								case 1:
										echo "Pendiente";
									break;
									case 2:
										echo "En Proceso"; //movimiento derivado a otra ofi
									break;
									case 3:
										echo "En Proceso"; //por delegar a otro trabajador
									break;
									case 4:
										echo "Finalizado";
									break;
									}
							?>
				</td>
				</tr> 
				<?
				$contaMov++;
		    }
		    ?>
		    </table> 
		    </div>
		    <img src="images/space.gif" width="0" height="0"> 
		  	</fieldset>
		</td>
		</tr>


		</table>
					</div>
                 </div>
             </div>
         </div>
     </div>
 </main>

<div>		
</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>
