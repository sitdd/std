<?php
session_start();
if ($_SESSION['CODIGO_TRABAJADOR'] != ""){
    include_once("../conexion/conexion.php");
    //Inicia valores paginacion
    $pag = $_GET['pag'];
    if (!isset($pag)) $pag = 1; // Por defecto, pagina 1
    if(isset($_GET['cantidadfilas'])){$tampag = $_GET['cantidadfilas'];}
    else{$tampag=5;}

    $rsFont = mssql_query("SELECT iCodSubMenu FROM Tra_M_Menu_Items WHERE cScriptSubMenu LIKE '%iu_trabajadores.php%'",$cnx);
    $RsFont = mssql_fetch_array($rsFont);
    $sqlSub = "SELECT iCodMenuLista FROM Tra_M_Menu_Lista WHERE iCodSubMenu = $RsFont[iCodSubMenu] AND iCodMenu IN (SELECT iCodMenu FROM Tra_M_Menu WHERE iCodPerfil ='$_SESSION[iCodPerfilLogin]')";
    $rsSub  = mssql_query($sqlSub,$cnx);
    $numProfile = mssql_num_rows($rsSub);
}
if ($numProfile > 0){
?>
<!DOCTYPE html>
<html lang="es">
<head>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />

<?php include("includes/head.php"); ?>

</head>
<body>
<style>
    .wrapper {
        width: auto%;
        position: absolute;
        z-index: 100;
        height: auto;
    }
    #sidebar {
        display: none;
    }
    #tablaResutado{
        width: 100%;
    }
    #sidebar.active {
        display: flex;
        width: 95%;
    }
    #sidebar label{
        font-size: 0.8rem!important;
        margin-bottom: 0!important;
    }
    .info-end ,.page-link{
        font-size: 0.8rem!important;
    }
    @media (min-width: 576px) {
        #sidebar.active {
            width: 80%;
        }
    }
    @media (min-width: 768px) {
        #sidebar.active {
            width: 60%;
        }
    }
    @media (min-width: 992px) {
        #sidebar.active {
            width: 60%;
        }
        #sidebarCollapse{
            margin-left: -35px!important;
        }
        .info-end ,.page-link{
            font-size: 1rem!important;
        }
    }
    @media (min-width: 1200px) {
        #sidebar.active {
            width: 38%;
        }
    }

    .dropdown-content li>a, .dropdown-content li>span {
        font-size: 0.8rem!important;
    }
    .select-wrapper .search-wrap {
        padding-top: 0rem!important;
        margin: 0 0.2rem!important;
    }
    .select-wrapper input.select-dropdown {
        font-size: 0.9rem!important;
    }
    .md-form{
        margin-bottom: 0.5rem!important;
    }
    thead a{
        color:white!important;
    }
    thead a:hover{
        color: #bdbdbd !important;
    }
</style>

<?php include("includes/menu.php"); ?>
<!--Main layout-->
<main class="mx-lg-5">
    <div class="container-fluid">
        <!--Grid row-->
        <div class="row wow fadeIn">
            <!--Grid column-->
            <div class="col-12">
                <!--Card-->
                <div class="card mb-5">
                    <!-- Card header -->
                    <div class="card-header text-center "> LISTA DE TRABAJADORES </div>
                    <!--Card content-->
                    <div class="card-body d-flex px-4 px-lg-5">
                        <?php
                          require_once("../conexion/conexion.php");
                        ?>
                        <div class="wrapper">
                                <nav class="navbar-expand py-0">
                                    <button title="Búsqueda" type="button" id="sidebarCollapse" class="botenviar float-left" style="padding: 0rem 8px!important; border: none; margin-left: -18px; border-radius: 10px;">
                                        <i class="fas fa-align-right"></i>
                                    </button>
                                </nav>


                            <!-- Sidebar -->
                            <nav id="sidebar" class="py-0">
                                <div class="card">
                                    <div class="card-header">Criterios de Búsqueda</div>
                                    <div class="card-body">

                                        <form name="form1" method="GET" action="<?=$PHP_SELF?>">
                                            <div class="form-row">
                                                <input type="hidden" name="cantidadfilas" value="<?=$tampag?>">
                                                <div class="col-6">
                                                    <div class="md-form">
                                                        <input name="cNombreTrabajador" type="text" id="nombre" class="FormPropertReg form-control" value="<?=$_GET[cNombreTrabajador]?>">
                                                        <label for="nombre">Nombre</label>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="md-form">
                                                        <input name="cApellidosTrabajador" type="text" id="apellido" class="FormPropertReg form-control" value="<?=$_GET[cApellidosTrabajador]?>">
                                                        <label for="apellido">Apellido</label>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <label>Documento:&nbsp;</label>
                                                    <?php
                                                    $sqlDoc = " SP_DOC_IDENTIDAD_LISTA_COMBO ";
                                                    $rsDoc  = mssql_query($sqlDoc,$cnx);
                                                    ?>
                                                    <select name="cTipoDocIdentidad" class="FormPropertReg mdb-select colorful-select dropdown-primary"   searchable="Buscar aqui.." >
                                                        <option value="">Seleccione:</option>
                                                        <?php
                                                        while ($RsDoc = mssql_fetch_array($rsDoc)){
                                                            if ($RsDoc["cTipoDocIdentidad"] == $_GET[cTipoDocIdentidad]){
                                                                $selecClas = "selected";
                                                            }else{
                                                                $selecClas = "";
                                                            }
                                                            echo "<option value=".$RsDoc["cTipoDocIdentidad"]." ".$selecClas." >".$RsDoc["cDescDocIdentidad"]."</option>";
                                                        }
                                                        mssql_free_result($rsDoc);
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-6">
                                                    <div class="md-form">
                                                        <input name="cNumDocIdentidad" type="text" id="nidentidad" class="FormPropertReg form-control" value="<?=$_GET[cNumDocIdentidad]?>" onkeypress="if (event.keyCode > 31 && ( event.keyCode < 48 || event.keyCode > 57)) event.returnValue = false;">
                                                        <label for="nidentidad">N° Identidad</label>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <label>Oficina:&nbsp;</label>
                                                    <select name="iCodOficina" class="FormPropertReg mdb-select colorful-select dropdown-primary"   searchable="Buscar aqui..">
                                                        <option value="">Seleccione:</option>
                                                        <?php
                                                        $sqlOfi = " SP_OFICINA_LISTA_COMBO ";
                                                        $rsOfi  = mssql_query($sqlOfi,$cnx);
                                                        while ($RsOfi = mssql_fetch_array($rsOfi)){
                                                            if ($RsOfi["iCodOficina"] == $_GET[iCodOficina]){
                                                                $selecClas = "selected";
                                                            }else{
                                                                $selecClas = "";
                                                            }
                                                            echo utf8_encode("<option value=".$RsOfi["iCodOficina"]." ".$selecClas.">".$RsOfi["cNomOficina"]."</option>");
                                                        }
                                                        mssql_free_result($rsOfi);
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-12">
                                                    <label>Perfil:&nbsp;</label>
                                                    <select  class="FormPropertReg mdb-select colorful-select dropdown-primary"   searchable="Buscar aqui.." name="iCodPerfil" >
                                                        <option value="">Seleccione:</option>
                                                        <?php
                                                        $sqlPer = " SP_PERFIL_LISTA_COMBO ";
                                                        $rsPer  = mssql_query($sqlPer,$cnx);
                                                        while ($RsPer = mssql_fetch_array($rsPer)){
                                                            if ($RsPer["iCodPerfil"] == $_GET[iCodPerfil]){
                                                                $selecP = "selected";
                                                            }else{
                                                                $selecP = "";
                                                            }
                                                            echo "<option value=".$RsPer["iCodPerfil"]." ".$selecP.">".$RsPer["cDescPerfil"]."</option>";
                                                        }
                                                        mssql_free_result($rsPer);
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-6">
                                                    <label>Categoria:&nbsp;</label>
                                                    <select class="FormPropertReg mdb-select colorful-select dropdown-primary"   searchable="Buscar aqui.." name="iCodCategoria" >
                                                        <option value="">Seleccione:</option>
                                                        <?php
                                                        $sqlCat = " SP_CATEGORIA_LISTA_COMBO ";
                                                        $rsCat  = mssql_query($sqlCat,$cnx);
                                                        while ($RsCat = mssql_fetch_array($rsCat)){
                                                            if ($RsCat["iCodCategoria"] == $_GET[iCodCategoria]){
                                                                $selecP = "selected";
                                                            }else{
                                                                $selecP = "";
                                                            }
                                                            echo "<option value=".$RsCat["iCodCategoria"]." ".$selecP.">".$RsCat["cDesCategoria"]."</option>";
                                                        }
                                                        mssql_free_result($rsCat);
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-6">
                                                    <label>Estado:&nbsp;</label>
                                                    <select name="txtestado"  class="FormPropertReg mdb-select colorful-select dropdown-primary"   searchable="Buscar aqui.." id="txtestado">
                                                        <option value="" selected="selected">Seleccione:</option>
                                                        <option value="1" <? if( $_GET[txtestado]=="1"){echo "selected";} ?> >Activo</option>
                                                        <option value="0" <? if( $_GET[txtestado]=="0"){echo "selected";} ?>>Inactivo</option>
                                                    </select>
                                                </div>
                                                <div class="col-12" align="center">

                                                    <div class="row mb-3 justify-content-center py-0">

                                                        <div class="col">
                                                            <button class="botenviar mb-1" type="submit" name="Submit" onmouseover="this.style.cursor='hand'"  >
                                                                Buscar
                                                            </button>
                                                        </div>

                                                        <div class="col">
                                                            <button class="botenviar"  name="Restablecer" onclick="window.open('<?=$PHP_SELF?>', '_self');" onmouseover="this.style.cursor='hand'" >
                                                                Restablecer
                                                            </button>
                                                        </div>
                                                    </div>

                                                    <?php // ordenamiento
                                                    if($_GET[campo]==""){$campo=" Tra_M_Trabajadores.iCodOficina, cApellidosTrabajador ";}else{$campo=$_GET[campo];}
                                                    if($_GET[orden]==""){$orden="ASC";}else{$orden=$_GET[orden];}
                                                    ?>

                                                    <div class="card" style="background-color: rgba(231,234,238,0.42)">
                                                        <div class="card-body">
                                                            <div class="row pl-3 pl-lg-5">
                                                                Exportar en:
                                                            </div>
                                                            <div class="row justify-content-center">
                                                                <div class="col-">
                                                                    <button class="botpelota mx-1" title="Excel" onclick="window.open('iu_trabajadores_xls.php?cNombreTrabajador=<?=$_GET[cNombreTrabajador]?>&cApellidosTrabajador=<?=$_GET[cApellidosTrabajador]?>&cNumDocIdentidad=<?=$_GET[cNumDocIdentidad]?>&cTipoDocIdentidad=<?=$_GET[cTipoDocIdentidad]?>&iCodOficina=<?=$_GET[iCodOficina]?>&iCodCategoria=<?=$_GET[iCodCategoria]?>&iCodPerfil=<?=$_GET[iCodPerfil]?>&txtestado=<?=$_GET[txtestado]?>&campo=<?=$campo?>&orden=<?=$orden?>&traRep=<?=$_SESSION['CODIGO_TRABAJADOR']?>', '_blank');" onmouseover="this.style.cursor='hand'">
                                                                        <i class="far fa-file-excel"></i>
                                                                    </button>
                                                                </div>
                                                                <div class="col-">
                                                                    <button class="botpelota mx-1" title="Pdf" onclick="window.open('iu_trabajadores_pdf.php?cNombreTrabajador=<?=$_GET[cNombreTrabajador]?>&cApellidosTrabajador=<?=$_GET[cApellidosTrabajador]?>&cNumDocIdentidad=<?=$_GET[cNumDocIdentidad]?>&cTipoDocIdentidad=<?=$_GET[cTipoDocIdentidad]?>&iCodOficina=<?=$_GET[iCodOficina]?>&iCodCategoria=<?=$_GET[iCodCategoria]?>&iCodPerfil=<?=$_GET[iCodPerfil]?>&txtestado=<?=$_GET[txtestado]?>&campo=<?=$campo?>&orden=<?=$orden?>', '_blank');" onmouseover="this.style.cursor='hand'">
                                                                        <i class="far fa-file-pdf"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </nav>

                        </div>

                        <?
                        function paginar($actual, $total, $por_pagina, $enlace, $maxpags=0) {
                            $total_paginas = ceil($total/$por_pagina);
                            $anterior = $actual - 1;
                            $posterior = $actual + 1;
                            $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
                            $maximo = $maxpags ? min($total_paginas, $actual+floor($maxpags/2)): $total_paginas;

                            if ($actual>1)
                                $texto = "<nav aria-label='Page navigation example'><ul class='pagination justify-content-center flex-wrap'>
                                          <li class='page-item'><a class='page-link' href='$enlace$anterior'>Anterior</a></li> ";
                            else
                                $texto = "<nav aria-label='Page navigation example'><ul class='pagination justify-content-center   flex-wrap'>
                                           <li class='page-item disabled'><a class='page-link' href='#'>Anterior</a></li> ";

                            if ($minimo!=1) $texto.= "... ";

                            for ($i=$minimo; $i<$actual; $i++)
                                $texto .= "  <li class='page-item'><a class='page-link' href='$enlace$i'>$i</a></li> ";

                            $texto .= "<li class='page-item active'>
                                      <a class='page-link' href='#'>$actual<span class='sr-only'>(current)</span></a></li>";

                            for ($i=$actual+1; $i<=$maximo; $i++)
                                $texto .= "<li class='page-item'><a class='page-link' href='$enlace$i'>$i</a></li> ";


                            if ($maximo!=$total_paginas) $texto.= "... ";

                            if ($actual<$total_paginas)
                                $texto .= "<li class='page-item'><a class='page-link' href='$enlace$posterior'>Siguiente</a></li></ul></nav>";
                            else
                                $texto .= "<li class='page-item disabled'><a class='page-link' href='$enlace$posterior'>Siguiente</a></li></ul></nav>";

                            return $texto;
                        }

                        //calcula para cantidad a mostrar
                        $reg1 = ($pag-1) * $tampag;

                        //invertir orden
                        if($orden=="DESC") $cambio="ASC";
                        if($orden=="ASC") $cambio="DESC";


                        $sql="SELECT  * FROM Tra_M_Trabajadores ";
                        $sql.=" WHERE (iCodTrabajador>0) AND ES_EXTERNO = 0 ";
                        if($_GET[cNombreTrabajador]!=""){
                        $sql.=" AND cNombresTrabajador LIKE '%$_GET[cNombreTrabajador]%' ";
                        }
                        if($_GET[cApellidosTrabajador]!=""){
                        $sql.=" AND cApellidosTrabajador LIKE '%$_GET[cApellidosTrabajador]%' ";
                        }
                        if($_GET[cNumDocIdentidad]!=""){
                        $sql.=" AND cNumDocIdentidad LIKE '%$_GET[cNumDocIdentidad]%' ";
                        }
                        if($_GET[cTipoDocIdentidad]!=""){
                        $sql.=" AND cTipoDocIdentidad='$_GET[cTipoDocIdentidad]'";
                                }
                        if($_GET[iCodOficina]!=""){
                        $sql.=" AND iCodOficina='$_GET[iCodOficina]'";
                                }
                        if($_GET[iCodPerfil]!=""){
                        $sql.=" AND iCodPerfil='$_GET[iCodPerfil]'";
                                }
                        if($_GET[iCodCategoria]!=""){
                        $sql.=" AND iCodCategoria='$_GET[iCodCategoria]'";
                                }
                        if($_GET[txtestado]!=""){
                            $sql.=" AND nFlgEstado='$_GET[txtestado]'";
                        }else{
                            $sql.=" AND nFlgEstado IN (1,0)"; // Activo=1 y Inactivo=0... Eliminado lógico=3
                        }
                        $sql.="ORDER BY $campo  $orden ";
                        ?>

                        <div class="card ml-3" id="tablaResutado">

                            <div class="card-body px-4 px-lg-5">

                                <div class="row">
                                    <a class="botpelota ml-auto mb-4" title="Agregar" href='iu_nuevo_trabajador.php' style="margin-bottom: 1rem"><i class="fas fa-plus"></i></a>
                                </div>

                                <div class="row justify-content-center">
                                    <div class="col-12 col-xl-11">
                                        <div class="row justify-content-between">
                                            <div class="col-10 col-sm-3 col-md-2 mt-3">
                                                    <select name="cantidadfilas" id="filas" class="mdb-select" onchange="actualizarfilas()" >
                                                        <option value="5"  id="5">5</option>
                                                        <option value="10" id="10">10</option>
                                                        <option value="20" id="20">20</option>
                                                        <option value="50" id="50">50</option>
                                                    </select>
                                                    <label>Cantidad</label>
                                            </div>
                                        </div>

                                        <table id="tablaDatos" class="table-sm table-responsive table-hover">
                                            <thead class="text-center text-white" style="border-bottom: solid 1px rgba(0,0,0,0.47);background-color: #0f58ab">
                                            <tr>
                                                <th>Oficina</th>
                                                <th>
                                                    <a href="<?= $_SERVER['PHP_SELF'] ?>?campo=cApellidosTrabajador&orden=<?= $cambio ?>&cNombreTrabajador=<?= $_GET[cNombreTrabajador] ?>&cApellidosTrabajador=<?= $_GET[cApellidosTrabajador] ?>&cTipoDocIdentidad=<?= $_GET[cTipoDocIdentidad] ?>&cNumDocIdentidad=<?= $_GET[cNumDocIdentidad] ?>&iCodOficina=<?= $_GET[iCodOficina] ?>&iCodPerfil=<?= $_GET[iCodPerfil] ?>&iCodCategoria=<?= $_GET[iCodCategoria] ?>&txtestado=<?= $_GET[txtestado] ?>"
                                                       style=" text-decoration:<?if($campo=="cApellidosTrabajador"){ echo "underline"; }Else{ echo "none";}?>">
                                                        Apellidos y Nombres</a>
                                                </th>
                                                <th>
                                                    <a href="<?= $_SERVER['PHP_SELF'] ?>?campo=cTipoDocIdentidad&orden=<?= $cambio ?>&cNombreTrabajador=<?= $_GET[cNombreTrabajador] ?>&cApellidosTrabajador=<?= $_GET[cApellidosTrabajador] ?>&cTipoDocIdentidad=<?= $_GET[cTipoDocIdentidad] ?>&cNumDocIdentidad=<?= $_GET[cNumDocIdentidad] ?>&iCodOficina=<?= $_GET[iCodOficina] ?>&iCodPerfil=<?= $_GET[iCodPerfil] ?>&iCodCategoria=<?= $_GET[iCodCategoria] ?>&txtestado=<?= $_GET[txtestado] ?>"
                                                       style=" text-decoration:<?if($campo=="cTipoDocIdentidad"){ echo "underline"; }Else{ echo "none";}?>">
                                                        Documento</a>
                                                </th>
                                                <th>
                                                    <a href="<?= $_SERVER['PHP_SELF'] ?>?campo=cMailTrabajador&orden=<?= $cambio ?>&cNombreTrabajador=<?= $_GET[cNombreTrabajador] ?>&cApellidosTrabajador=<?= $_GET[cApellidosTrabajador] ?>&cTipoDocIdentidad=<?= $_GET[cTipoDocIdentidad] ?>&cNumDocIdentidad=<?= $_GET[cNumDocIdentidad] ?>&iCodOficina=<?= $_GET[iCodOficina] ?>&iCodPerfil=<?= $_GET[iCodPerfil] ?>&iCodCategoria=<?= $_GET[iCodCategoria] ?>&txtestado=<?= $_GET[txtestado] ?>"
                                                       style=" text-decoration:<?if($campo=="cMailTrabajador"){ echo "underline"; }Else{ echo "none";}?>">
                                                        Mail</a>
                                                </th>
                                                <th>
                                                    <a href="<?= $_SERVER['PHP_SELF'] ?>?campo=iCodPerfil&orden=<?= $cambio ?>&cNombreTrabajador=<?= $_GET[cNombreTrabajador] ?>&cApellidosTrabajador=<?= $_GET[cApellidosTrabajador] ?>&cTipoDocIdentidad=<?= $_GET[cTipoDocIdentidad] ?>&cNumDocIdentidad=<?= $_GET[cNumDocIdentidad] ?>&iCodOficina=<?= $_GET[iCodOficina] ?>&iCodPerfil=<?= $_GET[iCodPerfil] ?>&iCodCategoria=<?= $_GET[iCodCategoria] ?>&txtestado=<?= $_GET[txtestado] ?>"
                                                       style=" text-decoration:<?if($campo=="iCodPerfil"){ echo "underline"; }Else{ echo "none";}?>">
                                                        Perfil</a>
                                                </th>
                                                <th>
                                                    <a href="<?= $_SERVER['PHP_SELF'] ?>?campo=cUsuario&orden=<?= $cambio ?>&cNombreTrabajador=<?= $_GET[cNombreTrabajador] ?>&cApellidosTrabajador=<?= $_GET[cApellidosTrabajador] ?>&cTipoDocIdentidad=<?= $_GET[cTipoDocIdentidad] ?>&cNumDocIdentidad=<?= $_GET[cNumDocIdentidad] ?>&iCodOficina=<?= $_GET[iCodOficina] ?>&iCodPerfil=<?= $_GET[iCodPerfil] ?>&iCodCategoria=<?= $_GET[iCodCategoria] ?>&txtestado=<?= $_GET[txtestado] ?>"
                                                       style=" text-decoration:<?if($campo=="cUsuario"){ echo "underline"; }Else{ echo "none";}?>">
                                                        Usuario</a>
                                                </th>
                                                <th>
                                                    <a href="<?= $_SERVER['PHP_SELF'] ?>?campo=nFlgEstado&orden=<?= $cambio ?>&cNombreTrabajador=<?= $_GET[cNombreTrabajador] ?>&cApellidosTrabajador=<?= $_GET[cApellidosTrabajador] ?>&cTipoDocIdentidad=<?= $_GET[cTipoDocIdentidad] ?>&cNumDocIdentidad=<?= $_GET[cNumDocIdentidad] ?>&iCodOficina=<?= $_GET[iCodOficina] ?>&iCodPerfil=<?= $_GET[iCodPerfil] ?>&iCodCategoria=<?= $_GET[iCodCategoria] ?>&txtestado=<?= $_GET[txtestado] ?>"
                                                       style=" text-decoration:<?if($campo=="nFlgEstado"){ echo "underline"; }Else{ echo "none";}?>">
                                                        Estado</a>
                                                </th>
                                                <th>Opciones</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <?php
                                            $rs = mssql_query($sql,$cnx);
                                            $total = mssql_num_rows($rs);
                                            $numrows = mssql_num_rows($rs);
                                            if ($numrows !== 0){

                                            for ($i=$reg1; $i<min($reg1+$tampag, $total); $i++) {
                                                mssql_data_seek($rs, $i);
                                                $Rs = mssql_fetch_array($rs);
                                            ?>

                                                <tr>
                                                    <td>
                                                        <?php
                                                        $sqlOfi = "SP_OFICINA_LISTA_AR  '$Rs[iCodOficina]'";
                                                        $rsOfi = mssql_query($sqlOfi, $cnx);
                                                        $RsOfi = mssql_fetch_array($rsOfi);
                                                        echo utf8_encode($RsOfi[cNomOficina]);
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php echo utf8_encode($Rs[cApellidosTrabajador]); ?>
                                                        , <?php echo utf8_encode($Rs[cNombresTrabajador]); ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $sqlTDoc = "SP_DOC_IDENTIDAD_LISTA_AR '$Rs[cTipoDocIdentidad]'";
                                                        $rsTDoc = mssql_query($sqlTDoc, $cnx);
                                                        $RsTDoc = mssql_fetch_array($rsTDoc);
                                                        echo utf8_encode($RsTDoc[cDescDocIdentidad] . ": " . $Rs[cNumDocIdentidad]);
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php echo utf8_encode($Rs[cMailTrabajador]); ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $sqlPerf = "SP_PERFIL_LISTA_AR '$Rs[iCodPerfil]'";
                                                        $rsPerf = mssql_query($sqlPerf, $cnx);
                                                        $RsPerf = mssql_fetch_array($rsPerf);
                                                        echo utf8_encode($RsPerf[cDescPerfil]);
                                                        ?>
                                                    </td>
                                                    <td>

                                                        <?php echo utf8_encode($Rs[cUsuario]); ?>
                                                    </td>

                                                    <td>
                                                        <?
                                                        if ($Rs[nFlgEstado] == 1) {
                                                            ?>
                                                            <div style="color:#268c1a">Activo&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                                        <?
                                                        } else {
                                                            ?>
                                                            <div style="color:#f10000">Inactivo&nbsp;&nbsp;</div>
                                                        <?
                                                        } ?>
                                                    </td>

                                                    <td>
                                                        <a href="../cLogicaNegocio_SITD/ln_elimina_trabajador.php?id=<?= $Rs[iCodTrabajador] ?>&cNombreTrabajador=<?= $_GET[cNombreTrabajador] ?>&cApellidosTrabajador=<?= $_GET[cApellidosTrabajador] ?>&cTipoDocIdentidad=<?= $_GET[cTipoDocIdentidad] ?>&cNumDocIdentidad=<?= $_GET[cNumDocIdentidad] ?>&iCodOficina=<?= $_GET[iCodOficina] ?>&iCodPerfil=<?= $_GET[iCodPerfil] ?>&iCodCategoria=<?= $_GET[iCodCategoria] ?>&txtestado=<?= $_GET[txtestado] ?>&pag=<?= $pag ?>"
                                                           onClick='ConfirmarBorrado();' title="Eliminar" ">
                                                        <i class="far fa-trash-alt"></i>
                                                        </a>
                                                        <a title="Editar" href="../cInterfaseUsuario_SITD/iu_actualiza_trabajadores.php?cod=<?= $Rs[iCodTrabajador] ?>&sw=1&cNombreTrabajador=<?= $_GET[cNombreTrabajador] ?>&cApellidosTrabajador=<?= $_GET[cApellidosTrabajador] ?>&cTipoDocIdentidad=<?= $_GET[cTipoDocIdentidad] ?>&cNumDocIdentidad=<?= $_GET[cNumDocIdentidad] ?>&iCodOficina=<?= $_GET[iCodOficina] ?>&iCodPerfil=<?= $_GET[iCodPerfil] ?>&iCodCategoria=<?= $_GET[iCodCategoria] ?>&txtestado=<?= $_GET[txtestado] ?>&pag=<?= $pag ?>">
                                                            <i class="fas fa-edit"></i>
                                                        </a>
                                                        <a href="../cInterfaseUsuario_SITD/iu_actualiza_key.php?cod=<?= $Rs[iCodTrabajador] ?>&usr=<?= trim($Rs[cUsuario]) ?>&cod=<?= trim($Rs[iCodTrabajador]) ?>&sw=1&cNombreTrabajador=<?= $_GET[cNombreTrabajador] ?>&cApellidosTrabajador=<?= $_GET[cApellidosTrabajador] ?>&cTipoDocIdentidad=<?= $_GET[cTipoDocIdentidad] ?>&cNumDocIdentidad=<?= $_GET[cNumDocIdentidad] ?>&iCodOficina=<?= $_GET[iCodOficina] ?>&iCodPerfil=<?= $_GET[iCodPerfil] ?>&iCodCategoria=<?= $_GET[iCodCategoria] ?>&txtestado=<?= $_GET[txtestado] ?>&pag=<?= $pag ?>"
                                                           rel="lyteframe" title="Cambio de Contraseña"
                                                           rev="width: 380px; height: 80px; scrolling: auto; border:no">
                                                            <img src="images/icon_key.png" width="16" height="16"
                                                                 border="0">
                                                        </a>
                                                    </td>
                                                </tr>

                                        <?
                                                }
                                            }
                                        ?>
                                            </tbody>
                                        </table>
                                        <!--Información inferior-->
                                        <div class="info-end">
                                            <br>
                                            <b>
                                                Resultados del <?php echo $reg1+1 ; ?> al <?php echo min($reg1+$tampag, $total) ; ?>
                                            </b>
                                            <br>
                                            <b>
                                                Total: <?php echo $total; ?>
                                            </b>
                                            <br>
                                        </div>
                                        <!--/Información inferior-->
                                        <br>
                                         <? echo paginar($pag, $total, $tampag, "iu_trabajadores.php?cNombreTrabajador=".$_GET[cNombreTrabajador]."&cApellidosTrabajador=".$_GET[cApellidosTrabajador]."&iCodOficina=".$_GET[iCodOficina]."&cTipoDocIdentidad=".$_GET[cTipoDocIdentidad]."&cNumDocIdentidad=".$_GET[cNumDocIdentidad]."&iCodPerfil=".$_GET[iCodPerfil]."&iCodCategoria=".$_GET[iCodCategoria]."&txtestado=".$_GET[txtestado]."&campo=".$campo."&orden=".$orden."&cantidadfilas=".$tampag."&pag=");?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php include("includes/userinfo.php"); ?>
<?php include("includes/pie.php"); ?>
<script>
   function ConfirmarBorrado()
        {
             if (confirm("Esta seguro de eliminar el registro?")){
                  return true;
             }else{
                  return false;
             }
        }

   $(document).ready(function() {
         $('.mdb-select').material_select();
   });
</script>

<script type="text/javascript" language="javascript" src="includes/lytebox.js"></script>

<script>
    //Sidebar
    $(document).ready(function () {

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });

    });
</script>

<script>
    //Para Cantidad de filas
    document.getElementById(<?php echo $tampag?>).selected = true;

    function actualizarfilas(){
        var valor =Number.parseInt(document.getElementById('filas').value);
        var direc =window.location.pathname;
        window.location =direc+"?cantidadfilas="+valor;
    }
</script>

</body>
</html>
<?php
}else{
    header("Location: ../index.php?alter=5");
}
?>