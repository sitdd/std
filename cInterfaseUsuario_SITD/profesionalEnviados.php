<?php
session_start();
if($_SESSION['CODIGO_TRABAJADOR']!=""){
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
    <link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="css/dhtmlgoodies_calendar.css" media="screen"/>
    <style>
        .wrapper {
            width: auto%;
            position: absolute;
            z-index: 100;
            height: auto;
        }
        #sidebar {
            display: none;
        }
        #tablaResutado{
            width: 100%;
        }
        #sidebar.active {
            display: flex;
            width: 95%;
        }
        #sidebar label{
            font-size: 0.8rem!important;
            margin-bottom: 0!important;
        }
        .info-end ,.page-link{
            font-size: 0.8rem!important;
        }
        @media (min-width: 576px) {
            #sidebar.active {
                width: 80%;
            }
        }
        @media (min-width: 768px) {
            #sidebar.active {
                width: 60%;
            }
        }
        @media (min-width: 992px) {
            #sidebar.active {
                width: 60%;
            }
            #sidebarCollapse{
                margin-left: -35px!important;
            }
            .info-end ,.page-link{
                font-size: 1rem!important;
            }
        }
        @media (min-width: 1200px) {
            #sidebar.active {
                width: 38%;
            }
        }

        .dropdown-content li>a, .dropdown-content li>span {
            font-size: 0.8rem!important;
        }
        .select-wrapper .search-wrap {
            padding-top: 0rem!important;
            margin: 0 0.2rem!important;
        }
        .select-wrapper input.select-dropdown {
            font-size: 0.9rem!important;
        }
        .md-form{
            margin-bottom: 0.5rem!important;
        }

    </style>
</head>
<body>

<?include("includes/menu.php");?>

<!--Main layout-->
 <main class="mx-lg-5">
     <div class="container-fluid">
          <!--Grid row-->
         <div class="row wow fadeIn">
              <!--Grid column-->
             <div class="col-md-12 mb-12">
                  <!--Card-->
                 <div class="card">
                      <!-- Card header -->
                     <div class="card-header text-center ">
                         documentos enviados - Profesional
                     </div>
                      <!--Card content-->
                     <div class="card-body d-flex px-4 px-lg-5">
                         <div class="wrapper">
                             <nav class="navbar-expand py-0">
                                 <button type="button" id="sidebarCollapse" class="botenviar float-left" style="padding: 0rem 8px!important; border: none; margin-left: -18px; border-radius: 10px;">
                                     <i class="fas fa-align-right"></i>
                                 </button>
                             </nav>
                             <!-- Sidebar -->
                             <nav id="sidebar" class="py-0">
                                 <div class="card">
                                     <div class="card-header">Criterios de Búsqueda</div>
                                     <div class="card-body">
                                         <form name="frmConsulta" method="GET">
                                             <tr>
                                                 <td width="110" >Documentos:</td>
                                                 <td width="390" align="left"><input type="checkbox" name="Entrada" value="1" <?if($_GET[Entrada]==1) echo "checked"?> onclick="activaEntrada();">Entrada  &nbsp;&nbsp;&nbsp;<input type="checkbox" name="Interno" value="1" <?if($_GET[Interno]==1) echo "checked"?> onclick="activaInterno();">Internos</td>
                                                 <td width="110" >Desde:</td>
                                                 <td align="left">

                                                 <td><input type="text" readonly name="fDesde" value="<?=$_GET[fDesde]?>" style="width:75px" class="FormPropertReg form-control"></td><td><div class="boton" style="width:24px;height:20px"><a href="javascript:;" onclick="displayCalendar(document.forms[0].fDesde,'dd-mm-yyyy',this,false)"><img src="images/icon_calendar.png" width="22" height="20" border="0"></a></div></td>
                                                 <td width="20"></td>
                                                 <td >Hasta:&nbsp;<input type="text" readonly name="fHasta" value="<?=$_GET[fHasta]?>" style="width:75px" class="FormPropertReg form-control"></td><td><div class="boton" style="width:24px;height:20px"><a href="javascript:;" onclick="displayCalendar(document.forms[0].fHasta,'dd-mm-yyyy',this,false)"><img src="images/icon_calendar.png" width="22" height="20" border="0"></a></div></td>
                                             </tr></table>
                                             </td>
                                             </tr>
                                             <tr>
                                                 <td width="110" >N&ordm; Documento:</td>
                                                 <td width="390" align="left"><input type="txt" name="cCodificacion" value="<?=$_GET[cCodificacion]?>" size="28" class="FormPropertReg form-control"></td>
                                                 <td width="110" >Asunto:</td>
                                                 <td align="left"><input type="txt" name="cAsunto" value="<?=$_GET[cAsunto]?>" size="65" class="FormPropertReg form-control">
                                                 </td>
                                             </tr>
                                             <tr>
                                                 <td width="110" >Tipo Documento:</td>
                                                 <td width="390" align="left">
                                                     <select name="cCodTipoDoc" class="FormPropertReg form-control" style="width:180px" />
                                                     <option value="">Seleccione:</option>
                                                     <?
                                                     include_once("../conexion/conexion.php");
                                                     $sqlTipo="SELECT * FROM Tra_M_Tipo_Documento ";
                                                     $sqlTipo.="ORDER BY cDescTipoDoc ASC";
                                                     $rsTipo=mssql_query($sqlTipo,$cnx);
                                                     while ($RsTipo=MsSQL_fetch_array($rsTipo)){
                                                         if($RsTipo["cCodTipoDoc"]==$_GET[cCodTipoDoc]){
                                                             $selecTipo="selected";
                                                         }Else{
                                                             $selecTipo="";
                                                         }
                                                         echo "<option value=".$RsTipo["cCodTipoDoc"]." ".$selecTipo.">".$RsTipo["cDescTipoDoc"]."</option>";
                                                     }
                                                     mssql_free_result($rsTipo);
                                                     ?>
                                                     </select>
                                                 </td>
                                                 <td width="110" ></td>
                                                 <td align="left" class="CellFormRegOnly">

                                                 </td>
                                             </tr>

                                             <tr>
                                                 <td colspan="4" align="right">
                                                     <button class="btn btn-primary" onclick="Buscar();" onMouseOver="this.style.cursor='hand'"> <b>Buscar</b> <img src="images/icon_buscar.png" width="17" height="17" border="0"> </button>
                                                     &nbsp;
                                                     <button class="btn btn-primary" onclick="window.open('<?=$PHP_SELF?>', '_self'); return false;" onMouseOver="this.style.cursor='hand'"> <b>Restablecer</b> <img src="images/icon_clear.png" width="17" height="17" border="0"> </button>
                                                 </td>
                                             </tr>
                                         </form>
                                     </div>
                                 </div>
                             </nav>
                         </div>
                            <?php
                    function paginar($actual, $total, $por_pagina, $enlace, $maxpags=0) {
                    $total_paginas = ceil($total/$por_pagina);
                    $anterior = $actual - 1;
                    $posterior = $actual + 1;
                    $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
                    $maximo = $maxpags ? min($total_paginas, $actual+floor($maxpags/2)): $total_paginas;
                    if ($actual>1)
                    $texto = "<a href=\"$enlace$anterior\"> << </a> ";
                    else
                    $texto = "<b> << </b> ";
                    if ($minimo!=1) $texto.= "... ";
                    for ($i=$minimo; $i<$actual; $i++)
                    $texto .= "<a href=\"$enlace$i\">$i</a> ";
                    $texto .= "<b>$actual</b> ";
                    for ($i=$actual+1; $i<=$maximo; $i++)
                    $texto .= "<a href=\"$enlace$i\">$i</a> ";
                    if ($maximo!=$total_paginas) $texto.= "... ";
                    if ($actual<$total_paginas)
                    $texto .= "<a href=\"$enlace$posterior\"> >> </a>";
                    else
                    $texto .= "<b> >> </b>";
                    return $texto;
                   }


                   if (!isset($pag)) $pag = 1; // Por defecto, pagina 1
                   $tampag = 10;
                   $reg1 = ($pag-1) * $tampag;

                  // ordenamiento
                   if($_GET[campo]==""){
                   $campo="Tra_M_Tramite_Movimientos.iCodMovimiento";
                   }Else{
                   $campo=$_GET[campo];
                   }

                  if($_GET[orden]==""){
                 $orden="DESC";
                 }Else{
                $orden=$_GET[orden];
                 }

                 //invertir orden
                 if($orden=="ASC") $cambio="DESC";
                 if($orden=="DESC") $cambio="ASC";

                            $fDesde=date("Ymd", strtotime($_GET[fDesde]));
                            $fHasta=date("Y-m-d", strtotime($_GET[fHasta]));

                            function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
                                  $date_r = getdate(strtotime($date));
                                  $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),($date_r["year"]+$yy)));
                                  return $date_result;
                            }
                            $fHasta=dateadd($fHasta,1,0,0,0,0,0); // + 1 dia

                            $sqlTra = "SELECT *,  c.cFlgTipoMovimiento, c.nEstadoMovimiento, c.fFecDelegadoRecepcion,c.fFecRecepcion 
                               FROM Tra_M_Tramite a, Tra_M_Tramite_Trabajadores b, Tra_M_Tramite_Movimientos c 
                               WHERE a.iCodTramite = b.iCodTramite ";
                   if($_GET[Entrada]==1 AND $_GET[Interno]==""){
                    $sqlTra.="AND a.nFlgTipoDoc=1 ";
                   }
                   if($_GET[Entrada]=="" AND $_GET[Interno]==1){
                    $sqlTra.="AND a.nFlgTipoDoc=2 ";
                   }
                   if($_GET[Entrada]==1 AND $_GET[Interno]==1){
                    $sqlTra.="AND (a.nFlgTipoDoc=1 OR a.nFlgTipoDoc=2) ";
                   }
                   if($_GET[fDesde]!="" AND $_GET[fHasta]==""){
                    $sqlTra.="AND b.fFecEnvio>'$fDesde' ";
                   }
                   if($_GET[fDesde]=="" AND $_GET[fHasta]!=""){
                    $sqlTra.="AND b.fFecEnvio<='$fHasta' ";
                   }
                   if($_GET[fDesde]!="" AND $_GET[fHasta]!=""){
                    $sqlTra.="AND b.fFecEnvio BETWEEN '$fDesde' AND '$fHasta' ";
                   }
                   if($_GET[cCodificacion]!=""){
                    $sqlTra.="AND a.cCodificacion='$_GET[cCodificacion]' ";
                   }
                   if($_GET[cAsunto]!=""){
                    $sqlTra.="AND a.cAsunto LIKE '%$_GET[cAsunto]%' ";
                   }
                   if($_GET[cCodTipoDoc]!=""){
                    $sqlTra.="AND a.cCodTipoDoc='$_GET[cCodTipoDoc]' ";
                   }
                    $sqlTra.=" AND (b.iCodTrabajadorOrigen='$_SESSION[CODIGO_TRABAJADOR]' AND b.iCodTrabajadorDestino!='$_SESSION[CODIGO_TRABAJADOR]' ) ";
                    $sqlTra.="AND b.iCodOficina='$_SESSION[iCodOficinaLogin]' and b.iCodMovimiento =c.iCodMovimiento  ";
                    $sqlTra.= "ORDER BY a.iCodTramite DESC";
                    $rsTra  = mssql_query($sqlTra,$cnx);
                    $total  = mssql_num_rows($rsTra);
                ?>
                         <div class="row">
                             <div class="col-lg-12">
                                 <div class="table-responsive">
                                     <table class="table table-sm">
                                         <thead>
                                             <tr>
                                                 <td>N&ordm; Documento</td>
                                                 <td>Nombre / Razón Social</td>
                                                 <td>Asunto / Procedimiento TUPA</td>
                                                 <td>Envio</td>
                                                 <td>Recepción</td>
                                                 <td>Estado</td>
                                             </tr>
                                         </thead>
                                         <tbody>
                                         <?php
                                         $numrows=MsSQL_num_rows($rsTra);
                                         if($numrows==0){
                                             echo "NO SE ENCONTRARON REGISTROS<br>";
                                         }else{

///////////////////////////////////////////////////////
                                             for ($i=$reg1; $i<min($reg1+$tampag, $total); $i++) {
                                                 mssql_data_seek($rsTra, $i);
                                                 $RsTra=MsSQL_fetch_array($rsTra);
///////////////////////////////////////////////////////	
                                                 // while ($RsTra=MsSQL_fetch_array($rsTra))
                                                 if ($color == "#DDEDFF"){
                                                     $color = "#F9F9F9";
                                                 }else{
                                                     $color = "#DDEDFF";
                                                 }
                                                 if ($color == ""){
                                                     $color = "#F9F9F9";
                                                 }
                                                 ?>
                                                 <tr bgcolor="<?=$color?>" onMouseOver="this.style.backgroundColor='#BFDEFF'" OnMouseOut="this.style.backgroundColor='<?=$color?>'">
                                                     <td>
                                                         <?if($RsTra[nFlgTipoDoc]==1){?>
                                                             <a href="registroEnviadosDetalles.php?iCodTramite=<?=$RsTra[iCodTramite]?>"  rel="lyteframe"
                                                                title="Detalle del TRÁMITE" rev="width: 970px; height: 550px; scrolling: auto; border:no">
                                                                 <?=utf8_encode($RsTra[cCodificacion])?>
                                                             </a>
                                                         <?}
                                                         if($RsTra[nFlgTipoDoc]==2){
                                                             echo "INTERNO";}
                                                         if($RsTra[nFlgTipoDoc]==3){
                                                             echo "SALIDA";}
                                                         if($RsTra[nFlgTipoDoc]==4){
                                                             ?>
                                                             <a href="registroEnviadosDetalles.php?iCodTramite=<?=$RsTra[iCodTramiteRel]?>"  rel="lyteframe"
                                                                title="Detalle del TRÁMITE" rev="width: 970px; height: 550px; scrolling: auto; border:no">
                                                                 <?=utf8_encode($RsTra[cCodificacion])?>
                                                             </a>
                                                         <?}?>
                                                         <?
                                                         echo "<div style=color:#727272>".date("d-m-Y G:i:s", strtotime($RsTra[fFecRegistro]))/*date("d-m-Y", strtotime($RsTra[fFecRegistro]))*/."</div>";
                                                         //echo "<div style=color:#727272;font-size:10px>".date("G:i", strtotime($RsTra[fFecRegistro]))."</div>";
                                                         if($RsTra[cFlgTipoMovimiento]==6){
                                                             echo "<div style=color:#800000;font-size:10px;text-align:center>COPIA</div>";
                                                         }
                                                         ?>
                                                     </td>
                                                     <td>
                                                         <?php
                                                         $sqlTipDoc="SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$RsTra[cCodTipoDoc]'";
                                                         $rsTipDoc=mssql_query($sqlTipDoc,$cnx);
                                                         $RsTipDoc=MsSQL_fetch_array($rsTipDoc);
                                                         echo utf8_encode("<div>".$RsTipDoc[cDescTipoDoc]."</div>");
                                                         if($RsTra[nFlgTipoDoc]==1 ){
                                                             echo "<div style=color:#808080;text-transform:uppercase>".$RsTra[cNroDocumento]."</div>";
                                                         }else if($RsTra[nFlgTipoDoc]==2 ){
                                                             echo "<a style=\"color:#0067CE\" href=\"registroEnviadosDetalles.php?iCodTramite=".$RsTra[iCodTramite]."\" rel=\"lyteframe\" title=\"Detalle del TRÁMITE\" rev=\"width: 970px; height: 450px; scrolling: auto; border:no\">";
                                                             echo $RsTra[cCodificacion];
                                                             echo "</a>";
                                                         }else if($RsTra[nFlgTipoDoc]==3 ){
                                                             echo "<br>";
                                                             echo "<a style=\"color:#0067CE\" href=\"registroEnviadosDetalles.php?iCodTramite=".$RsTra[iCodTramite]."\" rel=\"lyteframe\" title=\"Detalle del TRÁMITE\" rev=\"width: 970px; height: 290px; scrolling: auto; border:no\">";
                                                             echo $RsTra[cCodificacion];
                                                             echo "</a>";
                                                         }
                                                         $rsRem=mssql_query("SELECT * FROM Tra_M_Remitente WHERE iCodRemitente='$RsTra[iCodRemitente]'",$cnx);
                                                         $RsRem=MsSQL_fetch_array($rsRem);
                                                         echo utf8_encode($RsRem["cNombre"]);
                                                         mssql_free_result($rsRem);
                                                         ?>
                                                     </td>
                                                     <td><?=utf8_encode($RsTra[cAsunto])?></td>
                                                     <td>
                                                         <div><?=date("d-m-Y G:i:s", strtotime($RsTra[fFecEnvio]))/*date("d-m-Y", strtotime($RsTra[fFecEnvio]))*/;?></div>
                                                         <div style="font-size:10px"><?/*=date("G:i", strtotime($RsTra[fFecEnvio]));*/?></div>
                                                     </td>
                                                     <td>
                                                         <?
                                                         if($RsTra[cFlgTipoMovimiento]!=2){
                                                             if($RsTra[nEstadoMovimiento]==3){
                                                                 if($RsTra[fFecDelegadoRecepcion]==""){
                                                                     echo "<div style=color:#ff0000>sin aceptar</div>";
                                                                 }Else{
                                                                     echo "<div style=color:#0154AF>aceptado</div>";
                                                                     echo "<div style=color:#0154AF>".date("d-m-Y G:i:s", strtotime($RsTra[fFecDelegadoRecepcion]))/*date("d-m-Y", strtotime($RsTra[fFecDelegadoRecepcion]))*/."</div>";
                                                                     //echo "<div style=color:#0154AF;font-size:10px>".date("G:i", strtotime($RsTra[fFecDelegadoRecepcion]))."</div>";
                                                                 }
                                                             }else{
                                                                 if($RsTra[cFlgTipoMovimiento]==6){
                                                                     if($RsTra[fFecDelegadoRecepcion]==""){
                                                                         echo "<div style=color:#ff0000>sin aceptar</div>";
                                                                     }Else{
                                                                         echo "<div style=color:#0154AF>aceptado</div>";
                                                                         echo "<div style=color:#0154AF>".date("d-m-Y G:i:s", strtotime($RsTra[fFecDelegadoRecepcion]))/*date("d-m-Y", strtotime($RsTra[fFecDelegadoRecepcion]))*/."</div>";
                                                                         //echo "<div style=color:#0154AF;font-size:10px>".date("G:i", strtotime($RsTra[fFecDelegadoRecepcion]))."</div>";
                                                                     }
                                                                 }
                                                                 else {
                                                                     if($RsTra[fFecRecepcion]==""){
                                                                         echo "<div style=color:#ff0000>sin aceptar</div>";
                                                                     }Else{
                                                                         echo "<div style=color:#0154AF>aceptado</div>";
                                                                         echo "<div style=color:#0154AF>".date("d-m-Y G:i:s", strtotime($RsTra[fFecRecepcion]))/*date("d-m-Y", strtotime($RsTra[fFecRecepcion]))*/."</div>";
                                                                         //echo "<div style=color:#0154AF;font-size:10px>".date("G:i", strtotime($RsTra[fFecRecepcion]))."</div>";
                                                                     }
                                                                 }
                                                             }
                                                         }else {
                                                             if($RsTra[fFecRecepcion]==""){
                                                                 echo "<div style=color:#ff0000>sin aceptar</div>";
                                                             }Else{
                                                                 echo "<div style=color:#0154AF>aceptado</div>";
                                                                 echo "<div style=color:#0154AF>".date("d-m-Y G:i:s", strtotime($RsTra[fFecRecepcion]))/*date("d-m-Y", strtotime($RsTra[fFecRecepcion]))*/."</div>";
                                                                 //echo "<div style=color:#0154AF;font-size:10px>".date("G:i", strtotime($RsTra[fFecRecepcion]))."</div>";
                                                             }
                                                         }
                                                         ?>
                                                     </td>
                                                     <td>
                                                         <?
                                                         echo "<div style=color:#0154AF><b>";
                                                         switch ($RsTra[nEstadoMovimiento]){
                                                             case 1:
                                                                 echo "En proceso";
                                                                 break;
                                                             case 2:
                                                                 echo "Derivado";
                                                                 break;
                                                             case 3:
                                                                 echo "Delegado";
                                                                 break;
                                                             case 4:
                                                                 echo "<a style=\"color:#0067CE\" href=\"pendientesControlVerRpta.php?iCodMovimiento=".$RsTra[iCodMovimiento]."\" rel=\"lyteframe\" title=\"Detalle Respuesta\" rev=\"width: 500px; height: 300px; scrolling: auto; border:no\">Respondido</a>";
                                                                 break;
                                                             case 5:
                                                                 echo "Finalizado";
                                                                 break;
                                                         }
                                                         echo "</b></div>";
                                                         if($RsTra[cFlgTipoMovimiento]==2){
                                                             echo "<div style=color:#773C00>Enviado</div>";
                                                         }
                                                         ?>
                                                     </td>

                                                 </tr>
                                                 <?
                                             }
                                         }
                                         mssql_free_result($rsTra);
                                         ?>
                                         </tbody>
                                     </table>
                                 </div>
                             </div>
                             <div class="col-md-12">
                                 <?php echo "TOTAL DE REGISTROS : ".$numrows;   ?>
                                 <? echo paginar($pag, $total, $tampag, "profesionalEnviados.php?fDesde=".$_GET[fDesde]."&fHasta=".$_GET[fHasta]."&cCodificacion=".$_GET[cCodificacion]."&cAsunto=".$_GET[cAsunto]."&cCodTipoDoc=".$_GET[cCodTipoDoc]."&Entrada=".$_GET[Entrada]."&Interno=".$_GET[Interno]."&pag="); ?>
                             </div>
                         </div>
					</div>
                 </div>
             </div>
         </div>
     </div>
 </main>
 <?php include("includes/userinfo.php"); ?>

<?include("includes/pie.php");?>
    <script type="text/javascript" language="javascript" src="includes/lytebox.js"></script>

    <script type="text/javascript" src="scripts/dhtmlgoodies_calendar.js"></script>
    <script Language="JavaScript">
        $(document).ready(function() {
            $('.datepicker').pickadate({
                monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
                format: 'dd-mm-yyyy',
                formatSubmit: 'dd-mm-yyyy',
            });
            $('.mdb-select').material_select();

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });


        });

        function activaOpciones1(){
            for (j=0;j<document.formulario.elements.length;j++){
                if(document.formulario.elements[j].type == "radio"){
                    document.formulario.elements[j].checked=0;
                }
            }
            document.formulario.OpAceptar.disabled=false;
            document.formulario.OpEnviar.disabled=true;
            document.formulario.OpDelegar.disabled=true;
            document.formulario.OpFinalizar.disabled=true;
            document.formulario.OpAvance.disabled=true;
            document.formulario.OpAceptar.filters.alpha.opacity=100;
            document.formulario.OpEnviar.filters.alpha.opacity=50;
            document.formulario.OpDelegar.filters.alpha.opacity=50;
            document.formulario.OpFinalizar.filters.alpha.opacity=50;
            document.formulario.OpAvance.filters.alpha.opacity=50;
            return false;
        }

        function activaOpciones2(){
            for (i=0;i<document.formulario.elements.length;i++){
                if(document.formulario.elements[i].type == "checkbox"){
                    document.formulario.elements[i].checked=0;
                }
            }
            document.formulario.OpAceptar.disabled=true;
            document.formulario.OpEnviar.disabled=false;
            document.formulario.OpDelegar.disabled=false;
            document.formulario.OpFinalizar.disabled=false;
            document.formulario.OpAvance.disabled=false;
            document.formulario.OpAceptar.filters.alpha.opacity=50;
            document.formulario.OpEnviar.filters.alpha.opacity=100;
            document.formulario.OpDelegar.filters.alpha.opacity=100;
            document.formulario.OpFinalizar.filters.alpha.opacity=100;
            document.formulario.OpAvance.filters.alpha.opacity=100;
            return false;
        }


        function activaAceptar()
        {
            document.formulario.opcion.value=10;
            document.formulario.method="POST";
            document.formulario.action="profesionalData.php";
            document.formulario.submit();
        }

        function activaEnviar()
        {
            document.formulario.OpAceptar.value="";
            document.formulario.OpEnviar.value="";
            document.formulario.OpDelegar.value="";
            document.formulario.OpFinalizar.value="";
            document.formulario.OpAvance.value="";
            document.formulario.opcion.value=1;
            document.formulario.method="GET";
            document.formulario.action="profesionalEnviar.php";
            document.formulario.submit();
        }

        function activaDelegar()
        {
            document.formulario.action="profesionalResponder.php";
            document.formulario.submit();
        }

        function activaFinalizar()
        {
            document.formulario.action="profesionalFinalizar.php";
            document.formulario.submit();
        }

        function activaAvance()
        {
            document.formulario.action="profesionalAvance.php";
            document.formulario.submit();
        }

        function Buscar()
        {
            document.frmConsulta.action="<?=$PHP_SELF?>";
            document.frmConsulta.submit();
        }


        //--></script>

</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>