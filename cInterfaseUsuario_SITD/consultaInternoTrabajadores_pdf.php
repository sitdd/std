<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: consultaInternoTrabajadores_pdf.php
SISTEMA: SISTEMA  DE TRÁMITE DOCUMENTARIO DIGITAL
OBJETIVO: Reporte General en PDF de los Documentos Internos de Trabajadores
PROPIETARIO: AGENCIA PERUANA DE COOPERACIÓN INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripción
------------------------------------------------------------------------
1.0   Larry Ortiz        05/09/2018      Creación del programa.
------------------------------------------------------------------------
*****************************************************************************************/
session_start();
ob_start();
//*************************************
include_once("../conexion/conexion.php");
?>
<page backtop="25mm" backbottom="15mm" backleft="10mm" backright="10mm">
	<page_header>
		<br>
		<table style="width: 1000px; border: solid 0px black;">
			<tr>
				<td style="text-align:left;	width: 20px"></td>
				<td style="text-align:left;	width: 980px">
					<img style="width: 220px" src="images/cab.jpg" alt="Logo">
				</td>
			</tr>
		</table>
        <br><br>
	</page_header>
	<page_footer>
		<table style="width: 100%; border: solid 0px black;">
			<tr>
                <td style="text-align: center;	width: 40%">
				<? 
				   $sqllog="select cNombresTrabajador, cApellidosTrabajador from tra_m_trabajadores where iCodTrabajador='$_SESSION[CODIGO_TRABAJADOR]' "; 
				   $rslog=mssql_query($sqllog,$cnx);
				   $Rslog=MsSQL_fetch_array($rslog);
				   echo $Rslog[cNombresTrabajador]." ".$Rslog[cApellidosTrabajador];
				?></td>
				<td style="text-align: right;	width: 60%">p�gina [[page_cu]]/[[page_nb]]</td>
			</tr>
		</table>
        <br>
        <br>
	</page_footer>
	
	
	<table style="width: 100%; border: solid 0px black;">
	<tr>
	<td style="text-align: left;	width: 50%"><span style="font-size: 15px; font-weight: bold">Reporte Interno de Trabajadores</span></td>
	<td style="text-align: right;	width: 50%"><span style="font-size: 15px; font-weight: bold"><?=date("d-m-Y")?></span></td>
	</tr>
	</table>
	<br>
	<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="center">
		<thead>
			<tr>
				<th style="width: 15%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Documento</th>
				<th style="width: 25%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N&ordm; Doc.</th>
				<th style="width: 15%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Fecha</th>
                <th style="width: 20%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Asunto</th>
				<th style="width: 20%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Observaciones</th>
				<th style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Enviado</th>
			</tr>
		</thead>
		<tbody>
	<?
	
	if($_GET[fDesde]!='' && $_GET[fHasta]!=''){
       $fDesde=date("Ymd", strtotime($_GET[fDesde]));
	   $fHasta=date("Y-m-d", strtotime($_GET[fHasta]));
	   function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
       $date_r = getdate(strtotime($date));
       $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
       return $date_result;
				}
	   $fHasta=dateadd($fHasta,1,0,0,0,0,0); // + 1 dia
}/*
      $sql="  SELECT TOP (200) cDescTipoDoc,cCodificacion,fFecRegistro,cAsunto,cObservaciones,Tra_M_Tramite_Movimientos.nFlgEnvio ";
    $sql.=" FROM Tra_M_Tramite LEFT OUTER JOIN Tra_M_Tipo_Documento ON Tra_M_Tramite.cCodTipoDoc=Tra_M_Tipo_Documento.cCodTipoDoc ";
    $sql.=" LEFT OUTER JOIN Tra_M_Tramite_Movimientos ON Tra_M_Tramite.iCodTramite=Tra_M_Tramite_Movimientos.iCodTramite ";
	$sql.=" WHERE Tra_M_Tramite.nFlgTipoDoc=2 AND Tra_M_Tramite.nFlgClaseDoc=2 ";
    if($_GET[fDesde]!="" AND $_GET[fHasta]==""){
  	$sql.=" AND Tra_M_Tramite.fFecRegistro>'$fDesde' ";
    }
    if($_GET[fDesde]=="" AND $_GET[fHasta]!=""){
  	$sql.=" AND Tra_M_Tramite.fFecRegistro<='$fHasta' ";
    }
  
    if($_GET[fDesde]=="" AND $_GET[fHasta]!=""){
  	$sql.=" AND Tra_M_Tramite.fFecRegistro<='$fHasta' ";
    }
	
	if($_GET[SI]==1 AND $_GET[NO]==1 ){
    $sql.=" AND (Tra_M_Tramite_Movimientos.nFlgEnvio=1 OR Tra_M_Tramite_Movimientos.nFlgEnvio=0) ";
    }
    if($_GET[SI]==0 AND $_GET[NO]==1 ){
    $sql.=" AND Tra_M_Tramite_Movimientos.nFlgEnvio=0 ";
    }
    if($_GET[SI]==1 AND $_GET[NO]==0 ){
    $sql.=" AND Tra_M_Tramite_Movimientos.nFlgEnvio=1 ";
    }	
	if($_GET[cCodificacion]!=""){
    $sql.=" AND Tra_M_Tramite.cCodificacion='$_GET[cCodificacion]' ";
    }
	if($_GET[cAsunto]!=""){
    $sql.=" AND Tra_M_Tramite.cAsunto LIKE '%$_GET[cAsunto]%' ";
    }
	if($_GET[cObservaciones]!=""){
    $sql.=" AND Tra_M_Tramite.cObservaciones LIKE '%$_GET[cObservaciones]%' ";
    }
	if($_GET[cCodTipoDoc]!=""){
    $sql.=" AND Tra_M_Tramite.cCodTipoDoc='$_GET[cCodTipoDoc]' ";
    }	   
	$sql.= " ORDER BY Tra_M_Tramite.iCodTramite DESC ";	 */
	
$sql.= " SP_CONSULTA_INTERNO_TRABAJADOR '$fDesde', '$fHasta',  '$_GET[SI]', '$_GET[NO]',  '%$_GET[cCodificacion]%', '%$_GET[cAsunto]%',  '%$_GET[cObservaciones]%', '$_GET[cCodTipoDoc]', '$campo', '$orden' ";
    $rs=mssql_query($sql,$cnx);
   //echo $sql;
	   
	   while ($Rs=MsSQL_fetch_array($rs)){
	?>
	    <tr>
        <td style="width: 15%; text-align: center; border: solid 1px #6F6F6F;font-size:10px"><? echo $Rs[cDescTipoDoc];?></td>
        <td style="width: 20%; text-align: center; border: solid 1px #6F6F6F;font-size:10px"><? echo $Rs[cCodificacion];?></td>
         <td style="width: 15%; border: solid 1px #6F6F6F;font-size:10px">
		    <? echo "<div style=color:#0154AF;text-align:center>".date("d-m-Y", strtotime($Rs[fFecRegistro]))."</div>";
               echo "<div style=color:#0154AF;font-size:10px;text-align:center>".date("h:i A", strtotime($Rs[fFecRegistro]))."</div>";?></td>
        <td style="width: 20%; text-align: justify; border: solid 1px #6F6F6F;font-size:10px"><? echo $Rs[cAsunto];?></td>
        <td style="width: 20%; text-align: justify; border: solid 1px #6F6F6F;font-size:10px"><? echo $Rs[cObservaciones];?></td>
        <td style="width: 10%; text-align: center; border: solid 1px #6F6F6F;font-size:10px">
		       <? 
			      if($Rs[nFlgEnvio]==0){
					echo "NO";
				   }
				  if($Rs[nFlgEnvio]==1){
					echo "SI";
					} 
				?></td>
        </tr>
      <?
         }
      ?>
	   	
      </tbody>
	</table>
</page>

<?
//*************************************


	$content = ob_get_clean();  set_time_limit(0);     ini_set('memory_limit', '640M');

	// conversion HTML => PDF
	require_once(dirname(__FILE__).'/html2pdf/html2pdf.class.php');
	try
	{
		$html2pdf = new HTML2PDF('P','A4', 'es', false, 'UTF-8', 3);
		$html2pdf->pdf->SetDisplayMode('fullpage');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output('exemple03.pdf');
	}
	catch(HTML2PDF_exception $e) { echo $e; }
?>   
         		
