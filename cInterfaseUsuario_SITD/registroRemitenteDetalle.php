<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: PendienteData.php
SISTEMA: SISTEMA  DE TR�MITE DOCUMENTARIO DIGITAL
OBJETIVO: Seleccion remitente
PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripci�n
------------------------------------------------------------------------
1.0   APCI    12/11/2010      Creaci�n del programa.
------------------------------------------------------------------------
*****************************************************************************************/
session_start();
If($_SESSION['CODIGO_TRABAJADOR']!=""){
require_once("../conexion/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv=Content-Type content=text/html; charset=utf-8>
<title>SITDD</title>

<link type="text/css" rel="stylesheet" href="css/tramite.css" media="screen" />
<script Language="JavaScript">
<!--
function sendValue (s,t,x,y){
var selvalue1 = s.value;
var selvalue2 = t.value;
var selvalue3 = x.value;
var selvalue4 = y.value;
window.opener.document.getElementById('txtdirec_remitente').value = selvalue1;
window.opener.document.getElementById('cCodDepartamento').value = selvalue2;
window.opener.document.getElementById('cCodProvincia').value = selvalue3;
window.opener.document.getElementById('cCodDistrito').value = selvalue4;
window.opener.document.frmRegistro.cNomRemite.focus();
window.close();
}

function releer(){
  document.frmRegistro.method="POST";
  document.frmRegistro.action="<?=$PHP_SELF?>?clear=1";
  document.frmRegistro.submit();
}

function NuevoRemitente()
{
  if (document.frmRegistro.txtnom_remitente.value.length == "")
  {
    alert("Ingrese Nombre o Raz�n Social");
    document.frmRegistro.cNombreRemitente.focus();
    return (false);
  }
  document.frmRegistro.method="POST";
  document.frmRegistro.action="registroRemitentesDt.php";
  document.frmRegistro.submit();
}
 
//--></script>
</head>
<body>

<table width="500" height="300" cellpadding="0" cellspacing="0" border="1" bgcolor="#ffffff">
<tr>
<td align="left" valign="top">

<!--Main layout-->
 <main class="mx-lg-5">
     <div class="container-fluid">
          <!--Grid row-->
         <div class="row wow fadeIn">
              <!--Grid column-->
             <div class="col-md-12 mb-12">
                  <!--Card-->
                 <div class="card">
                      <!-- Card header -->
                     <div class="card-header text-center ">
                         >>
                     </div>
                      <!--Card content-->
                     <div class="card-body">

<div class="AreaTitulo">Informacion de institucion:</div>	
<form name="frmRegistro">
<table cellpadding="0" cellspacing="0" border="0" width="520">
<tr>		
			<input type="hidden" name="opcion" value="1">
<?
 if($_GET[clear]==""){ 
 	if(trim($_GET[txtdirec_remitentex])!=""){  $dir=$_GET[txtdirec_remitentex]; } }
 else{ $dir = $_POST[txtdirec_remitente]; }
 
 if($_GET[clear]==""){ 
 	if(trim($_GET[cCodDepartamentox])!=""){  $dep=$_GET[cCodDepartamentox]; } }
 else{ $dep = $_POST[cCodDepartamento]; }
 
 if($_GET[clear]==""){ 
 	if(trim($_GET[cCodProvinciax])!=""){ $pro=$_GET[cCodProvinciax]; } }
 else{ $pro = $_POST[cCodProvincia]; }
 
 if($_GET[clear]==""){ 
 	if(trim($_GET[cCodDistritox])!=""){ $dis=$_GET[cCodDistritox]; } }
 else{ $dis =  $_POST[cCodDistrito]; }
 
  	?>
     <input id="iCodRemitentex" name="iCodRemitentex" type="hidden" value="<?=$_REQUEST[iCodRemitente]?>">
      <input id="txtdirec_remitentex" name="txtdirec_remitentex" type="hidden" value="<?=$dir?>">
      <input id="cCodDepartamentox" name="cCodDepartamentox" type="hidden" value="<?=$dep?>">
      <input id="cCodProvinciax" name="cCodProvinciax" type="hidden" value="<?=$pro?>">
      <input id="cCodDistritox" name="cCodDistritox" type="hidden" value="<?=$dis?>">	 
			
<td class="FondoFormRegistro">
<?

?>
		<table border="0" align="center">
    <tr>
    <td >Direcci�n:</td>
    <td  align="left"><input name="txtdirec_remitente" type="text" value="<?=$dir?>" maxlength="120" size="70" class="FormPropertReg form-control"></td>
    </tr>
    <tr>
    <td >Departamento:</td>
    <td align="left">
				<select name="cCodDepartamento" class="FormPropertReg form-control" id="cCodDepartamento" style="width:236px" onChange="releer();">
				<option value="">Seleccione:</option>
				<?
        $sqlDep="select * from Tra_U_Departamento "; 
        $rsDep=mssql_query($sqlDep,$cnx);
				while ($RsDep=MsSQL_fetch_array($rsDep)){
	  	  		if($RsDep["cCodDepartamento"]==$dep){
          			$selecClas="selected";
						}else{
          			$selecClas="";
						}
            echo "<option value=".$RsDep["cCodDepartamento"]." ".$selecClas.">".$RsDep["cNomDepartamento"]."</option>";
				}
        mssql_free_result($rsDep);
        ?>
				</select>
		</td>
    </tr>
    
    <tr>
    <td >Provincia:</td>
    <td align="left">
    		<select name="cCodProvincia"  class="FormPropertReg form-control" id="cCodProvincia" onChange="releer();" style="width:236px" <?if($dep=="") echo "disabled"?> >
     	  <option value="">Seleccione:</option>
    		<?
        $sqlPro="SELECT * from Tra_U_Provincia WHERE cCodDepartamento=".$dep;
        $rsPro=mssql_query($sqlPro,$cnx);
				while ($RsPro=MsSQL_fetch_array($rsPro)){
	  	  		if($RsPro["cCodProvincia"]==$pro){
          			$selecPro="selected";
						}else{
          			$selecPro="";
						}
            echo "<option value=".$RsPro["cCodProvincia"]." ".$selecPro.">".$RsPro["cNomProvincia"]."</option>";
				}
        mssql_free_result($rsPro);
			  ?>
				</select>
		</td>
    </tr>
    
    <tr>
    <td >Distrito:</td>
    <td align="left">
    		<select name="cCodDistrito" class="FormPropertReg form-control" id="cCodDistrito" style="width:236px" <?if($dep=="" || $pro=="" ) echo "disabled"?> >
				<option value="">Seleccione:</option>
    		<?
        $sqlDis="SELECT * from Tra_U_Distrito WHERE cCodDepartamento='$dep' AND cCodProvincia='$pro'"; 
        $rsDis=mssql_query($sqlDis,$cnx);
				while ($RsDis=MsSQL_fetch_array($rsDis)){
	  	  		if($RsDis["cCodDistrito"]==$dis){
          		$selecDis="selected";
          	}else{
          		$selecDis="";
          	}
            echo "<option value=".$RsDis["cCodDistrito"]." ".$selecDis.">".$RsDis["cNomDistrito"]."</option>";
				}
				mssql_free_result($rsDis);
        ?>
				</select>
		</td>
    </tr>
    <tr>
    <td colspan="2" align="center">
                <input name="button" type="button" class="btn btn-primary" value="Modificar" onClick="sendValue(this.form.txtdirec_remitente,this.form.cCodDepartamento,this.form.cCodProvincia,this.form.cCodDistrito);">
		</td>
    </tr>
    </table>
					</div>
                 </div>
             </div>
         </div>
     </div>
 </main>

</form>
					</div>
                 </div>
             </div>
         </div>
     </div>
 </main>

</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>