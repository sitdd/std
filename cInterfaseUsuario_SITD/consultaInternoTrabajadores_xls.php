<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: consultaInternoTrabajadores_xls.php
SISTEMA: SISTEMA  DE TR�MITE DOCUMENTARIO DIGITAL
OBJETIVO: Reporte general en EXCEL de los Documentos Internos de Trabajadores
PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripci�n
------------------------------------------------------------------------
1.0   Larry Ortiz        05/09/2018      Creaci�n del programa.
------------------------------------------------------------------------
*****************************************************************************************/
include_once("../conexion/conexion.php");
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=consultaInternoTrabajadores.xls");
	
	$anho = date("Y");
	$datomes = date("m");
	$datomes = $datomes*1;
	$datodia = date("d");
	$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre");
	
	echo "<table width=780 border=0><tr><td align=center colspan=5>";
	echo "<H3>REPORTE - DOCUMENTOS INTERNOS DE TRABAJADORES</H3>";
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=right colspan=5>";
	echo "SITD, ".$datodia." ".$meses[$datomes].' del '.$anho;
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=left colspan=7>";
	$sqllog="select cNombresTrabajador, cApellidosTrabajador from tra_m_trabajadores where iCodTrabajador='$traRep' "; 
	$rslog=mssql_query($sqllog,$cnx);
	$Rslog=MsSQL_fetch_array($rslog);
	echo "GENERADO POR : ".$Rslog[cNombresTrabajador]." ".$Rslog[cApellidosTrabajador];
	echo " ";
?>	
	<table style="width: 780px;border: solid 1px #5544DD; border-collapse: collapse" align="center">
     <thead>
      <tr>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Documento</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N&ordm; Doc.e</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Fecha</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Asunto</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Observaciones</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Enviado</th>
      </tr>
	 </thead>
     <tbody>
<?			

if($_GET[fDesde]!='' && $_GET[fHasta]!=''){

	   $fDesde=date("Ymd", strtotime($_GET[fDesde]));
	   $fHasta=date("Y-m-d", strtotime($_GET[fHasta]));
	   function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
       $date_r = getdate(strtotime($date));
       $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
       return $date_result;
				}
	   $fHasta=dateadd($fHasta,1,0,0,0,0,0); // + 1 dia
}/*
      $sql="  SELECT cDescTipoDoc,cCodificacion,fFecRegistro,cAsunto,cObservaciones,Tra_M_Tramite_Movimientos.nFlgEnvio ";
    $sql.=" FROM Tra_M_Tramite LEFT OUTER JOIN Tra_M_Tipo_Documento ON Tra_M_Tramite.cCodTipoDoc=Tra_M_Tipo_Documento.cCodTipoDoc ";
    $sql.=" LEFT OUTER JOIN Tra_M_Tramite_Movimientos ON Tra_M_Tramite.iCodTramite=Tra_M_Tramite_Movimientos.iCodTramite ";
	$sql.=" WHERE Tra_M_Tramite.nFlgTipoDoc=2 AND Tra_M_Tramite.nFlgClaseDoc=2 ";
       if($_GET[fDesde]!="" AND $_GET[fHasta]==""){
  	$sql.=" AND Tra_M_Tramite.fFecRegistro>'$fDesde' ";
    }
    if($_GET[fDesde]=="" AND $_GET[fHasta]!=""){
  	$sql.=" AND Tra_M_Tramite.fFecRegistro<='$fHasta' ";
    }
  
    if($_GET[fDesde]=="" AND $_GET[fHasta]!=""){
  	$sql.=" AND Tra_M_Tramite.fFecRegistro<='$fHasta' ";
    }
	
	if($_GET[SI]==1 AND $_GET[NO]==1 ){
    $sql.=" AND (Tra_M_Tramite_Movimientos.nFlgEnvio=1 OR Tra_M_Tramite_Movimientos.nFlgEnvio=0) ";
    }
    if($_GET[SI]==0 AND $_GET[NO]==1 ){
    $sql.=" AND Tra_M_Tramite_Movimientos.nFlgEnvio=0 ";
    }
    if($_GET[SI]==1 AND $_GET[NO]==0 ){
    $sql.=" AND Tra_M_Tramite_Movimientos.nFlgEnvio=1 ";
    }	
	if($_GET[cCodificacion]!=""){
    $sql.=" AND Tra_M_Tramite.cCodificacion='$_GET[cCodificacion]' ";
    }
	if($_GET[cAsunto]!=""){
    $sql.=" AND Tra_M_Tramite.cAsunto LIKE '%$_GET[cAsunto]%' ";
    }
	if($_GET[cObservaciones]!=""){
    $sql.=" AND Tra_M_Tramite.cObservaciones LIKE '%$_GET[cObservaciones]%' ";
    }
	if($_GET[cCodTipoDoc]!=""){
    $sql.=" AND Tra_M_Tramite.cCodTipoDoc='$_GET[cCodTipoDoc]' ";
    }	   
	
	
	
	$sql.= " ORDER BY Tra_M_Tramite.iCodTramite DESC ";*/
$sql.= " SP_CONSULTA_INTERNO_TRABAJADOR '$fDesde', '$fHasta',  '$_GET[SI]', '$_GET[NO]',  '%$_GET[cCodificacion]%', '%$_GET[cAsunto]%',  '%$_GET[cObservaciones]%', '$_GET[cCodTipoDoc]', '$campo', '$orden' ";
    $rs=mssql_query($sql,$cnx);
   //echo $sql;

       while ($Rs=MsSQL_fetch_array($rs)){
?>
    <tr>
    <td style="width: 8%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><? echo $Rs[cDescTipoDoc];?> </td>
    <td style="width: 8%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><? echo $Rs[cCodificacion];?> </td>
    <td style="width: 8%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
	 <?
    	echo "<div style=color:#727272>".date("d-m-Y", strtotime($Rs[fFecRegistro]))."</div>";
        echo "<div style=color:#727272;font-size:10px>".date("h:i A", strtotime($Rs[fFecRegistro]))."</div>";
	 ?>	</td>
    <td style="width: 8%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><? echo $Rs[cAsunto];?> </td>
    <td style="width: 8%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><? echo $Rs[cObservaciones];?> </td>
 <td style="width: 8%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><? if($Rs[nFlgEnvio]==0){ echo "NO";} if($Rs[nFlgEnvio]==1){echo "SI";}?> 
 </td>
   </tr>
<? }?>
	  </tbody>
    </table>  	   	 