<?php
session_start();
if($_SESSION['CODIGO_TRABAJADOR']!=""){
  include_once("../conexion/conexion.php");
//cDescTipoDoc=&Entrada=1&Interno=&Salida=&pag=2
$pag = $_GET['pag'];

?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
<style type="text/css">
<!--
.Estilo1 {color: #FFFFFF}
-->
</style>
</head>
<body>
<?include("includes/menu.php");?>

<!--Main layout-->
<main class="mx-lg-5">
    <div class="container-fluid">
        <!--Grid row-->
        <div class="row wow fadeIn">
            <!--Grid column-->
            <div class="col-md-12 mb-12">
                <!--Card-->
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header text-center ">Mantenimiento >> M. Tipo de Documentos</div>
                    <!--Card content-->
                    <div class="card-body">
                        <form name="form1" method="GET" action="iu_tipo_doc.php">
                            <div class="form-row">
                                <div class="col-lg-2">
                                    Tipo de Documento:
                                    <input name="cDescTipoDoc"  class="FormPropertReg form-control" type="text" value="" />
                                </div>

                                <div class="col-lg-2">
                                    <div class="form-check">
                                        <input type="checkbox" name="Entrada" class="form-check-input" id="Entrada" value="1" <?if($_GET[Entrada]==1) echo "checked"?> >
                                        <label class="form-check-label" for="Entrada">Entradas</label>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" name="Interno" class="form-check-input" id="Interno" value="1" <?if($_GET[Interno]==1) echo "checked"?> >
                                        <label class="form-check-label" for="Interno">Internos</label>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" name="Salida" class="form-check-input" id="Salida" value="1" <?if($_GET[Salida]==1) echo "checked"?> >
                                        <label class="form-check-label" for="Salida">Salidas</label>
                                    </div>
                                </div>
                            </div>
                              &nbsp;&nbsp;&nbsp;
                            <button class="btn btn-primary" type="submit" name="Submit" onMouseOver="this.style.cursor='hand'">
                                <b>Buscar</b> <img src="images/icon_buscar.png" width="17" height="17" border="0">
                            </button>
                            <button class="btn btn-primary"  name="Restablecer" onClick="window.open('<?=$PHP_SELF?>', '_self');" onMouseOver="this.style.cursor='hand'"> <b>Restablecer</b>
                                <img src="images/icon_clear.png" width="17" height="17" border="0">
                            </button>
                            <button class="btn btn-primary" onClick="window.open('iu_tipo_doc_xls.php?cDescTipoDoc=<?=$_GET[cDescTipoDoc]?>&cSiglaDoc=<?=$_GET[cSiglaDoc]?>&Entrada=<?=$_GET[Entrada]?>&Interno=<?=$_GET[Interno]?>&Salida=<?=$_GET[Salida]?>&orden=<?=$_GET[orden]?>&campo=<?=$_GET[campo]?>&traRep=<?=$_SESSION['CODIGO_TRABAJADOR']?>', '_blank');" onMouseOver="this.style.cursor='hand'">
                                <b>a Excel</b> <img src="images/icon_excel.png" width="17" height="17" border="0">
                            </button>
                            <button class="btn btn-primary" onClick="window.open('iu_tipo_doc_pdf.php?cDescTipoDoc=<?=$_GET[cDescTipoDoc]?>&cSiglaDoc=<?=$_GET[cSiglaDoc]?>&Entrada=<?=$_GET[Entrada]?>&Interno=<?=$_GET[Interno]?>&Salida=<?=$_GET[Salida]?>&orden=<?=$_GET[orden]?>&campo=<?=$_GET[campo]?>', '_blank');" onMouseOver="this.style.cursor='hand'">
                                <b>a Pdf</b> <img src="images/icon_pdf.png" width="17" height="17" border="0">
                            </button>
                            <?echo "<a class='btn btn-primary' href='iu_nuevo_tipo_doc.php'>Nuevo Tipo Documento</a>";?>
                        </form>
                        <?
                        function paginar($actual, $total, $por_pagina, $enlace, $maxpags=0) {
                            $total_paginas = ceil($total/$por_pagina);
                            $anterior = $actual - 1;
                            $posterior = $actual + 1;
                            $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
                            $maximo = $maxpags ? min($total_paginas, $actual+floor($maxpags/2)): $total_paginas;
                            if ($actual>1)
                                $texto = "<a href=\"$enlace$anterior\">«</a> ";
                            else
                                $texto = "<b>«</b> ";
                            if ($minimo!=1) $texto.= "... ";
                            for ($i=$minimo; $i<$actual; $i++)
                                $texto .= "<a href=\"$enlace$i\">$i</a> ";
                            $texto .= "<b>$actual</b> ";
                            for ($i=$actual+1; $i<=$maximo; $i++)
                                $texto .= "<a href=\"$enlace$i\">$i</a> ";
                            if ($maximo!=$total_paginas) $texto.= "... ";
                            if ($actual<$total_paginas)
                                $texto .= "<a href=\"$enlace$posterior\">»</a>";
                            else
                                $texto .= "<b>»</b>";
                            return $texto;
                        }


                        if (!isset($pag)) $pag = 1; // Por defecto, pagina 1
                        $tampag = 15;
                        $reg1 = ($pag-1) * $tampag;

                        // ordenamiento
                        if($_GET[campo]==""){
                            $campo="Tipo";
                        }Else{
                            $campo=$_GET[campo];
                        }

                        if($_GET[orden]==""){
                            $orden="ASC";
                        }Else{
                            $orden=$_GET[orden];
                        }

                        //invertir orden
                        if($orden=="ASC") $cambio="DESC";
                        if($orden=="DESC") $cambio="ASC";

                        /*
                          $sql="select * from Tra_M_Tipo_Documento ";
                          $sql.=" WHERE cCodTipoDoc>0 ";
                          if($_GET[Entrada]==1 ){
                          $sql.="AND nFlgEntrada='1'  ";
                          }
                          if($_GET[Interno]==1){
                          $sql.="AND nFlgInterno='1'  ";
                          }
                          if($_GET[Salida]==1){
                          $sql.="AND nFlgSalida='1' ";
                          }
                         if($_GET[cDescTipoDoc]!=""){
                        $sql.=" AND cDescTipoDoc like '%$_GET[cDescTipoDoc]%' ";
                        }
                        if($_GET[cSiglaDoc]!=""){
                        $sql.=" AND cSiglaDoc='$_GET[cSiglaDoc]' ";
                        }
                        $sql.="ORDER BY $campo $orden";
                        */

                        echo "==>".$_GET[cDescTipoDoc3];
                        $sql="SP_TIPO_DOCUMENTO_LISTA '$_GET[Entrada]' , '$_GET[Interno]', '$_GET[Salida]' , '%$_GET[cDescTipoDoc]%' , '%$_GET[cSiglaDoc]%' ,'".$orden."' , '".$campo."' ";
                        $rs=mssql_query($sql,$cnx);
                        $total = MsSQL_num_rows($rs);

                        ?>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="181" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=Tipo&orden=<?=$cambio?>&cDescTipoDoc=<?=$_GET[cDescTipoDoc]?>&Entrada=<?=$_GET[Entrada]?>&Interno=<?=$_GET[Interno]?>&Salida=<?=$_GET[Salida]?>"  style=" text-decoration:<?if($campo=="Tipo"){ echo "underline"; }Else{ echo "none";}?>">Tipo de Documento</a></th>
                                    <th width="80" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=Entrada&orden=<?=$cambio?>&Entrada=<?=$_GET[Entrada]?>&Interno=<?=$_GET[Interno]?>&Salida=<?=$_GET[Salida]?>&cDescTipoDoc=<?=$_GET[cDescTipoDoc]?>"  style=" text-decoration:<?if($campo=="Entrada"){ echo "underline"; }Else{ echo "none";}?>">Entradas</a></th>
                                    <th width="80" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=Interno&orden=<?=$cambio?>&Entrada=<?=$_GET[Entrada]?>&Interno=<?=$_GET[Interno]?>&Salida=<?=$_GET[Salida]?>&cDescTipoDoc=<?=$_GET[cDescTipoDoc]?>"  style=" text-decoration:<?if($campo=="Interno"){ echo "underline"; }Else{ echo "none";}?>">Internos</a></th>
                                    <th width="80" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=Salida&orden=<?=$cambio?>&Entrada=<?=$_GET[Entrada]?>&Interno=<?=$_GET[Interno]?>&Salida=<?=$_GET[Salida]?>&cDescTipoDoc=<?=$_GET[cDescTipoDoc]?>"  style=" text-decoration:<?if($campo=="Salida"){ echo "underline"; }Else{ echo "none";}?>">Salidas</a></th>
                                    <th width="80" class="headCellColum">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <?php
                                $numrows=MsSQL_num_rows($rs);
                                if($numrows==0){
                                        echo "NO SE ENCONTRARON REGISTROS<br>";
                                        echo "TOTAL DE REGISTROS : ".$numrows;
                                }else{
                                         echo "TOTAL DE REGISTROS : ".$numrows;
                                    for ($i=$reg1; $i<min($reg1+$tampag, $total); $i++) {
                                mssql_data_seek($rs, $i);
                                $Rs=MsSQL_fetch_array($rs);
                                //while ($Rs=MsSQL_fetch_array($rs)){
                                            if ($color == "#CEE7FF"){
                                              $color = "#F9F9F9";
                                                }else{
                                              $color = "#CEE7FF";
                                                }
                                                if ($color == ""){
                                              $color = "#F9F9F9";
                                                }
                                ?>
                                <tr bgcolor="<?=$color?>">
                                  <? /*
                                   <td><? echo $Rs[cCodTipoDoc];?></td>
                                  */ ?>
                                    <td height="23" align="left"><? echo utf8_encode($Rs[cDescTipoDoc]);?></td>
                                   <? /* <td> <? echo $Rs[cSiglaDoc];?></td> */ ?>
                                    <td><a href="../cLogicaNegocio_SITD/ln_actualiza_flg_tipo_doc.php?Entrada=<? echo $Rs[nFlgEntrada];?>&id=<? echo $Rs[cCodTipoDoc];?>" ><? if($Rs[nFlgEntrada]=='1'){echo SI;} else{echo NO;}?></a></td>
                                    <td><a href="../cLogicaNegocio_SITD/ln_actualiza_flg_tipo_doc.php?Interno=<? echo $Rs[nFlgInterno];?>&id=<? echo $Rs[cCodTipoDoc];?>" ><?  if($Rs[nFlgInterno]=='1'){echo SI;} else{echo NO;}?></a></td>
                                    <td><a href="../cLogicaNegocio_SITD/ln_actualiza_flg_tipo_doc.php?Salida=<? echo $Rs[nFlgSalida];?>&id=<? echo $Rs[cCodTipoDoc];?>" ><? if($Rs[nFlgSalida]=='1'){echo SI;} else{echo NO;}?></a></td>
                                    <td>

                                        <?php
                                          $sqlTipoDeDoc = "SELECT COUNT(*) AS 'TOTAL' FROM Tra_M_Tramite WHERE cCodTipoDoc = ".$Rs['cCodTipoDoc'];
                                          $rsTipoDeDoc  = mssql_query($sqlTipoDeDoc,$cnx);
                                          $RsTipoDeDoc  = mssql_fetch_array($rsTipoDeDoc);

                                          if ($RsTipoDeDoc['TOTAL'] == 0) {
                                        ?>
                                          <a href="../cLogicaNegocio_SITD/ln_elimina_tipo_doc.php?id=<? echo $Rs[cCodTipoDoc];?>&Entrada=<?=$_REQUEST[Entrada]?>&Interno=<?=$_REQUEST[Interno]?>&Salida=<?=$_REQUEST[Salida]?>&cDescTipoDoc=<?=$_REQUEST[cDescTipoDoc]?>&pag=<?=$pag?>" onClick='return ConfirmarBorrado();'">
                                          <i class="far fa-trash-alt"></i>
                                        </a>
                                        <?php
                                          }
                                        ?>
                                            <a href="../cInterfaseUsuario_SITD/iu_actualiza_tipo_doc.php?cod=<? echo $Rs[cCodTipoDoc];?>&sw=5&Entrada=<? echo $Rs[nFlgEntrada]?>&Interno=<? echo $Rs[nFlgInterno]?>&Salida=<? echo $Rs[nFlgSalida]?>&cDescTipoDoc=<?echo utf8_encode($Rs[cDescTipoDoc])?>&pag=<?=$pag?>">
                                          <i class="fas fa-edit"></i>
                                        </a>
                                      </td>
                                  </tr>

                                <?
                                }
                                }
                                ?>
                            </tbody>
                        </table>

                        <? echo paginar($pag, $total, $tampag, "iu_tipo_doc.php?cDescTipoDoc=".$_GET[cDescTipoDoc]."&Entrada=".$_GET[Entrada]."&Interno=".$_GET[Interno]."&Salida=".$_GET[Salida]."&pag=");?>

                    </div>
                </div>
            </div>
        </div>
    </div>
 </main>

<?include("includes/userinfo.php");?>

<?include("includes/pie.php");?>

    <script>
        function ConfirmarBorrado()
        {
            if (confirm("Esta seguro de eliminar el registro?")){
                return true;
            }else{
                return false;
            }
        }
    </script>
</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>