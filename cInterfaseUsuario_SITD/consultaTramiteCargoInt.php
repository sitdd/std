<?php
	include_once("../conexion/conexion.php");
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=consultaTramiteCargo.xls");
	
	$anho    = date("Y");
	$datomes = date("m");
	$datomes = $datomes*1;
	$datodia = date("d");
	$meses   = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre");
	$sqlOfis1 = " SELECT iCodOficina, cSiglaOficina FROM Tra_M_Oficinas WHERE iFlgEstado = 1 ORDER BY cSiglaOficina ASC"; 		   
	$rsOfis1  = mssql_query($sqlOfis1,$cnx);
	$num      = mssql_num_rows($rsOfis1);
	
	function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    $date_r = getdate(strtotime($date));
    $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
    return $date_result;
	}
	
	if ($fDesde != ''){
		$fDesde = date("Ymd", strtotime($_REQUEST[fDesde]));
	}else{
		$fDesde = 0;
	}
  
  if ($_REQUEST[fHasta] != ''){
  	$fHasta = date("Y-m-d", strtotime($_REQUEST[fHasta]));
		$fHasta = dateadd($fHasta,1,0,0,0,0,0); // + 1 dia 
	}else{
		$fHasta = 0;
	}
	
	echo "<table width=780 border=0><tr><td align=center colspan=12>";
	echo "<H3>ESTADISTICA DE ENVIO A NIVEL INTERNACIONAL</H3>";
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=right colspan=12>";
	echo "SITD, ".$datodia." ".$meses[$datomes].' del '.$anho;
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=left colspan=7>";
	$sqllog = "SELECT cNombresTrabajador, cApellidosTrabajador FROM tra_m_trabajadores WHERE iCodTrabajador='$traRep' "; 
	$rslog  = mssql_query($sqllog,$cnx);
	$Rslog  = mssql_fetch_array($rslog);
	echo "GENERADO POR : ".$Rslog[cNombresTrabajador]." ".$Rslog[cApellidosTrabajador];
	echo " ";
	?>
	<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="center">
		<thead>
    	<tr>
      	<th colspan="1" style="text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">ENVIADOS POR</th>
				<th colspan="<?=$num?>" style="text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">PRO INVERSION</th>
				<th colspan="1" style="text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">TOTAL POR</th>	
			</tr>
      
      <tr>
      	<th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">DEPARTAMENTO</th>
        <? $sqlOfis = " SELECT iCodOficina,cSiglaOficina FROM Tra_M_Oficinas WHERE iFlgEstado=1 ORDER BY cSiglaOficina ASC";
        	 $rsOfis  = mssql_query($sqlOfis,$cnx);
		    		while ($RsOfis = mssql_fetch_array($rsOfis)){
		   	?>
        	<th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8"><? echo $RsOfis[cSiglaOficina];?></th>
        <?  }  ?>                      
          <th style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">DEPART.</th>			
				</tr>
			</thead>
			
			<tbody>
				<? $sqlDep = "SELECT cCodDepartamento,cNomDepartamento FROM Tra_U_Departamento"; 		  
				   $rsDep  = mssql_query($sqlDep,$cnx);
				   while ($RsDep = mssql_fetch_array($rsDep)){
				?>
			  <tr>
        	<td style="width:8%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><? echo $RsDep[cNomDepartamento];?></td>
          <? $sqlOfis2 = " SELECT iCodOficina,cSiglaOficina FROM Tra_M_Oficinas WHERE iFlgEstado = 1 ORDER BY cSiglaOficina ASC";	$rsOfis2  = mssql_query($sqlOfis2,$cnx);
						 while ($RsOfis2 = mssql_fetch_array($rsOfis2)){
		  			// while ($RsOfis2=MsSQL_fetch_array($rsOfis2)){
		   		?>         
          <td style="width:8%;text-align:center;border: solid 1px #6F6F6F;font-size:10px;">           
						<?
						 	// echo $RsDep[cNomDepartamento];
						  $sqlContar = "SELECT dbo.fn_Rep_Cargo($RsOfis2[iCodOficina],'$RsDep[cCodDepartamento]','$fDesde','$fHasta') ";
	   					$rsContar  = mssql_query($sqlContar,$cnx); 
						 	$RsContar  = mssql_fetch_array($rsContar);
							echo $RsContar[0];
						?>
					</td>
					<? }  ?>                   
        </tr>	
      <?  }  ?>
        <tr>
        	<td colspan="1" style="text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">TOTAL POR OFICINAS</td> 
          <?  for($i=1; $i<=$num; $i++) {
		  		?>     
          <td style="text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8"><? ?></td>
          <?  }  ?>      
          <td style="text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8"><? ?></td>
				</tr>   		  			
			</tbody>
		</table>