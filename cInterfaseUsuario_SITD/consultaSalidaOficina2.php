<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: consultaSalidaOficina.php
SISTEMA: SISTEMA  DE TRÁMITE DOCUMENTARIO DIGITAL
OBJETIVO: Consulta de los Documentos de Salida por Oficina
PROPIETARIO: AGENCIA PERUANA DE COOPERACIÓN INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver      Autor             Fecha        Descripción
------------------------------------------------------------------------
1.0   APCI       03/08/2018   Creación del programa.
 
------------------------------------------------------------------------


*****************************************************************************************/
?>
<?
session_start();
If($_SESSION['CODIGO_TRABAJADOR']!=""){
include_once("../conexion/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<script type="text/javascript" language="javascript" src="includes/lytebox.js"></script>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
<link type="text/css" rel="stylesheet" href="css/dhtmlgoodies_calendar.css" media="screen"/>
<script type="text/javascript" src="scripts/dhtmlgoodies_calendar.js"></script>
<script Language="JavaScript">

function Buscar()
{
  document.frmConsultaEntrada.action="<?=$PHP_SELF?>";
  document.frmConsultaEntrada.submit();
}

//--></script>
</head>
<body>

	<?include("includes/menu.php");?>



<!--Main layout-->
 <main class="mx-lg-5">
     <div class="container-fluid">
          <!--Grid row-->
         <div class="row wow fadeIn">
              <!--Grid column-->
             <div class="col-md-12 mb-12">
                  <!--Card-->
                 <div class="card">
                      <!-- Card header -->
                     <div class="card-header text-center ">
                         >>
                     </div>
                      <!--Card content-->
                     <div class="card-body">

<div class="AreaTitulo">Consulta de Documentos de Salida por oficina</div>
    



							<form name="frmConsultaEntrada" method="GET" action="consultaEntradaGeneral.php">
						<tr>
							<td width="120" >N&ordm; Documento:</td>
							<td width="390" colspan="4" align="left"><input type="txt" name="cCodificacion" value="<?=$_GET[cCodificacion]?>" size="28" class="FormPropertReg form-control"></td>
							<td width="110" >Desde:</td>
							<td colspan="4" align="left">

									<td><input type="text" readonly name="fDesde" value="<?=$_GET[fDesde]?>" style="width:75px" class="FormPropertReg form-control"></td><td><div class="boton" style="width:24px;height:20px"><a href="javascript:;" onclick="displayCalendar(document.forms[0].fDesde,'dd-mm-yyyy',this,false)"><img src="images/icon_calendar.png" width="22" height="20" border="0"></a></div></td>
									<td width="20"></td>
									<td >Hasta:&nbsp;<input type="text" readonly name="fHasta" value="<?=$_GET[fHasta]?>" style="width:75px" class="FormPropertReg form-control"></td><td><div class="boton" style="width:24px;height:20px"><a href="javascript:;" onclick="displayCalendar(document.forms[0].fHasta,'dd-mm-yyyy',this,false)"><img src="images/icon_calendar.png" width="22" height="20" border="0"></a></div></td>
									</tr></table>							</td>
						</tr>
						<tr>
							<td width="120" >Tipo Documento:</td>
							<td width="390" colspan="4" align="left"><select name="cCodTipoDoc" class="FormPropertReg form-control" style="width:180px" />
									<option value="">Seleccione:</option>
									<?
									$sqlTipo="SELECT * FROM Tra_M_Tipo_Documento WHERE nFlgSalida=1 ";
          				            $sqlTipo.="ORDER BY cDescTipoDoc ASC";
          				            $rsTipo=mssql_query($sqlTipo,$cnx);
          				while ($RsTipo=MsSQL_fetch_array($rsTipo)){
          					if($RsTipo["cCodTipoDoc"]==$_GET[cCodTipoDoc]){
          						$selecTipo="selected";
          					}Else{
          						$selecTipo="";
          					}
          				echo "<option value=".$RsTipo["cCodTipoDoc"]." ".$selecTipo.">".$RsTipo["cDescTipoDoc"]."</option>";
          				}
          				mssql_free_result($rsTipo);
									?>
									</select></td>
							<td width="110" >Asunto:</td>
							<td colspan="4" align="left"><input type="txt" name="cAsunto" value="<?=$_GET[cAsunto]?>" size="65" class="FormPropertReg form-control">							</td>
						</tr>
						<tr>
							<td width="120" >Remitente:</td>
							<td width="390" colspan="4" align="left"><input type="txt" name="cNombre" value="<?=$_GET[cNombre]?>" size="55" class="FormPropertReg form-control"></td>
							<td width="110" >Observaciones:</td>
							<td colspan="4" align="left" class="CellFormRegOnly"><input type="txt" name="cObservaciones" value="<?=$_GET[cObservaciones]?>" size="55" class="FormPropertReg form-control"></td>
						</tr>
						<tr>
							<td width="120" height="10" >Requiere Respuesta:</td>
							<td height="10" align="left" colspan="4">
                  SI<input type="checkbox" name="RespuestaSI" value="1" onclick="this.form.Respuesta.disabled = (this.checked) ? false : true;" <?if($_GET[RespuestaSI]==1) echo "checked"?> />
							   	&nbsp;&nbsp;&nbsp;
	                NO<input type="checkbox" name="RespuestaNO" value="1" <?if($_GET[RespuestaNO]==1) echo "checked"?> />							</td>
               <td >Registros :</td>
              <td  > Propios:							</td>
							<td  align="left"><input type="checkbox" name="RegistroPersonal" value="1" <?if($_GET[RegistroPersonal]==1) echo "checked"?> /></td>
							<td  >Solicitados:</td>
							<td  align="left"><input type="checkbox" name="RegistroSolicitado" value="1" <?if($_GET[RegistroSolicitado]==1) echo "checked"?> /></td>
						</tr>
									
					<tr>
						  <td width="120" >Respuesta:</td>
              <td colspan="4" align="left">
              		<select name="Respuesta" id="Respuesta" class="FormPropertReg form-control" <? if($_GET[RespuestaSI]!=1){ echo "disabled";} ?> >
              		    <option>Seleccione:</option>
              		    <option value="1" <? if($_GET[Respuesta]==1){echo 'selected';} ?> style=color:#950000>Pendiente</option>
              		    <option value="2" <? if($_GET[Respuesta]==2){echo 'selected';} ?> style=color:#0154AF>Recibido</option>
              		</select>							</td>
             <tr>           
					<td colspan="10" align="right">
              <table width="400" border="0" align="left">
              <tr>
              <td align="left">
                  Descarga &nbsp; <img src="images/icon_download.png" width="16" height="16" border="0" > &nbsp; &nbsp;
	              	| &nbsp; &nbsp;  Editar &nbsp; <i class="fas fa-edit"></i>&nbsp;&nbsp;
                  | &nbsp; &nbsp; Respuesta &nbsp; <img src="images/icon_avance.png" width="16" height="16" border="0">&nbsp;&nbsp;&nbsp;&nbsp;
                  | &nbsp; &nbsp; Multiple &nbsp; <img src="images/page_copy.png" width="16" height="16" border="0">                           </td>
               </tr>
              </table>
							<button class="btn btn-primary" onclick="Buscar();" onMouseOver="this.style.cursor='hand'"> <b>Buscar</b> <img src="images/icon_buscar.png" width="17" height="17" border="0"> </button>
							�
                           <button class="btn btn-primary" onclick="window.open('<?=$PHP_SELF?>', '_self');" onMouseOver="this.style.cursor='hand'"> <b>Restablecer</b> <img src="images/icon_clear.png" width="17" height="17" border="0"> </button>
              �<? // ordenamiento
              if($_GET[campo]==""){ $campo="Tra_M_Tramite.iCodTramite"; }Else{ $campo=$_GET[campo];}
              if($_GET[orden]==""){ $orden="DESC"; }Else{ $orden=$_GET[orden];}
              ?>
							<button class="btn btn-primary" onclick="window.open('consultaSalidaOficina_xls.php?fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&RespuestaSI=<?=$_GET[RespuestaSI]?>&RespuestaNO=<?=$_GET[RespuestaNO]?>&cCodificacion=<?=$_GET[cCodificacion]?>&cAsunto=<?=$_GET[cAsunto]?>&cObservaciones=<?=$_GET[cObservaciones]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cNombre=<?=$_GET[cNombre]?>&Respuesta=<?=$_GET[Respuesta]?>&RegistroPersonal=<?=$_GET[RegistroPersonal]?>&RegistroSolicitado=<?=$_GET[RegistroSolicitado]?>&campo=<?=$campo?>&orden=<?=$orden?>&traRep=<?=$_SESSION['CODIGO_TRABAJADOR']?>&session=<?=$_SESSION[iCodOficinaLogin]?>', '_self');" onMouseOver="this.style.cursor='hand'"> <b>a Excel</b> <img src="images/icon_excel.png" width="17" height="17" border="0"> </button>
							�
                           <button class="btn btn-primary" onclick="window.open('consultaSalidaOficina_pdf.php?fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&RespuestaSI=<?=$_GET[RespuestaSI]?>&RespuestaNO=<?=$_GET[RespuestaNO]?>&cCodificacion=<?=$_GET[cCodificacion]?>&cAsunto=<?=$_GET[cAsunto]?>&cObservaciones=<?=$_GET[cObservaciones]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cNombre=<?=$_GET[cNombre]?>&Respuesta=<?=$_GET[Respuesta]?>&RegistroPersonal=<?=$_GET[RegistroPersonal]?>&RegistroSolicitado=<?=$_GET[RegistroSolicitado]?>&campo=<?=$campo?>&orden=<?=$orden?>', '_blank');" onMouseOver="this.style.cursor='hand'"> <b>a Pdf</b> <img src="images/icon_pdf.png" width="17" height="17" border="0"> </button>    			</td>
					</tr>
					</form>
					</table>
			</fieldset>

</form>



<?   
function paginar($actual, $total, $por_pagina, $enlace, $maxpags=0) {
$total_paginas = ceil($total/$por_pagina);
$anterior = $actual - 1;
$posterior = $actual + 1;
$minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
$maximo = $maxpags ? min($total_paginas, $actual+floor($maxpags/2)): $total_paginas;
if ($actual>1)
$texto = "<a href=\"$enlace$anterior\">�</a> ";
else
$texto = "<b>�</b> ";
if ($minimo!=1) $texto.= "... ";
for ($i=$minimo; $i<$actual; $i++)
$texto .= "<a href=\"$enlace$i\">$i</a> ";
$texto .= "<b>$actual</b> ";
for ($i=$actual+1; $i<=$maximo; $i++)
$texto .= "<a href=\"$enlace$i\">$i</a> ";
if ($maximo!=$total_paginas) $texto.= "... ";
if ($actual<$total_paginas)
$texto .= "<a href=\"$enlace$posterior\">�</a>";
else
$texto .= "<b>�</b>";
return $texto;
}


if (!isset($pag)) $pag = 1; // Por defecto, pagina 1
$tampag = 15;
$reg1 = ($pag-1) * $tampag;


//invertir orden
if($orden=="ASC") $cambio="DESC";
if($orden=="DESC") $cambio="ASC";
	
    $fDesde=date("Ymd", strtotime($_GET[fDesde]));
	$fHasta=date("Y-m-d", strtotime($_GET[fHasta]));
	function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    $date_r = getdate(strtotime($date));
    $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
    return $date_result;
				}
	$fHasta=dateadd($fHasta,1,0,0,0,0,0); // + 1 dia

  $sql= "SELECT TOP 500  Tra_M_Tramite.iCodTramite,cDescTipoDoc,cCodificacion,fFecRegistro,cApellidosTrabajador,cNombresTrabajador,cReferencia,cAsunto,Tra_M_Tramite.cObservaciones,nFlgRpta,cRptaOK,nFlgClaseDoc,iCodOficinaSolicitado,Tra_M_Tramite.iCodRemitente,cNombre ";
  $sql.="FROM Tra_M_Tramite ";
  $sql.="LEFT OUTER JOIN Tra_M_Tipo_Documento ON Tra_M_Tramite.cCodTipoDoc=Tra_M_Tipo_Documento.cCodTipoDoc ";
  $sql.="LEFT OUTER JOIN Tra_M_Trabajadores ON Tra_M_Tramite.iCodTrabajadorRegistro=Tra_M_Trabajadores.iCodTrabajador  ";
  $sql.=" LEFT OUTER JOIN Tra_M_Remitente ON Tra_M_Tramite.iCodRemitente=Tra_M_Remitente.iCodRemitente    ";
  $sql.="WHERE Tra_M_Tramite.nFlgTipoDoc=3 ";
  $sql.=" AND (iCodOficina='$_SESSION[iCodOficinaLogin]' OR iCodOficinaSolicitado='$_SESSION[iCodOficinaLogin]') ";
  if($_GET[fDesde]!="" AND $_GET[fHasta]==""){
  	$sql.=" AND Tra_M_Tramite.fFecRegistro>'$fDesde' ";
  }
  if($_GET[fDesde]=="" AND $_GET[fHasta]!=""){
  	$sql.=" AND Tra_M_Tramite.fFecRegistro<='$fHasta' ";
  }
  if($_GET[fDesde]!="" && $_GET[fHasta]!=""){
  $sql.=" AND Tra_M_Tramite.fFecRegistro BETWEEN  '$fDesde' and '$fHasta' ";
  }
  	 if($_GET[RespuestaSI]==1 AND $_GET[RespuestaNO]==1 ){
    $sql.=" AND (nFlgRpta=1 OR nFlgRpta=0) ";
    }
    if($_GET[RespuestaSI]==0 AND $_GET[RespuestaNO]==1 ){
    $sql.=" AND nFlgRpta=0 ";
    }
    if($_GET[RespuestaSI]==1 AND $_GET[RespuestaNO]==0 ){
    $sql.=" AND nFlgRpta=1 ";
    }		   	   
	if($_GET[cCodificacion]!=""){
     $sql.="AND Tra_M_Tramite.cCodificacion LIKE '%$_GET[cCodificacion]%' ";
  }
	if($_GET[cNroDocumento]!=""){
     $sql.="AND Tra_M_Tramite.cNroDocumento LIKE '%$_GET[cNroDocumento]%' ";
  }
  if($_GET[cAsunto]!=""){
     $sql.="AND Tra_M_Tramite.cAsunto LIKE '%$_GET[cAsunto]%' ";
  }
  if($_GET[cObservaciones]!=""){
    $sql.="AND Tra_M_Tramite.cObservaciones LIKE '%$_GET[cObservaciones]%' ";
    }
	if($_GET[iCodTupa]!=""){
     $sql.="AND Tra_M_Tramite.iCodTupa='$_GET[iCodTupa]' ";
  }
	if($_GET[cCodTipoDoc]!=""){
        $sql.="AND Tra_M_Tramite.cCodTipoDoc='$_GET[cCodTipoDoc]' ";
  }
  if($_GET[cNombre]!=""){
   $sql.="AND Tra_M_Remitente.cNombre LIKE '%$_GET[cNombre]%' ";
  }
  if($_GET[RespuestaSI]==1 AND $_GET[Respuesta]==1){
     $sql.="AND cRptaOK is null ";
	 $sql.="AND nFlgRpta=1 ";
	 }
  if($_GET[RespuestaSI]==1 AND $_GET[Respuesta]==2){
    $sql.="AND cRptaOK!=''  ";
	 $sql.="AND nFlgRpta= 1 ";
	 }	 
  if($_GET[RegistroPersonal]==1 AND $_GET[RegistroSolicitado]==1){
   $sql.="AND (iCodOficina='$_SESSION[iCodOficinaLogin]' OR iCodOficinaSolicitado='$_SESSION[iCodOficinaLogin]')  ";
  }
  else if($_GET[RegistroPersonal]==1  AND $_GET[RegistroSolicitado]==0){
   $sql.="AND iCodOficina='$_SESSION[iCodOficinaLogin]' ";
  }
  else if($_GET[RegistroPersonal]==0  AND $_GET[RegistroSolicitado]==1){
   $sql.="AND iCodOficinaSolicitado='$_SESSION[iCodOficinaLogin]' ";
      }
   $sql.= " ORDER BY $campo $orden";	   
   $rs=mssql_query($sql,$cnx);
   $total = MsSQL_num_rows($rs);
  //echo $sql;
?>
<br>
<table width="1022" border="0" cellpadding="3" cellspacing="3" align="center">
<tr>
    <td width="64" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=Tra_M_Tramite.fFecRegistro&orden=<?=$cambio?>&cCodificacion=<?=$_GET[cCodificacion]?>&fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cAsunto=<?=$_GET[cAsunto]?>&RespuestaSI=<?=$_GET[RespuestaSI]?>&RespuestaNO=<?=$_GET[RespuestaNO]?>&cObservaciones=<?=$_GET[cObservaciones]?>&Respuesta=<?=$_GET[Respuesta]?>&cNombre=<?=$_GET[cNombre]?>&RegistroPersonal=<?=$_GET[RegistroPersonal]?>&RegistroSolicitado=<?=$_GET[RegistroSolicitado]?>" class="Estilo1" style=" text-decoration:<?if($campo=="Tra_M_Tramite.fFecRegistro"){ echo "underline"; }Else{ echo "none";}?>">Fecha</a></td>
	<td width="140" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=cDescTipoDoc&orden=<?=$cambio?>&cDescTipoDoc=<?=$_GET[cDescTipoDoc]?>&cCodificacion=<?=$_GET[cCodificacion]?>&fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cAsunto=<?=$_GET[cAsunto]?>&RespuestaSI=<?=$_GET[RespuestaSI]?>&RespuestaNO=<?=$_GET[RespuestaNO]?>&cObservaciones=<?=$_GET[cObservaciones]?>&Respuesta=<?=$_GET[Respuesta]?>&cNombre=<?=$_GET[cNombre]?>&RegistroPersonal=<?=$_GET[RegistroPersonal]?>&RegistroSolicitado=<?=$_GET[RegistroSolicitado]?>"  style=" text-decoration:<?if($campo=="cDescTipoDoc"){ echo "underline"; }Else{ echo "none";}?>">Tipo de Documento</a></td>
	<td width="250" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=Tra_M_Tramite.cAsunto&orden=<?=$cambio?>&cAsunto=<?=$_GET[cAsunto]?>&cCodificacion=<?=$_GET[cCodificacion]?>&fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&RespuestaSI=<?=$_GET[RespuestaSI]?>&RespuestaNO=<?=$_GET[RespuestaNO]?>&cObservaciones=<?=$_GET[cObservaciones]?>&Respuesta=<?=$_GET[Respuesta]?>&cNombre=<?=$_GET[cNombre]?>&RegistroPersonal=<?=$_GET[RegistroPersonal]?>&RegistroSolicitado=<?=$_GET[RegistroSolicitado]?>"  style=" text-decoration:<?if($campo=="Tra_M_Tramite.cAsunto"){ echo "underline"; }Else{ echo "none";}?>">Asunto</a></td>
	<td width="105" class="headCellColum">Observaciones</td>
    <td width="90" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=Tra_M_Tramite.cReferencia&orden=<?=$cambio?>&cReferencia=<?=$_GET[cReferencia]?>&cCodificacion=<?=$_GET[cCodificacion]?>&fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cAsunto=<?=$_GET[cAsunto]?>&RespuestaSI=<?=$_GET[RespuestaSI]?>&RespuestaNO=<?=$_GET[RespuestaNO]?>&cObservaciones=<?=$_GET[cObservaciones]?>&Respuesta=<?=$_GET[Respuesta]?>&cNombre=<?=$_GET[cNombre]?>&RegistroPersonal=<?=$_GET[RegistroPersonal]?>&RegistroSolicitado=<?=$_GET[RegistroSolicitado]?>" class="Estilo1" style=" text-decoration:<?if($campo=="Tra_M_Tramite.cReferencia"){ echo "underline"; }Else{ echo "none";}?>">Nro Referencia</a></td>
    <td width="60" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=cNombre&orden=<?=$cambio?>&cNombre=<?=$_GET[cNombre]?>&cCodificacion=<?=$_GET[cCodificacion]?>&fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cAsunto=<?=$_GET[cAsunto]?>&RespuestaSI=<?=$_GET[RespuestaSI]?>&RespuestaNO=<?=$_GET[RespuestaNO]?>&cObservaciones=<?=$_GET[cObservaciones]?>&Respuesta=<?=$_GET[Respuesta]?>&RegistroPersonal=<?=$_GET[RegistroPersonal]?>&RegistroSolicitado=<?=$_GET[RegistroSolicitado]?>" class="Estilo1" style=" text-decoration:<?if($campo=="cNombre"){ echo "underline"; }Else{ echo "none";}?>">Destino</a></td>
    <td width="123" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=Tra_M_Tramite.nFlgRpta&orden=<?=$cambio?>&cCodificacion=<?=$_GET[cCodificacion]?>&fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cAsunto=<?=$_GET[cAsunto]?>&RespuestaSI=<?=$_GET[RespuestaSI]?>&RespuestaNO=<?=$_GET[RespuestaNO]?>&cObservaciones=<?=$_GET[cObservaciones]?>&Respuesta=<?=$_GET[Respuesta]?>&cNombre=<?=$_GET[cNombre]?>&RegistroPersonal=<?=$_GET[RegistroPersonal]?>&RegistroSolicitado=<?=$_GET[RegistroSolicitado]?>"  style=" text-decoration:<?if($campo=="Tra_M_Tramite.nFlgRpta"){ echo "underline"; }Else{ echo "none";}?>">Requiere Respuesta</a></td>
  <td width="80" class="headCellColum">Opciones</td>
	</tr>
<?
if($_GET[cCodificacion]=="" && $_GET[fDesde]=="" && $_GET[fHasta]=="" && $_GET[cCodTipoDoc]=="" && $_GET[cAsunto]=="" && $_GET[RespuestaSI]=="" && $_GET[RespuestaNO]=="" && $_GET[Respuesta]=="" && $_GET[cNombre]=="" && $_GET[cObservaciones]=="" && $_GET[RegistroPersonal] && $_GET[RegistroSolicitado] ){
  $sqlsa=" SELECT Tra_M_Tramite.iCodTramite FROM Tra_M_Tramite  ";
  $sqlsa.="LEFT OUTER JOIN Tra_M_Trabajadores ON Tra_M_Tramite.iCodTrabajadorRegistro=Tra_M_Trabajadores.iCodTrabajador  ";
  $sqlsa.="WHERE Tra_M_Tramite.nFlgTipoDoc=3 ";
  $sqlsa.=" AND (iCodOficina='$_SESSION[iCodOficinaLogin]' OR iCodOficinaSolicitado='$_SESSION[iCodOficinaLogin]') ";
  if($_GET[RegistroPersonal]==1){
$sqlsa.="AND Tra_M_Trabajadores.iCodOficina='$_SESSION[iCodOficinaLogin]' ";
  }
  if($_GET[RegistroSolicitado]==1){
$sqlsa.="AND Tra_M_Tramite.iCodOficinaSolicitado='$_SESSION[iCodOficinaLogin]' ";
  }
 $rssa=mssql_query($sqlsa,$cnx);
$numrows=MsSQL_num_rows($rssa);
 }
else{
$numrows=MsSQL_num_rows($rs);	
}
if($numrows==0){ 
		echo "NO SE ENCONTRARON REGISTROS<br>";
		echo "TOTAL DE REGISTROS : ".$numrows;
}else{
        echo "TOTAL DE REGISTROS : ".$numrows;
for ($i=$reg1; $i<min($reg1+$tampag, $total); $i++) {
mssql_data_seek($rs, $i);
$Rs=MsSQL_fetch_array($rs);
//while ($Rs=MsSQL_fetch_array($rs)){
        		if ($color == "#DDEDFF"){
			  			$color = "#F9F9F9";
	    			}else{
			  			$color = "#DDEDFF";
	    			}
	    			if ($color == ""){
			  			$color = "#F9F9F9";
	    			}	
?>

 <tr bgcolor="<?=$color?>" onMouseOver="this.style.backgroundColor='#BFDEFF'" OnMouseOut="this.style.backgroundColor='<?=$color?>'" >
    <td valign="middle"  align="center">
    	<?
    	echo "<div style=color:#727272>".date("d-m-Y", strtotime($Rs[fFecRegistro]))."</div>";
    	echo "<div style=color:#727272;font-size:10px>".date("h:i A", strtotime($Rs[fFecRegistro]))."</div>";
	  	?>
	  </td> 
    <td valign="middle" align="left">
		<?
	 	echo $Rs[cDescTipoDoc];
    echo "<div>";
    if($Rs[nFlgClaseDoc]==1){
    	echo "<a style=\"color:#0067CE\" href=\"registroSalidaDetalles.php?iCodTramite=".$Rs[iCodTramite]."\" rel=\"lyteframe\" title=\"Detalle del TRÁMITE\" rev=\"width: 970px; height: 290px; scrolling: auto; border:no\">";
    }
    if($Rs[nFlgClaseDoc]==2){
    	echo "<a style=\"color:#0067CE\" href=\"registroEspecialDetalles.php?iCodTramite=".$Rs[iCodTramite]."\" rel=\"lyteframe\" title=\"Detalle del TRÁMITE\" rev=\"width: 970px; height: 290px; scrolling: auto; border:no\">";
    }
    echo $Rs[cCodificacion];
    echo "</a>";
    echo "</div>";
    ?>
     </td>
      
    <td valign="middle" align="left"><?=$Rs[cAsunto]?>   	</td>
    <td valign="middle" align="left"><?=$Rs[cObservaciones]?></td>
    <td valign="middle" align="left">
    	<?
    	$rsBusc=mssql_query("SELECT * FROM Tra_M_Tramite WHERE cCodificacion='$Rs[cReferencia]'",$cnx);
			if(MsSQL_num_rows($rsBusc)>0){
				$RsBusc=MsSQL_fetch_array($rsBusc);
				echo "<a href=\"registroDetalles.php?iCodTramite=".$RsBusc[iCodTramite]."\" rel=\"lyteframe\" title=\"Detalle del TRÁMITE\" rev=\"width: 970px; height: 550px; scrolling: auto; border:no\">";
			}
    		echo "<div style=text-transform:uppercase>".$Rs[cReferencia]."</div>";
    		echo "</a>"
    	?>
    </td>
     <td valign="middle" align="left">
    <?
	if($Rs[iCodRemitente]!=0){
	   echo  $Rs[cNombre];
	}
	else if($Rs[iCodRemitente]==0){
	   echo "MULTIPLE"; 
	}
	else if($Rs[iCodRemitente]==""){
	   echo "NO ASIGNADO"; 
	}
	?></td> 
    <td   align="center" valign="middle"> 
    <? 
	    if($Rs[nFlgRpta]==1 && $Rs[cRptaOK]!="" )
		   { echo "<div style='color:#0154AF'>SI / RECIBIDA</div>";
		     echo "<div>".$Rs[cRptaOK]."</div>"; 
		   } 
		 else if($Rs[nFlgRpta]==1 && $Rs[cRptaOK]=="" )
		   { echo "<div style='color:#950000'>SI / PENDIENTE</div>";
		     } 
		 else if($Rs[nFlgRpta]==0 && $Rs[cRptaOK]=="" )
		   { echo "NO / ------"; }  
    
	 ?>    </td>
    <td valign="middle">

    			<?
    			$sqlDw="SELECT TOP 1 * FROM Tra_M_Tramite_Digitales WHERE iCodTramite='$Rs[iCodTramite]'";
      		$rsDw=mssql_query($sqlDw,$cnx);
      		if(MsSQL_num_rows($rsDw)>0){
      			$RsDw=MsSQL_fetch_array($rsDw);
      			if($RsDw["cNombreNuevo"]!=""){
				 			if (file_exists("../cAlmacenArchivos/".trim($Rs1[nombre_archivo]))){
								echo "<a href=\"download.php?direccion=../cAlmacenArchivos/&file=".trim($RsDw["cNombreNuevo"])."\"><img src=images/icon_download.png border=0 width=16 height=16 alt=\"".trim($RsDw["cNombreNuevo"])."\"></a>";
							}
						}
      		}Else{
      			echo "<img src=images/space.gif width=16 height=16 border=0>";
      		}
      		if($Rs[nFlgClaseDoc]==1){
    				echo "<a href=\"registroSalidaEdit.php?iCodTramite=".$Rs[iCodTramite]."&URI=".$_SERVER['REQUEST_URI']."\"><img src=\"images/icon_edit.png\" width=\"16\" height=\"16\" border=\"0\"></a>";
    			}
    			if($Rs[nFlgClaseDoc]==2){
    				echo "<a href=\"registroEspecialEdit.php?iCodTramite=".$Rs[iCodTramite]."&URI=".$_SERVER['REQUEST_URI']."\"><img src=\"images/icon_edit.png\" width=\"16\" height=\"16\" border=\"0\"></a>";
    			}

		if($Rs[iCodRemitente]==0){
        echo "<a href='iu_doc_salidas_multiple.php?cod=$Rs[iCodTramite]'><img src='images/page_copy.png' width='16' height='16' border='0'></a>";
		}Else{
      	echo "<img src=images/space.gif width=16 height=16 border=0>";
    }
		 if($Rs[nFlgRpta]==1 ){
		 echo " <a style=\"color:#0067CE\" href=\"registroRespuestaOficina.php?iCodTramite=".$Rs[iCodTramite]."\" rel=\"lyteframe\" title=\"Detalle de la Respuesta\" rev=\"width: 410px; height: 280px; scrolling: no; border:no\"><img src='images/icon_avance.png' width='16' height='16' border='0'></a>";
    }
	?> 
		   </td>
</tr>
  
<?
}
}
?> 
</table>
  <? echo paginar($pag, $total, $tampag, "consultaSalidaOficina.php?cCodificacion=".$_GET[cCodificacion]."&fDesde=".$_GET[fDesde]."&fHasta=".$_GET[fHasta]."&cCodTipoDoc=".$_GET[cCodTipoDoc]."&cAsunto=".$_GET[cAsunto]."&RespuestaSI=".$_GET[RespuestaSI]."&RespuestaNO=".$_GET[RespuestaNO]."&cObservaciones=".$_GET[cObservaciones]."&Respuesta=".$_GET[Respuesta]."&cNombre=".$_GET[cNombre]."&RegistroPersonal=".$_GET[RegistroPersonal]."&RegistroSolicitado=".$_GET[RegistroSolicitado]."&pag=");?>


</tr>

<?include("includes/userinfo.php");?> <?include("includes/pie.php");?>


<map name="Map" id="Map"><area shape="rect" coords="1,4,19,15" href="#" /></map>
<map name="Map2" id="Map2"><area shape="rect" coords="0,5,15,13" href="#" /></map></body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>