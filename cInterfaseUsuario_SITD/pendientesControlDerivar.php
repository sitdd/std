<?php
date_default_timezone_set('America/Lima');
session_start();
if($_SESSION['CODIGO_TRABAJADOR']!=""){
	if (!isset($_SESSION["cCodSessionDrv"])){
		$fecSesDrv = date("Ymd-Gis");	
		$_SESSION['cCodSessionDrv']=$_SESSION['CODIGO_TRABAJADOR']."-".$_SESSION['iCodOficinaLogin']."-".$fecSesDrv;
	}
	if (!isset($_SESSION["cCodRef"])){ 
		$fecSesRef=date("Ymd-Gis");	
		$_SESSION['derDerivo']=$_SESSION['CODIGO_TRABAJADOR']."-".$_SESSION['iCodOficinaLogin']."-".$fecSesRef;
	}
	include_once("../conexion/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
    <link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="css/dhtmlgoodies_calendar.css" media="screen"/>
</head>
<body>
<?include("includes/menu.php");?>
<!--Main layout-->
<main class="mx-lg-5">
    <div class="container-fluid">
        <!--Grid row-->
        <div class="row wow fadeIn">
            <!--Grid column-->
            <div class="col-md-12 mb-12">
                <!--Card-->
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header text-center ">Derivar Documento : <?=$RsDoc[cCodificacion]?> </div>
                    <!--Card content-->
                    <div class="card-body">
                        <?
                        $sqlDoc=" SELECT Tra_M_Tramite.cCodificacion,Tra_M_Tramite_Movimientos.cFlgTipoMovimiento FROM Tra_M_Tramite,Tra_M_Tramite_Movimientos WHERE Tra_M_Tramite.iCodTramite=Tra_M_Tramite_Movimientos.iCodTramite AND iCodMovimiento='$_GET[iCodMovimientoAccion]'";
                        $rsDoc=mssql_query($sqlDoc,$cnx);
                        $RsDoc=MsSQL_fetch_array($rsDoc);
                        ?>
                        <form name="frmConsulta" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="opcion" value="">
                                <input type="hidden" name="dev" value="1">
                                <input type="hidden" name="iCodMovimiento" value="<?=$_GET[iCodMovimientoAccion]?>">
                                <input type="hidden" name="nFlgCopias" value="<?if($_POST[nFlgCopias]==1) echo "1"?>">
                                <input type="hidden" name="cFlgTipoMovimientoOrigen" value="<?=$RsDoc[cFlgTipoMovimiento]?>">
                                <?
                                    for ($h=0;$h<count($_POST[MovimientoAccion]);$h++){
                                    $MovimientoAccion=$_POST[MovimientoAccion];
                                    $est= "disabled";
                                    ?>
                                    <input type="hidden" name="MovimientoAccion[]" value="<?=$MovimientoAccion[$h]?>" size="65" class="FormPropertReg form-control">
                                    <?
                                    }
                                    if($_POST[iCodMovimientoAccion]!=""){
                                    $est= "";
                                    ?>
                                <input type="hidden" name="iCodMovimientoAccion" value="<?=$_POST[iCodMovimientoAccion]?>" size="65"
                                            class="FormPropertReg form-control">
                                <? } ?>
                            <td width="120" >Derivar a:
                                <select name="iCodOficinaDerivar"  class="FormPropertReg mdb-select colorful-select dropdown-primary"
                                                    searchable="Buscar aqui.." autofocus onChange="releer();">
                                                <option value="">Seleccione:</option>
                                                <?php
                                                    $sqlOfVirtual = "SELECT iCodOficina FROM Tra_M_Oficinas /* WHERE cNomOficina  LIKE '%VIRTUAL%' */";
                                                    $rsOfVirtual  = mssql_query($sqlOfVirtual,$cnx);
                                                    $RsOfVirtual  = mssql_fetch_array($rsOfVirtual);
                                                    $iCodOficinaVirtual = $RsOfVirtual[iCodOficina];

                                                    $sqlDep2 = "SELECT * FROM Tra_M_Oficinas 
                                                                            WHERE iFlgEstado != 0 
                                                                                        AND iCodOficina != '$_SESSION[iCodOficinaLogin]' 
                                                                                        AND iCodOficina != $iCodOficinaVirtual
                                                                            ORDER BY cNomOficina ASC";
                                    $rsDep2  = mssql_query($sqlDep2,$cnx);
                                    while ($RsDep2 = mssql_fetch_array($rsDep2)){
                                        if($RsDep2[iCodOficina] == $_POST[iCodOficinaDerivar]){
                                            $selecOfi = "selected";
                                        }else{
                                            $selecOfi = "";
                                        }
                                  echo utf8_encode("<option value=".$RsDep2["iCodOficina"]." ".$selecOfi.">".trim($RsDep2["cNomOficina"])." | ".trim($RsDep2["cSiglaOficina"])."</option>");
                                    }
                                    mssql_free_result($rsDep2);
                                                ?>
                                            </select>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td width="120" >
                                            Responsable:
                                            <select name="iCodTrabajadorDerivar" class="FormPropertReg mdb-select colorful-select dropdown-primary"
                                                    searchable="Buscar aqui..">
                                                <?php if($_POST[iCodOficinaDerivar]==""){?>
                                                <option value="" >Seleccione Trabajador:</option>
                                                <?}?>
                                                <?php
                                                    // $sqlTrb = "SELECT * FROM Tra_M_Trabajadores
                                                    // 					 WHERE iCodOficina='$_POST[iCodOficinaDerivar]' AND nFlgEstado = 1
                                                    // 					 ORDER BY iCodCategoria DESC, cNombresTrabajador ASC";
                                                    $sqlTrb = "SELECT * FROM Tra_M_Perfil_Ususario TPU
                                                                         INNER JOIN Tra_M_Trabajadores TT ON TPU.iCodTrabajador = TT.iCodTrabajador
                                                                         WHERE TPU.iCodPerfil = 3 AND TPU.iCodOficina = '$_POST[iCodOficinaDerivar]'";
                                        $rsTrb  = mssql_query($sqlTrb,$cnx);
                                    while ($RsTrb = mssql_fetch_array($rsTrb)){
                                        if($RsTrb['iCodTrabajador']==$_POST['iCodTrabajadorDerivar']){
                                            $selecTrab="selected";
                                        }else{
                                            $selecTrab="";
                                        }
                                        if ($RsTrb["encargado"] == 1){
                                            $encar = "(Encargado)";
                                        }else{
                                            $encar = "";
                                        }
                                      echo utf8_encode("<option value=\"".$RsTrb["iCodTrabajador"]."\" ".$selecTrab.">".$RsTrb["cNombresTrabajador"]." ".$RsTrb["cApellidosTrabajador"]." ".$encar."</option>");
                                    }
                                         mssql_free_result($rsTrb);
                                                ?>
                                            </select>
                                        </td>
                                        </tr>

                                        <tr>
                                        <td width="120" >Indicación:</td>
                                        <td align="left" class="CellFormRegOnly">
                                                <select name="iCodIndicacionDerivar" class="FormPropertReg mdb-select colorful-select dropdown-primary"
                                                        searchable="Buscar aqui..">
                                                <option value="">Seleccione Indicación:</option>
                                                <?
                                                $sqlIndic="SELECT * FROM Tra_M_Indicaciones ";
                                $sqlIndic .= "ORDER BY cIndicacion ASC";
                                $rsIndic=mssql_query($sqlIndic,$cnx);
                                while ($RsIndic=MsSQL_fetch_array($rsIndic)){
                                    if($RsIndic[iCodIndicacion]==$_POST[iCodIndicacionDerivar]){
                                        $selecIndi="selected";
                                    }Else{
                                        $selecIndi="";
                                    }
                                  echo utf8_encode("<option value=".$RsIndic["iCodIndicacion"]." ".$selecIndi.">".$RsIndic["cIndicacion"]."</option>");
                                }
                                mssql_free_result($rsIndic);
                                                ?>
                                                </select>
                                        </td>
                                        </tr>

                                        <tr>
                                            <td >Prioridad:</td>
                                            <td class="CellFormRegOnly">
                                                <select name="cPrioridad" id="cPrioridad" class="size9 FormPropertReg mdb-select colorful-select dropdown-primary"
                                                        searchable="Buscar aqui..">
                                                    <option <?if($_POST[cPrioridad]=="Alta") echo "selected"?> value="Alta">Alta</option>
                                                    <option <?if($_POST[cPrioridad]=="Media") echo "selected"?> value="Media" selected>Media</option>
                                                    <option <?if($_POST[cPrioridad]=="Baja") echo "selected"?> value="Baja">Baja</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                    </fieldset>
                                     <fieldset>
                  <legend class="LnkZonas"></legend>

                                    <table>
                                    <tr>

                                        <td width="120" >Tipo de Documento:</td>
                                        <td align="left" class="CellFormRegOnly">
                                            <select name="cCodTipoDoc" class="FormPropertReg mdb-select colorful-select dropdown-primary"
                                                    searchable="Buscar aqui.." <?=$est?>  >
                                                <span style="color:#F00; size:14pt">Cambiar, para adjuntar nuevo documento </span>

                                                <?php
                                                    include_once("../conexion/conexion.php");
                                                    $sqlTipo = "SELECT * FROM Tra_M_Tipo_Documento WHERE nFlgInterno=1 ORDER BY cDescTipoDoc ASC ";
                                        $rsTipo  = mssql_query($sqlTipo,$cnx);
                                        while ($RsTipo = mssql_fetch_array($rsTipo)){
                                            //if($RsTipo["cCodTipoDoc"] == $_POST[cCodTipoDoc] OR $RsTipo["cCodTipoDoc"] == 45){
                                            if($RsTipo["cCodTipoDoc"] == $_POST[cCodTipoDoc]){
                                            // if($RsTipo["cCodTipoDoc"] == $_POST[cCodTipoDoc] OR $RsTipo["cCodTipoDoc"] == 81){
                                                $selecTipo = "selected";
                                            }else{
                                                $selecTipo = "";
                                            }
                                        echo utf8_encode("<option value=".$RsTipo["cCodTipoDoc"]." ".$selecTipo.">".$RsTipo["cDescTipoDoc"]."</option>");
                                    }
                                    mssql_free_result($rsTipo);
                                                ?>
                                                </select>
                                        </td>
                                      </tr>


                                        <tr>
                                        <td width="15%"  valign="top">Asunto:</td>
                                        <td align="left" width="30%">
                                            <textarea name="cAsuntoDerivar" style="width:100%;height:55px" class="FormPropertReg form-control"><?=$_POST[cAsuntoDerivar]?></textarea>
                                        </td>
                                        <td width="15%"  valign="top">Observaciones:</td>
                                        <td align="left" width="40%">
                                            <textarea name="cObservacionesDerivar" style="width:100%;height:55px" class="FormPropertReg form-control"><?=$_POST[cObservacionesDerivar]?></textarea></td>
                                        </tr>

                                       </table>
             </fieldset>
                <?
                $y=0;
                if($_POST[iCodMovimientoAccion]!=""){
                $rsCop=mssql_query("SELECT icodtramite,iCodOficinaDerivar FROM Tra_M_Tramite_Movimientos WHERE iCodMovimiento=".$_POST[iCodMovimientoAccion],$cnx);
                $RsCop=mssql_fetch_array($rsCop);
                $rsTip=mssql_query("SELECT nFlgTipoDoc FROM Tra_M_Tramite WHERE icodtramite=".$RsCop[icodtramite],$cnx);
                $RsTip=mssql_fetch_array($rsTip);
                if($RsTip[nFlgTipoDoc]!=2){
                        $y=1;
                        }
                }else {
                    $x=0;
                    $y=0;
                    for ($h=0;$h<count($_POST[MovimientoAccion]);$h++){
                    $MovimientoAccion=$_POST[MovimientoAccion];
                    $rsCop=mssql_query("SELECT icodtramite,iCodOficinaDerivar FROM Tra_M_Tramite_Movimientos WHERE iCodMovimiento=".$MovimientoAccion[$h],$cnx);    	$RsCop=mssql_fetch_array($rsCop);
                    $rsTip=mssql_query("SELECT nFlgTipoDoc FROM Tra_M_Tramite WHERE icodtramite=".$RsCop[icodtramite],$cnx);
                    $RsTip=mssql_fetch_array($rsTip);
                        if($RsTip[nFlgTipoDoc]!=2){
                        $x++;
                        }
                    }
                    $y=$x;
                }
                ?>
                  <fieldset>
                  <legend class="LnkZonas">
                  <? if($y==0){ echo "Otros Destinos:"; } else { echo "Copias a Otros Destinos:";}?>
                  </legend>
                          <table align="left">
                        <tr>
                        <td  valign="top"></td>
                        <td align="left">

                            <table border=0><tr>
                      <? if($_POST[nFlgCopias]==1){
                        $compactada=serialize($_POST[MovimientoAccion]);
                        $compactada=urlencode($compactada);
                        ?>
                      <td align="center">
                        <div  >
                            <a class="btn btn-primary"
                                 href="PendientesCopiasOficinasLs.php?iCodMovimientoAccion=<?=$_POST[iCodMovimientoAccion]?>&MovimientoAccion=<?=$compactada?>&cCodTipoDoc=<?=$_POST[cCodTipoDoc]?>&iCodOficinaDerivar=<?=$_POST[iCodOficinaDerivar]?>&iCodTrabajadorDerivar=<?=$_POST[iCodTrabajadorDerivar]?>&cAsuntoDerivar=<?=$_POST[cAsuntoDerivar]?>&cObservacionesDerivar=<?=$_POST[cObservacionesDerivar]?>&iCodIndicacionDerivar=<?=$_POST[iCodIndicacionDerivar]?>&nFlgCopias=<?=$_POST[nFlgCopias]?>"
                                 rel="lyteframe" title="Lista de Oficinas" rev="width: 500px; height: 550px; scrolling: auto; border:no">Seleccione Oficinas
                            </a>
                        </div>
                      </td>
                      <?}?>
                      <?if($_POST[nFlgCopias]==""){?>
                      <td valign="top">
                          <div class="form-check">
                              <input type="radio" class="form-check-input" id="ad" name="radioMultiple" onclick="activaCopias();">
                              <label class="form-check-label" for="ad">Activar</label>
                          </div>
                      </td>
                        <?}?>
                      </tr></table>

                                <?php
                                    // selec de copias temporales
                                    $sqlMovs = "SELECT * FROM Tra_M_Tramite_Temporal 
                                                            WHERE cCodSession='$_SESSION[cCodSessionDrv]' 
                                                            ORDER BY iCodTemp ASC";
                          $rsMovs  = mssql_query($sqlMovs,$cnx);
                                    if(mssql_num_rows($rsMovs)>0){
                                ?>
                                <table border=1 width="100%">
                                    <tr>
                            <td class="headColumnas" width="25">De</td>
                                        <td class="headColumnas" width="375">Oficina</td>
                          <td class="headColumnas" width="375">Responsable</td>
                                        <td class="headColumnas" width="175">Indicacion</td>
                                        <td class="headColumnas" width="60">Prioridad</td>
                            <? if($y==0){?>
                                <td class="headColumnas" width="60">Copia</td>
                          <? } ?>
                                        <td class="headColumnas">X</td>
                                    </tr>


                                        <?
                            while ($RsMovs = mssql_fetch_array($rsMovs)){
                                        ?>
                                        <tr>
                                    <td align="center" valign="top">
                                    <?
                                    $sqlOfO="SELECT cNomOficina,cSiglaOficina FROM Tra_M_Oficinas WHERE iCodOficina='$RsCop[iCodOficinaDerivar]'";
                                $rsOfO=mssql_query($sqlOfO,$cnx);
                                $RsOfO=MsSQL_fetch_array($rsOfO);
                                    echo "<a style=text-decoration:none href=javascript:; title=\"".trim($RsOfO[cNomOficina])."\">".trim($RsOfO[cSiglaOficina])."</a>";
                                    ?>
                                    </td>
                                        <td align="left">
                                        <?
                                        $sqlOfc="SELECT * FROM Tra_M_Oficinas WHERE iCodOficina='$RsMovs[iCodOficina]'";
                            $rsOfc=mssql_query($sqlOfc,$cnx);
                            $RsOfc=MsSQL_fetch_array($rsOfc);
                            echo utf8_encode($RsOfc["cNomOficina"]);
                                        ?>
                                        </td>
                          <td align="left">
                                        <?
                                            // $sqlTrab = "SELECT * FROM Tra_M_Trabajadores
                                            // 					  WHERE iCodTrabajador='$RsMovs[iCodTrabajador]' AND iCodCategoria=5";
                                          $sqlTrab = "SELECT * FROM Tra_M_Trabajadores 
                                                                WHERE iCodTrabajador='$RsMovs[iCodTrabajador]'";
                                $rsTrab = mssql_query($sqlTrab,$cnx);
                                $RsTrab = mssql_fetch_array($rsTrab);
                                echo $RsTrab["cNombresTrabajador"]." ".$RsTrab["cApellidosTrabajador"];
                                        ?>
                            <!-- <a style=" text-decoration:none" href="PendientesCopiasRespEdit.php?cod=<?=$RsMovs[iCodTemp]?>&ofi=<?=$RsMovs[iCodOficina]?>&trab=<?=$RsMovs[iCodTrabajador]?>&iCodMovimientoAccion=<?=$_POST[iCodMovimientoAccion]?>&MovimientoAccion=<?=$compactada?>&cCodTipoDoc=<?=$_POST[cCodTipoDoc]?>&iCodOficinaDerivar=<?=$_POST[iCodOficinaDerivar]?>&iCodTrabajadorDerivar=<?=$_POST[iCodTrabajadorDerivar]?>&cAsuntoDerivar=<?=$_POST[cAsuntoDerivar]?>&cObservacionesDerivar=<?=$_POST[cObservacionesDerivar]?>&iCodIndicacionDerivar=<?=$_POST[iCodIndicacionDerivar]?>&nFlgCopias=<?=$_POST[nFlgCopias]?>" rel="lyteframe" title="Editar Responsable" rev="width: 500px; height: 550px; scrolling: auto; border:no"><img src="images/icon_edit.png" width="16" height="16" alt="Editar Responsable" border="0"></a> -->
                                        </td>
                                        <td align="left">
                                            <?
                                            $sqlInd="SELECT * FROM Tra_M_Indicaciones WHERE iCodIndicacion='$RsMovs[iCodIndicacion]'";
                              $rsInd=mssql_query($sqlInd,$cnx);
                              $RsInd=MsSQL_fetch_array($rsInd);
                              echo $RsInd["cIndicacion"];
                                            ?>
                                        </td>
                                        <td align="left">
                                            <?=$RsMovs[cPrioridad]?>
                                        </td>
                                            <? if($y==0){?>
                                        <td align="left">
                                            <div class="form-check">
                                                <input type="checkbox" name="Copia[]" id="ma2" value="<?=$RsMovs[iCodTemp]?>" class="form-check-input"
                                                       onclick="activaOpciones1();" >
                                                <label class="form-check-label" for="ma2"></label>
                                            </div>
                                        </td>
                                          <? }?>
                                        <td align="center">
                                            <a class="btn btn-primary"
                                                    href="registroData.php?iCodTemp=<?=$RsMovs[iCodTemp]?>&opcion=24&iCodMovimientoAccion=<?=$_POST[iCodMovimientoAccion]?>&MovimientoAccion=<?=$compactada?>&cCodTipoDoc=<?=$_POST[cCodTipoDoc]?>&iCodOficinaDerivar=<?=$_POST[iCodOficinaDerivar]?>&iCodTrabajadorDerivar=<?=$_POST[iCodTrabajadorDerivar]?>&cAsuntoDerivar=<?=$_POST[cAsuntoDerivar]?>&cObservacionesDerivar=<?=$_POST[cObservacionesDerivar]?>&iCodIndicacionDerivar=<?=$_POST[iCodIndicacionDerivar]?>&nFlgCopias=<?=$_POST[nFlgCopias]?>">
                                                <img src="images/icon_del.png" border="0" width="16" height="16">
                                                tertertrerter
                                            </a>
                                        </td>
                                        </tr>
                                        <?}?>
                                        </table>
                                <?}?>
                        </td>
                        </tr>
                    </table>
                   </fieldset>
                   <table>
                                        <tr>
                                        <td colspan="2" align="right">
                                        <button class="btn btn-primary" onclick="Derivar();" onMouseOver="this.style.cursor='hand'"> <b>Registrar</b> <img src="images/icon_
                                        .png" width="17" height="17" border="0"> </button>&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;
                                        <button class="btn btn-primary" onclick="Volver();" onMouseOver="this.style.cursor='hand'"> <b>Cancelar</b> <img src="images/icon_retornar.png" width="17" height="17" border="0"> </button>
                                        </td>
                                        </tr>
                                        </form>



                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php include("includes/userinfo.php"); ?>
<?include("includes/pie.php");?>
    <script type="text/javascript" language="javascript" src="includes/lytebox.js"></script>
    <script type="text/javascript" src="scripts/dhtmlgoodies_calendar.js"></script>
    <script src="ckeditor/ckeditor.js"></script>
    <script Language="JavaScript">
        $(document).ready(function() {
            $('.mdb-select').material_select();

        });

            function activaCopias(){
                document.frmConsulta.nFlgCopias.value="1";
                document.frmConsulta.action="<?=$_SERVER['PHP_SELF']?>?clear=1#area";
                document.frmConsulta.submit();
                return false;
            }

        var miPopup
        function Buscar(){
            miPopup=window.open('registroBuscarDoc.php','popuppage','width=745,height=360,toolbar=0,status=yes,resizable=0,scrollbars=yes,top=100,left=100');
        }

        function AddReferencia(){
            document.frmConsulta.opcion.value=29;
            document.frmConsulta.action="registroData.php";
            document.frmConsulta.submit();
        }

        function releer(){
            document.frmConsulta.action="<?=$_SERVER['PHP_SELF']?>?clear=1#area";
            document.frmConsulta.submit();
        }

        function Derivar()
        {
            if (document.frmConsulta.cCodTipoDoc.value.length == "")
            {
                alert("Seleccione Tipo Documento");
                document.frmConsulta.cCodTipoDoc.focus();
                return (false);
            }
            if (document.frmConsulta.iCodOficinaDerivar.value.length == "")
            {
                alert("Seleccione Derivar a:");
                document.frmConsulta.iCodOficinaDerivar.focus();
                return (false);
            }
            if (document.frmConsulta.iCodTrabajadorDerivar.value.length == "")
            {
                alert("Seleccione Responsable");
                document.frmConsulta.iCodTrabajadorDerivar.focus();
                return (false);
            }
            if (document.frmConsulta.iCodIndicacionDerivar.value.length == "")
            {
                alert("Seleccione Indicación");
                document.frmConsulta.iCodIndicacionDerivar.focus();
                return (false);
            }

            document.frmConsulta.action="pendientesData.php";
            document.frmConsulta.opcion.value=2;
            document.frmConsulta.submit();
        }
        function Volver(){
            document.frmConsulta.action="registroData.php";
            document.frmConsulta.opcion.value=27;
            document.frmConsulta.submit();
        }
    </script>

<script>
    CKEDITOR.replace( 'descripcion' );
</script>

</body>
</html>

<?
}else{
   header("Location: ../index.php?alter=5");
}
?>