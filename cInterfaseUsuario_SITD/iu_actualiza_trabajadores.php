<?php
session_start();
if($_SESSION['CODIGO_TRABAJADOR']!=""){
  include_once("../conexion/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />

</head>
<body>
<?include("includes/menu.php");?>

<style>
    .form-control{
        font-size: 0.8rem!important;
    }
    .md-form{
        margin-top: 1rem!important;
        margin-bottom: 0.8rem!important;
    }
    .dropdown-content li>a, .dropdown-content li>span {
        font-size: 0.8rem!important;
    }
    .select-wrapper .search-wrap {
        padding-top: 0rem!important;
        margin: 0 0.2rem!important;
    }
    .select-wrapper input.select-dropdown {
        font-size: 0.9rem!important;
    }
    label.select{
        margin-bottom: 0!important;
        font-size: 0.8rem!important;
    }
    .subir{
        padding: 5px 10px;
        background: #f55d3e;
        color:#fff;
        border:0px solid #fff;
    }

    .subir:hover{
        color:#fff;
        background: #f7cb15;
    }
</style>

<!--Main layout-->
<main class="mx-lg-5">
    <div class="container-fluid">
        <!--Grid row-->
        <div class="row wow fadeIn">
            <!--Grid column-->
            <div class="col-md-12">
                <!--Card-->
                <div class="card mb-5">
                    <!-- Card header -->
                    <div class="card-header text-center ">EDITAR DATOS DEL TRABAJADOR</div>
                    <!--Card content-->
                    <div class="card-body">
                            <?php
                            $sql= "select * from Tra_M_Trabajadores where iCodTrabajador='$_GET[cod]'";
                            $rs=mssql_query($sql,$cnx);
                            $Rs=MsSQL_fetch_array($rs);
                            $img=base64_encode($Rs['firma']);
                            ?>
                        <form action="../cLogicaNegocio_SITD/ln_actualiza_trabajador.php" method="post"  name="form1" enctype="multipart/form-data">
                            <input type="hidden" name="iCodTrabajador" value="<?=$Rs[iCodTrabajador];?>">
                            <input type="hidden" name="cNombreTrabajadorx"    value="<?=$cNombreTrabajador?>">
                            <input type="hidden" name="cApellidosTrabajadorx"   value="<?=$cApellidosTrabajador?>">
                            <input type="hidden" name="cTipoDocIdentidadx"    value="<?=$cTipoDocIdentidad?>">
                            <input type="hidden" name="cNumDocIdentidadx"     value="<?=$cNumDocIdentidad?>">
                            <input type="hidden" name="iCodOficinax"      value="<?=$iCodOficina?>">
                            <input type="hidden" name="iCodPerfilx"       value="<?=$iCodPerfil?>">
                            <input type="hidden" name="iCodCategoriax"      value="<?=$iCodCategoria?>">
                            <input type="hidden" name="txtestadox"        value="<?=$txtestado?>">
                            <input type="hidden" name="pagx"          value="<?=$pag?>">
                            <?php
                            $cNombreTrabajador=$_GET[cNombreTrabajador];$cApellidosTrabajador=$_GET[cApellidosTrabajador];$cTipoDocIdentidad=$_GET[cTipoDocIdentidad];
                            $cNumDocIdentidad=$_GET[cNumDocIdentidad];$iCodOficina=$_GET[iCodOficina];$iCodPerfil=$_GET[iCodPerfil];
                            $iCodCategoria=$_GET[iCodCategoria];$txtestado=$_GET[txtestado];$pag=$_GET[pag];
                             ?>
                            <div class="row justify-content-center">
                                <div class="col-12 col-md-7 col-lg-7 col-xl-7">
                                    <div class="card mb-3">
                                        <div class="card-header">Datos Personales</div>
                                        <div class="card-body px-4">
                                            <div class="row justify-content-around">
                                                <div class="col-12 mb-3">
                                                    <?php
                                                    if($Rs[encargado]==1){
                                                        $act="checked";
                                                    }else{
                                                        $act="";
                                                    }
                                                    ?>
                                                    <div class="form-check pl-0">
                                                        <input type="checkbox" name="jf_encargado"  class="form-check-input" id="encargado" <?php echo $act;?>>
                                                        <label class="form-check-label" for="encargado">Jefe Encargado</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="md-form">
                                                        <input name="cNombresTrabajador" type="text" id="cNombresTrabajador" class="FormPropertReg form-control" value="<? echo trim($Rs[cNombresTrabajador]); ?>">
                                                        <label for="cNombresTrabajador">Nombres</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="md-form">
                                                        <input name="cApellidosTrabajador" type="text" id="cApellidosTrabajador" class="FormPropertReg form-control" value="<? echo trim($Rs[cApellidosTrabajador]); ?>">
                                                        <label for="cApellidosTrabajador">Apellidos</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <label class="select">Documento:&nbsp;</label>
                                                    <?php
                                                    $sqlDoc="select * from Tra_M_Doc_Identidad ";
                                                    $rsDoc=mssql_query($sqlDoc,$cnx);
                                                    ?>
                                                    <select name="cTipoDocIdentidad" class="FormPropertReg mdb-select colorful-select dropdown-primary"   searchable="Buscar aqui.."  id="cTipoDocIdentidad">
                                                        <option value="">Seleccione:</option>
                                                        <?php
                                                        while ($RsDoc=MsSQL_fetch_array($rsDoc)){
                                                            if($RsDoc["cTipoDocIdentidad"]==$Rs[cTipoDocIdentidad]){
                                                                $selecClas="selected";
                                                            }Else{
                                                                $selecClas="";
                                                            }
                                                            echo "<option value=".$RsDoc["cTipoDocIdentidad"]." ".$selecClas.">".$RsDoc["cDescDocIdentidad"]."</option>";
                                                        }
                                                        mssql_free_result($rsDoc);
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-12 col-sm-6" style="margin-top: 1.3rem!important">
                                                    <div class="md-form">
                                                        <input name="cNumDocIdentidad" type="text" id="cNumDocIdentidad" class="FormPropertReg form-control" value="<? echo trim($Rs[cNumDocIdentidad]); ?>" onkeypress="if (event.keyCode > 31 && ( event.keyCode < 48 || event.keyCode > 57)) event.returnValue = false;">
                                                        <label for="cNumDocIdentidad">N° Doc.</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 ">
                                                    <div class="md-form">
                                                        <input name="cDireccionTrabajador" type="text" id="cDireccionTrabajador" class="FormPropertReg form-control" value="<? echo trim($Rs[cDireccionTrabajador]); ?>">
                                                        <label for="cDireccionTrabajador">Direccion</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-lg-5">
                                                    <div class="md-form">
                                                        <input name="cMailTrabajador" type="text" id="cMailTrabajador" class="FormPropertReg form-control" value="<? echo trim($Rs[cMailTrabajador]); ?>">
                                                        <label for="cMailTrabajador">E-mail</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-lg-3">
                                                    <div class="md-form">
                                                        <input name="cTlfTrabajador1" type="text" id="cTlfTrabajador1" class="FormPropertReg form-control" value="<? echo trim($Rs[cTlfTrabajador1]); ?>">
                                                        <label for="cTlfTrabajador1">Telefono 1</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-lg-3">
                                                    <div class="md-form">
                                                        <input name="cTlfTrabajador2" type="text" id="cTlfTrabajador2" class="FormPropertReg form-control" value="<? echo trim($Rs[cTlfTrabajador2]); ?>">
                                                        <label for="cTlfTrabajador2">Telefono 2</label>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <label class="select">Oficina:&nbsp;</label>
                                                    <?php
                                                    //Consulta para rellenar el combo Oficina
                                                    $sqlOfi="SP_OFICINA_LISTA_COMBO ";
                                                    $rsOfi=mssql_query($sqlOfi,$cnx);
                                                    ?>
                                                    <select name="iCodOficina" class="FormPropertReg mdb-select colorful-select dropdown-primary"   searchable="Buscar aqui.."  id="iCodOficina">
                                                        <option value="">Seleccione:</option>
                                                        <?php
                                                        while ($RsOfi=MsSQL_fetch_array($rsOfi)){
                                                            if($RsOfi["iCodOficina"]==$Rs[iCodOficina]){
                                                                $selecClas="selected";
                                                            }Else{
                                                                $selecClas="";
                                                            }
                                                            echo utf8_encode("<option value=".$RsOfi["iCodOficina"]." ".$selecClas.">".$RsOfi["cNomOficina"]."</option>");
                                                        }
                                                        mssql_free_result($rsOfi);
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <label class="select">Categoria:&nbsp;</label>
                                                    <?php
                                                    //Consulta para rellenar el combo Categoria
                                                    $sqlCat="select * from Tra_M_Categoria ";
                                                    $rsCat=mssql_query($sqlCat,$cnx);
                                                    ?>
                                                    <select name="iCodCategoria" class="FormPropertReg mdb-select colorful-select dropdown-primary"   searchable="Buscar aqui.."  id="iCodCategoria">
                                                        <option value="">Seleccione:</option>
                                                        <?php
                                                        while ($RsCat=MsSQL_fetch_array($rsCat)){
                                                            if($RsCat["iCodCategoria"]==$Rs[iCodCategoria] || $RsCat["iCodCategoria"]=="Sin Categoria" ){
                                                                $selecClas="selected";
                                                            }Else{
                                                                $selecClas="";
                                                            }
                                                            echo "<option value=".$RsCat["iCodCategoria"]." ".$selecClas.">".$RsCat["cDesCategoria"]."</option>";
                                                        }
                                                        mssql_free_result($rsCat);
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <label class="select">Estado:&nbsp;</label>
                                                    <select name="txtestado" class="FormPropertReg mdb-select colorful-select dropdown-primary"   searchable="Buscar aqui.."  id="txtestado">
                                                        <option value="">Seleccione:</option>
                                                        <?
                                                        if ($Rs[nFlgEstado]==1){
                                                            echo "<OPTION value=1 selected>Activo</OPTION> ";
                                                        }
                                                        else{
                                                            echo "<OPTION value=1 selected>Activo</OPTION> ";
                                                        }
                                                        if ($Rs[nFlgEstado]==0){
                                                            echo "<OPTION value=0>Inactivo</OPTION> ";
                                                        }
                                                        else{
                                                            echo "<OPTION value=0>Inactivo</OPTION> ";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 col-md-5 col-lg-5 col-xl-5">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card mb-3">
                                                <div class="card-header">Usuario</div>
                                                <div class="card-body">
                                                    <div class="row ml-5">
                                                        <div class="col-8 col-sm-6 col-xl-4">
                                                            <div class="md-form">
                                                                <input name="cUsuario" type="text" id="cUsuario" class="FormPropertReg form-control" value="<? echo trim($Rs[cUsuario]); ?>">
                                                                <label for="cUsuario">Usuario</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="card mb-3">
                                                <div class="card-body pl-5 pl-md-2 pl-xl-5">
                                                    <div class="row justify-content-center">
                                                        <a title="Agregar" class='botpelota ml-auto mr-4' href='iu_nuevo_nivel.php?op=1&id=<? echo $_GET[cod];?>' style="margin-bottom: 1rem"><i class="fas fa-plus"></i></a>
                                                        <?php
                                                        $sqlTrabajadorPerfiles = "SELECT * FROM Tra_M_Perfil_Ususario where iCodTrabajador='".$_GET['cod']."'";
                                                        $rs  = mssql_query($sqlTrabajadorPerfiles,$cnx);
                                                        ?>
                                                        <div class="col-12">
                                                            <table class="table-sm table-responsive table-hover">
                                                                <thead class="text-center" style="border-bottom: solid 1px rgba(0,0,0,0.47)">
                                                                <tr>
                                                                    <th>Perfil</th>
                                                                    <th>Oficina</th>
                                                                    <th>Opciones</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                $numrows = mssql_num_rows($rs);
                                                                if ($numrows==0){
                                                                    echo "NO SE ENCONTRARON REGISTROS<br>";
                                                                    echo "TOTAL DE REGISTROS : ".$numrows;
                                                                }else {
                                                                    while ($Rs = mssql_fetch_array($rs)) {
                                                                        ?>
                                                                        <tr>
                                                                            <td>
                                                                                <?php
                                                                                $sqlPerfil = "SELECT cDescPerfil FROM Tra_M_Perfil WHERE iCodPerfil = " . $Rs['iCodPerfil'];
                                                                                $rsPerfil = mssql_query($sqlPerfil, $cnx);
                                                                                $RsPerfil = mssql_fetch_object($rsPerfil);
                                                                                echo $RsPerfil->cDescPerfil;
                                                                                ?>
                                                                            </td>
                                                                            <td>
                                                                                <?php
                                                                                $sqlOficina = "SELECT cNomOficina, cSiglaOficina FROM Tra_M_oficinas WHERE iCodOficina = " . $Rs['iCodOficina'];
                                                                                $rsOficina = mssql_query($sqlOficina, $cnx);
                                                                                $RsOficina = mssql_fetch_object($rsOficina);
                                                                                echo utf8_encode($RsOficina->cSiglaOficina . " | " . $RsOficina->cNomOficina);
                                                                                ?>
                                                                            </td>
                                                                            <td class="text-center">
                                                                                <a title="Eliminar" href="../cInterfaseUsuario_SITD/data_nuevo_nivel.php?op=2&id_perfil=<?php echo $Rs[iCodPerfilUsuario]; ?>&idd=<?php echo $_GET['cod'] ?>">
                                                                                    <i class="far fa-trash-alt"></i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="card mb-3">
                                                <div class="card-body">
                                                    <label title="Cargar Firma" for="file-upload" class="botenviar mb-3" style="display: inline"><i class="fas fa-cloud-upload-alt">&nbsp;Firma</i></label>
                                                    <input name="firma" id="file-upload" onchange='cambiar()' type="file" style='display: none;'/>
                                                    <div id="info" class="my-3"></div>
                                                    <img style="width: 100px!important" src="data:image/png;charset=utf8;base64,<?php echo $img;?>"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body px-0 pb-0">
                                            <div class="row justify-content-center">
                                                <div class="col- mx-4">
                                                    <button class="botenviar"  type="submit" id="Actualizar Trabajador"  onMouseOver="this.style.cursor='hand'">Actualizar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                                <div class="col- mx-4">
                                                    <button class="botenviar" type="button"  onclick="window.open('iu_trabajadores.php', '_self');" onMouseOver="this.style.cursor='hand'">Cancelar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?include("includes/userinfo.php");?>
<?include("includes/pie.php");?>

<script type="text/javascript" language="javascript" src="ckeditor/adapters/jquery.js"></script>

<script>
    //boton subir archivo
    function cambiar(){
        var pdrs = document.getElementById('file-upload').files[0].name;
        document.getElementById('info').innerHTML = pdrs;
    }
</script>

<script>
    function ConfirmarBorrado(){
        if (confirm("Esta seguro de eliminar el registro?")){
            return true;
        }else{
            return false;
        }
    }
    $(document).ready(function() {
        $('.mdb-select').material_select();
    });
</script>
</body>
</html>

<?
}else{
   header("Location: ../index.php?alter=5");
}
?>
