<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: consultaTramiteCargo_pdf.php
SISTEMA: SISTEMA  DE TRÁMITE DOCUMENTARIO DIGITAL
OBJETIVO: Reporte General en PDF de los Documentos de Cargos
PROPIETARIO: AGENCIA PERUANA DE COOPERACIÓN INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripción
------------------------------------------------------------------------
1.0  Larry Ortiz          05/09/2018      Creación del programa.
------------------------------------------------------------------------
*****************************************************************************************/
session_start();
ob_start();
//*************************************
include_once("../conexion/conexion.php");
?>
<page backtop="15mm" backbottom="15mm" backleft="10mm" backright="10mm">
	<page_header>
		<br>
		<table style="width: 1000px; border: solid 0px black;">
			<tr>
				<td style="text-align:left;	width: 20px"></td>
				<td style="text-align:left;	width: 980px">
					<img style="width: 220px" src="images/cab.jpg" alt="Logo">
				</td>
			</tr>
		</table>
        <br><br>
	</page_header>
	<page_footer>
		<table style="width: 100%; border: solid 0px black;">
			<tr>
                <td style="text-align: center;	width: 40%">
				<? 
				   $sqllog="select cNombresTrabajador, cApellidosTrabajador from tra_m_trabajadores where iCodTrabajador='$_SESSION[CODIGO_TRABAJADOR]' "; 
				   $rslog=mssql_query($sqllog,$cnx);
				   $Rslog=MsSQL_fetch_array($rslog);
				   echo $Rslog[cNombresTrabajador]." ".$Rslog[cApellidosTrabajador];
				?></td>
				<td style="text-align: right;	width: 60%">p�gina [[page_cu]]/[[page_nb]]</td>
			</tr>
		</table>
        <br>
        <br>
	</page_footer>
	
	<table style="width: 1000px; border: solid 0px black;">
	<tr>
	<td style="text-align:center;width:1000px"><span style="font-size: 15px; font-weight: bold">Lista de Documentos de Salidas Multiples</span></td>
	</tr>
	</table>
	
	<br><br>
					<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="center">
                            <thead>
                                <tr>
                                    <th colspan="7" style="text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">DOCUMENTOS ENVIADOS SERVICIO NACIONAL</th>
                                    <th colspan="3" style="text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">DEVOLUCION DE CARGOS</th>
                                </tr>
                            	<tr>
                                    <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Fecha de Envio</th>
                                    <th style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N� Pedido de Servicio</th>
									<th style="width: 7%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Area Usuaria</th>
                                    <th style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N� Guia de Servicio</th>
                                    <th style="width: 12%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Tipo de Documento</th>
                                    <th style="width: 13%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Destinatario</th>
                                    <th style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Destino</th>
                                    <th style="width: 7%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N� Guia</th>
                                    <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Fecha Devolucion</th>
                                    <th style="width: 15%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Observaciones</th>
								</tr>
							</thead>
							<tbody>
							<?
	  if($_GET[fHasta]!=""){
    $fDesde=date("Ymd", strtotime($_GET[fDesde]));
	$fHasta=date("d-m-Y", strtotime($_GET[fHasta]));
	function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    $date_r = getdate(strtotime($date));
    $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
    return $date_result;
				}
	$fHasta=dateadd($fHasta,1,0,0,0,0,0); // + 1 dia
	}

	$sql.= " SP_CONSULTA_TRAMITE_CARGO '$_REQUEST[fDesde]', '$fHasta','$_REQUEST[ChxfRespuesta]','$_REQUEST[fEntrega]', '%$_REQUEST[cCodificacion]%', '%$_REQUEST[cNombre]%', '%$_REQUEST[cDireccion]%', '$_REQUEST[cCodTipoDoc]', '$_REQUEST[cOrdenServicio]', '$_REQUEST[cFlgUrgente]','$_REQUEST[iCodTrabajadorEnvio]', '$_REQUEST[cFlgLocal]', '$_REQUEST[cFlgNacional]','$_REQUEST[iCodOficina]' ,'$_REQUEST[cFlgEstado]', '$_REQUEST[cCodDepartamento]' ,'$_REQUEST[cCodProvincia]' ,'$_REQUEST[cCodDistrito]','$campo','$orden'  ";						 				  
							$rs=mssql_query($sql,$cnx);
						  //echo $sql;
							  
					 while ($Rs=MsSQL_fetch_array($rs)){
				 ?>
					 <tr>
				      <td style="width:8%;text-align:center;border: solid 1px #6F6F6F;font-size:10px;"><? if($Rs[fEntrega]!="") echo date("d-m-Y", strtotime($Rs[fEntrega]))?></td>
				      <td style="width:10%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase;"><?=$Rs[cOrdenServicio]?></td>
				      <td style="width:7%;text-align:center;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase;"><?=$Rs[cSiglaOficina]?></td>
					   <td style="width:10%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><?=$Rs[cNumGuiaServicio]?></td>
                       <td style="width:12%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><?=$Rs[cDescTipoDoc]?><br><?=$Rs[cCodificacion]?></td>
                       <td style="width:13%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><?=$Rs[cNombre]?></td>
					   <td style="width:10%;text-align:left; border: solid 1px #6F6F6F;font-size:10px;vertical-align:top">
					   <? echo $Rs[cDireccion]; echo "<br>"; 
					      if($Rs[cNomDepartamento]!=""){echo $Rs[cNomDepartamento];} if($Rs[cNomProvincia]!=""){ echo " - ".$Rs[cNomProvincia];} if($Rs[cNomDistrito]!=""){ echo " - ".$Rs[cNomDistrito];}
					   ?></td>
					   <td style="width:7%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase"><?=$Rs[cNumGuia]?></td> 
                       <td style="width:8%;text-align:center;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase"><? if($Rs[fRespuesta]!="") echo date("d-m-Y", strtotime($Rs[fRespuesta]))?></td> 
                       <td  style="width:15%;text-align:justify;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase"><?=$Rs[cObservaciones]?></td> 
                       
						  </tr>
						  <?}?>
						  </tbody>
							 </table>  
						<?
						
				
				?>
</page>

<?
//*************************************


	$content = ob_get_clean();  set_time_limit(0);     ini_set('memory_limit', '640M');

	// conversion HTML => PDF
	require_once(dirname(__FILE__).'/html2pdf/html2pdf.class.php');
	try
	{
		$html2pdf = new HTML2PDF('L','A4', 'es', false, 'UTF-8', 3);
		$html2pdf->pdf->SetDisplayMode('fullpage');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output('exemple03.pdf');
	}
	catch(HTML2PDF_exception $e) { echo $e; }
?>   
         		
