<?php
date_default_timezone_set('America/Lima');
session_start();
if (isset($_SESSION['CODIGO_TRABAJADOR'])){
	include_once("../conexion/conexion.php");
	///////////////////////////////////////////////
	//PARA LCP
	$fFecActual = date("Ymd")." ".date("H:i:s");
	//PARA PRO INVERSION
	//$fFecActual = date("Ydm")." ".date("H:i:s");
	///////////////////////////////////////////////
	$rutaUpload = "../cAlmacenArchivos/";
	$nNumAno    = date("Y");
	
  function add_ceros($numero,$ceros){
  	$order_diez = explode(".",$numero);
    $dif_diez = $ceros - strlen($order_diez[0]);
    for($m=0; $m<$dif_diez; $m++){
    	@$insertar_ceros .= 0;
    }
    return $insertar_ceros .= $numero;
  }		

  $sqlUsr = "SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$_SESSION[CODIGO_TRABAJADOR]'"; 
	$rsUsr  = mssql_query($sqlUsr,$cnx);
	$RsUsr  = mssql_fetch_array($rsUsr);
  
  switch ($_POST['opcion']){
  	case 1:
  		// Actualizar fecha de los pendientes al momento de aceptarlos.
  		for ($h=0; $h < count($_POST[iCodMovimiento]); $h++){
	    	$iCodMovimiento = $_POST[iCodMovimiento];
	   		$sqlMov = "UPDATE Tra_M_Tramite_Movimientos SET fFecRecepcion = '$fFecActual' 
	   							 WHERE iCodMovimiento='$iCodMovimiento[$h]'";
	   		$rsUpdMov = mssql_query($sqlMov,$cnx);
	   			
				$sqlMovData = "SELECT iCodTramite,iCodMovimiento,iCodTramiteDerivar,nEstadoMovimiento,iCodTrabajadorDelegado 
											 FROM Tra_M_Tramite_Movimientos WHERE iCodMovimiento='$iCodMovimiento[$h]'";
	   		$rsMovData  = mssql_query($sqlMovData,$cnx);
				$RsMovData  = mssql_fetch_array($rsMovData);
			
				if ($RsMovData[nEstadoMovimiento] == 3 && $RsMovData[iCodTrabajadorDelegado] == $_SESSION['CODIGO_TRABAJADOR'] ){
					$sqlMovDel = "UPDATE Tra_M_Tramite_Movimientos SET fFecDelegadoRecepcion='$fFecActual' 
												WHERE iCodMovimiento='$iCodMovimiento[$h]'";
	   			$rsUpdMovDel = mssql_query($sqlMovDel,$cnx);
				}
			
				if($RsMovData[iCodTramiteDerivar]!=""){
					$sqlUpdDev = "UPDATE Tra_M_Tramite_Movimientos SET fFecRecepcion='$fFecActual' 
												WHERE iCodTramite='$RsMovData[iCodTramiteDerivar]'";
					$rsUpdDev  = mssql_query($sqlUpdDev,$cnx);	
				}
   			$sqlUpd = "UPDATE Tra_M_Tramite SET nFlgEstado = 2 WHERE iCodTramite='$RsMovData[iCodTramite]'";
				$rsUpd  = mssql_query($sqlUpd,$cnx);
			}// Fin de for
			header("Location: pendientesControl.php");
			break;
			///////////////////////7//////////////////////////////////////////////////////////////////////////////////
  	case 2:
  		// Derivación movimiento
  		if($_POST['iCodMovimientoAccion'] == ""){
  			for ($h=0; $h < count($_POST['MovimientoAccion']); $h++){
  				$MovimientoAccion = $_POST['MovimientoAccion'];
					$cCodificacion    = "";
					$cCodTipoDoc      = 45;
	
     			$sqlAdd = "INSERT INTO Tra_M_Tramite(nFlgTipoDoc,nFlgClaseDoc,cCodificacion,iCodTrabajadorRegistro,iCodOficinaRegistro, cCodTipoDoc,fFecDocumento,cAsunto,cObservaciones,fFecRegistro,nFlgEstado,iCodTrabajadorSolicitado,descripcion)";
    			$sqlAdd.=" VALUES ";
    			$sqlAdd.="(2,1,'$cCodificacion','$_SESSION[CODIGO_TRABAJADOR]','$_SESSION[iCodOficinaLogin]','$cCodTipoDoc','$fFecActual', '$_POST[cAsuntoDerivar]','$_POST[cObservacionesDerivar]','$fFecActual', 1,'$_SESSION[JEFE]','".str_replace( '\"', '"', $_POST[descripcion] )."')";

    			echo "tramite--->".$sqlAdd;

    			$rs = mssql_query($sqlAdd,$cnx);
    			//Ultimo registro de tramite
					$rsUltTra = mssql_query("SELECT TOP 1 iCodTramite FROM Tra_M_Tramite 
																   WHERE iCodTrabajadorRegistro ='$_SESSION[CODIGO_TRABAJADOR]' 
																   ORDER BY iCodTramite DESC",$cnx);
					$RsUltTra = mssql_fetch_array($rsUltTra);
    			
    			$sqlAdMv = "INSERT INTO Tra_M_Tramite_Movimientos (iCodTramite,iCodTrabajadorRegistro,nFlgTipoDoc,iCodOficinaOrigen, iCodOficinaDerivar,iCodTrabajadorDerivar,iCodIndicacionDerivar,cPrioridadDerivar,cAsuntoDerivar,           cObservacionesDerivar,fFecDerivar,fFecMovimiento,nEstadoMovimiento,cFlgTipoMovimiento)";
    			$sqlAdMv.=" VALUES ";
    			$sqlAdMv.="('$RsUltTra[iCodTramite]','$_SESSION[CODIGO_TRABAJADOR]',2,'$_SESSION[iCodOficinaLogin]','$_POST[iCodOficinaDerivar]','$_POST[iCodTrabajadorDerivar]','$_POST[iCodIndicacionDerivar]','Media','".str_replace( '\"', '"', $_POST[cAsuntoDerivar] )."','$_POST[cObservacionesDerivar]','$fFecActual','$fFecActual',1,1)";

    			echo "Movimientos--->".$sqlAdMv;

    			$rsAdMv = mssql_query($sqlAdMv,$cnx);
	  			// cambiar de estado al movimiento
	   			$sqlMov   = "UPDATE Tra_M_Tramite_Movimientos SET nEstadoMovimiento=2 WHERE iCodMovimiento='$MovimientoAccion[$h]'";
		  		$rsUpdMov = mssql_query($sqlMov,$cnx);
		
		  		$rsMovData = mssql_query("SELECT * FROM Tra_M_Tramite_Movimientos WHERE iCodMovimiento='$MovimientoAccion[$h]'",$cnx);
		  		$RsMovData = mssql_fetch_array($rsMovData);
		
		  		$sqlTip = "SELECT nFlgTipoDoc FROM Tra_M_Tramite WHERE iCodTramite='$RsMovData[iCodTramite]'";
		  		$rsTip  = mssql_query($sqlTip,$cnx);
		  		$RsTip  = mssql_fetch_array($rsTip);		
					// crear nuevo registro en movimiento por derivacion de oficina a otra
					// por movimiento copias
					if ($_POST['cFlgTipoMovimientoOrigen'] == 4){
						$cFlgTipoMovimientoRec = 4;
					}else{
						$cFlgTipoMovimientoRec = 1;
					}
		
					$sqlMov = "INSERT INTO Tra_M_Tramite_Movimientos ";
					$sqlMov.="(iCodTramite,iCodTrabajadorRegistro,nFlgTipoDoc,iCodOficinaOrigen,iCodOficinaDerivar,           iCodTrabajadorDerivar,iCodIndicacionDerivar,cCodTipoDocDerivar,cAsuntoDerivar,cObservacionesDerivar,    			fFecDerivar,cNumDocumentoDerivar,nEstadoMovimiento,fFecMovimiento,nFlgEnvio,cFlgTipoMovimiento,       iCodTramiteDerivar)";
					$sqlMov.=" VALUES ";
					$sqlMov.="('$RsMovData[iCodTramite]','$_SESSION[CODIGO_TRABAJADOR]',$RsMovData[nFlgTipoDoc],'$_SESSION[iCodOficinaLogin]','$_POST[iCodOficinaDerivar]','$_POST[iCodTrabajadorDerivar]','$_POST[iCodIndicacionDerivar]','$cCodTipoDoc','".str_replace( '\"', '"', $_POST[cAsuntoDerivar] )."','$_POST[cObservacionesDerivar]','$fFecActual','$cCodificacion',1,'$fFecActual',1,'$cFlgTipoMovimientoRec','$RsUltTra[iCodTramite]')";
			
   				$rsMov  = mssql_query($sqlMov,$cnx);
					$sqlTmp = "SELECT * FROM Tra_M_Tramite_Temporal WHERE cCodSession='$_SESSION[cCodSessionDrv]' ORDER BY iCodTemp ASC";
    			$rsTmp  = mssql_query($sqlTmp,$cnx);
				
					if ($RsTip['nFlgTipoDoc'] != 2){
	    			while ($RsTmp = mssql_fetch_array($rsTmp)){
	   					$sqlCpy = "INSERT INTO Tra_M_Tramite_Movimientos ";
							$sqlCpy.="(iCodTramite,iCodTrabajadorRegistro,nFlgTipoDoc,iCodOficinaOrigen,iCodOficinaDerivar,    iCodTrabajadorDerivar,iCodIndicacionDerivar,cPrioridadDerivar,cAsuntoDerivar, 	        cObservacionesDerivar,cCodTipoDocDerivar,fFecDerivar,cNumDocumentoDerivar,nEstadoMovimiento,fFecMovimiento, nFlgEnvio,cFlgTipoMovimiento,iCodTramiteDerivar)";
							$sqlCpy.=" VALUES ";
							$sqlCpy.="('$RsMovData[iCodTramite]','$_SESSION[CODIGO_TRABAJADOR]',$RsMovData[nFlgTipoDoc],'$_SESSION[iCodOficinaLogin]','$RsTmp[iCodOficina]','$RsTmp[iCodTrabajador]','$RsTmp[iCodIndicacion]','$RsTmp[cPrioridad]','".str_replace( '\"', '"', $_POST[cAsuntoDerivar] )."','$_POST[cObservacionesDerivar]','$cCodTipoDoc','$fFecActual','$cCodificacion',1,'$fFecActual',1,4,'$RsUltTra[iCodTramite]')";
							$rsCpy = mssql_query($sqlCpy,$cnx);
	   				}
					}

					if ($RsTip['nFlgTipoDoc'] == 2){
						if (isset($_POST['Copia'])){
	  					$Copia = $_POST['Copia'];
	  					$n     = count($Copia);
	  					$w     = 0;
						}
		
						while ($RsTmp = mssql_fetch_array($rsTmp)){
							$x = 1;
							for ($w = 0; $w<$n; $w++){
								if ($RsTmp[iCodTemp] == $Copia[$w]  ){   //  Seleccion de Copia
					 				$x = 4;
								}else{		// Sin Copia
									$y =1;
								}
							}
				
							if ($x == 4){
								$cFlgTipoMovimiento = 4;
							}else if ($x != 4){
								$cFlgTipoMovimiento = 1;
							}
	   		
	   					$sqlCpy = "INSERT INTO Tra_M_Tramite_Movimientos ";
							$sqlCpy.="(iCodTramite,iCodTrabajadorRegistro,nFlgTipoDoc,iCodOficinaOrigen,iCodOficinaDerivar,    iCodTrabajadorDerivar,iCodIndicacionDerivar,cPrioridadDerivar,cAsuntoDerivar, 	        cObservacionesDerivar,cCodTipoDocDerivar,fFecDerivar,nEstadoMovimiento,fFecMovimiento,nFlgEnvio, cFlgTipoMovimiento,cNumDocumentoDerivar,iCodTramiteDerivar)";
							$sqlCpy.=" VALUES ";
							$sqlCpy.="('$RsMovData[iCodTramite]','$_SESSION[CODIGO_TRABAJADOR]',$RsMovData[nFlgTipoDoc],'$_SESSION[iCodOficinaLogin]','$RsTmp[iCodOficina]','$RsTmp[iCodTrabajador]','$RsTmp[iCodIndicacion]','$RsTmp[cPrioridad]','".str_replace( '\"', '"', $_POST[cAsuntoDerivar] )."','$_POST[cObservacionesDerivar]','$cCodTipoDoc','$fFecActual',1,'$fFecActual',1,'$cFlgTipoMovimiento','$cCodificacion','$RsUltTra[iCodTramite]')";
							$rsCpy = mssql_query($sqlCpy,$cnx);
	   				}
					}
   	
   			// descrip de tipo de documento
   			$sqlTipDoc = "SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$cCodTipoDoc'";
				$rsTipDoc  = mssql_query($sqlTipDoc,$cnx);
				$RsTipDoc  = mssql_fetch_array($rsTipDoc);
		
   			if ($_FILES['fileUpLoadDigital']['name'] != ""){
  				$extension = explode(".",$_FILES['fileUpLoadDigital']['name']);
  				$num = count($extension)-1;
					$cNombreOriginal = $_FILES['fileUpLoadDigital']['name'];
					if($extension[$num] == "exe" OR $extension[$num] == "dll" OR $extension[$num] == "EXE" OR $extension[$num] == "DLL"){
						$nFlgRestricUp = 1;
   				}else{
						$nuevo_nombre = str_replace(" ","-",trim($RsTipDoc[cDescTipoDoc]))."-".str_replace("/","-",$cCodificacion).".".$extension[$num];
						move_uploaded_file($_FILES['fileUpLoadDigital']['tmp_name'], "$rutaUpload$nuevo_nombre");
						
						$sqlDigt = "INSERT INTO Tra_M_Tramite_Digitales (iCodTramite, cNombreOriginal, cNombreNuevo) 
											VALUES ('$RsUltTra[iCodTramite]', '$cNombreOriginal', '$nuevo_nombre')";
   					$rsDigt  = mssql_query($sqlDigt,$cnx);
   				}
	  		}
			}
		}else if($_POST['iCodMovimientoAccion'] != ""){
 			if($_POST['cCodTipoDoc'] != 45){
	    	// comprobar o recoger correlativo para generar interno
	    	$sqlCorr = "SELECT * FROM Tra_M_Correlativo_Oficina 
	    						  WHERE cCodTipoDoc='$_POST[cCodTipoDoc]' AND iCodOficina='$_SESSION[iCodOficinaLogin]' AND nNumAno='$nNumAno'";
	    	$rsCorr  = mssql_query($sqlCorr,$cnx);
	    	
	    	if (mssql_num_rows($rsCorr) > 0){
	    		$RsCorr = mssql_fetch_array($rsCorr);
	    		$nCorrelativo = $RsCorr[nCorrelativo]+1;
	    				
	    		$sqlUpd = "UPDATE Tra_M_Correlativo_Oficina SET nCorrelativo='$nCorrelativo' WHERE iCodCorrelativo='$RsCorr[iCodCorrelativo]'";
					$rsUpd = mssql_query($sqlUpd,$cnx);
	    	}else{
	    		$sqlAdCorr = "INSERT INTO Tra_M_Correlativo_Oficina (cCodTipoDoc, iCodOficina, nNumAno, nCorrelativo) 
	    									VALUES ('$_POST[cCodTipoDoc]', '$_SESSION[iCodOficinaLogin]', '$nNumAno',1)";
	    		$rsAdCorr  = mssql_query($sqlAdCorr,$cnx);
	    		$nCorrelativo = 1;
	    	}
		
	    	//leer sigla oficina
	    	$rsSigla = mssql_query("SELECT * FROM Tra_M_Oficinas WHERE iCodOficina='$_SESSION[iCodOficinaLogin]'",$cnx);
	    	$RsSigla = mssql_fetch_array($rsSigla);
	    			
	    	// armar correlativo
	    	$cCodificacion=add_ceros($nCorrelativo,5)."-".date("Y")."/".trim($RsSigla[cSiglaOficina]);
    	}else{
				$cCodificacion = "";
			}

			// $sqlTrb = "SELECT * FROM Tra_M_Perfil_Ususario TPU
			// 		 			 INNER JOIN Tra_M_Trabajadores TT ON TPU.iCodTrabajador = TT.iCodTrabajador
			// 		 			 WHERE TPU.iCodPerfil = 3 AND TPU.iCodOficina = '$_SESSION[iCodOficinaLogin]'";
			// $rsTrb  = mssql_query($sqlTrb,$cnx);
			// $RsTrb  = mssql_fetch_array($rsTrb);
			// if ($RsTrb['iCodTrabajador'] == $_SESSION['CODIGO_TRABAJADOR']) {
			// 	$esAprobado = 1;
			// }else{
			// 	$esAprobado = 0;
			// }
			// nFlgEnvio = $esAprobado asi era con el código anterior

			// Código insert anterior
			
    	// $sqlAdd = "INSERT INTO Tra_M_Tramite";
    	// $sqlAdd.="(nFlgTipoDoc,nFlgClaseDoc,cCodificacion,iCodTrabajadorRegistro,iCodOficinaRegistro,cCodTipoDoc,fFecDocumento,	cAsunto,cObservaciones,fFecRegistro,nFlgEstado,iCodTrabajadorSolicitado,descripcion,nFlgEnvio)";
    	// $sqlAdd.=" VALUES ";
    	// $sqlAdd.="(2,1,'$cCodificacion','$_SESSION[CODIGO_TRABAJADOR]','$_SESSION[iCodOficinaLogin]','$_POST[cCodTipoDoc]','$fFecActual','$_POST[cAsuntoDerivar]','$_POST[cObservacionesDerivar]','$fFecActual',1,'$_SESSION[JEFE]','".str_replace( '\"', '"', $_POST[descripcion] )."',0)";

    	// Si nFlgTipoDerivo es 1, entonces el documento proviene de una derivación.
    	// Si nFlgTipoDerivo no es 1 (NULL), entonces el documento proviene de un registro de documento interno.








            /*INICIO DEL INSERTAR DEL ARCHIVO PROFESIONALDATA.PHP*/
            $sqlAdd="INSERT INTO Tra_M_Tramite ";
            $sqlAdd.="(nFlgEnvio, nFlgTipoDoc, nFlgClaseDoc, cCodificacion,     iCodTrabajadorRegistro,        iCodOficinaRegistro,           cCodTipoDoc,           fFecDocumento,	cAsunto,           cObservaciones,           fFecPlazo,    fFecRegistro, nFlgEstado,	 	iCodTrabajadorSolicitado	)";
            $sqlAdd.=" VALUES ";
            $sqlAdd.="(1,2,           2,			'$cCodificacion',	'$_SESSION[CODIGO_TRABAJADOR]', '$_SESSION[iCodOficinaLogin]', '$_POST[cCodTipoDoc]', '$fFecActual', '$_POST[cAsuntoDerivar]', '$_POST[cObservacionesDerivar]', '$fFecPlazo', '$fFecActual',1, '$_SESSION[JEFE]')";
            $rs=mssql_query($sqlAdd,$cnx);

            //Ultimo registro de tramite
            $rsUltTra=mssql_query("SELECT TOP 1 iCodTramite , nFlgClaseDoc FROM Tra_M_Tramite ORDER BY iCodTramite DESC",$cnx);
            $RsUltTra=MsSQL_fetch_array($rsUltTra);
            $sqlTipDoc="SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$_POST[cCodTipoDoc]'";
            $rsTipDoc=mssql_query($sqlTipDoc,$cnx);
            $RsTipDoc=MsSQL_fetch_array($rsTipDoc);

            // Movimiento generado para el documento creado por derivacion
            $sqlAdMv="INSERT INTO Tra_M_Tramite_Movimientos ";
            $sqlAdMv.="(iCodTramite,              iCodTrabajadorRegistro,             nFlgTipoDoc,  iCodOficinaOrigen,             iCodOficinaDerivar,           iCodTrabajadorDerivar,           iCodIndicacionDerivar,            cPrioridadDerivar,   cAsuntoDerivar,           cObservacionesDerivar,            fFecDerivar,  fFecMovimiento, nEstadoMovimiento,cFlgTipoMovimiento)";
            $sqlAdMv.=" VALUES ";
            $sqlAdMv.="('$RsUltTra[iCodTramite]', '$_SESSION[CODIGO_TRABAJADOR]',     2,            '$_SESSION[iCodOficinaLogin]', '$_POST[iCodOficinaDerivar]', '$_POST[iCodTrabajadorDerivar]', '$_POST[iCodIndicacionDerivar]', 'Media',              '$_POST[cAsuntoDerivar]', '$_POST[cObservacionesDerivar]', '$fFecActual', '$fFecActual',  1, 						   1)";
            $rsAdMv=mssql_query($sqlAdMv,$cnx);

            /*FIN DEL INSERTAR DEL ARCHIVO PROFESIONALDATA.PHP*/





        /*INICIO DE LAS LINEAS DE CODIGO PARA INSERTAR*/
        /* $sqlAdd = "INSERT INTO Tra_M_Tramite";
    	$sqlAdd.="(nFlgTipoDoc,nFlgClaseDoc,cCodificacion,iCodTrabajadorRegistro,iCodOficinaRegistro,cCodTipoDoc,fFecDocumento,	cAsunto,cObservaciones,fFecRegistro,nFlgEstado,iCodTrabajadorSolicitado,descripcion,nFlgEnvio,nFlgTipoDerivo)";
    	$sqlAdd.=" VALUES ";
    	$sqlAdd.="(2,1,'$cCodificacion','$_SESSION[CODIGO_TRABAJADOR]','$_SESSION[iCodOficinaLogin]','$_POST[cCodTipoDoc]','$fFecActual','$_POST[cAsuntoDerivar]','$_POST[cObservacionesDerivar]','$fFecActual',1,'$_SESSION[JEFE]','".str_replace( '\"', '"', $_POST[descripcion] )."',0,1)";
    	echo "TRAMITE 2--->".$sqlAdd;

    	$rs = mssql_query($sqlAdd,$cnx);
    			

			$rsUltTra = mssql_query("SELECT TOP 1 iCodTramite FROM Tra_M_Tramite 
															 WHERE iCodTrabajadorRegistro ='$_SESSION[CODIGO_TRABAJADOR]' 
															 ORDER BY iCodTramite DESC",$cnx);
			$RsUltTra = mssql_fetch_array($rsUltTra);

			$sqlAdMv = "INSERT INTO Tra_M_Tramite_Movimientos ";
			$sqlAdMv.="(iCodTramite,iCodTrabajadorRegistro,nFlgTipoDoc,iCodOficinaOrigen,iCodOficinaDerivar,           iCodTrabajadorDerivar,iCodIndicacionDerivar,cPrioridadDerivar,cAsuntoDerivar,cObservacionesDerivar,fFecDerivar,fFecMovimiento, nEstadoMovimiento,cFlgTipoMovimiento)";
			$sqlAdMv.=" VALUES ";
			$sqlAdMv.="('$RsUltTra[iCodTramite]','$_SESSION[CODIGO_TRABAJADOR]',2,'$_SESSION[iCodOficinaLogin]','$_POST[iCodOficinaDerivar]','$_POST[iCodTrabajadorDerivar]','$_POST[iCodIndicacionDerivar]','$_POST[cPrioridad]','".str_replace( '\"', '"', $_POST[cAsuntoDerivar] )."', '$_POST[cObservacionesDerivar]','$fFecActual','$fFecActual',1,1)";

			echo "MOVIMIENTOS 2--->".$sqlAdMv;

			$rsAdMv = mssql_query($sqlAdMv,$cnx);*/












  		// cambiar de estado al movimiento
   		$sqlMov   = "UPDATE Tra_M_Tramite_Movimientos SET nEstadoMovimiento = 2 WHERE iCodMovimiento='$_POST[iCodMovimientoAccion]'";
			$rsUpdMov = mssql_query($sqlMov,$cnx);
		
			$rsMovData = mssql_query("SELECT * FROM Tra_M_Tramite_Movimientos WHERE iCodMovimiento='$_POST[iCodMovimientoAccion]'",$cnx);
			$RsMovData = mssql_fetch_array($rsMovData);
		
		 	$sqlTip = "SELECT nFlgTipoDoc FROM Tra_M_Tramite WHERE icodtramite='$RsMovData[iCodTramite]'";
			$rsTip  = mssql_query($sqlTip,$cnx);
			$RsTip  = mssql_fetch_array($rsTip);		
		
			// crear nuevo registro en movimiento por derivacion de oficina a otra
			// por movimiento copias
			if($_POST[cFlgTipoMovimientoOrigen] == 4){
				$cFlgTipoMovimientoRec = 4;
			}else{
				$cFlgTipoMovimientoRec = 1;
			}

			$sqlMov = "INSERT INTO Tra_M_Tramite_Movimientos ";
			$sqlMov.="(iCodTramite,iCodTrabajadorRegistro,nFlgTipoDoc,iCodOficinaOrigen,iCodOficinaDerivar,           iCodTrabajadorDerivar,iCodIndicacionDerivar,cCodTipoDocDerivar,cAsuntoDerivar,cObservacionesDerivar,cPrioridadDerivar,fFecDerivar,	 cNumDocumentoDerivar,nEstadoMovimiento,fFecMovimiento,nFlgEnvio,cFlgTipoMovimiento,iCodTramiteDerivar)";
			$sqlMov.=" VALUES ";
			$sqlMov.="('$RsMovData[iCodTramite]','$_SESSION[CODIGO_TRABAJADOR]',$RsMovData[nFlgTipoDoc],'$_SESSION[iCodOficinaLogin]','$_POST[iCodOficinaDerivar]','$_POST[iCodTrabajadorDerivar]','$_POST[iCodIndicacionDerivar]','$_POST[cCodTipoDoc]','".str_replace( '\"', '"', $_POST[cAsuntoDerivar] )."','$_POST[cObservacionesDerivar]','$_POST[cPrioridad]','$fFecActual','$cCodificacion',1,'$fFecActual',1,'$cFlgTipoMovimientoRec','$RsUltTra[iCodTramite]')";

			$rsMov = mssql_query($sqlMov,$cnx);
   	
			$sqlMovfst = "SELECT TOP 1 iCodMovimiento FROM Tra_M_tramite_Movimientos 
										WHERE iCodTramite ='$RsMovData[iCodTramite]' and iCodOficinaOrigen= '$_SESSION[iCodOficinaLogin]' 
										ORDER BY iCodMovimiento DESC";
			$rsMovfst = mssql_query($sqlMovfst,$cnx);
			$RsMovfst = mssql_fetch_array($rsMovfst);

			$sqlTmp = "SELECT * FROM Tra_M_Tramite_Temporal WHERE cCodSession='$_SESSION[cCodSessionDrv]' ORDER BY iCodTemp ASC";
    	$rsTmp  = mssql_query($sqlTmp,$cnx);
			
			if ($RsTip[nFlgTipoDoc] != 2){
    		while ($RsTmp=MsSQL_fetch_array($rsTmp)){
   				$sqlCpy = "INSERT INTO Tra_M_Tramite_Movimientos ";
					$sqlCpy.="(iCodTramite,iCodTrabajadorRegistro,nFlgTipoDoc,iCodOficinaOrigen,iCodOficinaDerivar,iCodTrabajadorDerivar,    iCodIndicacionDerivar,cPrioridadDerivar,cAsuntoDerivar,cObservacionesDerivar,cCodTipoDocDerivar,fFecDerivar,   cNumDocumentoDerivar,nEstadoMovimiento,fFecMovimiento,nFlgEnvio,cFlgTipoMovimiento,iCodTramiteDerivar)";
					$sqlCpy.=" VALUES ";
					$sqlCpy.="('$RsMovData[iCodTramite]','$_SESSION[CODIGO_TRABAJADOR]',$RsMovData[nFlgTipoDoc],'$_SESSION[iCodOficinaLogin]','$RsTmp[iCodOficina]','$RsTmp[iCodTrabajador]','$RsTmp[iCodIndicacion]','$RsTmp[cPrioridad]','".str_replace( '\"', '"', $_POST[cAsuntoDerivar] )."','$_POST[cObservacionesDerivar]','$_POST[cCodTipoDoc]','$fFecActual','$cCodificacion', 1,                '$fFecActual',1,4,'$RsUltTra[iCodTramite]')";
					$rsCpy = mssql_query($sqlCpy,$cnx);
   			}
			}
	
			if ($RsTip[nFlgTipoDoc] == 2){
				if (isset($_POST['Copia'])){
	  			$Copia = $_POST['Copia'];
	  			$n     = count($Copia);
	  			$h     = 0;
				}
			
				while ($RsTmp=MsSQL_fetch_array($rsTmp)){
					$x=1;
					
					if($x==4){
						$cFlgTipoMovimiento=4;
					}else if($x!=4){
						$cFlgTipoMovimiento=1;
					}
	   			$sqlCpy="INSERT INTO Tra_M_Tramite_Movimientos ";
					$sqlCpy.="(iCodTramite,               iCodTrabajadorRegistro,         nFlgTipoDoc,              iCodOficinaOrigen,             iCodOficinaDerivar,    iCodTrabajadorDerivar,    iCodIndicacionDerivar,    cPrioridadDerivar,    cAsuntoDerivar, 	        cObservacionesDerivar,           cCodTipoDocDerivar,    fFecDerivar,   nEstadoMovimiento, fFecMovimiento, nFlgEnvio, cFlgTipoMovimiento,cNumDocumentoDerivar, iCodTramiteDerivar )";
					$sqlCpy.=" VALUES ";
					$sqlCpy.="('$RsMovData[iCodTramite]', '$_SESSION[CODIGO_TRABAJADOR]', $RsMovData[nFlgTipoDoc], 	'$_SESSION[iCodOficinaLogin]', '$RsTmp[iCodOficina]', '$RsTmp[iCodTrabajador]', '$RsTmp[iCodIndicacion]', '$RsTmp[cPrioridad]','".str_replace( '\"', '"', $_POST[cAsuntoDerivar] )."', '$_POST[cObservacionesDerivar]', '$_POST[cCodTipoDoc]', '$fFecActual', 1,                '$fFecActual',   1,	'$cFlgTipoMovimiento', '$cCodificacion' ,'$RsUltTra[iCodTramite]')";
					//*************************************echo "<br>Numero 8 ".$sqlCpy."<br>";
					$rsCpy=mssql_query($sqlCpy,$cnx);
   			}  
			}
  
	    // relacion por ferencias
    	$sqlRefs="SELECT * FROM Tra_M_Tramite_Referencias WHERE cCodSession='$_SESSION[cCodRef]'";
    	$rsRefs=mssql_query($sqlRefs,$cnx);
    	if(MsSQL_num_rows($rsRefs)>0){
    		while ($RsRefs=MsSQL_fetch_array($rsRefs)){
    			$sqlBusRef="SELECT * FROM Tra_M_Tramite WHERE iCodTramite='$RsRefs[iCodTramiteRef]'";
					$rsBusRef=mssql_query($sqlBusRef,$cnx);
					if(MsSQL_num_rows($rsBusRef)>0){
								
						$sqlAdRf="INSERT INTO Tra_M_Tramite_Movimientos ";
		    		$sqlAdRf.="(iCodTramite,              iCodTrabajadorRegistro,             nFlgTipoDoc, iCodOficinaOrigen,              cCodTipoDocDerivar,    iCodOficinaDerivar,    iCodTrabajadorDerivar,    iCodIndicacionDerivar,    cPrioridadDerivar,    cAsuntoDerivar,    cObservacionesDerivar,    fFecDerivar,   cReferenciaDerivar, fFecMovimiento, nEstadoMovimiento,cFlgTipoMovimiento	,			iCodTramiteDerivar)";
		    		$sqlAdRf.=" VALUES ";
		    		$sqlAdRf.="('$RsRefs[iCodTramiteRef]', '$_SESSION[CODIGO_TRABAJADOR]',     2,           '$_SESSION[iCodOficinaLogin]', '$_POST[cCodTipoDoc]',  '$_POST[iCodOficinaDerivar]', '$_POST[iCodTrabajadorDerivar]', '$RsMv2[iCodIndicacionDerivar]', '', '".str_replace( '\"', '"', $_POST[cAsuntoDerivar] )."', '$_POST[cObservacionesDerivar]', '$fFecActual', '$cCodificacion',  '$fFecActual',   1, 						    5	,			'$RsUltTra[iCodTramite]')";
		    		//***********************************echo "<br>Numero 9 ".$sqlAdRf."<br>";
		    		$rsAdRf=mssql_query($sqlAdRf,$cnx);
					}
					$sqlUpdR="UPDATE Tra_M_Tramite_Referencias SET iCodTramite='$RsUltTra[iCodTramite]', cDesEstado='REGISTRADO' WHERE iCodReferencia='$RsRefs[iCodReferencia]'";
					$rsUpdR=mssql_query($sqlUpdR,$cnx);
				}
    	}
   	
   		// descrip de tipo de documento
   		$sqlTipDoc = "SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$_POST[cCodTipoDoc]'";
			$rsTipDoc  = mssql_query($sqlTipDoc,$cnx);
			$RsTipDoc  = MsSQL_fetch_array($rsTipDoc);
	
			if($RsTipDoc["cCodTipoDoc"]==45){ $TipDoc="SinDocumento";} else { $TipDoc=$RsTipDoc["cCodTipoDoc"];}	
   		if($_FILES['fileUpLoadDigital']['name']!=""){
  			$extension = explode(".",$_FILES['fileUpLoadDigital']['name']);
  			$num = count($extension)-1;
				$cNombreOriginal=$_FILES['fileUpLoadDigital']['name'];
				if($extension[$num]=="exe" OR $extension[$num]=="dll" OR $extension[$num]=="EXE" OR $extension[$num]=="DLL"){
					$nFlgRestricUp=1;
	   		}else{
					$nuevo_nombre = str_replace(" ","-",trim($RsTipDoc[cDescTipoDoc]))."-".str_replace("/","-",$cCodificacion).".".$extension[$num];
					move_uploaded_file($_FILES['fileUpLoadDigital']['tmp_name'], "$rutaUpload$nuevo_nombre");
					$sqlDigt="INSERT INTO Tra_M_Tramite_Digitales (iCodTramite, cNombreOriginal, cNombreNuevo,iCodMovimiento) VALUES ('$RsUltTra[iCodTramite]', '$cNombreOriginal', '$nuevo_nombre','$RsMovfst[iCodMovimiento]')";
	   			$rsDigt=mssql_query($sqlDigt,$cnx);
	   		}
			}
		}	
		// header("Location: pendientesControl.php");
		$sqlX="DELETE FROM Tra_M_Tramite_Temporal WHERE cCodSession='$_SESSION[cCodSessionDrv]'";
		$rsX=mssql_query($sqlX,$cnx);
		unset($_SESSION[cCodSessionDrv]);  
		unset($_SESSION[cCodRef]); 

		echo "<html>";
		echo "<head>";
		echo "</head>";
		echo "<body OnLoad=\"document.form_envio.submit();\">";
		echo "<form method=POST name=form_envio action=pendientesControl.php>";
		echo "<input type=hidden name=iCodTramite value=\"".$RsUltTra[iCodTramite]."\">";
		echo "</form>";
		echo "</body>";
		echo "</html>";
		break;
		///////////////////////////////////////////////////////////////////////////////////////////////////
  case 3:
  	// delegar movimiento
	for ($h=0;$h<count($_POST[iCodMovimiento]);$h++){
	  $iCodMovimiento = $_POST[iCodMovimiento];
		$sqlMov = "UPDATE Tra_M_Tramite_Movimientos SET nEstadoMovimiento=3, iCodTrabajadorDelegado='$_POST[iCodTrabajadorDelegado]', iCodIndicacionDelegado='$_POST[iCodIndicacionDelegado]',  cObservacionesDelegado='$_POST[cObservacionesDelegado]', fFecDelegado='$fFecActual'  WHERE iCodMovimiento='$iCodMovimiento[$h]'";
		$rsUpdMov=mssql_query($sqlMov,$cnx);
		
		if($_SESSION['CODIGO_TRABAJADOR']==$_POST[iCodTrabajadorDelegado]){
			$sqlMovDe="UPDATE Tra_M_Tramite_Movimientos SET fFecDelegadoRecepcion='$fFecActual' WHERE iCodMovimiento='$iCodMovimiento[$h]'";
		$rsUpdMovDe=mssql_query($sqlMovDe,$cnx);
		}
		if($_POST[iCodDelOrig]!=$_POST[iCodTrabajadorDelegado]){
		$sqlMovFec="UPDATE Tra_M_Tramite_Movimientos SET fFecDelegadoRecepcion=NULL WHERE iCodMovimiento='$iCodMovimiento[$h]'";
		$rsUpdMovFec=mssql_query($sqlMovFec,$cnx);		
		}
		
		$rsMovData=mssql_query("SELECT * FROM Tra_M_Tramite_Movimientos WHERE iCodMovimiento='$iCodMovimiento[$h]'",$cnx);
		$RsMovData=MsSQL_fetch_array($rsMovData);
		
		$rsDelCc=mssql_query("DELETE FROM Tra_M_Tramite_Movimientos WHERE iCodMovimientoRel='$iCodMovimiento[$h]'",$cnx);
		
		for ($i=0;$i<count($_POST[lstTrabajadoresSel]);$i++){
			$lstTrabajadoresSel=$_POST[lstTrabajadoresSel];
   		
   		$sqlCpy="INSERT INTO Tra_M_Tramite_Movimientos ";
			$sqlCpy.="(iCodTramite,               iCodTrabajadorRegistro,         nFlgTipoDoc,             iCodOficinaOrigen,             iCodOficinaDerivar,            iCodTrabajadorEnviar,      iCodIndicacionDerivar,					  cObservacionesDerivar,            fFecDelegado,  fFecDerivar,   fFecMovimiento, nEstadoMovimiento, nFlgEnvio, cFlgTipoMovimiento,iCodMovimientoRel)";
			$sqlCpy.=" VALUES ";
			$sqlCpy.="('$RsMovData[iCodTramite]', '$_SESSION[CODIGO_TRABAJADOR]', $RsMovData[nFlgTipoDoc], '$_SESSION[iCodOficinaLogin]', '$_SESSION[iCodOficinaLogin]', '$lstTrabajadoresSel[$i]', '$_POST[iCodIndicacionDelegado]', '$_POST[cObservacionesDelegado]', '$fFecActual', '$fFecActual', '$fFecActual',  1,                 1,					6, '$iCodMovimiento[$h]')";
			$rsCpy=mssql_query($sqlCpy,$cnx);
		}
		
	}
		header("Location: pendientesControl.php");
		break;
		///////////////////////////////////////////////////////////////////////////////////////////////////
  case 4:
  	// finalizar movimiento
	for ($h=0;$h<count($_POST[iCodMovimiento]);$h++){
	  $iCodMovimiento= $_POST[iCodMovimiento];
	
		$sqlMov = "UPDATE Tra_M_Tramite_Movimientos 
							 SET nEstadoMovimiento = 5, 
							 		 iCodTrabajadorFinalizar = '$_SESSION[CODIGO_TRABAJADOR]', 
							 		 cObservacionesFinalizar = '$_POST[cObservacionesFinalizar]',
							 		 fFecFinalizar = '$fFecActual'
							 WHERE iCodMovimiento = '$iCodMovimiento[$h]'";
		$rsUpdMov = mssql_query($sqlMov,$cnx);
		
		// buscar iCodTramite
	$rsCodTra=mssql_query("SELECT * FROM Tra_M_Tramite_Movimientos WHERE iCodMovimiento='$iCodMovimiento[$h]' And cFlgTipoMovimiento=1 ",$cnx);
		$RsCodTra=MsSQL_fetch_array($rsCodTra);
		
		//listar movimientos
	$rsListaMov=mssql_query("SELECT TOP 1 * FROM Tra_M_Tramite_Movimientos WHERE iCodTramite='$RsCodTra[iCodTramite]' And cFlgTipoMovimiento=1 ORDER BY iCodMovimiento DESC ",$cnx);
		$RsListaMov=MsSQL_fetch_array($rsListaMov);
		
		if($RsCodTra[iCodMovimiento]==$RsListaMov[iCodMovimiento]){
		$sqlUpdTra="UPDATE Tra_M_Tramite SET nFlgEstado=3 WHERE iCodTramite='$RsCodTra[iCodTramite]'";
		$rsUpdTra=mssql_query($sqlUpdTra,$cnx);
		}
		}
		header("Location: pendientesControl.php");  
		break;
		///////////////////////////////////////////////////////////////////////////////////////////////////
  case 5:
  for ($h=0;$h<count($_POST[iCodMovimiento]);$h++){
	  $iCodMovimiento= $_POST[iCodMovimiento];
	  $sqlMovData="SELECT * FROM Tra_M_Tramite_Movimientos WHERE iCodMovimiento='$iCodMovimiento[$h]'";
  	  $rsMovData=mssql_query($sqlMovData,$cnx);
	  	$RsMovData=MsSQL_fetch_array($rsMovData);
		
		// añadir avances del movimiento.
		$sqlMov="INSERT INTO Tra_M_Tramite_Avance ";
		$sqlMov.="(iCodTramite,               iCodMovimiento,           iCodTrabajadorAvance,            cObservacionesAvance, 					fFecAvance)";
		$sqlMov.=" VALUES ";
		$sqlMov.="('$RsMovData[iCodTramite]', '$iCodMovimiento[$h]', '$_SESSION[CODIGO_TRABAJADOR]', '$_POST[cObservacionesAvance]', '$fFecActual')";
   	$rsMov=mssql_query($sqlMov,$cnx);
   	//echo $sqlMov;
  }
		header("Location: pendientesControl.php");  
		break;
		///////////////////////////////////////////////////////////////////////////////////////////////////
  case 6:
		$sqlUpdTra="UPDATE Tra_M_Tramite_Movimientos SET iCodOficinaDerivar='$_POST[iCodOficinaDerivar]', iCodTrabajadorDerivar='$_POST[iCodTrabajadorDerivar]', cAsuntoDerivar='".str_replace( '\"', '"', $_POST[cAsuntoDerivar] )."', cObservacionesDerivar='$_POST[cObservacionesDerivar]', iCodIndicacionDerivar='$_POST[iCodIndicacionDerivar]' WHERE iCodMovimiento='$_POST[iCodMovimiento]'";
		$rsUpdTra=mssql_query($sqlUpdTra,$cnx);
		// Seleccion del Documento intermedio		
		$sqlDocDev=" SELECT cNumDocumentoDerivar, iCodTramiteDerivar FROM Tra_M_Tramite_Movimientos WHERE iCodMovimiento = '$_POST[iCodMovimiento]' ";
		$rsDocDev=mssql_query($sqlDocDev,$cnx);
		$RsDocDev=mssql_fetch_array($rsDocDev);
		// Actualizacion del documento Intermedio
		// $sqlUpdDev="UPDATE Tra_M_Tramite SET cAsunto='".str_replace( '\"', '"', $_POST[cAsuntoDerivar] )."', cObservaciones='$_POST[cObservacionesDerivar]' WHERE iCodTramite='$RsDocDev[iCodTramiteDerivar]'";
		// $rsUpdDev=mssql_query($sqlUpdDev,$cnx);
		
		$sqlSelMov="SELECT iCodMovimiento FROM Tra_M_Tramite_Movimientos WHERE iCodTramite='$RsDocDev[iCodTramiteDerivar]' ";
		$rsSelMov=mssql_query($sqlSelMov,$cnx);
		
    	if(mssql_num_rows($rsSelMov)==1){
		$sqlUpdDev="UPDATE Tra_M_Tramite_Movimientos SET iCodOficinaDerivar='$_POST[iCodOficinaDerivar]', iCodTrabajadorDerivar='$_POST[iCodTrabajadorDerivar]', cAsuntoDerivar='".str_replace( '\"', '"', $_POST[cAsuntoDerivar] )."', cObservacionesDerivar='$_POST[cObservacionesDerivar]', iCodIndicacionDerivar='$_POST[iCodIndicacionDerivar]' WHERE iCodTramite='$RsDocDev[iCodTramiteDerivar]' AND iCodOficinaOrigen = '$_SESSION[iCodOficinaLogin]' ";
		$rsUpdDev=mssql_query($sqlUpdDev,$cnx);
		}

		header("Location: pendientesControlDerivarGenerarDocumentoEditar.php?iCodMovimientoDerivar=$_POST[iCodMovimiento]");  
		//header("Location: pendientesDerivados.php");  
		break;
	}
	
	if($_GET[opcion]==11){ //anular finalizado
		$sqlMov="UPDATE Tra_M_Tramite_Movimientos SET nEstadoMovimiento=1, iCodTrabajadorFinalizar=NULL, cObservacionesFinalizar=NULL, fFecFinalizar=NULL  WHERE iCodMovimiento='$_GET[iCodMovimiento]'";
		$rsUpdMov=mssql_query($sqlMov,$cnx);
		
		// buscar iCodTramite
		$rsCodTra=mssql_query("SELECT * FROM Tra_M_Tramite_Movimientos WHERE iCodMovimiento='$_GET[iCodMovimiento]'",$cnx);
		$RsCodTra=MsSQL_fetch_array($rsCodTra);
		
		$sqlUpdTra="UPDATE Tra_M_Tramite SET nFlgEstado=2 WHERE iCodTramite='$RsCodTra[iCodTramite]'";
		$rsUpdTra=mssql_query($sqlUpdTra,$cnx);
		header("Location: pendientesFinalizados.php");
	}	

}else{
	header("Location: ../index.php?alter=5");
}
?>