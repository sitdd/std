<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: consultaTramiteCargoDep.php
SISTEMA: SISTEMA  DE TR�MITE DOCUMENTARIO DIGITAL
OBJETIVO: Reporte general en EXCEL  de los Documentos de Cargos por Zonas
PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripci�n
------------------------------------------------------------------------
1.0   Larry Ortiz    05/09/2018      Creaci�n del programa.
------------------------------------------------------------------------
*****************************************************************************************/
include_once("../conexion/conexion.php");
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=consultaTramiteCargo.xls");
	
$anho = date("Y");
	$datomes = date("m");
	$datomes = $datomes*1;
	$datodia = date("d");
	$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre");
	
	echo "<table width=780 border=0><tr><td align=center colspan=12>";
	echo "<H3>REPORTE - DOCUMENTOS DE SALIDAS MULTIPLES</H3>";
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=right colspan=12>";
	echo "SITD, ".$datodia." ".$meses[$datomes].' del '.$anho;
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=left colspan=7>";
	$sqllog="select cNombresTrabajador, cApellidosTrabajador from tra_m_trabajadores where iCodTrabajador='$traRep' "; 
	$rslog=mssql_query($sqllog,$cnx);
	$Rslog=MsSQL_fetch_array($rslog);
	echo "GENERADO POR : ".$Rslog[cNombresTrabajador]." ".$Rslog[cApellidosTrabajador];
	echo " ";
	?>
							<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="center">
                            <thead>
                                <tr>
                                    <th colspan="9" style="text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">DOCUMENTOS ENVIADOS</th>
                                    <th colspan="3" style="text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">DEVOLUCION DE CARGOS</th>
                                </tr>
                            	<tr>
                                    <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">DEPARTAMENTO</th>
        <? $sqlOfis="SELECT iCodOficina,cSiglaOficina FROM Tra_M_Oficinas WHERE iCodOficina IN (SELECT DISTINCT iCodOficina  FROM Tra_M_Doc_Salidas_Multiples )"; 		   
		$rsOfis=mssql_query($sqlOfis,$cnx);
		    while ($RsOfis=MsSQL_fetch_array($rsOfis)){
		   ?>
                     <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8"><? echo $RsOfis[cSiglaOficina];?></th>
          <?   }  ?>                      
                      <th style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">DEPART.</th>
								
								</tr>
							</thead>
							<tbody>
							<?
			if($_GET[fHasta]!=""){
    $fDesde=date("Ymd", strtotime($_GET[fDesde]));
	$fHasta=date("d-m-Y", strtotime($_GET[fHasta]));
	function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    $date_r = getdate(strtotime($date));
    $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
    return $date_result;
				}
	$fHasta=dateadd($fHasta,1,0,0,0,0,0); // + 1 dia
	}
		?>
         <? $sqlDep="SELECT cCodDepartamento,cNomDepartamento FROM Tra_U_Departamento"; 		  
				   $rsDep=mssql_query($sqlDep,$cnx);
		    while ($RsDep=MsSQL_fetch_array($rsDep)){
		   ?>
			  <tr>
                
				      <td style="width:8%;text-align:center;border: solid 1px #6F6F6F;font-size:10px;"><? echo $RsDep[cNomDepartamento];?></td>
                        
      <? $sqlOfis2=" SELECT iCodOficina FROM Tra_M_Doc_Salidas_Multiples WHERE iCodOficina='$RsOfis[iCodOficina]' 
	  					AND cDepartamento='$RsDep[cCodDepartamento]'"; 		   
						
						$rsOfis2=mssql_query($sqlOfis2,$cnx);
		    while ($RsOfis2=MsSQL_fetch_array($rsOfis2)){
		   ?>
                     <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">
					 <? $rsContar=mssql_query("SELECT dbo.fn_Rep_Cargo(RsOfis2[iCodOficina],RsDep[cCodDepartamento],0,0)",$cnx); 
					 	$RsContar=MsSQL_fetch_array($rsContar);
						echo $RsContar[0];
					 ?></th>
          <?   }  ?>                      
							       
                       
						  </tr>
			  <?   }  ?> 			
						  </tbody>
							 </table>  