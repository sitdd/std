<?php
session_start();
ob_start();
include_once("../conexion/conexion.php");
?>
<page backtop="15mm" backbottom="15mm" backleft="10mm" backright="10mm">
	<page_header>
		<table style="width: 1000px; border: solid 0px black;">
			<tr>
				<td style="text-align:left;	width: 20px"></td>
				<td style="text-align:left;	width: 980px">
					<img style="width: 280px" src="images/pdf_apci.jpg" alt="Logo">
				</td>
			</tr>
		</table>
		<br><br>
	</page_header>
	<page_footer>
		<table style="width: 100%; border: solid 0px black;">
			<tr>
				<td style="text-align: center;	width: 40%">
					<?php 
				  	 $sqllog = "SELECT cNombresTrabajador, cApellidosTrabajador FROM tra_m_trabajadores 
				   						  WHERE iCodTrabajador='$_SESSION[CODIGO_TRABAJADOR]' "; 
				   	$rslog  = mssql_query($sqllog,$cnx);
				   	$Rslog  = mssql_fetch_array($rslog);
				   	echo $Rslog[cNombresTrabajador]." ".$Rslog[cApellidosTrabajador];
					?>
				</td>
				<td style="text-align: right;	width: 60%">página [[page_cu]]/[[page_nb]]</td>
			</tr>
		</table>
    <br><br>
	</page_footer>
	
	<table style="width: 1000px; border: solid 0px black;">
		<tr>
			<td style="text-align:center;width:1000px">
				<span style="font-size: 15px; font-weight: bold">Lista de Documentos por Enviar por Oficinas</span>
			</td>
		</tr>
	</table>
	<br><br>
		<?php
			$fecini = $_GET['fecini'];
			$fecfin = $_GET['fecfin'];

			if ($fecini != ''){
				$fecini = date("Ymd", strtotime($fecini));
			}
   		if ($fecfin != ''){
    		$fecfin = date("Y-m-d", strtotime($fecfin));
				function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    			$date_r = getdate(strtotime($date));
    			$date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
    			return $date_result;
				}
				$fecfin = dateadd($fecfin,1,0,0,0,0,0); // + 1 dia
			}
			
			$sql = " SP_CONSULTA_DOC_ENTRADA_EXTERNO '$fecini','$fecfin','%$_GET[cCodificacion]%','%$_GET[cReferencia]%','%$_GET[cAsunto]%','$_GET[cCodTipoDoc]','%$_GET[cNombre]%','%$_GET[cNomRemite]%','%$_GET[cNroDocumento]%','$campo','$orden',$_SESSION[CODIGO_TRABAJADOR]";
   		$rs = mssql_query($sql,$cnx);
   ?>
  <table style="width: 1000px; border: solid 0px black;">
  	<tr>
  		<td style="text-align:left;width:1000px"><br>
  			<span style="font-size: 15px; font-weight: bold"><?=$RsOfis[cNomOficina]?></span>
  		</td>
		</tr>
	</table>

	<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="center">
		<thead>
			<tr>
				<th style="width: 120px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N&ordm; Documento</th>
				<th style="width: 130px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N&ordm; Referencia</th>
				<th style="width: 150px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Fecha Derivo</th>
				<th style="width: 300px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Asunto</th>
				<th style="width: 150px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Firma / Sello</th>
			</tr>
		</thead>
	<tbody>
		<?php                    
			while ($Rs = mssql_fetch_array($rs)){
		?>
		<tr>
			<td style="width:120px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;vertical-align:top">
				<?php echo $Rs[cCodificacion]; ?>
			</td>

			<td style="width:130px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase;vertical-align:top">
				<?php echo $Rs[cNroDocumento]; ?>
			</td>

			
			
			<td style="width:150px;text-align:center;border: solid 1px #6F6F6F;font-size:10px;vertical-align:top">
				<?php
					if ($Rs[nFlgEnvio] == 1){
						$sqlM = "SELECT TOP 1 * FROM Tra_M_Tramite_Movimientos WHERE iCodTramite='$Rs[iCodTramite]'";
      			$rsM  = mssql_query($sqlM,$cnx);
	    			$RsM  = mssql_fetch_array($rsM);
						//echo date("d-m-Y G:i", strtotime($RsM[fFecDerivar]));
						echo date("d-m-Y G:i:s", strtotime($RsM[fFecDerivar]));
					}
				?>
			</td>
			
			<td style="width:300px;text-align:justify; border: solid 1px #6F6F6F;font-size:10px;vertical-align:top">
				<?php
					echo $Rs[cAsunto];
					if ($Rs[iCodTupa] != ""){
						$sqlTup = "SELECT * FROM Tra_M_Tupa WHERE iCodTupa='$Rs[iCodTupa]'";
						$rsTup  = mssql_query($sqlTup,$cnx);
      			$RsTup  = mssql_fetch_array($rsTup);
      	?>
       	<br>
     		<?php 
     				echo $RsTup["cNomTupa"];
					}
				?>
			</td>
			
			<td style="width:150px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase">&nbsp;</td> 
		</tr>
		<?php
			}
		?>
	</tbody>
</table> 
</page>

<?php
	$content = ob_get_clean();  set_time_limit(0);     ini_set('memory_limit', '640M');

	// conversion HTML => PDF
	require_once(dirname(__FILE__).'/html2pdf/html2pdf.class.php');
	try
	{
		$html2pdf = new HTML2PDF('L','A4', 'es', false, 'UTF-8', 3);
		$html2pdf->pdf->SetDisplayMode('fullpage');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output('exemple03.pdf');
	}
	catch(HTML2PDF_exception $e) { echo $e; }
?>   
         		
