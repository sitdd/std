<?php
session_start();
if($_SESSION['CODIGO_TRABAJADOR']!=""){
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv=Content-Type content=text/html; charset=utf-8>
<title>SITDD</title>

<link type="text/css" rel="stylesheet" href="css/tramite.css" media="screen" />
    <?php include("includes/head.php"); ?>
</head>
<body>
<?php include("includes/menu.php"); ?>


					<!--Main layout-->
 <main class="mx-lg-5">
     <div class="container-fluid">
          <!--Grid row-->
         <div class="row wow fadeIn justify-content-center">
              <!--Grid column-->
             <div class="col-11 col-sm-6 col-md-4">
                  <!--Card-->
                 <div class="card">
                      <!-- Card header -->
                     <div class="card-header text-center ">CAMBIO DE CONTRASEÑA DEL TRABAJADOR</div>
                      <!--Card content-->
                     <div class="card-body">
                         <div class="row justify-content-center">
                                     <form class="col-12" method="POST" name="formulario" action="../cAccesoBaseDato_SITD/ad_actualiza_key.php" target="_parent">
                                         <input type="hidden" name="usr" value="<?=trim($_GET[usr])?>">
                                         <input type="hidden" name="cod" value="<?=trim($_GET[cod])?>">
                                         <input type="hidden" name="iCodTrabajador" value="<?=$Rs[iCodTrabajador];?>">
                                         <input type="hidden" name="cNombreTrabajadorx" 		value="<?=$cNombreTrabajador?>">
                                         <input type="hidden" name="cApellidosTrabajadorx" 	value="<?=$cApellidosTrabajador?>">
                                         <input type="hidden" name="cTipoDocIdentidadx"		value="<?=$cTipoDocIdentidad?>">
                                         <input type="hidden" name="cNumDocIdentidadx" 		value="<?=$cNumDocIdentidad?>">
                                         <input type="hidden" name="iCodOficinax"			value="<?=$iCodOficina?>">
                                         <input type="hidden" name="iCodPerfilx" 			value="<?=$iCodPerfil?>">
                                         <input type="hidden" name="iCodCategoriax" 			value="<?=$iCodCategoria?>">
                                         <input type="hidden" name="txtestadox" 				value="<?=$txtestado?>">
                                         <input type="hidden" name="pagx" 					value="<?=$pag?>">

                                         <div class="row justify-content-center px-3 px-xl-1">
                                             <div class="col-11 col-xl-4">
                                                 <div class="md-form">
                                                     <input type="text" id="usuario" name="usuario" class="form-control" value="<?=trim($_GET[usr])?>" disabled style="border-bottom: none!important;">
                                                     <label for="usuario">Usuario</label>
                                                 </div>
                                             </div>
                                             <div class="col-11 col-xl-6">
                                                 <div class="md-form">
                                                     <input type="password" id="contrasena" name="contrasena" class="form-control" >
                                                     <label for="contrasena">Nueva Contraseña</label>
                                                     <a id="nuevoboton" style="float: right;color: #007bff!important;" onclick="mostrar()"><i class="far fa-eye prefix"></i></a>
                                                 </div>
                                             </div>
                                         </div>
                                         <div class="row justify-content-center">
                                             <div class="col-">
                                                 <button class="btn botenviar" type="submit" onclick="ingresar()" >Actualizar</button>
                                             </div>
                                         </div>
                                     </form>
                                 </div>
					</div>
                 </div>
             </div>
         </div>
     </div>
 </main>

<script>
    function ingresar() {
        document.datos.submit();
    }
    function mostrar() {
        document.getElementById("contrasena").setAttribute('type','text');
        document.getElementById('nuevoboton').setAttribute('onclick','nomostrar()');
        document.getElementById('nuevoboton').innerHTML = '<i class="far fa-eye-slash prefix"></i>';
    }
    function nomostrar() {
        document.getElementById('contrasena').setAttribute('type','password');
        document.getElementById('nuevoboton').setAttribute('onclick','mostrar()');
        document.getElementById('nuevoboton').innerHTML = '<i class="far fa-eye prefix"></i>';
    }
</script>

<?php include("includes/userinfo.php"); ?>
<?php include("includes/pie.php"); ?>
</body>
</html>
<?php
}else{
   header("Location: ../index.php?alter=5");
}
?>