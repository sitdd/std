<?php
session_start();
If($_SESSION['CODIGO_TRABAJADOR']!=""){
include_once("../conexion/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
<script>
function validar(f) {
 var error = "Por favor, antes de crear complete:\n\n";
 var a = "";
 
  if (f.cNombresTrabajador.value == "") {
  a += " Ingrese Nombre de Trabajador";
  alert(error + a);
 }
 else if (f.cApellidosTrabajador.value == "") {
  a += " Ingrese Apellidos de Trabajador";
  alert(error + a);
 } 

 else if (f.cUsuario.value == "") {
  a += " Ingrese el Usuario";
  alert(error + a);
 }
 else if (f.cPassword.value == "") {
  a += " Ingrese el Password";
  alert(error + a);
 }
 else if (f.txtestado.value == "") {
  a += " Seleccione Estado del Trabajador";
  alert(error + a);
 }
  
 return (a == "");
 
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.form1.cMailTrabajador.value)){
	} else {
		alert("Email incorrecto");
		document.form1.cMailTrabajador.focus();
		return false;
	}

 
}
 
function validarEmail(valor) {
	onclick="validarEmail(this.form1.cMailTrabajador.value);"
if (/^w+([.-]?w+)*@w+([.-]?w+)*(.w{2,3})+$/.test(valor)){
alert("La dirección de email " + valor + " es correcta.") 
return (true)
} else {
alert("La dirección de email " + valor + " es incorrecta.");
return (false);
}
}
</script>
</head>
<body>
<?include("includes/menu.php");?>

<style>
    .form-control{
        font-size: 0.8rem!important;
    }
    .md-form{
        margin-top: 1rem!important;
        margin-bottom: 0.8rem!important;
    }
    .dropdown-content li>a, .dropdown-content li>span {
        font-size: 0.8rem!important;
    }
    .select-wrapper .search-wrap {
        padding-top: 0rem!important;
        margin: 0 0.2rem!important;
    }
    .select-wrapper input.select-dropdown {
        font-size: 0.9rem!important;
    }
    label.select{
        margin-bottom: 0!important;
        font-size: 0.8rem!important;
    }
</style>

<!--Main layout-->
<main class="mx-lg-5">
    <div class="container-fluid">
        <!--Grid row-->
        <div class="row wow fadeIn">
            <!--Grid column-->
            <div class="col-md-12 mb-12">
                <!--Card-->
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header text-center ">NUEVO TRABAJADOR</div>
                    <!--Card content-->
                    <div class="card-body">
                        <?
                        require_once("../cAccesoBaseDato_SITD/ad_busqueda.php");
                        ?>

                        <form action="../cLogicaNegocio_SITD/ln_nuevo_trabajador.php" onSubmit="return validar(this)" method="post"  name="form1">
                            <div class="row justify-content-center">
                                <div class="col-12 col-md-8 col-lg-9 col-xl-7">
                                    <div class="card mb-3">
                                        <div class="card-header">Datos Personales</div>
                                        <div class="card-body px-4">
                                            <div class="row justify-content-around">
                                                <div class="col-12 col-sm-6">
                                                    <div class="md-form">
                                                        <input name="cNombresTrabajador" type="text" id="cNombresTrabajador" class="FormPropertReg form-control" value="<?=$_GET[cNombresTrabajador]?>">
                                                        <label for="cNombresTrabajador">Nombres</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="md-form">
                                                        <input name="cApellidosTrabajador" type="text" id="cApellidosTrabajador" class="FormPropertReg form-control" value="<?=$_GET[cApellidosTrabajador]?>">
                                                        <label for="cApellidosTrabajador">Apellidos</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <label class="select">Documento:&nbsp;</label>
                                                    <?php
                                                    $sqlDoc="select * from Tra_M_Doc_Identidad ";
                                                    $rsDoc=mssql_query($sqlDoc,$cnx);
                                                    ?>
                                                    <select name="cTipoDocIdentidad" class="FormPropertReg mdb-select colorful-select dropdown-primary"   searchable="Buscar aqui.."  id="cTipoDocIdentidad">
                                                        <option value="">Seleccione:</option>
                                                        <?php
                                                        while ($RsDoc=MsSQL_fetch_array($rsDoc)){
                                                            if($RsDoc["cTipoDocIdentidad"]==$_POST[cTipoDocIdentidad]){
                                                                $selecClas="selected";
                                                            }Else{
                                                                $selecClas="";
                                                            }
                                                            echo "<option value=".$RsDoc["cTipoDocIdentidad"]." ".$selecClas.">".$RsDoc["cDescDocIdentidad"]."</option>";
                                                        }
                                                        mssql_free_result($rsDoc);
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-12 col-sm-6" style="margin-top: 1.3rem!important">
                                                    <div class="md-form">
                                                        <input name="cNumDocIdentidad" type="text" id="cNumDocIdentidad" class="FormPropertReg form-control" value="<?=$_GET[cNumDocIdentidad]?>" onkeypress="if (event.keyCode > 31 && ( event.keyCode < 48 || event.keyCode > 57)) event.returnValue = false;">
                                                        <label for="cNumDocIdentidad">N° Doc.</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 ">
                                                    <div class="md-form">
                                                        <input name="cDireccionTrabajador" type="text" id="cDireccionTrabajador" class="FormPropertReg form-control" value="<?=$_GET[cDireccionTrabajador]?>">
                                                        <label for="cDireccionTrabajador">Direccion</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-lg-5">
                                                    <div class="md-form">
                                                        <input name="cMailTrabajador" type="text" id="cMailTrabajador" class="FormPropertReg form-control" value="<?=$_GET[cMailTrabajador]?>">
                                                        <label for="cMailTrabajador">E-mail</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-lg-3">
                                                    <div class="md-form">
                                                        <input name="cTlfTrabajador1" type="text" id="cTlfTrabajador1" class="FormPropertReg form-control" value="<?=$_GET[cTlfTrabajador1]?>">
                                                        <label for="cTlfTrabajador1">Telefono 1</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-lg-3">
                                                    <div class="md-form">
                                                        <input name="cTlfTrabajador2" type="text" id="cTlfTrabajador2" class="FormPropertReg form-control" value="<?=$_GET[cTlfTrabajador2]?>">
                                                        <label for="cTlfTrabajador2">Telefono 2</label>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <label class="select">Oficina:&nbsp;</label>
                                                    <?php
                                                    //Consulta para rellenar el combo Oficina
                                                    $sqlOfi="SP_OFICINA_LISTA_COMBO ";
                                                    $rsOfi=mssql_query($sqlOfi,$cnx);
                                                    ?>
                                                    <select name="iCodOficina" class="FormPropertReg mdb-select colorful-select dropdown-primary"   searchable="Buscar aqui.."  id="iCodOficina">
                                                        <option value="">Seleccione:</option>
                                                        <?php
                                                        while ($RsOfi=MsSQL_fetch_array($rsOfi)){
                                                            if($RsOfi["iCodOficina"]==$_POST[iCodOficina]){
                                                                $selecClas="selected";
                                                            }Else{
                                                                $selecClas="";
                                                            }
                                                            echo utf8_encode("<option value=".$RsOfi["iCodOficina"]." ".$selecClas.">".$RsOfi["cNomOficina"]."</option>");
                                                        }
                                                        mssql_free_result($rsOfi);
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <label class="select">Categoria:&nbsp;</label>
                                                    <?php
                                                    //Consulta para rellenar el combo Categoria
                                                    $sqlCat="select * from Tra_M_Categoria ";
                                                    $rsCat=mssql_query($sqlCat,$cnx);
                                                    ?>
                                                    <select name="iCodCategoria" class="FormPropertReg mdb-select colorful-select dropdown-primary"   searchable="Buscar aqui.."  id="iCodCategoria">
                                                        <option value="">Seleccione:</option>
                                                        <?php
                                                        while ($RsCat=MsSQL_fetch_array($rsCat)){
                                                            if($RsCat["iCodCategoria"]==$_POST[iCodCategoria] || $RsCat["iCodCategoria"]=="Sin Categoria" ){
                                                                $selecClas="selected";
                                                            }Else{
                                                                $selecClas="";
                                                            }
                                                            echo "<option value=".$RsCat["iCodCategoria"]." ".$selecClas.">".$RsCat["cDesCategoria"]."</option>";
                                                        }
                                                        mssql_free_result($rsCat);
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <label class="select">Estado:&nbsp;</label>
                                                    <select name="txtestado" class="FormPropertReg mdb-select colorful-select dropdown-primary"   searchable="Buscar aqui.."  id="txtestado">
                                                        <option value="">Seleccione:</option>
                                                        <?
                                                        if ($Rs[nFlgEstado]==1){
                                                            echo "<OPTION value=1 selected>Activo</OPTION> ";
                                                        }
                                                        else{
                                                            echo "<OPTION value=1 selected>Activo</OPTION> ";
                                                        }
                                                        if ($Rs[nFlgEstado]==0){
                                                            echo "<OPTION value=0>Inactivo</OPTION> ";
                                                        }
                                                        else{
                                                            echo "<OPTION value=0>Inactivo</OPTION> ";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 col-md-4 col-lg-3 col-xl-3">
                                    <div class="card mb-3">
                                        <div class="card-header">Usuario</div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <label class="select">Perfil:&nbsp;</label>
                                                    <?php
                                                    //Consulta para rellenar el combo Perfil
                                                    $sqlPer="select * from Tra_M_Perfil ";
                                                    $rsPer=mssql_query($sqlPer,$cnx);
                                                    ?>
                                                    <select name="iCodPerfil" class="FormPropertReg mdb-select colorful-select dropdown-primary"   searchable="Buscar aqui.."  id="iCodPerfil">
                                                        <option value="">Seleccione:</option>
                                                        <?
                                                        while ($RsPer=MsSQL_fetch_array($rsPer)){
                                                            echo "<option value=".$RsPer["iCodPerfil"]." ".$selecClas.">".$RsPer["cDescPerfil"]."</option>";
                                                        }
                                                        mssql_free_result($rsPer);
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-12 col-sm-6  col-md-12 col-xl-6">
                                                    <div class="md-form">
                                                        <input name="cUsuario" type="text" id="cUsuario" class="FormPropertReg form-control" value="<?=$_GET[cUsuario]?>">
                                                        <?
                                                        if($_GET[cUsuario]!="") {echo "*";}
                                                        if ($mensaje=="1") {echo "<script> alert('El Nombre de Usuario ya Existe')</script>";}
                                                        ?>
                                                        <label for="cUsuario">Usuario</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6  col-md-12 col-xl-6">
                                                    <div class="md-form">
                                                        <input name="cPassword" type="text" id="cPassword" class="FormPropertReg form-control" value="<?=$_GET[cPassword]?>">
                                                        <label for="cPassword">Password</label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 col-xl-10">
                                    <div class="card mb-4">
                                        <div class="card-body px-0 pb-0">
                                            <div class="row justify-content-center">
                                                <div class="col- mx-4">
                                                    <button class="botenviar"  type="submit" id="Insert Trabajador"  onMouseOver="this.style.cursor='hand'">Crear</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                                <div class="col- mx-4">
                                                    <button class="botenviar" type="button" onclick="window.open('iu_trabajadores.php', '_self');" onMouseOver="this.style.cursor='hand'">Cancelar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include("includes/userinfo.php"); ?>
<?php include("includes/pie.php"); ?>
<script>
    $(document).ready(function() {
        $('.mdb-select').material_select();
    });
</script>
</body>
</html>

<?
}else{
   header("Location: ../index.php?alter=5");
}
?>
