<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: consultaEntradaGeneral_xls.php
SISTEMA: SISTEMA   DE TR�MITE DOCUMENTARIO DIGITAL
OBJETIVO: Reporte general en EXCEL
PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripci�n
------------------------------------------------------------------------
1.0   APCI    05/09/2018      Creaci�n del programa.
------------------------------------------------------------------------
*****************************************************************************************/
include_once("../conexion/conexion.php");
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=consultaEntradaGeneral.xls");
    
	$anho = date("Y");
	$datomes = date("m");
	$datomes = $datomes*1;
	$datodia = date("d");
	$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre");
	
	echo "<table width=780 border=0><tr><td align=center colspan=8>";
	echo "<H3>REPORTE - ENTRADAS GENERALES</H3>";
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=left colspan=8>";
	echo "SITD, ".$datodia." ".$meses[$datomes].' del '.$anho;
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=left colspan=8>";
	$sqllog="select cNombresTrabajador, cApellidosTrabajador from tra_m_trabajadores where iCodTrabajador='$traRep' "; 
	$rslog=mssql_query($sqllog,$cnx);
	$Rslog=MsSQL_fetch_array($rslog);
	echo "GENERADO POR : ".$Rslog[cNombresTrabajador]." ".$Rslog[cApellidosTrabajador];
	echo " ";
	
$sqlOfis="SELECT * FROM Tra_M_Oficinas ";
if($_GET[iCodOficina]!=""){
$sqlOfis.="WHERE iCodOficina='$_GET[iCodOficina]' ";
  }
$sqlOfis.="ORDER BY cNomOficina ASC ";
$rsOfis=mssql_query($sqlOfis,$cnx);

error_log("$fecini 1 ==>".$fecini." $fecini 2 ==>".$_GET[fecini]);
error_log("$fecfin 1 ==>".$fecfin." $fecfin 2 ==>".$_GET[fecfin]);
	if($_GET[fecini]!=''){$_GET[fecini]=date("Ymd", strtotime($_GET[fecini]));}
    if($_GET[fecfin]!=''){
    $_GET[fecfin]=date("Y-m-d", strtotime($_GET[fecfin]));
	function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    $date_r = getdate(strtotime($date));
    $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
    return $date_result;
				}
	$_GET[fecfin]=dateadd($_GET[fecfin],0,0,0,0,0,0); // + 1 dia
	}
while ($RsOfis=MsSQL_fetch_array($rsOfis)){	
		$sql1="SELECT * FROM Tra_M_Tramite ";
  		$sql1.=" LEFT OUTER JOIN Tra_M_Tramite_Movimientos ON Tra_M_Tramite.iCodTramite=Tra_M_Tramite_Movimientos.iCodTramite ";
  		if($_GET[cNombre]!=""){
  		$sql1.=" LEFT OUTER JOIN Tra_M_Remitente ON Tra_M_Tramite.iCodRemitente=Tra_M_Remitente.iCodRemitente ";
			}
  		$sql1.=" WHERE (Tra_M_Tramite.nFlgTipoDoc=1 OR Tra_M_Tramite.nFlgTipoDoc=4)  And iCodOficinaOrigen=1 AND (Tra_M_Tramite_Movimientos.iCodOficinaDerivar='$RsOfis[iCodOficina]' and Tra_M_Tramite_Movimientos.iCodOficinaDerivar!=1) ";
			if($_GET[fecini]!="" AND $_GET[fecfin]==""){
  			$sql1.=" AND Tra_M_Tramite.fFecRegistro>'$_GET[fecini]' ";
  		}
  		if($_GET[fDesde]=="" AND $_GET[fHasta]!=""){
  			$sql1.=" AND Tra_M_Tramite.fFecRegistro<='$_GET[fecfin]' ";
  		}
  		if($_GET[fDesde]!="" && $_GET[fHasta]!=""){
  			$sql1.=" AND Tra_M_Tramite.fFecRegistro>'$_GET[fecini]' and Tra_M_Tramite.fFecRegistro<'$_GET[fecfin]' ";
  		}
			if($_GET[cCodificacion]!=""){
  		  $sql1.="AND Tra_M_Tramite.cCodificacion like '%$_GET[cCodificacion]%' ";
  		}
  		if($_GET[cNroDocumento]!=""){
     		$sql1.="AND Tra_M_Tramite.cNroDocumento like '%$_GET[cNroDocumento]%' ";
  		}
			if($_GET[cReferencia]!=""){
  		   $sql1.="AND Tra_M_Tramite.cReferencia LIKE '%$_GET[cReferencia]%' ";
  		}
  		if($_GET[cAsunto]!=""){
  		  $sql1.="AND Tra_M_Tramite.cAsunto LIKE '%$_GET[cAsunto]%' ";
  		}
			if($_GET[iCodTupa]!=""){
  		  $sql1.="AND Tra_M_Tramite.iCodTupa='$_GET[iCodTupa]' ";
  		}
			if($_GET[cCodTipoDoc]!=""){
  		  $sql1.="AND Tra_M_Tramite.cCodTipoDoc='$_GET[cCodTipoDoc]' ";
  		}
  		if($_GET[cNombre]!=""){
  		  $sql1.="AND Tra_M_Remitente.cNombre LIKE '%$_GET[cNombre]%' ";
  		}
  		$sql1.= " ORDER BY Tra_M_Tramite.iCodTramite DESC";
		error_log("SQL ==>".$sql1);
  		$rs1=mssql_query($sql1,$cnx);
  		If(MsSQL_num_rows($rs1)>0){
			?>
							<table style="width: 1000px; border: solid 0px black;">
							<tr>
							<td style="text-align:left;width:1000px" colspan="6"><br>&nbsp;<br><span style="font-size: 15px; font-weight: bold"><?=$RsOfis[cNomOficina]?></span></td>
							</tr>
							</table>
						
							<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="center">
							<thead>
								<tr>
									<th style="width: 120px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N&ordm; Documento</th>
									<th style="width: 130px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N&ordm; Referencia</th>
									<th style="width: 150px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Instituci&oacute;n</th>
									<th style="width: 150px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Destinatario</th>
									<th style="width: 150px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Fecha Derivo</th>
									<th style="width: 300px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Asunto</th>
									<th style="width: 150px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Firma / Sello</th>
								</tr>
							</thead>
							<tbody>
							<?
							
							$sql="SELECT * FROM Tra_M_Tramite ";
						  $sql.=" LEFT OUTER JOIN Tra_M_Tramite_Movimientos ON Tra_M_Tramite.iCodTramite=Tra_M_Tramite_Movimientos.iCodTramite ";
						  if($_GET[cNombre]!=""){
						  $sql.=" LEFT OUTER JOIN Tra_M_Remitente ON Tra_M_Tramite.iCodRemitente=Tra_M_Remitente.iCodRemitente ";
							}
						  $sql.=" WHERE (Tra_M_Tramite.nFlgTipoDoc=1 OR Tra_M_Tramite.nFlgTipoDoc=4)  And iCodOficinaOrigen=1 AND ( Tra_M_Tramite_Movimientos.iCodOficinaDerivar='$RsOfis[iCodOficina]' and Tra_M_Tramite_Movimientos.iCodOficinaDerivar!=1) ";
							if($_GET[fecini]!="" AND $_GET[fecfin]==""){
						  	$sql.=" AND Tra_M_Tramite.fFecRegistro>'$_GET[fecini]' ";
						  }
						  if($_GET[fecini]=="" AND $_GET[fecfin]!=""){
						  	$sql.=" AND Tra_M_Tramite.fFecRegistro<='$_GET[fecfin]' ";
						  }
						  if($_GET[fecini]!="" && $_GET[fecfin]!=""){
						  	$sql.=" AND Tra_M_Tramite.fFecRegistro>'$_GET[fecini]' and Tra_M_Tramite.fFecRegistro<'$_GET[fecfin]' ";
						  }
							if($_GET[cCodificacion]!=""){
						    $sql.="AND Tra_M_Tramite.cCodificacion like '%$_GET[cCodificacion]%' ";
						  }
						  if($_GET[cNroDocumento]!=""){
     						$sql.="AND Tra_M_Tramite.cNroDocumento like '%$_GET[cNroDocumento]%' ";
  						}
							if($_GET[cReferencia]!=""){
						     $sql.="AND Tra_M_Tramite.cReferencia LIKE '%$_GET[cReferencia]%' ";
						  }
						  if($_GET[cAsunto]!=""){
						    $sql.="AND Tra_M_Tramite.cAsunto LIKE '%$_GET[cAsunto]%' ";
						  }
							if($_GET[iCodTupa]!=""){
						    $sql.="AND Tra_M_Tramite.iCodTupa='$_GET[iCodTupa]' ";
						  }
							if($_GET[cCodTipoDoc]!=""){
						    $sql.="AND Tra_M_Tramite.cCodTipoDoc='$_GET[cCodTipoDoc]' ";
						  }
						  if($_GET[cNombre]!=""){
						    $sql.="AND Tra_M_Remitente.cNombre LIKE '%$_GET[cNombre]%' ";
						  }
						  $sql.= " ORDER BY Tra_M_Tramite.iCodTramite DESC";
							$rs=mssql_query($sql,$cnx);
							while ($Rs=MsSQL_fetch_array($rs)){
							?>
							 <tr>
						      <td style="width:120px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;vertical-align:top"><?=$Rs[cCodificacion]?></td>
						      <td style="width:130px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase;vertical-align:top"><?=$Rs[cNroDocumento]?></td>
						      <td style="width:150px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase;vertical-align:top">
						      	<?
						      	$sqlRemi="SELECT * FROM Tra_M_Remitente WHERE iCodRemitente='$Rs[iCodRemitente]'";
												$rsRemi=mssql_query($sqlRemi,$cnx);
												$RsRemi=MsSQL_fetch_array($rsRemi);
												echo "<div>".$RsRemi[cNombre]."</div>";
												/*	if($Rs[cNomRemite]!=""){
														if($RsRemi[cTipoPersona]==1){ echo "<div style=\"color:#408080\">Personal Natural:</div>"; }
													}
								echo "<div style=\"text-transform:uppercase\">".$Rs[cNomRemite]."</div>";*/
      							if($Rs[nFlgTipoDoc]==4){
      									echo "<div>ANEXO</div>";
      							}
						      	?>
                              </td>
							  <td style="width:150px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase;vertical-align:top">
						      	<?
						      		if($Rs[cNomRemite]!=""){
									if($RsRemi[cTipoPersona]==1){ echo "<div style=\"color:#408080\">Personal Natural:</div>"; }
													}
									echo "<div style=\"text-transform:uppercase\">".$Rs[cNomRemite]."</div>";
      							
						      	?>
                              </td>
						      <td style="width:150px;text-align:center;border: solid 1px #6F6F6F;font-size:10px;vertical-align:top">
						      	<?
						      	if($Rs[nFlgEnvio]==1){
						      		$sqlM="select TOP 1 * from Tra_M_Tramite_Movimientos WHERE iCodTramite='$Rs[iCodTramite]'";
      								$rsM=mssql_query($sqlM,$cnx);
	    								$RsM=MsSQL_fetch_array($rsM);
						      		echo date("d-m-Y G:i", strtotime($RsM[fFecDerivar]));
						      	}
						      	?>
						      </td>
						      <td style="width:300px;text-align:justify; border: solid 1px #6F6F6F;font-size:10px;vertical-align:top">
							  <? echo $Rs[cAsunto];
							   if($Rs[iCodTupa]!=""){
    							$sqlTup="SELECT * FROM Tra_M_Tupa WHERE iCodTupa='$Rs[iCodTupa]'";
      							$rsTup=mssql_query($sqlTup,$cnx);
      							$RsTup=MsSQL_fetch_array($rsTup);
								?>
       							 <br>
     							 <? echo $RsTup["cNomTupa"];
							  }?></td>
						      <td style="width:150px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase">&nbsp;</td> 
						  </tr>
						  <?}?>
						  </tbody>
							 </table>  
						<?
						}
				}
				?>