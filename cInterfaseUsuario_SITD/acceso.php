<?
session_start();
If($_SESSION['CODIGO_TRABAJADOR']!=""){
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript" language="javascript" src="includes/lytebox.js"></script>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
<link type="text/css" rel="stylesheet" href="css/dhtmlgoodies_calendar.css" media="screen">
<script type="text/javascript" src="scripts/dhtmlgoodies_calendar.js"></script>
</head>
<body>

<?include("includes/menu.php");?>
<?
require_once("../conexion/conexion.php");
$sql= "select * from Tra_M_Trabajadores where iCodTrabajador='$_SESSION[CODIGO_TRABAJADOR]'";
$rs=mssql_query($sql,$cnx);
$Rs=MsSQL_fetch_array($rs);
?>

<style>

</style>

<!--Main layout-->
<main class="mx-lg-5">
    <div class="container-fluid">
        <!--Grid row-->
        <div class="row wow fadeIn justify-content-center">
            <!--Grid column-->
            <div class="col-11 col-sm-6 col-md-4">
                <!--Card-->
                <div class="card">
                    <!--Card header -->
                    <div class="card-header text-center ">
                        ACTUALIZAR CONTRASEÑA
                    </div>
                    <!--Card content-->
                    <div class="card-body">
                        <div class="row justify-content-center">
                                <form class="col-12" action="../cAccesoBaseDato_SITD/ad_trabajadores_data.php" method="post"  name="datos">
                                    <input type="hidden" name="opcion" value="3">
                                    <div class="row justify-content-center px-3 px-xl-1">
                                            <div class="col-11 col-xl-4">
                                                <div class="md-form">
                                                    <input type="text" id="usuario" name="usuario" class="form-control" value="<?php echo $Rs['cUsuario'];?>" disabled style="border-bottom: none!important;">
                                                    <input name="cUsuario" type="hidden" value="<? echo trim($Rs['cUsuario']); ?>">
                                                    <label for="usuario">Usuario</label>
                                                </div>
                                            </div>
                                            <div class="col-11 col-xl-6">
                                                <div class="md-form">
                                                    <input type="password" id="nuevo" name="nuevo" class="form-control" placeholder="Nueva contraseña"  >
                                                    <label for="nuevo">Nueva Contraseña</label>
                                                    <a id="nuevoboton" style="float: right;color: #007bff!important;" onclick="mostrar()"><i class="far fa-eye prefix"></i></a>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="row justify-content-center">
                                            <div class="col-">
                                                <button class="btn botenviar" type="submit" onclick="ingresar()" >Actualizar</button>
                                            </div>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    function ingresar() {
        document.datos.submit();
    }
    function mostrar() {
        document.getElementById("nuevo").setAttribute('type','text');
        document.getElementById('nuevoboton').setAttribute('onclick','nomostrar()');
        document.getElementById('nuevoboton').innerHTML = '<i class="far fa-eye-slash prefix"></i>';
    }
    function nomostrar() {
        document.getElementById('nuevo').setAttribute('type','password');
        document.getElementById('nuevoboton').setAttribute('onclick','mostrar()');
        document.getElementById('nuevoboton').innerHTML = '<i class="far fa-eye prefix"></i>';
    }
</script>


<?include("includes/userinfo.php");?>
<?include("includes/pie.php");?>


</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>