<?php header('Content-Type: text/html; charset=UTF-8');
session_start();
if($_SESSION['CODIGO_TRABAJADOR']!=""){
include_once("../conexion/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv=Content-Type content=text/html; charset=utf-8>
<title>SITDD</title>

<link type="text/css" rel="stylesheet" href="css/detalle.css" media="screen" />

<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
<script language="javascript" type="text/javascript">
    function muestra(nombrediv) {
        if(document.getElementById(nombrediv).style.display == '') {
                document.getElementById(nombrediv).style.display = 'none';
        } else {
                document.getElementById(nombrediv).style.display = '';
        }
    }
</script>
</head>
<body>
		<?
		$rs=mssql_query("SELECT * FROM Tra_M_Tramite WHERE iCodTramite='$_GET[iCodTramite]'",$cnx);
		$Rs=MsSQL_fetch_array($rs);
		?>

<!--Main layout-->
 <main class="mx-lg-5">
     <div class="container-fluid">
          <!--Grid row-->
         <div class="row wow fadeIn">
              <!--Grid column-->
             <div class="col-md-12 mb-12">
                  <!--Card-->
                 <div class="card">
                      <!-- Card header -->
                     <div class="card-header text-center ">
                         >>
                     </div>
                      <!--Card content-->
                     <div class="card-body">

<div class="AreaTitulo">DETALLE DE DOCUMENTO INTERNO - OFICINA</div>
<table cellpadding="0" cellspacing="0" border="0" width="910">
<tr>
<td class="FondoFormRegistro">
		
		<table width="880" border="0" align="center">
		<tr>
		<td>
				<fieldset id="tfa_GeneralDoc" class="fieldset">
				<legend class="legend"><a href="javascript:;" onClick="muestra('zonaGeneral')" class="LnkZonas">Datos Generales <img src="images/icon_expand.png" width="16" height="13" border="0"></a></legend>
		    <div id="zonaGeneral">
		    <table border="0" width="860">
		    <tr>
		        <td width="130" >Tipo de Documento:&nbsp;</td>
		        <td width="300">
		        		<? 
			          $sqlTipDoc="SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$Rs[cCodTipoDoc]'";
			          $rsTipDoc=mssql_query($sqlTipDoc,$cnx);
			          $RsTipDoc=MsSQL_fetch_array($rsTipDoc);
			          echo $RsTipDoc[cDescTipoDoc];
		            ?>
		        </td>
		        <td width="130" >Fecha:&nbsp;</td>
		        <td>
		        	<span><?echo date("d-m-Y G:i:s", strtotime($Rs[fFecRegistro]))?></span>
        			<span style=font-size:10px><?/*=date("h:i A", strtotime($Rs[fFecRegistro]))*/?></span>
		        </td>
		    </tr>

		    <tr>
		    	<td width="130" >N&ordm; Documento:&nbsp;</td>
		      <td style="text-transform:uppercase"><?=$Rs[cCodificacion]?></td>
		      <td width="130" >Digital:&nbsp;</td>
		      <td style="text-transform:uppercase">
		      	<?php
		        	$tramitePDF   = mssql_query("SELECT * FROM Tra_M_Tramite WHERE iCodTramite='$_GET[iCodTramite]'",$cnx);
  						$RsTramitePDF = mssql_fetch_object($tramitePDF);
							if ($RsTramitePDF->descripcion != NULL AND $RsTramitePDF->descripcion!=' ') {
            ?>
            <a href="pdf_digital.php?iCodTramite=<?php echo $RsTramitePDF->iCodTramite;?>
            				"target="_blank" title="Documento Electronico">
            	<img src="images/1471041812_pdf.png" border="0" height="17" width="17">
            </a>
            <? } ?>
            <?
		        	$sqlDw = "SELECT TOP 1 * FROM Tra_M_Tramite_Digitales WHERE iCodTramite='$_GET[iCodTramite]'";
      				$rsDw  = mssql_query($sqlDw,$cnx);
      				if (mssql_num_rows($rsDw) > 0){
      					$RsDw = mssql_fetch_array($rsDw);
      					if ($RsDw["cNombreNuevo"] != ""){
      						if (file_exists("../cAlmacenArchivos/".trim($Rs1[nombre_archivo]))){
										$sqlCondicion = " SP_CONDICION_REGISTRO_TRABAJADOR '$Rs[iCodOficinaRegistro]', '$_SESSION[iCodOficinaLogin]','$_SESSION[CODIGO_TRABAJADOR]' ";
										$rsCondicion = mssql_query($sqlCondicion,$cnx);
										$RsCondicion = mssql_fetch_array($rsCondicion);
										if ($RsCondicion["Total"] == "1"){
											echo "<a href=\"download.php?direccion=../cAlmacenArchivos/&file=".trim($RsDw["cNombreNuevo"])."\"><img src=images/icon_download.png border=0 width=16 height=16 alt=\"".trim($RsDw["cNombreNuevo"])."\"></a>";
										}else{
											$sqlCondicion2 = " SP_CONDICION_DOCUMENTO_REGISTRO '$Rs[iCodOficinaRegistro]' ";
											$rsCondicion2  = mssql_query($sqlCondicion2,$cnx);
											$RsCondicion2  = mssql_fetch_array($rsCondicion2);
											if($RsCondicion2["Total"]=="0"){
												echo "<a title=\"Documento Complementario\" href=\"download.php?direccion=../cAlmacenArchivos/&file=".trim($RsDw["cNombreNuevo"])."\"><img src=images/icon_download.png border=0 width=16 height=16 alt=\"".trim($RsDw["cNombreNuevo"])."\"></a>";
											}
										}
									}
								}
      				}else{
      					echo "<img src=images/space.gif width=16 height=16>";
      				}
						?>	
		      </td>
		    </tr>
	    
		    <tr>
		    	<td width="130"  valign="top">Asunto:&nbsp;</td>
		      <td width="300"><?=$Rs[cAsunto]?></td>
		      <td width="130"  valign="top">Observaciones:&nbsp;</td>
		      <td width="300"><?=$Rs[cObservaciones]?></td>
		    </tr>

		    <tr>
		        <td width="130" >Referencias:&nbsp;</td>
		        <td >
								<?
								$sqlRefs="SELECT * FROM Tra_M_Tramite_Referencias WHERE iCodTramite='$_GET[iCodTramite]'";
          			$rsRefs=mssql_query($sqlRefs,$cnx);
          			while ($RsRefs=MsSQL_fetch_array($rsRefs)){
          					$sqlTrRf="SELECT * FROM Tra_M_Tramite WHERE iCodTramite='$RsRefs[iCodTramiteRef]'";
          					$rsTrRf=mssql_query($sqlTrRf,$cnx);
          					$RsTrRf=MsSQL_fetch_array($rsTrRf);
          					switch ($RsTrRf[nFlgTipoDoc]){
  										case 1: $ScriptPHP="registroDetalles.php"; break;
  										case 2: $ScriptPHP="registroOficinaDetalles.php"; break;
  										case 3: $ScriptPHP="registroSalidaDetalles.php"; break;
  									}
								?>
								<span><a href="<?=$ScriptPHP?>?iCodTramite=<?=$RsTrRf[iCodTramite]?>"><?=trim($RsRefs[cReferencia])?></a></span>&nbsp;&nbsp;&nbsp;
								<?}?>								
		        </td>
                <td width="130"  valign="top">Estado:&nbsp;</td>
		        <td>
								<? 
								switch ($Rs[nFlgEstado]) {
  							case 1:
									echo "Pendiente";
								break;
								case 2:
									echo "En Proceso";
								break;
								case 3:
									echo "Finalizado";
									$sqlFinTxt="SELECT * FROM Tra_M_Tramite_Movimientos WHERE nEstadoMovimiento=5 And cFlgTipoMovimiento!=4 AND iCodTramite='$_GET[iCodTramite]'";
			            $rsFinTxt=mssql_query($sqlFinTxt,$cnx);
			            $RsFinTxt=MsSQL_fetch_array($rsFinTxt);
			            echo "<div style=color:#7C7C7C>".$RsFinTxt[cObservacionesFinalizar]."</div>";
			            echo "<div style=color:#0154AF>".date("d-m-Y", strtotime(substr($RsFinTxt[fFecFinalizar], 0, -6)))/*date("d-m-Y", strtotime($RsFinTxt[fFecFinalizar]))*/."</div>";
								break;
								}
								?>		        	
		        </td>
		    </tr>
			<tr>
		        <td width="130" >Doc. Principal:&nbsp;</td>
		        <td >
								<?
								 
	$rsTraDe=mssql_query("SELECT iCodTramite FROM Tra_M_Tramite_Movimientos WHERE iCodTramiteDerivar ='$Rs[iCodTramite]' and cFlgTipoMovimiento != 5 ",$cnx);
	$RsTraDe=mssql_fetch_array($rsTraDe);
	$rsTrax=mssql_query("SELECT nFlgTipoDoc,cCodificacion FROM Tra_M_Tramite WHERE iCodTramite ='$RsTraDe[iCodTramite]' ",$cnx);
	$RsTrax=mssql_fetch_array($rsTrax);
		switch ($RsTrax[nFlgTipoDoc]){
  				case 1: $ScriptPHP="registroDetalles.php"; break;
  				case 2: $ScriptPHP="registroOficinaDetalles.php"; break;
  				case 3: $ScriptPHP="registroSalidaDetalles.php"; break;
  				}
								?>
								<span><a href="<?=$ScriptPHP?>?iCodTramite=<?=$RsTraDe[iCodTramite]?>"><?=trim($RsTrax[cCodificacion])?></a></span>&nbsp;&nbsp;&nbsp;
																
		        </td>
                <td width="130"  valign="top">&nbsp;</td>
		        <td>        	
		        </td>
		    </tr>
				
		    </table>
		  	</div>
		  	<img src="images/space.gif" width="0" height="0">
				</fieldset>
		</td>
		</tr>
				
    <tr>
		<td>  
			<fieldset id="tfa_FlujoOfi" class="fieldset">
		  	<legend class="legend">
		  		<a href="javascript:;" onClick="muestra('zonaOficina')" class="LnkZonas">Flujo <img src="images/icon_expand.png" width="16" height="13" border="0"></a>
		  	</legend>
		    <div id="zonaOficina">
		    	<table border="0" align="center" width="860">
		    	<tr>
		       	<td class="headCellColum" width="100">Oficinas</td>
		       	<td class="headCellColum" width="200">Documento</td>
		       	<td class="headCellColum" width="300">Indicacion</td>
           	<td class="headCellColum" width="400">Responsable / Delegado</td>
           	<td class="headCellColum" width="120">Fecha Derivo</td>
           	<td class="headCellColum" width="120">Fecha de Aceptado</td>
		       	<td class="headCellColum" width="100">Estado</td>
           	<td width="106" class="headCellColum">Avance</td>
           	<td width="106" class="headCellColum">PDF</td>
          </tr>
		   	<?php 
		   		$sqlM = "SELECT * FROM Tra_M_Tramite_Movimientos 
		   						 WHERE (iCodTramite='$_GET[iCodTramite]' OR iCodTramiteRel='$_GET[iCodTramite]') AND 
		   						 			 (cFlgTipoMovimiento = 1 OR cFlgTipoMovimiento = 3 OR cFlgTipoMovimiento = 5) 
		   						 ORDER BY iCodMovimiento ASC";	
		   		//echo $sqlM; 
		   		$rsM  = mssql_query($sqlM,$cnx);
    
                $filapdfelectronico=0;
    
		    	while ($RsM = mssql_fetch_array($rsM)){
		    		if ($color == "#CEE7FF"){
		    			$color = "#F9F9F9";
		    		}else{
			  			$color = "#CEE7FF";
		  			}
		  			if ($color == ""){
			  			$color = "#F9F9F9";
		  			}	
				?>
		    <tr bgcolor="<?=$color?>">
		    	<td valign="top">

		    			<?php 
			    		 	echo "<table width=\"100\" border=\"0\"><tr>";
			    		 	$sqlOfiO = "SELECT * FROM Tra_M_Oficinas WHERE iCodOficina='$RsM[iCodOficinaOrigen]'";
				       	$rsOfiO  = mssql_query($sqlOfiO,$cnx);
				       	$RsOfiO  = mssql_fetch_array($rsOfiO);
			       	 	echo "<td width=90 align=right><a href=\"javascript:;\" title=\"".trim($RsOfiO[cNomOficina])."\">".$RsOfiO[cSiglaOficina]."</a></td>";
								echo "<td width=20>&nbsp;-&nbsp;</td>";
							 
			     	 	  $sqlOfiD = "SELECT * FROM Tra_M_Oficinas WHERE iCodOficina='$RsM[iCodOficinaDerivar]'";
				        $rsOfiD  = mssql_query($sqlOfiD,$cnx);
				        $RsOfiD  = mssql_fetch_array($rsOfiD);
			     	 	  echo "<td width=90 align=left><a href=\"javascript:;\" title=\"".trim($RsOfiD[cNomOficina])."\">".$RsOfiD[cSiglaOficina]."</a></td>";
			     	 	  echo "</tr></table>";
			     	 	?>
			    </td>
		    	<td valign="top" align="left" width="250">
		    		<?php
		       		if($RsM[cCodTipoDocDerivar]==''){
              			$sqlTipDoc1="SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$Rs[cCodTipoDoc]'";
					          $rsTipDoc1=mssql_query($sqlTipDoc1,$cnx);
					          $RsTipDoc1=MsSQL_fetch_array($rsTipDoc1);
			          		echo $RsTipDoc1[cDescTipoDoc]."<br>";
					echo "<a style=\"color:#0067CE\" href=\"registroOficinaDetalles2.php?iCodTramite=".$Rs[iCodTramite]."\" rel=\"lyteframe\" title=\"Detalle del Documento\" rev=\"width: 850px; height: 370px; scrolling: auto; border:no\">";
			          		echo $Rs[cCodificacion];
							echo "</a>";   
              }else{
              			$sqlTipDoc2="SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$RsM[cCodTipoDocDerivar]'";
					          $rsTipDoc2=mssql_query($sqlTipDoc2,$cnx);
					          $RsTipDoc2=MsSQL_fetch_array($rsTipDoc2);
			          		echo $RsTipDoc2[cDescTipoDoc]."<br>";
			          		echo "<a style=\"color:#0067CE\" href=\"registroOficinaDetalles2.php?iCodTramite=".$RsM[iCodTramiteDerivar]."\" rel=\"lyteframe\" title=\"Detalle del Documento\" rev=\"width: 850px; height: 370px; scrolling: auto; border:no\">";
							echo $RsM[cNumDocumentoDerivar];
							echo "</a>"; 							          	
              }		
			  		if($RsM[cFlgTipoMovimiento]==5){ echo $RsM[cReferenciaDerivar]; echo "<br/>";   echo "Referencia : Interno"; }
		       		?>
		    </td>
		    <td valign="top" align="left">
		       		<?  $sqlIndi=" SELECT * FROM Tra_M_Indicaciones WHERE iCodIndicacion = '$RsM[iCodIndicacionDerivar]'";
			 	$rsIndi=mssql_query($sqlIndi,$cnx);
              	$RsIndi=MsSQL_fetch_array($rsIndi);
              	echo $RsIndi["cIndicacion"];
              	mssql_free_result($rsIndi);
			 ?>
					
		    </td>
            <td valign="top" align="left">
             <?
		       	if($RsM[iCodTrabajadorDerivar]!=""){
		       	$sqlTrbR="SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsM[iCodTrabajadorDerivar]'";
              	$rsTrbR=mssql_query($sqlTrbR,$cnx);
              	$RsTrbR=MsSQL_fetch_array($rsTrbR);
              	echo $RsTrbR["cNombresTrabajador"]." ".$RsTrbR["cApellidosTrabajador"];
              	mssql_free_result($rsTrbR);
		       		}
					if($RsM[iCodTrabajadorDelegado]!=""){
  					$rsDelg=mssql_query("SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsM[iCodTrabajadorDelegado]'",$cnx);
          	$RsDelg=MsSQL_fetch_array($rsDelg);
          	echo "<div style=color:#005B2E;font-size:12px>".$RsDelg["cApellidosTrabajador"]." ".$RsDelg["cNombresTrabajador"]."</div>";
						mssql_free_result($rsDelg);
						echo "<div style=color:#0154AF>".date("d-m-Y G:i:s", strtotime(substr($RsM[fFecDelegado], 0, -6)))/*date("d-m-Y", strtotime($RsM[fFecDelegado]))." ".date("G:i", strtotime($RsM[fFecDelegado]))*/."</div>";
        			
					}	
		       		?>
        </td>
		    <td valign="top">
		    	<span><?echo date("d-m-Y G:i:s", strtotime($RsM[fFecDerivar]))?></span>
		    </td>
			<td align="center" valign="top">
      <?php
      	$sqlEstado = "SELECT iCodJefe,cNomJefe,FECHA_DOCUMENTO FROM Tra_M_Tramite WHERE iCodTramite = ".$RsM['iCodTramite'];
      	$rsEstado  = mssql_query($sqlEstado,$cnx);
      	$RsEstado  = mssql_fetch_array($rsEstado);

      	if($RsEstado['FECHA_DOCUMENTO'] == ""){
      		echo "<div style=color:#ff0000>sin aceptar</div>";
        }else{
       		echo "<div style=color:#0154AF>aceptado</div>";  
          $sqlfecha = "SELECT TOP 1 iCodMovimiento, iCodTramite,iCodOficinaOrigen,fFecRecepcion,iCodOficinaDerivar,
          													iCodTrabajadorDerivar,cCodTipoDocDerivar,cAsuntoDerivar,cObservacionesDerivar, 
          													fFecDerivar,iCodTrabajadorDelegado,fFecDelegado,nEstadoMovimiento,cFlgTipoMovimiento, 
          													cNumDocumentoDerivar,cReferenciaDerivar,iCodTramiteDerivar 
											 FROM Tra_M_Tramite_Movimientos 
											 WHERE (iCodTramite='$_GET[iCodTramite]' OR iCodTramiteRel='$_GET[iCodTramite]') 
															AND (cFlgTipoMovimiento=1 OR cFlgTipoMovimiento=3 OR cFlgTipoMovimiento=5) 
											 ORDER BY iCodMovimiento DESC";
					$rsFecha = mssql_query($sqlfecha,$cnx);
					$RsFecha = mssql_fetch_array($rsFecha);  
        	echo "<div style=color:#0154AF>".$RsFecha['fFecRecepcion']."</div>";
        }

     //  	if($RsM['fFecRecepcion'] == ""){
					// if($RsM['cFlgTipoMovimiento'] == 5){ 
					// 	echo "";
					// }else{
					// 	echo "<div style=color:#ff0000>sin aceptar</div>";
					// }
     //    }else{
     //   		echo "<div style=color:#0154AF>aceptado</div>";
     //    	echo "<div style=color:#0154AF>".date("d-m-Y G:i:s", strtotime(substr($RsM[fFecRecepcion], 0, -6)))."</div>";
     //    }
      ?>
        </td>
		    <td valign="top" align="center">
		    	<?php
		    		if($RsM[fFecRecepcion]!=""){
		     	 		switch ($RsM[nEstadoMovimiento]){
  							case 1:
									echo "En Proceso";
									break;
								case 2:
									echo "Derivado"; //movimiento derivado a otra oficina
									break;
								case 3:
									echo "Delegado";
									break;
								case 4:
									//echo "Respondido";
  								echo "<a style=\"color:#0067CE\" href=\"pendientesControlVerRpta.php?iCodMovimiento=".$RsM[iCodMovimiento]."\" rel=\"lyteframe\" title=\"Detalle Respuesta\" rev=\"width: 500px; height: 300px; scrolling: auto; border:no\">RESPONDIDO</a>";
  								break;
								case 5:
									echo "Finalizado";
									break;
							}
						}else{
						  if($RsM[cFlgTipoMovimiento]==5){ 
						  	echo "";
						 	}else{
								echo "Pendiente";
							}
						}
		     	?>
		    </td>
        <td>
					<?php
    				$sqlAvan = "SELECT TOP(1) * FROM Tra_M_Tramite_Avance WHERE iCodMovimiento='$RsM[iCodMovimiento]' ORDER BY iCodAvance DESC";
    				$rsAvan  = mssql_query($sqlAvan,$cnx);
						if(mssql_num_rows($rsAvan) > 0){
     					$RsAvan = mssql_fetch_array($rsAvan);
							echo "<hr>";
							echo "<div style=font-size:10px>".$RsAvan[cObservacionesAvance]."</div>";
						}
					?>
				</td>
				<td valign="top" align="center">
					<?php 
						//$sqlDigital = "SELECT * FROM Tra_M_Tramite_Digitales WHERE iCodTramite=".$_GET['iCodTramite'];
						// if ($_GET['iCodTramite'] != "") {
						// 	$sqlDigital_ = "SELECT * FROM Tra_M_Tramite_Digitales WHERE iCodTramite=".$_GET['iCodTramite'];
						// }else{
						// 	$sqlDigital_ = "SELECT * FROM Tra_M_Tramite_Digitales WHERE iCodMovimiento=".$RsM['iCodMovimiento'];
						// }
						if (isset($RsM['iCodTramiteDerivar'])) {
							$sqlDigital = "SELECT * FROM Tra_M_Tramite_Digitales WHERE iCodTramite=".$RsM['iCodTramiteDerivar'];
							$rsDigital  = mssql_query($sqlDigital,$cnx);
							$RsDigital  = mssql_fetch_array($rsDigital);
							if (isset($RsDigital['cNombreNuevo'])) {
							echo "<div><a href=\"download.php?direccion=../cAlmacenArchivos/&file=".$RsDigital['cNombreNuevo']."\"><img src=\"images/icon_download.png\" width=18 height=18 border=0 alt=\"Descargar Adjunto\"></a></div>";
							}
						}else{
							$sqlDigital = "SELECT * FROM Tra_M_Tramite_Digitales WHERE iCodTramite=".$_GET['iCodTramite'];
							$rsDigital  = mssql_query($sqlDigital,$cnx);
							$RsDigital  = mssql_fetch_array($rsDigital);
							if (isset($RsDigital['cNombreNuevo'])) {
							echo "<div><a href=\"download.php?direccion=../cAlmacenArchivos/&file=".$RsDigital['cNombreNuevo']."\"><img src=\"images/icon_download.png\" width=18 height=18 border=0 alt=\"Descargar Adjunto\"></a></div>";
							}
						}
                    
                        // DOCUMENTOS ELECTRONICOS  ---- max henrry
                        $filapdfelectronico+=1;
                        if($filapdfelectronico==1){
                            $RsM[iCodTramiteDerivar]=$_GET['iCodTramite'];
                        }
                        
                        $sqlPDF = "select * from Tra_M_Tramite where iCodTramite='".$RsM[iCodTramiteDerivar]."'";	
                        $rsPDF  = mssql_query($sqlPDF,$cnx);
                        $RsPDF  = mssql_fetch_array($rsPDF);
                    
                        if (strlen(rtrim(ltrim($RsPDF[descripcion])))>0) {
                            ?>
                                <a href="pdf_digital.php?iCodTramite=<?=$RsM[iCodTramiteDerivar]?>" title="Documento Electronico" 
                                rev="width: 970px; height: 550px; scrolling: auto; border:no" target="_blank">
                                <img src="images/1471041812_pdf.png" border="0" height="17" width="17">
                                </a>&nbsp;
                            <?php
                        }
                        // --------------- max henrry
	 				?>
				</td>
		  </tr> 
		  	<?php
		    	$contaMov++;
		    	}
		    ?>
		</table> 
	</div>
	<img src="images/space.gif" width="0" height="0"> 
	</fieldset>
		</td>
		</tr>

    <tr>
		<td>  
		  	<fieldset id="tfa_FlujoOfi" class="fieldset">
		  	<legend class="legend"><a href="javascript:;" onClick="muestra('zonaCopias')" class="LnkZonas">Copias <img src="images/icon_expand.png" width="16" height="13" border="0"></a></legend>
		    <div  id="zonaCopias">
		    <table border="0" align="center" width="860">
		    <tr>
		       <td class="headCellColum" width="100">Oficinas</td>
		       <td class="headCellColum" width="200">Documento</td>
		       <td class="headCellColum" width="400">Indicacion</td>
               <td class="headCellColum" width="400">Responsable / Delegado</td>
               <td class="headCellColum" width="120">Fecha Derivo</td>
               <td class="headCellColum" width="120">Fecha de Aceptado</td>
               <td class="headCellColum" width="100">Estado</td>
                <td width="106" class="headCellColum">Avance</td>
		    </tr>
		   	<?php 
		   		$sqlM = "SELECT * FROM Tra_M_Tramite_Movimientos 
		   						 WHERE (iCodTramite='$_GET[iCodTramite]' OR iCodTramiteRel='$_GET[iCodTramite]') AND 
		   						 			 (cFlgTipoMovimiento=4) 
		   						 ORDER BY iCodMovimiento ASC";
		   		$rsM  = mssql_query($sqlM,$cnx);
		    	while ($RsM = mssql_fetch_array($rsM)){
		      	if ($color == "#FFECEC"){
			  			$color = "#F9F9F9";
		  			}else{
			  			$color = "#FFECEC";
		  			}
		  			if ($color == ""){
			  			$color = "#F9F9F9";
		  			}	
				?>
		    <tr bgcolor="<?=$color?>">
		    <td valign="top">
		    			
		    		 <? 
		    		 echo "<table width=\"100\" border=\"0\"><tr>";
		    		 
		       	 $sqlOfiO="SELECT * FROM Tra_M_Oficinas WHERE iCodOficina='$RsM[iCodOficinaOrigen]'";
			       $rsOfiO=mssql_query($sqlOfiO,$cnx);
			       $RsOfiO=MsSQL_fetch_array($rsOfiO);
		       	 echo "<td width=90 align=right><a href=\"javascript:;\" title=\"".trim($RsOfiO[cNomOficina])."\">".$RsOfiO[cSiglaOficina]."</a></td>";
							
						 echo "<td width=20>&nbsp;-&nbsp;</td>";
						 
		     	 	 $sqlOfiD="SELECT * FROM Tra_M_Oficinas WHERE iCodOficina='$RsM[iCodOficinaDerivar]'";
			       $rsOfiD=mssql_query($sqlOfiD,$cnx);
			       $RsOfiD=MsSQL_fetch_array($rsOfiD);
		     	 	 echo "<td width=90 align=left><a href=\"javascript:;\" title=\"".trim($RsOfiD[cNomOficina])."\">".$RsOfiD[cSiglaOficina]."</a></td>";
		     	 	 
		     	 	 echo "</tr></table>";
		     	 	?>
		     	
		    </td>
            <td valign="top" align="left" width="250">
         		  <?
		       		if($RsM[cCodTipoDocDerivar]==''){
              			$sqlTipDoc1="SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$Rs[cCodTipoDoc]'";
					          $rsTipDoc1=mssql_query($sqlTipDoc1,$cnx);
					          $RsTipDoc1=MsSQL_fetch_array($rsTipDoc1);
			          		echo $RsTipDoc1[cDescTipoDoc]."<br>";
							echo "<a style=\"color:#0067CE\" href=\"registroOficinaDetalles2.php?iCodTramite=".$Rs[iCodTramite]."\" rel=\"lyteframe\" title=\"Detalle del Documento\" rev=\"width: 850px; height: 370px; scrolling: auto; border:no\">";
			          		echo $Rs[cCodificacion];
							echo "</a>";   
              }Else{
              			$sqlTipDoc2="SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$RsM[cCodTipoDocDerivar]'";
					          $rsTipDoc2=mssql_query($sqlTipDoc2,$cnx);
					          $RsTipDoc2=MsSQL_fetch_array($rsTipDoc2);
			          		echo $RsTipDoc2[cDescTipoDoc]."<br>";
			          		echo "<a style=\"color:#0067CE\" href=\"registroOficinaDetalles2.php?iCodTramite=".$RsM[iCodTramiteDerivar]."\" rel=\"lyteframe\" title=\"Detalle del Documento\" rev=\"width: 850px; height: 370px; scrolling: auto; border:no\">";
							echo $RsM[cNumDocumentoDerivar];
							echo "</a>";             	
              }
		       		?>
		    </td>
             <td valign="top" align="left">
             <?  $sqlIndi=" SELECT * FROM Tra_M_Indicaciones WHERE iCodIndicacion = '$RsM[iCodIndicacionDerivar]'";
			 	$rsIndi=mssql_query($sqlIndi,$cnx);
              	$RsIndi=MsSQL_fetch_array($rsIndi);
              	echo $RsIndi["cIndicacion"];
              	mssql_free_result($rsIndi);
			 ?>
            </td>		    	
		    <td valign="top" align="left">
		       		<?
		       		if($RsM[iCodTrabajadorDerivar]!=""){
		       			$sqlTrbR="SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsM[iCodTrabajadorDerivar]'";
              	$rsTrbR=mssql_query($sqlTrbR,$cnx);
              	$RsTrbR=MsSQL_fetch_array($rsTrbR);
              	echo $RsTrbR["cNombresTrabajador"]." ".$RsTrbR["cApellidosTrabajador"];
              	mssql_free_result($rsTrbR);
		       		}
						if($RsM[iCodTrabajadorDelegado]!=""){
  					$rsDelg=mssql_query("SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsM[iCodTrabajadorDelegado]'",$cnx);
          	$RsDelg=MsSQL_fetch_array($rsDelg);
          	echo "<div style=color:#005B2E;font-size:12px>".$RsDelg["cApellidosTrabajador"]." ".$RsDelg["cNombresTrabajador"]."</div>";
						mssql_free_result($rsDelg);
						echo "<div style=color:#0154AF>".date("d-m-Y G:i:s", strtotime(substr($RsM[fFecDelegado], 0, -6)))/*date("d-m-Y", strtotime($RsM[fFecDelegado]))." ".date("G:i", strtotime($RsM[fFecDelegado]))*/."</div>";
					}
		       		?>
		    </td>
           
		    <td valign="top">
		       		<span><?=date("d-m-Y", strtotime(substr($RsM[fFecDerivar], 0, -6)))/*date("d-m-Y", strtotime($RsM[fFecDerivar]))*/?></span>
		    </td>
			<td align="center" valign="top">
            <?
        	if($RsM[fFecRecepcion]==""){
        			echo "<div style=color:#ff0000>sin aceptar</div>";
        	}Else{
        			echo "<div style=color:#0154AF>aceptado</div>";
        			echo "<div style=color:#0154AF>".date("d-m-Y G:i:s", strtotime(substr($RsM[fFecRecepcion], 0, -6)))/*date("d-m-Y", strtotime($RsM[fFecRecepcion]))*/."</div>";
        			//echo "<div style=color:#0154AF;font-size:10px>".date("G:i", strtotime($RsM[fFecRecepcion]))."</div>";
        	}
        	?>
            </td>
		    <td valign="top" align="center">
		     	 		<?
		     	 		if($RsM[fFecRecepcion]==""){
		     	 			switch ($RsM[nEstadoMovimiento]) {
  							case 1:
									echo "Pendiente";
								break;
								case 2:
									echo "En Proceso"; //movimiento derivado a otra ofi
								break;
								case 3:
									echo "En Proceso"; //por delegar a otro trabajador
								break;
								case 4:
									echo "Respondido";
								break;
								case 5:
									echo "Finalizado";
								break;
								}
  				}Else if($RsM[fFecRecepcion]!=""){ 
						switch ($RsM[nEstadoMovimiento]) {
  							case 1:
									echo "En Proceso";
								break;
								case 2:
									echo "En Proceso"; //movimiento derivado a otra ofi
								break;
								case 3:
									echo "En Proceso"; //por delegar a otro trabajador
								break;
								case 4:
									echo "Respondido";
								break;
								case 5:
									echo "Finalizado";
								break;
								}  					
  				}
		     	 		?>
		    </td>
             <td>
<?
    $sqlAvan="SELECT TOP(1) * FROM Tra_M_Tramite_Avance WHERE iCodMovimiento='$RsM[iCodMovimiento]'  ORDER BY iCodAvance DESC";
    $rsAvan=mssql_query($sqlAvan,$cnx);
	if(mssql_num_rows($rsAvan)>0){
     $RsAvan=MsSQL_fetch_array($rsAvan);
		echo "<hr>";
		echo "<div style=font-size:10px>".$RsAvan[cObservacionesAvance]."</div>";
	}
?>	
</td>
		    </tr> 
		    <?
		    $contaMov++;
		    }
		    ?>
		    </table> 
		    </div>
		    <img src="images/space.gif" width="0" height="0"> 
		  	</fieldset>
		</td>
		</tr>
        
		<tr>
		<td>   
		  	<fieldset id="tfa_FlujoOfi" class="fieldset">
		  	<legend class="legend"><a href="javascript:;" onClick="muestra('zonaTrabajador')" class="LnkZonas">Flujo Entre Trabajadores <img src="images/icon_expand.png" width="16" height="13" border="0"></a></legend>
		    <div  id="zonaTrabajador">
		    <table border="0" align="center" width="860">
		    <tr>
		       <td class="headCellColum" width="80">Oficina</td>
		       <td class="headCellColum" width="160">Origen</td>
		       <td class="headCellColum" width="160">Destino</td>
		       <td class="headCellColum" width="110">Enviado</td>
		       <td class="headCellColum" width="300">Observaciones</td>
		    </tr>
		   	<? 
		   	$sqlMvTr="SELECT * FROM Tra_M_Tramite_Trabajadores WHERE iCodTramite='$Rs[iCodTramite]' ORDER BY iCodMovTrabajador ASC";
		   	$rsMvTr=mssql_query($sqlMvTr,$cnx);
		   	//echo $sqlM;
		    while ($RsMvTr=MsSQL_fetch_array($rsMvTr)){
		      	if ($color == "#CEE7FF"){
			  			$color = "#F9F9F9";
		  			}else{
			  			$color = "#CEE7FF";
		  			}
		  			if ($color == ""){
			  			$color = "#F9F9F9";
		  			}	
				?>
		    <tr bgcolor="<?=$color?>">
		    <td>

						<? 
		       	$sqlOfiO="SELECT * FROM Tra_M_Oficinas WHERE iCodOficina='$RsMvTr[iCodOficina]'";
			      $rsOfiO=mssql_query($sqlOfiO,$cnx);
			      $RsOfiO=MsSQL_fetch_array($rsOfiO);
		       	echo "<a href=\"javascript:;\" title=\"".trim($RsOfiO[cNomOficina])."\">".$RsOfiO[cSiglaOficina]."</a>";
		       	?>		    	
		    </td>
		    <td valign="top">
		       	<?
          	$rsMvTrOr=mssql_query("SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsMvTr[iCodTrabajadorOrigen]'",$cnx);
          	$RsMvTrOr=MsSQL_fetch_array($rsMvTrOr);
          	echo $RsMvTrOr["cApellidosTrabajador"]." ".$RsMvTrOr["cNombresTrabajador"];
						mssql_free_result($rsMvTrOr);
        		?>
		    </td>
		    <td valign="top">
		       	<?
          	$rsMvTrDs=mssql_query("SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsMvTr[iCodTrabajadorDestino]'",$cnx);
          	$RsMvTrDs=MsSQL_fetch_array($rsMvTrDs);
          	echo $RsMvTrDs["cApellidosTrabajador"]." ".$RsMvTrDs["cNombresTrabajador"];
						mssql_free_result($rsMvTrDs);
        		?>
		    </td>
		    <td valign="top">
		    		<span><?=date("d-m-Y H:i", strtotime(substr($RsMvTr[fFecEnvio], 0, -6)))/*date("d-m-Y H:i", strtotime($RsMvTr[fFecEnvio]))*/?></span>
		    </td>
		    <td valign="top" align="left"><?=$RsMvTr[cObservaciones]?></td>		       
		    </tr> 
		    <?}?>
		    </table>
		    </div>
		    <img src="images/space.gif" width="0" height="0"> 
		  	</fieldset>
		</td>
		</tr>		
<tr>
		<td>   
		  	<fieldset id="tfa_FlujoOfi" class="fieldset">
		  	<legend class="legend"><a href="javascript:;" onClick="muestra('zonaTrabajadorDel')" class="LnkZonas">Copias a Trabajadores <img src="images/icon_expand.png" width="16" height="13" border="0"></a></legend>
		    <div  id="zonaTrabajador">
		    <table border="0" align="center" width="860">
		    <tr>
		       <td class="headCellColum" width="80">Oficina</td>
		       <td class="headCellColum" width="160">Trabajador</td>
		       <td class="headCellColum" width="110">Delegado</td>
		       <td class="headCellColum" width="300">Observaciones</td>
		    </tr>
		   	<? 
		   	$sqlMvTr="SELECT * FROM Tra_M_Tramite_Movimientos WHERE iCodTramite='$Rs[iCodTramite]' And cFlgTipoMovimiento= 6 ORDER BY iCodMovimiento ASC";
		   	$rsMvTr=mssql_query($sqlMvTr,$cnx);
		   	//echo $sqlM;
		    while ($RsMvTr=MsSQL_fetch_array($rsMvTr)){
		      	if ($color == "#CEE7FF"){
			  			$color = "#F9F9F9";
		  			}else{
			  			$color = "#CEE7FF";
		  			}
		  			if ($color == ""){
			  			$color = "#F9F9F9";
		  			}	
				?>
		    <tr bgcolor="<?=$color?>">
		    <td>

						<? 
		       	$sqlOfiO="SELECT * FROM Tra_M_Oficinas WHERE iCodOficina='$RsMvTr[iCodOficinaDerivar]'";
			      $rsOfiO=mssql_query($sqlOfiO,$cnx);
			      $RsOfiO=MsSQL_fetch_array($rsOfiO);
		       	echo "<a href=\"javascript:;\" title=\"".trim($RsOfiO[cNomOficina])."\">".$RsOfiO[cSiglaOficina]."</a>";
		       	?>		    	
		    </td>
		    <td valign="top">
		       	<?
          	$rsMvTrOr=mssql_query("SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsMvTr[iCodTrabajadorEnviar]'",$cnx);
          	$RsMvTrOr=MsSQL_fetch_array($rsMvTrOr);
          	echo $RsMvTrOr["cApellidosTrabajador"]." ".$RsMvTrOr["cNombresTrabajador"];
						mssql_free_result($rsMvTrOr);
        		?>
		    </td>
		   <td valign="top">
		    		<span><?=date("d-m-Y G:i:s", strtotime(substr($RsMvTr[fFecDelegado], 0, -6)))/*date("d-m-Y G:i", strtotime($RsMvTr[fFecDelegado]))*/?></span>
		    </td>
		    <td valign="top" align="left"><?=$RsMvTr[cObservacionesDerivar]?></td>		       
		    </tr> 
		    <?}?>
		    </table>
		    </div>
		    <img src="images/space.gif" width="0" height="0"> 
		  	</fieldset>

					</div>
                 </div>
             </div>
         </div>
     </div>
 </main>

<div>	

</body>
</html>
<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>
