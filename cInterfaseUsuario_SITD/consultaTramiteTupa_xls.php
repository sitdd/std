<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: consultaTramiteTupa_xls.php
SISTEMA: SISTEMA  DE TR�MITE DOCUMENTARIO DIGITAL
OBJETIVO: Reporte general de los Tupas en EXCEL
PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripci�n
------------------------------------------------------------------------
1.0  Larry Ortiz          05/09/2018      Creaci�n del programa.
------------------------------------------------------------------------
*****************************************************************************************/
include_once("../conexion/conexion.php");
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=consultaEntradaGeneral.xls");
	
	$anho = date("Y");
	$datomes = date("m");
	$datomes = $datomes*1;
	$datodia = date("d");
	$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre");
	
	echo "<table width=780 border=0><tr><td align=center colspan=8>";
	echo "<H3>REPORTE - TRAMITE TUPA</H3>";
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=right colspan=8>";
	echo "SITD, ".$datodia." ".$meses[$datomes].' del '.$anho;
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=left colspan=7>";
	$sqllog="select cNombresTrabajador, cApellidosTrabajador from tra_m_trabajadores where iCodTrabajador='$traRep' "; 
	$rslog=mssql_query($sqllog,$cnx);
	$Rslog=MsSQL_fetch_array($rslog);
	echo "GENERADO POR : ".$Rslog[cNombresTrabajador]." ".$Rslog[cApellidosTrabajador];
	echo " ";
?>	

	<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="center">
		<thead>
			<tr>
				<th width="8%" style="width: 15%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Nro Documento</th>
			  <th width="8%" style="width: 15%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Fecha de Registro</th>
			  <th width="8%" style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Oficina</th>
			  <th width="8%" style="width: 20%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Procedimiento TUPA</th>
				<th width="8%" style="width: 20%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N� de Dias Programados</th>
                <th width="8%" style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N� de Dias Ejecutados</th>
                <th width="8%" style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Estado</th>
                <th width="8%" style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Resultado</th>
          </tr>
		</thead>
		<tbody>
				<?	
				if($_GET[fDesde]!='' && $_GET[fHasta]!=''){
	$fDesde=date("Ymd", strtotime($_GET[fDesde]));
	$fHasta=date("Y-m-d", strtotime($_GET[fHasta]));
	function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    $date_r = getdate(strtotime($date));
    $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
    return $date_result;
				}
	$fHasta=dateadd($fHasta,1,0,0,0,0,0); // + 1 dia
	}
	/*$sql=" SELECT cCodificacion,fFecRegistro,fFecFinalizado,Tra_M_Tupa.iCodOficina,cNomOFicina,cNomTupa,nSilencio,nDias,DATEDIFF(DAY, fFecRegistro, GETDATE()) as Proceso ,DATEDIFF(DAY, fFecRegistro, fFecFinalizado) as Proceso2 ,nFlgEstado ";
    $sql.=" FROM Tra_M_Tramite LEFT OUTER JOIN Tra_M_Tupa ON Tra_M_Tramite.iCodTupa=Tra_M_Tupa.iCodTupa ";
    $sql.=" LEFT OUTER JOIN Tra_M_Oficinas ON Tra_M_Oficinas.iCodOficina=Tra_M_Tupa.iCodOficina ";
    $sql.=" WHERE Tra_M_Tramite.nFlgTipoDoc=1 AND Tra_M_Tramite.iCodTupa IS NOT NULL ";
    if($_GET[fDesde]!="" AND $_GET[fHasta]==""){
  	$sql.=" AND Tra_M_Tramite.fFecRegistro>'$fDesde' ";
    }
    if($_GET[fDesde]=="" AND $_GET[fHasta]!=""){
  	$sql.=" AND Tra_M_Tramite.fFecRegistro<='$fHasta' ";
    }
    if($_GET[fDesde]!="" && $_GET[fHasta]!=""){
    $sql.=" AND Tra_M_Tramite.fFecRegistro BETWEEN  '$fDesde' and '$fHasta' ";
    }
	if($_GET[nFlgEstado]!=""){
	$sql.=" AND nFlgEstado='$_GET[nFlgEstado]' ";
	}
	if($_GET[nSilencio]!=""){
	$sql.=" AND nSilencio='$_GET[nSilencio]'";
	}	
	if($_GET[cCodificacion]!=""){
     $sql.="AND Tra_M_Tramite.cCodificacion LIKE '%$_GET[cCodificacion]%' ";
    }
	if($_GET[cNroDocumento]!=""){
     $sql.="AND Tra_M_Tramite.cNroDocumento='$_GET[cNroDocumento]' ";
    }
	if($_GET[cAsunto]!=""){
     $sql.="AND Tra_M_Tramite.cAsunto LIKE '%$_GET[cAsunto]%' ";
    }
	if($_GET[iCodTupa]!=""){
     $sql.="AND Tra_M_Tramite.iCodTupa='$_GET[iCodTupa]' ";
    }
	if($_GET[iCodOficina]!=""){
    $sql.="AND Tra_M_Tupa.iCodOficina='$_GET[iCodOficina]' ";
    }
    $sql.= " ORDER BY $campo $orden ";*/
	
	$sql.= "SP_CONSULTA_TRAMITE_TUPA '$fDesde', '$fHasta','$_GET[nFlgEstado]','$_GET[nSilencio]',
	'%$_GET[cCodificacion]%','%$_GET[cNroDocumento]%', '%$_GET[cAsunto]%','$_GET[iCodTupa]', '$_GET[iCodOficina]','$campo', '$orden' ";	   	   
    $rs=mssql_query($sql,$cnx);
   //echo $sql;

       while ($Rs=MsSQL_fetch_array($rs)){
	     ?>
	    <tr>
        <td style="width: 15%; text-align: left; border: solid 1px #6F6F6F;font-size:10px"><? echo $Rs[cCodificacion];?></td>
        <td style="width: 10%; border: solid 1px #6F6F6F;font-size:10px">
		    <?  
			    echo "<div style=color:#727272;text-align:center>".date("d-m-Y", strtotime($Rs[fFecRegistro]))."</div>";
                echo "<div style=color:#727272;font-size:10px;text-align:center>".date("h:i A", strtotime($Rs[fFecRegistro]))."</div>";
		   ?>
              </td> 
        <td style="width: 20%; text-align: justify; border: solid 1px #6F6F6F;font-size:10px"><? echo $Rs[cNomOFicina];?></td>
        <td style="width: 15%; text-align: justify; border: solid 1px #6F6F6F;font-size:10px"><? echo $Rs[cNomTupa];?></td>
        <td style="width: 15%; text-align: justify; border: solid 1px #6F6F6F;font-size:10px"><? echo $Rs[nDias];?></td>
        <td style="width: 15%; text-align: justify; border: solid 1px #6F6F6F;font-size:10px">
		     <? if($Rs[nFlgEstado]==1){echo $Rs[Proceso]; }
				 else if($Rs[nFlgEstado]==2){ echo $Rs[Proceso]; }
				 else if($Rs[nFlgEstado]==3){ echo $Rs[Proceso2]; }   ?></td>
        <td style="width: 16%; text-align: center; border: solid 1px #6F6F6F;font-size:10px">
		     <?   if($Rs[nFlgEstado]==1){
					echo "<div style='color:#005E2F'>PENDIENTE</div>";
					}
					else if($Rs[nFlgEstado]==2){
					echo "<div style='color:#0154AF'>EN PROCESO</div>";
					}
					else if($Rs[nFlgEstado]==3){
					echo "FINALIZADO";
					echo "<div style=color:#0154AF;text-align:center >".date("d-m-Y", strtotime($Rs[fFecFinalizado]))."</div>";
                    echo "<div style=color:#0154AF;font-size:10px;text-align:center>".date("h:i A", strtotime($Rs[fFecFinalizado]))."</div>";
					}
			  ?>		
            </td>
         <td style="width: 9%; text-align: center; border: solid 1px #6F6F6F;font-size:10px">
		    <?  
			   if($Rs[Proceso] > $Rs[nDias] and $Rs[nSilencio]==1 and $Rs[nFlgEstado]!=3){ 
	                echo "<div style='color:#950000'>VENCIDO</div>"; 
					echo "<div style='color:#950000'>SAP</div>";
					}
					else if($Rs[Proceso] > $Rs[nDias] and $Rs[nSilencio]==0 and $Rs[nFlgEstado]!=3){
					echo "<div style='color:#950000'>VENCIDO</div>"; 
					echo "<div style='color:#950000'>SAN</div>"; 
					}
					else if($Rs[Proceso2] > $Rs[nDias] and $Rs[nSilencio]==1  and $Rs[nFlgEstado]==3){ 
	                echo "<div style='color:#950000'>VENCIDO</div>"; 
					echo "<div style='color:#950000'>SAP</div>";
					}
					else if($Rs[Proceso2] > $Rs[nDias] and $Rs[nSilencio]==0  and $Rs[nFlgEstado]==3){
					echo "<div style='color:#950000'>VENCIDO</div>"; 
					echo "<div style='color:#950000'>SAN</div>"; 
					}
		   ?>
          </td>
        </tr>
      <?
         }
      ?>
	   	
      </tbody>
	</table>