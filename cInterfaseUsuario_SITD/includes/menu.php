<?php

//--Consulta para obtener Menu--//
$menu_data = array();
$icono_menu_data = array();
include_once("includes/conexion.menu.php");
$sqlBtn = "SELECT * FROM Tra_M_Menu WHERE iCodPerfil='" . $_SESSION['iCodPerfilLogin'] . "' ORDER BY nNombreOrden ASC";
$rsBtn = mssql_query($sqlBtn, $cnx2);
$i = 0;
while ($RsBtn = MsSQL_fetch_array($rsBtn)) {
    $menu_data[$i]["cNombreMenu"] = strtoupper(trim($RsBtn['cNombreMenu']));
    $icono_menu_data[$i]= trim($RsBtn['cIcono']);
    $sqlSub = "SELECT * FROM Tra_M_Menu_Lista WHERE iCodMenu='" . $RsBtn['iCodMenu'] . "' ORDER BY nOrden ASC";
    $rsSub = mssql_query($sqlSub, $cnx2);
    $j = 0;
    while ($RsSub = MsSQL_fetch_array($rsSub)) {
        $sqlSubItm = "SELECT * FROM Tra_M_Menu_Items WHERE iCodSubMenu='" . $RsSub['iCodSubMenu'] . "'";
        $rsSubItm = mssql_query($sqlSubItm, $cnx2);
        $RsSubItm = MsSQL_fetch_array($rsSubItm);
        $menu_data[$i]['data'][$j]['cScriptSubMenu'] = trim($RsSubItm['cScriptSubMenu']);
        $menu_data[$i]['data'][$j]['cNombreSubMenu'] = trim($RsSubItm['cNombreSubMenu']);
        $j++;
    }
    $i++;
}
mssql_close($cnx2);

include_once("../conexion/conexion.php");
$sqlNom = "SELECT * from Tra_M_Trabajadores WHERE iCodTrabajador='$_SESSION[CODIGO_TRABAJADOR]'";
$rsNom  = mssql_query($sqlNom,$cnx);
$RsNom  = mssql_fetch_array($rsNom);

// --------------------------- Pendientes ----------------------------------
// Punto de control - Jefes = 3
// Punto de control - Asistentes = 19
// Profesional = 4
if($_SESSION['iCodPerfilLogin'] == 3 OR $_SESSION['iCodPerfilLogin'] == 19){
    $sqlBtn1 = "SP_BANDEJA_PENDIENTES  '','','','','','', ";
    $sqlBtn1.= "'','','','','','','','','$_SESSION[iCodOficinaLogin]','Fecha','DESC' ";
    $rsBtn1 = mssql_query($sqlBtn1, $cnx2);
    $total1 = mssql_num_rows($rsBtn1);
}else{
    $sqlBtn1 = "SELECT Tra_M_Tramite.iCodTramite AS Tramite, * FROM Tra_M_Tramite,Tra_M_Tramite_Movimientos 
                WHERE Tra_M_Tramite.iCodTramite = Tra_M_Tramite_Movimientos.iCodTramite 
                AND ((Tra_M_Tramite_Movimientos.iCodOficinaDerivar='$_SESSION[iCodOficinaLogin]' AND ( Tra_M_Tramite_Movimientos.iCodTrabajadorDelegado='$_SESSION[CODIGO_TRABAJADOR]' OR Tra_M_Tramite_Movimientos.iCodTrabajadorDerivar='$_SESSION[CODIGO_TRABAJADOR]')) OR (Tra_M_Tramite_Movimientos.iCodOficinaDerivar='$_SESSION[iCodOficinaLogin]' AND Tra_M_Tramite_Movimientos.iCodTrabajadorEnviar='$_SESSION[CODIGO_TRABAJADOR]') ) And Tra_M_Tramite_Movimientos.nEstadoMovimiento!=2 And Tra_M_Tramite.nFlgEnvio=1 AND (Tra_M_Tramite_Movimientos.nEstadoMovimiento=1 OR Tra_M_Tramite_Movimientos.nEstadoMovimiento=3 OR (Tra_M_Tramite_Movimientos.nEstadoMovimiento=1 AND Tra_M_Tramite_Movimientos.cFlgTipoMovimiento=6)) ORDER BY Tra_M_Tramite_Movimientos.iCodMovimiento DESC ";
    $rsBtn1 = mssql_query($sqlBtn1, $cnx2);
    $total1 = mssql_num_rows($rsBtn1);
}
// --------------------------- Derivados ----------------------------------

$sqlBtn2 = "SP_BANDEJA_DERIVADOS '','','','','%%','%%','$_SESSION[iCodOficinaLogin]','','' ,'','Derivado','DESC'";
$rsBtn2 = mssql_query($sqlBtn2, $cnx2);
$total2 = mssql_num_rows($rsBtn2);

// --------------------------- Finalizados ----------------------------------
// Punto de control - Jefes = 3
// Punto de control - Asistentes = 19
// Profesional = 4
if($_SESSION['iCodPerfilLogin']==3 OR $_SESSION['iCodPerfilLogin'] == 19){
    $sqlBtn3 = "SP_BANDEJA_FINALIZADOS 'op1' ,'','','','', '', '', '$_SESSION[iCodOficinaLogin]','','' , 'DESC' ";
    $rsBtn3 = mssql_query($sqlBtn3, $cnx2);
    $total3 = mssql_num_rows($rsBtn3);
}else{
    $sqlBtn3 = "SELECT Tra_M_Tramite.iCodTramite as Tramite, * FROM Tra_M_Tramite,Tra_M_Tramite_Movimientos WHERE Tra_M_Tramite.iCodTramite=Tra_M_Tramite_Movimientos.iCodTramite AND ((Tra_M_Tramite_Movimientos.iCodOficinaDerivar='$_SESSION[iCodOficinaLogin]' AND Tra_M_Tramite_Movimientos.iCodTrabajadorDelegado='$_SESSION[CODIGO_TRABAJADOR]' ) OR Tra_M_Tramite_Movimientos.iCodTrabajadorEnviar='$_SESSION[CODIGO_TRABAJADOR]' ) AND Tra_M_Tramite_Movimientos.nEstadoMovimiento=5 ORDER BY Tra_M_Tramite.iCodTramite DESC ";
    $rsBtn3 = mssql_query($sqlBtn3, $cnx2);
    $total3 = mssql_num_rows($rsBtn3);
}


// --------------------------- Enviados ----------------------------------
$sqlBtn4 = "SELECT *, c.cFlgTipoMovimiento,c.nEstadoMovimiento, c.fFecDelegadoRecepcion,c.fFecRecepcion FROM Tra_M_Tramite a, Tra_M_Tramite_Trabajadores b, Tra_M_Tramite_Movimientos c WHERE a.iCodTramite = b.iCodTramite AND (b.iCodTrabajadorOrigen='$_SESSION[CODIGO_TRABAJADOR]' AND b.iCodTrabajadorDestino!='$_SESSION[CODIGO_TRABAJADOR]' ) AND b.iCodOficina='$_SESSION[iCodOficinaLogin]' and b.iCodMovimiento =c.iCodMovimiento ORDER BY a.iCodTramite DESC";
$rsBtn4 = mssql_query($sqlBtn4, $cnx2);
$total4 = mssql_num_rows($rsBtn4);


// --------------------------- Respondidos ----------------------------------
$sqlBtn5 = "SELECT Tra_M_Tramite.iCodTramite as Tramite, * FROM Tra_M_Tramite,Tra_M_Tramite_Movimientos WHERE Tra_M_Tramite.iCodTramite=Tra_M_Tramite_Movimientos.iCodTramite AND ((Tra_M_Tramite_Movimientos.iCodOficinaDerivar='$_SESSION[iCodOficinaLogin]' AND Tra_M_Tramite_Movimientos.iCodTrabajadorDelegado='$_SESSION[CODIGO_TRABAJADOR]' ) OR Tra_M_Tramite_Movimientos.iCodTrabajadorEnviar='$_SESSION[CODIGO_TRABAJADOR]' ) AND Tra_M_Tramite_Movimientos.nEstadoMovimiento=4 ORDER BY Tra_M_Tramite.iCodTramite DESC   ";
$rsBtn5 = mssql_query($sqlBtn5, $cnx2);
$total5 = mssql_num_rows($rsBtn5);

function numeros_totales($nombre,$t1,$t2,$t3,$t4,$t5){
    if($nombre=='Pendientes'){
        echo "(".$t1.")";
    }elseif($nombre=='Derivados'){
        echo "(".$t2.")";
    }elseif($nombre=='Finalizados'){
        echo "(".$t3.")";
    }elseif($nombre=='Enviados'){
        echo "(".$t4.")";
    }elseif($nombre=='Respondidos'){
        echo "(".$t5.")";
    }else{
        echo "";
    }
}

?>


<header>

    <div class="firstmenu">

        <!--Navbar-->
        <nav class="navbar navbar-expand-lg navbar-dark colorfondo fixed-top clearfix" id="navegacion">

            <a class="navbar-brand" href="main.php" class="shortcut"><b>SITDD</b></a>

            <!-- Siempre presente -->

            <div class="navbar-nav ml-auto">
                <div class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-transform: capitalize">
                        <img class="img-profile-menu" src="images/foto.jpg" alt="<?php echo  utf8_encode($RsNom[cNombresTrabajador]) ?>">&nbsp;
                        <?php echo utf8_encode($RsNom[cNombresTrabajador]); ?></a>
                    <div class="dropdown-menu dropdown-menu-right " aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="../index2.php" class="shortcut"><i class="fas fa-users"></i>&nbsp;Perfiles</a>
                        <a class="dropdown-item" href="perfil.php" class="shortcut"><i class="fas fa-cog"></i>&nbsp;Configuración</a>
                        <a class="dropdown-item" href="acceso.php" class="shortcut"><i class="fas fa-key"></i>&nbsp;Contraseña</a>
                        <a class="dropdown-item" href="logout.php" class="shortcut"><i class="fas fa-sign-out-alt"></i>&nbsp;Salida</a>
                    </div>
                </div>
            </div>
            <!-- fin Siempre presente -->
        </nav>
        <!--/.Navbar-->

        <!--Menu botones flotantes-->
        <div class="clearfix float-left fixed-top menu-flotante">

            <?php $i=0;?>
            <?php foreach ($menu_data as $tmpdata1): ?>

                <div class="dropdown dropright">
                    <button class="btn btn-floating boton-flotante blue darken-3" type="button" id="dropmenulink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php echo $icono_menu_data[$i]?>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropmenulink">
                        <p class="dropdown-header droptitulo"> <?php echo trim($tmpdata1['cNombreMenu']) ?> <p>

                            <?php foreach ($tmpdata1['data'] as $tmpdata2): ?>

                                <a class="dropdown-item" href="<?php echo $tmpdata2['cScriptSubMenu'] ?>">
                                    <?php echo utf8_encode(trim($tmpdata2['cNombreSubMenu'])) ?>
                                    <?php echo numeros_totales($tmpdata2['cNombreSubMenu'],$total1,$total2,$total3,$total4,$total5);?></a>

                            <?php endforeach ?>
                    </div>
                </div>
                <?php $i++;?>
            <?php endforeach ?>

        </div>
        <!--/. Menu botones flotantes-->

    </div>

    <div class="secondmenu">
        <!--Navbar-->
        <nav class="navbar navbar-expand-lg navbar-dark colorfondo fixed-top" id="navegacion">

            <!-- Navbar brand -->
            <a class="navbar-brand" href="main.php" class="shortcut"><b>SITDD</b></a>

            <!-- Collapse button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Collapsible content -->
            <div class="collapse navbar-collapse" id="navbarNav" style="text-align: left!important;">

                <!-- Links -->
                <div class="navbar-nav ml-auto">

                    <?php $i=0;?>
                    <?php foreach ($menu_data as $tmpdata1): ?>

                        <div class="nav-item dropdown">
                            <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $icono_menu_data[$i]?> &nbsp;<?php echo $tmpdata1['cNombreMenu'] ?> <span class="dropdown-toggle d-inline"></span></a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropmenulink">
                                <p class="dropdown-header droptitulo"> <?php echo $tmpdata1['cNombreMenu'] ?> <p>

                                    <?php foreach ($tmpdata1['data'] as $tmpdata2): ?>

                                        <a class="dropdown-item" href="<?php echo $tmpdata2['cScriptSubMenu'] ?>"><?php echo $tmpdata2['cNombreSubMenu'] ?> <?php echo numeros_totales($tmpdata2['cNombreSubMenu'],$total1,$total2,$total3,$total4,$total5);?></a>

                                    <?php endforeach ?>
                            </div>
                        </div>
                        <?php $i++;?>
                    <?php endforeach ?>


                    <!-- Siempre presente -->
                    <div class="nav-item dropdown">
                        <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-transform: capitalize"><img class="img-profile-menu"  src="images/foto.jpg" alt="<?php echo  $RsNom[cNombresTrabajador] ?>">&nbsp;<?php echo strtolower($RsNom[cNombresTrabajador]); ?><span class="dropdown-toggle d-inline"></span></a>
                        <div class="dropdown-menu dropdown-menu-right " aria-labelledby="navbarDropdownMenuLink">
                            <p class="dropdown-header droptitulo" style="text-transform: capitalize "><?php echo strtolower($RsNom[cNombresTrabajador]); ?><p>
                                <a class="dropdown-item" href="../index2.php" class="shortcut"><i class="fas fa-users"></i>&nbsp;Perfiles</a>
                                <a class="dropdown-item" href="perfil.php" class="shortcut"><i class="fas fa-cog"></i>&nbsp;Configuración</a>
                                <a class="dropdown-item" href="acceso.php" class="shortcut"><i class="fas fa-key"></i>&nbsp;Contraseña</a>
                                <a class="dropdown-item" href="logout.php" class="shortcut"><i class="fas fa-sign-out-alt"></i>&nbsp;Salida</a>
                        </div>
                    </div>
                    <!-- fin Siempre presente -->

                </div>
                <!-- Links -->


            </div>
            <!-- Collapsible content -->

        </nav>
        <!--/.Navbar-->
    </div>

</header>

