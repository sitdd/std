<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: consultaInternoOficina_pdf.php
SISTEMA: SISTEMA   DE TRÁMITE DOCUMENTARIO DIGITAL
OBJETIVO: Reporte General en PDF de los Documentos Internos por Oficina
PROPIETARIO: AGENCIA PERUANA DE COOPERACIÓN INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripción
------------------------------------------------------------------------
1.0  Larry Ortiz         05/09/2018      Creación del programa.
------------------------------------------------------------------------
*****************************************************************************************/
session_start();
ob_start();
//*************************************
include_once("../conexion/conexion.php");
?>
<page backtop="25mm" backbottom="15mm" backleft="10mm" backright="10mm">
	<page_header>
		<br>
		<table style="width: 1000px; border: solid 0px black;">
			<tr>
				<td style="text-align:left;	width: 20px"></td>
				<td style="text-align:left;	width: 980px">
					<img style="width: 220px" src="images/pdf_apci.jpg" alt="Logo">
				</td>
			</tr>
		</table>
        <br><br>
	</page_header>
	<page_footer>
		<table style="width: 100%; border: solid 0px black;">
			<tr>
                <td style="text-align: center;	width: 40%">
				<? 
				   $sqllog="select cNombresTrabajador, cApellidosTrabajador from tra_m_trabajadores where iCodTrabajador='$_SESSION[CODIGO_TRABAJADOR]' "; 
				   $rslog=mssql_query($sqllog,$cnx);
				   $Rslog=MsSQL_fetch_array($rslog);
				   echo $Rslog[cNombresTrabajador]." ".$Rslog[cApellidosTrabajador];
				?></td>
				<td style="text-align: right;	width: 60%">p�gina [[page_cu]]/[[page_nb]]</td>
			</tr>
		</table>
        <br>
        <br>
	</page_footer>
	
	
	<table style="width: 100%; border: solid 0px black;">
	<tr>
	<td style="text-align: left;	width: 50%"><span style="font-size: 15px; font-weight: bold">Reporte Interno General por Oficina</span></td>
	<td style="text-align: right;	width: 50%"><span style="font-size: 15px; font-weight: bold"><?=date("d-m-Y")?></span></td>
	</tr>
	</table>
	<br>
	<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="center">
		<thead>
			<tr>
                <th style="width: 15%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Fecha</th>
				<th style="width: 15%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Tipo Documento</th>			
                <th style="width: 20%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Asunto</th>
				<th style="width: 20%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Observaciones</th>
				<th style="width: 15%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Responsable</th>
                <th style="width: 15%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Referencia</th>
			</tr>
		</thead>
		<tbody>
	<?
 	if ($fecini!=''){$fecini=date("Ymd", strtotime($fecini));}
   	if( $fecfin!=''){
    $fecfin=date("Y-m-d", strtotime($fecfin));
	function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    $date_r = getdate(strtotime($date));
    $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
    return $date_result;
				}
	$fecfin=dateadd($fecfin,1,0,0,0,0,0); // + 1 dia
	}
 	
	$sql.= " SP_CONSULTA_INTERNO_OFICINA '$fecini', '$fecfin',  '$_GET[SI]', '$_GET[NO]',  '%$_GET[cCodificacion]%', '%$_GET[cAsunto]%',  '%$_GET[cObservaciones]%', '$_GET[cCodTipoDoc]' ,'$_GET[iCodOficina]','$_SESSION[iCodOficinaLogin]', '$campo', '$orden' ";
    $rs=mssql_query($sql,$cnx);
   //echo $sql;
	   
	   while ($Rs=MsSQL_fetch_array($rs)){
	?>
	    <tr>
        <td style="width: 15%; border: solid 1px #6F6F6F;font-size:10px">
		    <? echo "<div style=color:#0154AF;text-align:center>".date("d-m-Y G:i:s", strtotime($Rs[fFecRegistro]))/*date("d-m-Y", strtotime($Rs[fFecRegistro]))*/."</div>";
              /*echo "<div style=color:#0154AF;font-size:10px;text-align:center>".date("h:i A", strtotime($Rs[fFecRegistro]))."</div>";*/?></td>
        <td style="width: 15%; border: solid 1px #6F6F6F;font-size:10px">
		    <? echo "<div align='center'>".$Rs[cDescTipoDoc]."</div>"; 
			   echo "<div align='center' style=color:#727272>".$Rs[cCodificacion]."</div>";?></td>         
        <td style="width: 20%; text-align: justify; border: solid 1px #6F6F6F;font-size:10px"><? echo $Rs[cAsunto];?></td>
        <td style="width: 20%; text-align: justify; border: solid 1px #6F6F6F;font-size:10px"><? echo $Rs[cObservaciones];?></td>
        <td style="width: 15%; text-align: center; border: solid 1px #6F6F6F;font-size:10px">
		<? $sqlTra="SELECT cApellidosTrabajador,cNombresTrabajador FROM Tra_M_Trabajadores WHERE iCodTrabajador='".$_SESSION['JEFE']."'";
			$rsTra=mssql_query($sqlTra,$cnx);
			$RsTra=MsSQL_fetch_array($rsTra);
			echo $RsTra[cNombresTrabajador]." ".$RsTra[cApellidosTrabajador];	 ?></td>
        <td style="width: 15%; text-align: center; border: solid 1px #6F6F6F;font-size:10px"><? echo $Rs[cReferencia];?></td>
        </tr>
      <?
         }
      ?>
	   	
      </tbody>
	</table>
</page>

<?
//*************************************


	$content = ob_get_clean();  set_time_limit(0);     ini_set('memory_limit', '640M');

	// conversion HTML => PDF
	require_once(dirname(__FILE__).'/html2pdf/html2pdf.class.php');
	try
	{
		$html2pdf = new HTML2PDF('P','A4', 'es', false, 'UTF-8', 3);
		$html2pdf->pdf->SetDisplayMode('fullpage');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output('exemple03.pdf');
	}
	catch(HTML2PDF_exception $e) { echo $e; }
?>   
         		
