<?php
date_default_timezone_set('America/Lima');
session_start();
$nNumAno    = date("Y");
if($_SESSION['CODIGO_TRABAJADOR']!=""){
	if (!isset($_SESSION["cCodRef"])){ 
		$fecSesRef=date("Ymd-Gis");	
		$_SESSION['cCodRef']=$_SESSION['CODIGO_TRABAJADOR']."-".$_SESSION['iCodOficinaLogin']."-".$fecSesRef;
	}
	if (!isset($_SESSION["cCodOfi"])){ 
		$fecSesOfi=date("Ymd-Gis");	
		$_SESSION['cCodOfi']=$_SESSION['CODIGO_TRABAJADOR']."-".$_SESSION['iCodOficinaLogin']."-".$fecSesOfi;
	}		
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?php include("includes/head.php");?>
<script src="ckeditor/ckeditor.js"></script>
<style>
	.btn-default1 {
    color: #333;
    background-color: #eee;
    border-color: #ccc;
}
.btn1 {
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}
</style>
<script src="ckeditor/ckeditor.js"></script>

<script Language="JavaScript">
<!--
function mueveReloj(){ 
    momentoActual = new Date() 
    anho = momentoActual.getFullYear() 
    mes = (momentoActual.getMonth())+1
    dia = momentoActual.getDate() 
    hora = momentoActual.getHours() 
    minuto = momentoActual.getMinutes() 
    segundo = momentoActual.getSeconds()
    if((mes>=0)&&(mes<=9)){ mes="0"+mes; }
    if((dia>=0)&&(dia<=9)){ dia="0"+dia; }
    if((hora>=0)&&(hora<=9)){ hora="0"+hora; }
    if((minuto>=0)&&(minuto<=9)){ minuto="0"+minuto; }
    if ((segundo>=0)&&(segundo<=9)){ segundo="0"+segundo; }
    horaImprimible = dia + "-" + mes + "-" + anho + " " + hora + ":" + minuto + ":" + segundo 
    document.frmRegistro.reloj.value=horaImprimible 
    setTimeout("mueveReloj()",1000) 
}

function valorSelect(porAprobar){
	var cCodTipoDoc = document.getElementById("cCodTipoDoc").value;
	var parametros = {
  		'cCodTipoDoc' : cCodTipoDoc,
  		'iCodOficina' : <?php echo $_SESSION['iCodOficinaLogin']; ?>,
  		'nNumAno'			: <?php echo $nNumAno; ?>
  	};

	$.ajax({
		type: 'POST',
		url: 'ajaxCorrelativoSalida.php',
		data: parametros,
		dataType: 'json',
		success: function(correlativo){
			console.log(correlativo);
			var correlativo = correlativo;
			$.each(correlativo,function(index,value)
			{
				$('#posibleCodificacion').val(value + " "+ porAprobar);
			});
		},
		error: function(e){
			console.log(e);
			alert('Error Processing your Request 6!!');
		}
	});
}
   function muestra(nombrediv) {
        if(document.getElementById(nombrediv).style.display == '') {
                document.getElementById(nombrediv).style.display = 'none';
        } else {
                document.getElementById(nombrediv).style.display = '';
        }
    }
	
function activaRemitente()
{
document.frmRegistro.radioMultiple.checked = false;
document.frmRegistro.radioRemitente.checked = true;
document.frmRegistro.iCodRemitente.value=document.frmRegistro.Remitente.value;
document.frmRegistro.radioSeleccion.value="2";
muestra('areaRemitente');
document.frmRegistro.cNombreRemitente.focus();
return false;
}

function activaMultiple()
{
document.frmRegistro.radioMultiple.checked  = true;
document.frmRegistro.radioRemitente.checked = false;
document.frmRegistro.iCodRemitente.value    = 0;
document.frmRegistro.radioSeleccion.value   = "1";
document.getElementById('areaRemitente').style.display = 'none';
return true;
}

function buscarRemitente(){
window.open('registroRemitentesSalidaLs.php','popuppage','width=735,height=450,toolbar=0,statusbar=1,resizable=0,scrollbars=yes,top=100,left=100');
}

function infoRemitente() {
var w = document.frmRegistro.txtdirec_remitente.value;
var x = document.frmRegistro.cCodDepartamento.value;
var y = document.frmRegistro.cCodProvincia.value;
var z = document.frmRegistro.cCodDistrito.value ;
var t = document.frmRegistro.iCodRemitente.value;

window.open('registroRemitenteDetalle.php?iCodRemitente='+t+'&txtdirec_remitentex='+w+'&cCodDepartamentox='+x+'&cCodProvinciax='+y+'&cCodDistritox='+z,'popuppage','width=590,height=240,toolbar=0,statusbar=1,resizable=0,scrollbars=yes,top=100,left=100');
}

function AddOficina(){
	if (document.frmRegistro.iCodOficinaMov.value.length == "")
  {
    alert("Seleccione Oficina");
    document.frmRegistro.iCodOficinaMov.focus();
    return (false);
  }
	if (document.frmRegistro.iCodTrabajadorMov.value.length == "")
  {
    alert("Seleccione Trabajador");
    document.frmRegistro.iCodTrabajadorMov.focus();
    return (false);
  }
	if (document.frmRegistro.iCodIndicacionMov.value.length == "")
  {
    alert("Seleccione Indicación");
    document.frmRegistro.iCodIndicacionMov.focus();
    return (false);
  }  
  document.frmRegistro.opcion.value=23;
  document.frmRegistro.action="registroData.php";
  document.frmRegistro.submit();
}
function AddReferencia(){
  // document.frmRegistro.opcion.value=21;
  // document.frmRegistro.action="registroData.php";
  // document.frmRegistro.submit();
var parameters = {
        iCodTramiteRef: $("#iCodTramiteRef").val(),
        cReferencia: $("#cReferencia").val(),
        iCodTramite: "",
        id_identificador: document.getElementsByName("id_identificador")[0].value
    }
    var items="";

    $.ajax({
        type: 'POST',
        url: 'insertarReferenciaTemporal.php', 
        data: parameters, 
        dataType: 'json',
        success: function(s){
        	console.log(s);
        	$.each(s,function(index,value) 
                {
                	items += '<div class="col-sm-11">'
                    items +='<span style="background-color:#EAEAEA;">'+value.cReferencia
					items += '<a href="javascript: void(0)" onClick="eliminarReferenciaTemporal('+value.iCodReferencia+')">'
					items += '	<img src="images/icon_del.png" border="0" width="13" height="13">'
					items += '</a>'
					items += '</span>' 
                });
                $("#listaReferenciaTemporal").html(items);
        },
        error: function(e){
            alert('Error Processing your Request!!');
        }
    });
}

function Registrar(){
	if (document.frmRegistro.cCodTipoDoc.value.length == "")
	{
	  alert("Seleccione Tipo Documento");
	  document.frmRegistro.cCodTipoDoc.focus();
	  return (false);
	}
	  
	if (document.frmRegistro.nNumFolio.value.length == "")
	{
	  alert("Ingrese Número de Folios");
	  document.frmRegistro.nNumFolio.focus();
	  return (false);
	}  
    
    /*
	if(document.getElementById('nFlgRpta').checked  && document.getElementById('fFecPlazo').value=="" )
	{
		alert("Por favor Colocar la fecha de Plazo de Respuesta");
	    document.frmRegistro.fFecPlazo.focus();
	    return (false);
	}*/
  
	if (document.frmRegistro.iCodRemitente.value.length == "")
	{
	  document.frmRegistro.iCodRemitente.value=-1;
	}  
    /*
     if (document.frmRegistro.fechaDocumento.value.length == "")
  {
    alert("Seleccion fecha de documento");
    return (false);
  }*/
  
  	document.frmRegistro.opcion.value=5;
  	document.frmRegistro.action="registroData.php";
  	document.frmRegistro.submit();
}

function releer(){
  document.frmRegistro.action="<?=$PHP_SELF?>#area";
  document.frmRegistro.submit();
}

function copy(){

   if(document.frmRegistro.cNombreRemitente.value.length!="" ){
   		document.getElementById('areaCopiaOficina').style.display = '';
   		return false;
  	}

  else if(document.frmRegistro.cNombreRemitente.value.length=="" ){
    document.getElementById('areaCopiaOficina').style.display = 'none';
	return false;
	}
}

var miPopup
	function Buscar(){
 miPopup=window.open('registroBuscarDoc.php','popuppage','width=745,height=360,toolbar=0,status=0,resizable=0,scrollbars=yes,top=100,left=100');
	}

function go() {
	w = new ActiveXObject("WScript.Shell");
	//w.run("c:\\envioSMS.jar", 1, true);
	w.run("G:\\refirma\\1.1.0\\ReFirma-1.1.0.jar", 1, true);//G:\refirma\1.1.0\ReFirma-1.1.0.jar
	return true;
}
//--></script>
<script type="text/javascript" language="javascript" src="includes/lytebox.js"></script>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
<link type="text/css" rel="stylesheet" href="css/dhtmlgoodies_calendar.css" media="screen"/>
<script type="text/javascript" src="scripts/dhtmlgoodies_calendar.js"></script>
</head>
<body onload="mueveReloj()">

	<?include("includes/menu.php");?>
	<a name="area"></a>



<!--Main layout-->
 <main class="mx-lg-5">
     <div class="container-fluid">
          <!--Grid row-->
         <div class="row wow fadeIn">
              <!--Grid column-->
             <div class="col-md-12 mb-12">
                  <!--Card-->
                 <div class="card">
                      <!-- Card header -->
                     <div class="card-header text-center ">
                         >>
                     </div>
                      <!--Card content-->
                     <div class="card-body">

<div class="AreaTitulo">Registro >> Doc. de Salida Oficina</div>	
	<table class="table">
		<form name="frmRegistro" method="POST" action="registroData.php" enctype="multipart/form-data">
			<input type="hidden" name="id_identificador" value="<?php echo rand(1000000000,9999999999);?>">
			<input type="hidden" name="opcion" value="">
      <input type="hidden" name="sal" value="3">
			<input type="hidden" name="radioSeleccion" value="">
			<?php 
						include_once("../conexion/conexion.php");
						$sqlPerfil = "SELECT iCodPerfil FROM Tra_M_Perfil_Ususario 
									WHERE iCodTrabajador=".$_SESSION[CODIGO_TRABAJADOR]." AND iCodOficina = ".$_SESSION[iCodOficinaLogin];
						$rsPefil   = mssql_query($sqlPerfil,$cnx);
						$RsPefil   = mssql_fetch_array($rsPefil);
						if ($RsPefil['iCodPerfil'] == 3) {
							$porAprobar = "";
							//echo "Punto de Control - Jefes";
						}elseif ($RsPefil['iCodPerfil'] == 4) {
							$porAprobar = "(Por Aprobar)";
							//echo "Profesional";
						}elseif ($RsPefil['iCodPerfil'] == 19) {
							$porAprobar = "(Por Aprobar)";
							//echo "Punto de Control - Asistentes";
						}elseif ($RsPefil['iCodPerfil'] == 20) {
							$porAprobar = "(Por Aprobar)";
							//echo "Punto de Control - Especial";
						}
				 	?>
			<tr>
				<td class="FondoFormRegistro">
					<table border=0>
						<tr>
							<td valign="top"  width="160">Tipo de Documento:</td>
							<td valign="top">
								<select name="cCodTipoDoc" id="cCodTipoDoc" class="FormPropertReg form-control" style="width:180px" 
												onChange="valorSelect('<?php echo $porAprobar; ?>')"/>
									<option value="">Seleccione:</option>
									<?
									$sqlTipo="SELECT * FROM Tra_M_Tipo_Documento WHERE nFlgSalida=1";
				          $sqlTipo.="ORDER BY cDescTipoDoc ASC";
				          $rsTipo=mssql_query($sqlTipo,$cnx);
				          while ($RsTipo=MsSQL_fetch_array($rsTipo)){
				          	if($RsTipo["cCodTipoDoc"]==$_POST[cCodTipoDoc]){
				          		$selecTipo="selected";
				          	}Else{
				          		$selecTipo="";
				          	}
				          echo "<option value=".$RsTipo["cCodTipoDoc"]." ".$selecTipo.">".$RsTipo["cDescTipoDoc"]."</option>";
				          }
				          mssql_free_result($rsTipo);
									?>
								</select>&nbsp;<span class="FormCellRequisito">*</span>
							</td>
			<td  width="160">Fecha Registro:</td>
			<td>
				<input type="text" name="reloj" class="FormPropertReg form-control" style="width:120px" onfocus="window.document.frmRegistro.reloj.blur()"></td>
			</tr>	
			
				  <!--tr>
						<td  valign="top">Fecha del Documento:</td>
						<td>
							<input type="text" name="fechaDocumento" value="<?php echo $_POST[fechaDocumento]; ?>" style="width:120px" class="FormPropertReg form-control" readonly>
							<div class="boton" style="width:24px;height:20px;display:inline">
								<a href="javascript:;" onclick="displayCalendar(document.forms[0].fechaDocumento,'dd-mm-yyyy hh:ii',this,true)">
									<img src="images/icon_calendar.png" width="22" height="20" border="0">
								</a>
							</div>
							<span class="FormCellRequisito">*</span>
						</td>
					</tr-->	
						
							
			<tr>
				<td valign="top"  width="160">Correlativo:</td>
				<td valign="top"  width="160">
					<input type="text" id="posibleCodificacion" class="FormPropertReg form-control" style="width:320px; color: #0000FF" readonly/>
				</td>
			</tr>
			<tr>
			<td valign="top"  width="160">Asunto:</td>
			<td valign="top">
				<textarea name="cAsunto" style="width:320px;height:55px" class="FormPropertReg form-control"><?=$_POST[cAsunto]?></textarea>
				&nbsp;&nbsp;&nbsp;</td>
			
			<td valign="top"  width="160">Observaciones:</td>
			<td valign="top">
					<textarea name="cObservaciones" style="width:320px;height:55px" class="FormPropertReg form-control"><?=$_POST[cObservaciones]?></textarea>
			</td>
			</tr>

		
			<tr>
				<td valign="top" >Requiere Respuesta:</td>
				<td>Si<input type="radio" name="nFlgRpta"  id="nFlgRpta" value="1" <?if($_POST[nFlgRpta]==1) echo "checked"?> /> &nbsp; No<input type="radio" name="nFlgRpta"  id="nFlgRptaN" value="" <?if($_POST[nFlgRpta]=="") echo "checked"?> /></td>
                <td valign="top"  wdidth="160">Referencia:</td>
			<td valign="top">
					<table><tr>
					<td align="center"><input type="hidden" readonly="readonly" name="cReferencia" id="cReferencia" value="<?if($_GET[clear]==""){ echo trim($Rs[cReferencia]); }Else{ echo trim($_POST[cReferencia]);}?>" class="FormPropertReg form-control" style="width:140px;text-transform:uppercase" />
                    <input type="hidden" name="iCodTramiteRef" id="iCodTramiteRef"  value="<?=$_REQUEST[iCodTramiteRef]?>"  />
                    </td>
					<td align="center"></td>
					<td align="center"><div class="btn btn-primary" style="width:125px;height:17px;padding-top:4px;"><a style=" text-decoration:none" href="javascript:;" onClick="Buscar();">A&ntilde;adir Referencia</a> </div></td>
					</tr></table>
                    <table border=0><tr><td>
					<div id="listaReferenciaTemporal"></div>	
					 	
					
			</td>
			</tr>
			
			<tr>
				<td valign="top" >Folios:</td>
				<td><input type="text" name="nNumFolio" value="<? if($_POST[nNumFolio]==""){echo 1;} else { echo $_POST[nNumFolio];}?>" class="FormPropertReg form-control" style="width:40px;text-align:right" /></td>
				<!--td valign="top"  width="160">Fecha Plazo:</td>
				<td valign="top">
					<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td>
								<input type="text" readonly name="fFecPlazo"  id="fFecPlazo" value="<?=$_POST[fFecPlazo]?>" 
											 style="width:75px" class="FormPropertReg form-control" value="">
							</td>
							<td>
								<div class="boton" style="width:24px;height:20px"><a href="javascript:;" 
										 onclick="displayCalendar(document.forms[0].fFecPlazo,'dd-mm-yyyy',this,false)">
									<img src="images/icon_calendar.png" width="22" height="20" border="0"></a>&nbsp;
									<span class="FormCellRequisito"></span>
								</div>
							</td>
						</tr>
					</table>
				</td-->
			</tr>			
			<?php 
				$sqlOficina = "SELECT iCodOficina FROM Tra_M_Trabajadores WHERE iCodTrabajador = ".$_SESSION['CODIGO_TRABAJADOR'];
				$rsOficina  = mssql_query($sqlOficina,$cnx);
				$RsOficina  = mssql_fetch_array($rsOficina);
			?>
			<tr>
				<?php 
					//$sqlJefe = "SELECT iCodCategoria FROM Tra_M_Trabajadores WHERE iCodTrabajador = ".$_SESSION['CODIGO_TRABAJADOR'];
					// El Jefe de la Oficina tiene código de perfil (3)
					$sqlJefe = "SELECT iCodTrabajador FROM Tra_M_Perfil_Ususario WHERE iCodOficina = '$_SESSION[iCodOficinaLogin]' AND iCodPerfil = 3";
					$rsJefe = mssql_query($sqlJefe,$cnx);
					$RsJefe = mssql_fetch_array($rsJefe);
					if ($RsJefe['iCodTrabajador'] == $_SESSION['CODIGO_TRABAJADOR']) {
						$valor = 1;
						$codJefe = $RsJefe['iCodTrabajador'];
					}else{
						$valor = 0;
					}
				?>
				<input type="hidden" name="esJefe" value="<?php echo $valor; ?>">
				<input type="hidden" name="codJefe" value="<?php echo $codJefe; ?>">
				<input type="hidden" name="nFlgEnvio" value="0">
				<td valign="top" >Autor:</td>
				<td colspan="3">
					<select name="cSiglaAutor" class="FormPropertReg form-control" style="width:200px" />
						<option value="">Seleccione:</option>
            	<?php
              	$sqlTipo="select 
                                    iCodTrabajador,
                                    (select (rtrim(ltrim(cNombresTrabajador))+' '+cApellidosTrabajador) as nombre from Tra_M_Trabajadores where iCodTrabajador=mu.iCodTrabajador) as nombreapellido
                                    from  Tra_M_Perfil_Ususario mu where
                                    icodOficina ='".$_SESSION[iCodOficinaLogin]."'";
								$rsTipo = mssql_query($sqlTipo,$cnx);
                while ($RsTipo = mssql_fetch_array($rsTipo)){
                	if($RsTipo["iCodTrabajador"] == $_POST[cCodTipoDoc]){
                  	$selecTipo = "selected";
                  }else{
                  	$selecTipo = "";
                  }
                  if($RsTipo["nombreapellido"]!=''){
                                    echo "<option value=".$RsTipo["iCodTrabajador"]." ".$selecTipo.">".$RsTipo["nombreapellido"]."</option>";
                                }
                }
                mssql_free_result($rsTipo);
              ?>
          </select>
        </td>
      </tr>

			<tr>
				<td valign="top" >Archivo F&iacute;sico:</td>
				<td valign="top" colspan="3">
					<textarea name="archivoFisico" id="archivoFisico" class="FormPropertReg form-control" style="width:33%;height:45px"><?php echo trim($_POST['archivoFisico']); ?></textarea>
				</td>
			</tr>

			<tr>
			<td valign="top" >Destino:</td>
			<td valign="top" colspan="3">
				<table border=0>
				<tr>
					<td valign="top">
						<input type="radio" name="radioMultiple" onclick="activaMultiple();">M&uacute;ltiple</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td valign="top"><input type="radio" name="radioRemitente" onclick="activaRemitente();" >Un Destino</td>
					<td>
						<div style="display:none" id="areaRemitente">
							<table cellpadding="0" cellspacing="2" border="0">
								<tr>
									<td align="left" width="70" style="color:#7E7E7E">Instituci&oacute;n:</td>
									<td>
										<input id="cNombreRemitente" name="cNombreRemitente" class="FormPropertReg form-control" value="<?=$_POST[cNombreRemitente]?>" style="width:300px" readonly >
									</td>
									<td align="left">
										<div class="btn btn-primary" style="width:70px;height:17px;padding-top:4px;text-align:center">
											<a style=" text-decoration:none" href="javascript:;" onClick="buscarRemitente();">Buscar</a>
										</div>
									</td>
									<td align="center"></td>
								</tr>
                
                <tr>
									<td align="left" width="70" style="color:#7E7E7E">Destinatario:</td>
									<td colspan="1">
										<input id="cNomRemite" name="cNomRemite" value="<?=$_POST[cNomRemite]?>" class="FormPropertReg form-control" 
													 style="text-transform:uppercase;width:300px">
									</td>
                  <td align="left">
                  	<div class="btn btn-primary" style="width:70px;height:17px;padding-top:4px;text-align:center">
                  		<a style=" text-decoration:none" href="javascript:;"
                  			 onClick="infoRemitente(this);">+ Datos</a>
                  	</div>
                 </td>
                </tr>
              </table>
            </div>
		  		
		  			<input id="iCodRemitente" name="iCodRemitente" type="hidden" value="<?=$_POST[iCodRemitente]?>">
		        <input id="Remitente" name="Remitente" type="hidden" value="<? if(empty($_POST[iCodRemitente])){echo $iCodRemitente;}else{echo $_POST[iCodRemitente];}?>">		
		          <input id="txtdirec_remitente" name="txtdirec_remitente" type="hidden" value="<?=$_POST[txtdirec_remitente]?>">
		          <input id="cCodDepartamento" name="cCodDepartamento" type="hidden" value="<?=$_POST[cCodDepartamento]?>">
		          <input id="cCodProvincia" name="cCodProvincia" type="hidden" value="<?=$_POST[cCodProvincia]?>">
		          <input id="cCodDistrito" name="cCodDistrito" type="hidden" value="<?=$_POST[cCodDistrito]?>">				
            </td>
				</tr>	
				
				</table>
			</td>
			</tr>
			<!--<tr>
				<td valign="top" >Documento complementrio:</td> 
				<td valign="top"><input type="file" name="fileUpLoadDigital" class="FormPropertReg form-control" style="width:335px;" /></td>
				<td valign="top" >Abrir Firma Digital:</td>
				<td valign="top" colspan="1"> <input type="button" value="Abrir" onClick="return go()"></td>
			</tr>-->
			<tr>
				<td colspan="4"> <input name="button" type="button" class="btn btn-primary" style="font-size: 12px;height: 29px;width: 100px;" value="Grabar" onclick="Registrar();"> </td>
			</tr>
			<tr>
				<td colspan="4" style="padding-left:15px">
					<hr style="color:#ccc">
					<h3 style="color:#808080">ELABORAR DOCUMENTO ELECTR&Oacute;NICO</h3>
					<p style="padding:0px 0px 0px 14px;">
					<a href="javascript:void(0);" class="btn-default1 btn1" style="cursor:not-allowed">Documento electr&oacute;nico</a>
				    <div class="hiders" style="display:none;padding:0px 0px 0px 14px;" > 
						<textarea name="descripcion" id="descripcion" class="FormPropertReg form-control"><?php echo $_POST[descripcion];?></textarea>
					</div>
					</p>
					<!--H3 style="color:#808080">PASO 2 - ABRIR FIRMA DIGITAL</H3><input type="button" value="Abrir" onClick="return go()" disabled>
					
					<H3 style="color:#808080">PASO 3 - ADJUNTAR DOCUMENTO</H3>
					<h4 style="color:#808080">Documento electr&oacute;nico:</h4>
					<input type="file" name="documentoElectronicoPDF" disabled-->
					<h4 style="color:#808080">Documento complementario:</h4>
					<input type="file" name="fileUpLoadDigital" disabled/>
					<script>
						CKEDITOR.replace('descripcion');
						 	$('.majorpoints').click(function(){
							$('.hiders').toggle("slow");
						});
					</script>	
				</td>
			</tr>
			<tr>
				<td colspan="4">
					 <input name="button" type="button" class="btn btn-primary" value="Generar" disabled style="cursor:not-allowed">
				<td>
			</tr>
			
				<?if($_POST[radioSeleccion]==1){?>
					 <script language="javascript" type="text/javascript">
					 	activaMultiple();
					 </script>
				<?}?>
				<?if($_POST[radioSeleccion]==2){?>
					 <script language="javascript" type="text/javascript">
					 	activaRemitente();
					 </script>
				<?}?>	
			</table>

		</form>
</div>		

<?include("includes/userinfo.php");?>

<?include("includes/pie.php");?>
<script>
  
    $( document ).ready(function() {
    	var parameters = { id_identificador: document.getElementsByName("id_identificador")[0].value }
        var items = "";
    	$.ajax({
            type: 'POST',
            url: 'listarReferenciaTemporal.php', 
            data: parameters, 
            dataType: 'json',
            success: function(s){
            	$.each(s,function(index,value) 
                {
                	items += '<div class="col-sm-11">'
                    items +='<span style="background-color:#EAEAEA;">'+value.cReferencia
					items += '<a href="javascript: void(0)" onClick="eliminarReferenciaTemporal('+value.iCodReferencia+')">'
					items += '	<img src="images/icon_del.png" border="0" width="13" height="13">'
					items += '</a>'
					items += '</span>' 
                });
                $("#listaReferenciaTemporal").html(items);
            },
            error: function(e){
                alert('Error Processing your Request!!');
            }
        });

        
    });

    function eliminarReferenciaTemporal(argument) {

        var parameters = {
            iCodTramiteRef: argument,
            id_identificador: document.getElementsByName("id_identificador")[0].value
        }
        var items = "";

        $.ajax({
            type: 'POST',
            url: 'eliminarReferenciaTemporal.php', 
            data: parameters, 
            dataType: 'json',
            success: function(s){
            	$.each(s,function(index,value) 
                {
                	items += '<div class="col-sm-11">'
                    items +='<span style="background-color:#EAEAEA;">'+value.cReferencia
					items += '<a href="javascript: void(0)" onClick="eliminarReferenciaTemporal('+value.iCodReferencia+')">'
					items += '	<img src="images/icon_del.png" border="0" width="13" height="13">'
					items += '</a>'
					items += '</span>' 
                });
                $("#listaReferenciaTemporal").html(items);
            },
            error: function(e){
                alert('Error Processing your Request!!');
            }
        });
    }

</script>
</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>