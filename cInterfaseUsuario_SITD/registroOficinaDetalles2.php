<?php
session_start();
If($_SESSION['CODIGO_TRABAJADOR']!=""){
include_once("../conexion/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv=Content-Type content=text/html; charset=utf-8>
<title>SITDD</title>

<link type="text/css" rel="stylesheet" href="css/detalle.css" media="screen" />
<script type="text/javascript" language="javascript" src="includes/lytebox.js"></script>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
<script language="javascript" type="text/javascript">
    function muestra(nombrediv) {
        if(document.getElementById(nombrediv).style.display == '') {
                document.getElementById(nombrediv).style.display = 'none';
        } else {
                document.getElementById(nombrediv).style.display = '';
        }
    }
</script>
</head>
<body>
	<?php
	$rs=mssql_query("SELECT * FROM Tra_M_Tramite WHERE iCodTramite='$_GET[iCodTramite]'",$cnx);
	$Rs=MsSQL_fetch_array($rs);
	?>

<!--Main layout-->
 <main class="mx-lg-5">
     <div class="container-fluid">
          <!--Grid row-->
         <div class="row wow fadeIn">
              <!--Grid column-->
             <div class="col-md-12 mb-12">
                  <!--Card-->
                 <div class="card">
                      <!-- Card header -->
                     <div class="card-header text-center ">
                         >>
                     </div>
                      <!--Card content-->
                     <div class="card-body">

<div class="AreaTitulo">DETALLE DE DOCUMENTO INTERNO - OFICINA</div>
<table cellpadding="0" cellspacing="0" border="0" width="910">
<tr>
<td class="FondoFormRegistro">	
		<table width="880" border="0" align="center">
		<tr>
		<td>
				<fieldset id="tfa_GeneralDoc" class="fieldset">
				<legend class="legend"><a href="javascript:;" onClick="muestra('zonaGeneral')" class="LnkZonas">Datos Generales <img src="images/icon_expand.png" width="16" height="13" border="0"></a></legend>
		    <div id="zonaGeneral">
		    <table border="0" width="860">
		    <tr>
		        <td width="130" >Tipo de Documento:&nbsp;</td>
		        <td width="300">
		        		<? 
			          $sqlTipDoc="SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$Rs[cCodTipoDoc]'";
			          $rsTipDoc=mssql_query($sqlTipDoc,$cnx);
			          $RsTipDoc=MsSQL_fetch_array($rsTipDoc);
			          echo $RsTipDoc[cDescTipoDoc];
		            ?>
		        </td>
		        <td width="130" >Fecha:&nbsp;</td>
		        <td>
		        	<span><?=date("d-m-Y", strtotime($Rs[fFecRegistro]))?></span>
        			<span style=font-size:10px><?=date("h:i A", strtotime($Rs[fFecRegistro]))?></span>
		        </td>
		    </tr> 
		    
		    <tr>
		        <td width="130" >N&ordm; Documento:&nbsp;</td>
		        <td style="text-transform:uppercase"><?=$Rs[cCodificacion]?></td>
		        <td width="130" >Digital:&nbsp;</td>
		        <td style="text-transform:uppercase">
		        		<?
								$sqlDw="SELECT TOP 1 * FROM Tra_M_Tramite_Digitales WHERE iCodTramite='$_GET[iCodTramite]'";
      					$rsDw=mssql_query($sqlDw,$cnx);
      					if(MsSQL_num_rows($rsDw)>0){
      						$RsDw=MsSQL_fetch_array($rsDw);
      						if($RsDw["cNombreNuevo"]!=""){
				 						if (file_exists("../cAlmacenArchivos/".trim($Rs1[nombre_archivo]))){
											echo "<a href=\"download.php?direccion=../cAlmacenArchivos/&file=".trim($RsDw["cNombreNuevo"])."\"><img src=images/icon_download.png border=0 width=16 height=16 alt=\"".trim($RsDw["cNombreNuevo"])."\"></a>";
										}
									}
      					}Else{
      						echo "<img src=images/space.gif width=16 height=16>";
      					}
								?>	
		        </td>
		    </tr>
	    
		    <tr>
		        <td width="130"  valign="top">Asunto:&nbsp;</td>
		        <td width="300"><?=$Rs[cAsunto]?></td>
		        <td width="130"  valign="top">Observaciones:&nbsp;</td>
		        <td width="300"><?=$Rs[cObservaciones]?></td>
		    </tr>

		    <tr>
		        <td width="130" >Referencias:&nbsp;</td>
		        <td >
								<?
								$sqlRefs="SELECT * FROM Tra_M_Tramite_Referencias WHERE iCodTramite='$_GET[iCodTramite]'";
          			$rsRefs=mssql_query($sqlRefs,$cnx);
          			while ($RsRefs=MsSQL_fetch_array($rsRefs)){
          					$sqlTrRf="SELECT * FROM Tra_M_Tramite WHERE cCodificacion='$RsRefs[cReferencia]'";
          					$rsTrRf=mssql_query($sqlTrRf,$cnx);
          					$RsTrRf=MsSQL_fetch_array($rsTrRf);
          					switch ($RsTrRf[nFlgTipoDoc]){
  										case 1: $ScriptPHP="registroDetalles.php"; break;
  										case 2: $ScriptPHP="registroOficinaDetalles.php"; break;
  										case 3: $ScriptPHP="registroSalidaDetalles.php"; break;
  									}
								?>
								<span><a href="<?=$ScriptPHP?>?iCodTramite=<?=$RsTrRf[iCodTramite]?>"><?=trim($RsRefs[cReferencia])?></a></span>&nbsp;&nbsp;&nbsp;
								<?}?>								
		        </td>
                <td width="130"  valign="top">Estado:&nbsp;</td>
		        <td>
								<? 
								switch ($Rs[nFlgEstado]) {
  							case 1:
									echo "Pendiente";
								break;
								case 2:
									echo "En Proceso";
								break;
								case 3:
									echo "Finalizado";
									$sqlFinTxt="SELECT * FROM Tra_M_Tramite_Movimientos WHERE nEstadoMovimiento=5 And cFlgTipoMovimiento!=4 AND iCodTramite='$_GET[iCodTramite]'";
			            $rsFinTxt=mssql_query($sqlFinTxt,$cnx);
			            $RsFinTxt=MsSQL_fetch_array($rsFinTxt);
			            echo "<div style=color:#7C7C7C>".$RsFinTxt[cObservacionesFinalizar]."</div>";
			            echo "<div style=color:#0154AF>".date("d-m-Y", strtotime($RsFinTxt[fFecFinalizar]))."</div>";
								break;
								}
								?>		        	
		        </td>
		    </tr>
		    <tr>
		        <td width="130" >Doc. Principal:&nbsp;</td>
		        <td >
								<?
								 
	$rsTraDe=mssql_query("SELECT iCodTramite FROM Tra_M_Tramite_Movimientos WHERE iCodTramiteDerivar ='$Rs[iCodTramite]' ",$cnx);
	$RsTraDe=mssql_fetch_array($rsTraDe);
	$rsTrax=mssql_query("SELECT nFlgTipoDoc,cCodificacion FROM Tra_M_Tramite WHERE iCodTramite ='$RsTraDe[iCodTramite]' ",$cnx);
	$RsTrax=mssql_fetch_array($rsTrax);
		switch ($RsTrax[nFlgTipoDoc]){
  				case 1: $ScriptPHP="registroDetalles.php"; break;
  				case 2: $ScriptPHP="registroOficinaDetalles.php"; break;
  				case 3: $ScriptPHP="registroSalidaDetalles.php"; break;
  				}
								?>
								<span><a href="<?=$ScriptPHP?>?iCodTramite=<?=$RsTraDe[iCodTramite]?>"><?=trim($RsTrax[cCodificacion])?></a></span>&nbsp;&nbsp;&nbsp;
																
		        </td>
                <td width="130"  valign="top">&nbsp;</td>
		        <td>        	
		        </td>
		    </tr>
		    </table>
		  	</div>
		  	<img src="images/space.gif" width="0" height="0">
				</fieldset>
		</td>
		</tr>		
    <tr>
		<td>  
		  	<fieldset id="tfa_FlujoOfi" class="fieldset">
		  	<legend class="legend"><a href="javascript:;" onClick="muestra('zonaOficina')" class="LnkZonas">Flujo <img src="images/icon_expand.png" width="16" height="13" border="0"></a></legend>
		    <div id="zonaOficina">
		    <table border="0" align="center" width="860">
		    <tr>
		       <td class="headCellColum" width="100">Oficinas</td>
		       <td class="headCellColum" width="200">Documento</td>
		       <td class="headCellColum" width="300">Indicacion</td>
           <td class="headCellColum" width="400">Responsable</td>
		       <td class="headCellColum" width="120">Fecha Derivo</td>
                <td class="headCellColum" width="120">Fecha de Aceptado</td>
		       <td class="headCellColum" width="100">Estado</td>
		    </tr>
		   	<? 
		   	$sqlM="SELECT * FROM Tra_M_Tramite_Movimientos WHERE (iCodTramite='$_GET[iCodTramite]' OR iCodTramiteRel='$_GET[iCodTramite]') AND (cFlgTipoMovimiento=1 OR cFlgTipoMovimiento=3 OR cFlgTipoMovimiento=5) ORDER BY iCodMovimiento ASC";
		   	$rsM=mssql_query($sqlM,$cnx);
		   	//echo $sqlM;
		    while ($RsM=MsSQL_fetch_array($rsM)){
		      	if ($color == "#CEE7FF"){
			  			$color = "#F9F9F9";
		  			}else{
			  			$color = "#CEE7FF";
		  			}
		  			if ($color == ""){
			  			$color = "#F9F9F9";
		  			}	
				?>
		    <tr bgcolor="<?=$color?>">
		    <td valign="top">
		    			
		    		 <? 
		    		 echo "<table width=\"100\" border=\"0\"><tr>";
		    		 
		       	 $sqlOfiO="SELECT * FROM Tra_M_Oficinas WHERE iCodOficina='$RsM[iCodOficinaOrigen]'";
			       $rsOfiO=mssql_query($sqlOfiO,$cnx);
			       $RsOfiO=MsSQL_fetch_array($rsOfiO);
		       	 echo "<td width=90 align=right><a href=\"javascript:;\" title=\"".trim($RsOfiO[cNomOficina])."\">".$RsOfiO[cSiglaOficina]."</a></td>";
							
						 echo "<td width=20>&nbsp;-&nbsp;</td>";
						 
		     	 	 $sqlOfiD="SELECT * FROM Tra_M_Oficinas WHERE iCodOficina='$RsM[iCodOficinaDerivar]'";
			       $rsOfiD=mssql_query($sqlOfiD,$cnx);
			       $RsOfiD=MsSQL_fetch_array($rsOfiD);
		     	 	 echo "<td width=90 align=left><a href=\"javascript:;\" title=\"".trim($RsOfiD[cNomOficina])."\">".$RsOfiD[cSiglaOficina]."</a></td>";
		     	 	 
		     	 	 echo "</tr></table>";
		     	 	?>
		     	
		    </td>
		    <td valign="top" align="left" width="250">
         
		       		<?
		       		if($RsM[cCodTipoDocDerivar]==''){
              			$sqlTipDoc1="SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$Rs[cCodTipoDoc]'";
					          $rsTipDoc1=mssql_query($sqlTipDoc1,$cnx);
					          $RsTipDoc1=MsSQL_fetch_array($rsTipDoc1);
			          		echo $RsTipDoc1[cDescTipoDoc]."<br>";
							echo "<a style=\"color:#0067CE\" href=\"registroDetalles.php?iCodTramite=".$Rs[iCodTramite]."\" rel=\"lyteframe\" title=\"Detalle del Documento\" rev=\"width: 850px; height: 350px; scrolling: auto; border:no\">";
			          		echo $Rs[cCodificacion];
							echo "</a>";   
              }Else{
              			$sqlTipDoc2="SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$RsM[cCodTipoDocDerivar]'";
					          $rsTipDoc2=mssql_query($sqlTipDoc2,$cnx);
					          $RsTipDoc2=MsSQL_fetch_array($rsTipDoc2);
			          		echo $RsTipDoc2[cDescTipoDoc]."<br>";
			          		echo "<a style=\"color:#0067CE\" href=\"registroDetalles.php?iCodTramite=".$RsM[iCodTramiteDerivar]."\" rel=\"lyteframe\" title=\"Detalle del Documento\" rev=\"width: 850px; height: 350px; scrolling: auto; border:no\">";
							echo $RsM[cNumDocumentoDerivar];
							echo "</a>";             	
              }
		       		?>
		    </td>
		    <td valign="top" align="left">
		       		<?  $sqlIndi=" SELECT * FROM Tra_M_Indicaciones WHERE iCodIndicacion = '$RsM[iCodIndicacionDerivar]'";
			 	$rsIndi=mssql_query($sqlIndi,$cnx);
              	$RsIndi=MsSQL_fetch_array($rsIndi);
              	echo $RsIndi["cIndicacion"];
              	mssql_free_result($rsIndi);
			 ?>
					
		    </td>
            <td valign="top" align="left">
             <?
		       	if($RsM[iCodTrabajadorDerivar]!=""){
		       	$sqlTrbR="SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsM[iCodTrabajadorDerivar]'";
              	$rsTrbR=mssql_query($sqlTrbR,$cnx);
              	$RsTrbR=MsSQL_fetch_array($rsTrbR);
              	echo $RsTrbR["cNombresTrabajador"]." ".$RsTrbR["cApellidosTrabajador"];
              	mssql_free_result($rsTrbR);
		       		}
				if($RsM[iCodTrabajadorDelegado]!=""){
  					$rsDelg=mssql_query("SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsM[iCodTrabajadorDelegado]'",$cnx);
          	$RsDelg=MsSQL_fetch_array($rsDelg);
          	echo "<div style=color:#005B2E;font-size:12px>".$RsDelg["cApellidosTrabajador"]." ".$RsDelg["cNombresTrabajador"]."</div>";
						mssql_free_result($rsDelg);
					}		
		       		?>
            </td>
		    <td valign="top">
		       		<span><?=date("d-m-Y", strtotime($RsM[fFecDerivar]))?></span>
		    </td>
			<td align="center" valign="top">
            <?
        	if($RsM[fFecRecepcion]==""){
        			echo "<div style=color:#ff0000>sin aceptar</div>";
        	}Else{
        			echo "<div style=color:#0154AF>aceptado</div>";
        			echo "<div style=color:#0154AF>".date("d-m-Y", strtotime($RsM[fFecRecepcion]))."</div>";
        			echo "<div style=color:#0154AF;font-size:10px>".date("h:i A", strtotime($RsM[fFecRecepcion]))."</div>";
        	}
        	?>
            </td>
		    <td valign="top" align="center">
		     	 		<?
						  if($RsM[fFecRecepcion]!=""){
		     	 			switch ($RsM[nEstadoMovimiento]) {
  							case 1:
									echo "En Proceso";
								break;
								case 2:
									echo "Derivado"; //movimiento derivado a otra ofi
								break;
								case 3:
									echo "Delegado";
								break;
								case 5:
									echo "Finalizado";
								break;
								}
						  } else{
							  echo "Pendiente";
						  }
						  
		     	 		?>
		    </td>
		    </tr> 
		    <?
		    $contaMov++;
		    }
		    ?>
		    </table> 
		    </div>
		    <img src="images/space.gif" width="0" height="0"> 
		  	</fieldset>
		</td>
		</tr>

    <tr>
		<td>  
		  	<fieldset id="tfa_FlujoOfi" class="fieldset">
		  	<legend class="legend"><a href="javascript:;" onClick="muestra('zonaCopias')" class="LnkZonas">Copias <img src="images/icon_expand.png" width="16" height="13" border="0"></a></legend>
		    <div style="display:none" id="zonaCopias">
		    <table border="0" align="center" width="860">
		    <tr>
		       <td class="headCellColum" width="200">Oficinas</td>
		       <td class="headCellColum" width="400">Indicacion</td>
               <td class="headCellColum" width="400">Responsable</td>
               <td class="headCellColum" width="120">Fecha Derivo</td>
               <td class="headCellColum" width="120">Fecha de Aceptado</td>
               <td class="headCellColum" width="100">Estado</td>
		    </tr>
		   	<? 
		   	$sqlM="SELECT * FROM Tra_M_Tramite_Movimientos WHERE (iCodTramite='$_GET[iCodTramite]' OR iCodTramiteRel='$_GET[iCodTramite]') AND (cFlgTipoMovimiento=4) ORDER BY iCodMovimiento ASC";
		   	$rsM=mssql_query($sqlM,$cnx);
		   	//echo $sqlM;
		    while ($RsM=MsSQL_fetch_array($rsM)){
		      	if ($color == "#FFECEC"){
			  			$color = "#F9F9F9";
		  			}else{
			  			$color = "#FFECEC";
		  			}
		  			if ($color == ""){
			  			$color = "#F9F9F9";
		  			}	
				?>
		    <tr bgcolor="<?=$color?>">
		    <td valign="top">
		    			
		    		 <? 
		    		 echo "<table width=\"200\" border=\"0\"><tr>";
		    		 
		       	 $sqlOfiO="SELECT * FROM Tra_M_Oficinas WHERE iCodOficina='$RsM[iCodOficinaOrigen]'";
			       $rsOfiO=mssql_query($sqlOfiO,$cnx);
			       $RsOfiO=MsSQL_fetch_array($rsOfiO);
		       	 echo "<td width=90 align=right><a href=\"javascript:;\" title=\"".trim($RsOfiO[cNomOficina])."\">".$RsOfiO[cSiglaOficina]."</a></td>";
							
						 echo "<td width=20>&nbsp;-&nbsp;</td>";
						 
		     	 	 $sqlOfiD="SELECT * FROM Tra_M_Oficinas WHERE iCodOficina='$RsM[iCodOficinaDerivar]'";
			       $rsOfiD=mssql_query($sqlOfiD,$cnx);
			       $RsOfiD=MsSQL_fetch_array($rsOfiD);
		     	 	 echo "<td width=90 align=left><a href=\"javascript:;\" title=\"".trim($RsOfiD[cNomOficina])."\">".$RsOfiD[cSiglaOficina]."</a></td>";
		     	 	 
		     	 	 echo "</tr></table>";
		     	 	?>
		     	
		    </td>		    	
		    <td valign="top" align="left">
             <?  $sqlIndi=" SELECT * FROM Tra_M_Indicaciones WHERE iCodIndicacion = '$RsM[iCodIndicacionDerivar]'";
			 	$rsIndi=mssql_query($sqlIndi,$cnx);
              	$RsIndi=MsSQL_fetch_array($rsIndi);
              	echo $RsIndi["cIndicacion"];
              	mssql_free_result($rsIndi);
			 ?>
            </td>
            <td valign="top" align="left">
		       		<?
		       		if($RsM[iCodTrabajadorDerivar]!=""){
		       			$sqlTrbR="SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsM[iCodTrabajadorDerivar]'";
              	$rsTrbR=mssql_query($sqlTrbR,$cnx);
              	$RsTrbR=MsSQL_fetch_array($rsTrbR);
              	echo $RsTrbR["cNombresTrabajador"]." ".$RsTrbR["cApellidosTrabajador"];
              	mssql_free_result($rsTrbR);
		       		}
		       		?>
		    </td>
		    <td valign="top">
		       		<span><?=date("d-m-Y", strtotime($RsM[fFecDerivar]))?></span>
		    </td>
			<td align="center" valign="top">
            <?
        	if($RsM[fFecRecepcion]==""){
        			echo "<div style=color:#ff0000>sin aceptar</div>";
        	}Else{
        			echo "<div style=color:#0154AF>aceptado</div>";
        			echo "<div style=color:#0154AF>".date("d-m-Y", strtotime($RsM[fFecRecepcion]))."</div>";
        			echo "<div style=color:#0154AF;font-size:10px>".date("h:i A", strtotime($RsM[fFecRecepcion]))."</div>";
        	}
        	?>
            </td>
		    <td valign="top" align="center">
		     	 		<?
		     	 			switch ($RsM[nEstadoMovimiento]) {
  							case 1:
									echo "Pendiente";
								break;
								case 2:
									echo "En Proceso"; //movimiento derivado a otra ofi
								break;
								case 3:
									echo "En Proceso"; //por delegar a otro trabajador
								break;
								case 4:
									echo "Finalizado";
								break;
								}
		     	 		?>
		    </td>
		    </tr> 
		    <?
		    $contaMov++;
		    }
		    ?>
		    </table> 
		    </div>
		    <img src="images/space.gif" width="0" height="0"> 
		  	</fieldset>
		</td>
		</tr>

		</table>
					</div>
                 </div>
             </div>
         </div>
     </div>
 </main>

<div>		
</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>
