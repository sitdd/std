<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: consultaEntradaGeneral_pdf.php
SISTEMA: SISTEMA  DE TRÁMITE DOCUMENTARIO DIGITAL
OBJETIVO: Reporte General en PDF
PROPIETARIO: AGENCIA PERUANA DE COOPERACIÓN INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripción
------------------------------------------------------------------------
1.0   APCI    05/09/2018      Creación del programa.
------------------------------------------------------------------------
*****************************************************************************************/
session_start();
ob_start();
//*************************************
include_once("../conexion/conexion.php");
?>
<page backtop="15mm" backbottom="15mm" backleft="10mm" backright="10mm">
	<page_header>
		<table style="width: 1000px; border: solid 0px black;">
			<tr>
				<td style="text-align:left;	width: 20px"></td>
				<td style="text-align:left;	width: 980px">
					<img style="width: 280px" src="images/pdf_apci.jpg" alt="Logo">
				</td>
			</tr>
		</table>
		<br><br>
	</page_header>
	<page_footer>
		<table style="width: 100%; border: solid 0px black;">
			<tr>
                <td style="text-align: center;	width: 40%">
				<? 
				   $sqllog="select cNombresTrabajador, cApellidosTrabajador from tra_m_trabajadores where iCodTrabajador='$_SESSION[CODIGO_TRABAJADOR]' "; 
				   $rslog=mssql_query($sqllog,$cnx);
				   $Rslog=MsSQL_fetch_array($rslog);
				   echo $Rslog[cNombresTrabajador]." ".$Rslog[cApellidosTrabajador];
				?></td>
				<td style="text-align: right;	width: 60%">p�gina [[page_cu]]/[[page_nb]]</td>
			</tr>
		</table>
        <br>
        <br>
	</page_footer>
	

							
							<?
// ordenamiento
if($_GET[campo]==""){
	$campo="Tra_M_Tramite.iCodTramite";
}Else{
	$campo=$_GET[campo];
}

if($_GET[orden]==""){
	$orden="DESC";
}Else{
	$orden=$_GET[orden];
}

//invertir orden
if($orden=="ASC") $cambio="DESC";
if($orden=="DESC") $cambio="ASC";

	if($fecini!=''){$fecini=date("Ymd G:i", strtotime($fecini));}
    if($fecfin!=''){
    $fecfin=date("Y-m-d G:i", strtotime($fecfin));
	function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    $date_r = getdate(strtotime($date));
    $date_result = date("Ymd G:i", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
    return $date_result;
				}
	$fecfin=dateadd($fecfin,0,0,0,0,0,0); // + 1 dia
	}
							if($_GET[cReferenciaPCM]!="" && $_GET[cCodTipoDoc]!="" &&  $_GET[cCodificacion]=="" && $_GET[iCodOficina]=="" &&  $_GET[cAsunto]=="" && $_GET[cReferencia]=="" && $_GET[iCodTupa]=="" && $_GET[cNombre]=="" && $_GET[iCodTrabajadoResponsable]=="" && $_GET[cNroDocumento]=="" && $_GET[cNomRemite]==""){
							  $sqlpcm="SELECT * FROM Tra_M_Tramite ";
							  $sqlpcm.=" WHERE (Tra_M_Tramite.nFlgTipoDoc=3) ";
							  $sqlpcm.="AND Tra_M_Tramite.cCodificacion LIKE '%$_GET[cReferenciaPCM]%' ";
							 	$sqlpcm.="AND Tra_M_Tramite.cCodTipoDoc='$_GET[cCodTipoDoc]' ";
								$sqlpcm.="AND cReferencia IS NOT NULL AND cReferencia!='' ";
							  $rspcm=mssql_query($sqlpcm,$cnx);
								$salida = MsSQL_num_rows($rspcm);
								if($salida != 0 ){
							    while($Rspcm=MsSQL_fetch_array($rspcm)){
								 	$sqlcod="SELECT TOP 100 * FROM Tra_M_Tramite ";
								 	$sqlcod.=" WHERE (Tra_M_Tramite.nFlgTipoDoc=1) ";
							    $sqlcod.="AND Tra_M_Tramite.cCodificacion = '$Rspcm[cReferencia]' ";
							    $rs=mssql_query($sqlcod,$cnx);
								 	}
								}else if($salida == 0 ){
								 	$sqlcod="SELECT TOP 100 * FROM Tra_M_Tramite ";
								 	$sqlcod.=" WHERE (Tra_M_Tramite.nFlgTipoDoc=1) ";
							    $sqlcod.="AND Tra_M_Tramite.cCodificacion = '' ";
									$rs=mssql_query($sqlcod,$cnx);
								} 
							}
							  
							if($_GET[cReferenciaPCM]=="" or $fecini!="" or $fecfin!="" or $_GET[cCodificacion]!="" or $_GET[iCodOficina]!=""  or $_GET[cAsunto]!="" or $_GET[cReferencia]!="" or $_GET[iCodTupa]!="" or $_GET[cNombre]!="" or $_GET[iCodTrabajadoResponsable]!="" or $_GET[cNroDocumento]!="" or $_GET[cNomRemite]!=""){
								
								$sqlgrupo =" SELECT iCodGrupoTramite FROM Tra_M_Grupo_Tramite_Detalle WHERE iCodTrabajador='$_SESSION[CODIGO_TRABAJADOR]' ";
							   	$rsgrupo=mssql_query($sqlgrupo,$cnx);
							   	$Rsgrupo=MsSQL_fetch_array($rsgrupo);
							   
							   	$sqlDet= " SELECT iCodTrabajador FROM Tra_M_Grupo_Tramite_Detalle WHERE iCodGrupoTramite='$Rsgrupo[iCodGrupoTramite]' ";
							   	$rsDet=mssql_query($sqlDet,$cnx);
							   	$Det = MsSQL_num_rows($rsDet);
							   	$cont=0;$cont2=0;
								while($RsDet=MsSQL_fetch_array($rsDet)){
								   		$cont=$cont+1;
								   		$cont2=$cont2+1;
							  			$cadena= $cadena."  Tra_M_Tramite.iCodTrabajadorRegistro='$RsDet[iCodTrabajador]' "; 
							  			if($cont < $Det){ $cadena.=" OR  "; }
									}
								
								$sqlOfi=" SELECT DISTINCT iCodOficinaDerivar FROM Tra_M_Tramite_Movimientos WHERE (nFlgTipoDoc=1 OR nFlgTipoDoc=4) ";
								$sqlOfi.="And  iCodOficinaOrigen=1 and ( iCodOficinaDerivar != 0  and iCodOficinaDerivar != 1) ";
								$rsOfi=mssql_query($sqlOfi,$cnx);
							  while($RsOfi=MsSQL_fetch_array($rsOfi)){
							  $sqlOfis=" SELECT cNomOficina FROM Tra_M_Oficinas WHERE iCodOficina='".$RsOfi[iCodOficinaDerivar]."'" ;	
							  $rsOfis=mssql_query($sqlOfis,$cnx);
							  $RsOfis=MsSQL_fetch_array($rsOfis);
							  
							   $sql="SELECT TOP 4000 * FROM Tra_M_Tramite ";
   	$sql.=" LEFT OUTER JOIN Tra_M_Tramite_Movimientos ON Tra_M_Tramite.iCodTramite=Tra_M_Tramite_Movimientos.iCodTramite ";
  							  
							  if($_GET[cNombre]!=""){
							  	$sql.=" LEFT OUTER JOIN Tra_M_Remitente ON Tra_M_Tramite.iCodRemitente=Tra_M_Remitente.iCodRemitente ";
								}
							  	$sql.=" WHERE (Tra_M_Tramite.nFlgTipoDoc=1 OR Tra_M_Tramite.nFlgTipoDoc=4)   And iCodOficinaOrigen=1 And ( iCodOficinaDerivar ='".$RsOfi[iCodOficinaDerivar]."' and Tra_M_Tramite_Movimientos.iCodOficinaDerivar!=1 )  and Tra_M_Tramite_Movimientos.cFlgTipoMovimiento!=4 AND ( ";
							    $sql.=" $cadena ) ";
									
							  if($fecini!="" AND $fecfin==""){
							  	$sql.=" AND Tra_M_Tramite.fFecRegistro>'$fecini' ";
							  }
							  if($fecini=="" AND $fecfin!=""){
							  	$sql.=" AND Tra_M_Tramite.fFecRegistro<='$fecfin' ";
							  }
							  if($fecini!="" && $fecfin!=""){
							  //$sql.=" AND Tra_M_Tramite.fFecRegistro BETWEEN  '$fDesde' and '$fHasta' ";
							  $sql.=" AND Tra_M_Tramite.fFecRegistro BETWEEN '$fecini' and '$fecfin' ";
							  }
								if($_GET[cCodificacion]!=""){
							     $sql.="AND Tra_M_Tramite.cCodificacion LIKE '%$_GET[cCodificacion]%' ";
							  }
								if($_GET[cReferencia]!=""){
							     $sql.="AND Tra_M_Tramite.cReferencia LIKE '%$_GET[cReferencia]%' ";
							  }
							  if($_GET[cAsunto]!=""){
							     $sql.="AND Tra_M_Tramite.cAsunto LIKE '%$_GET[cAsunto]%' ";
							  }
								if($_GET[iCodTupa]!=""){
							     $sql.="AND Tra_M_Tramite.iCodTupa='$_GET[iCodTupa]' ";
							  }
								if($_GET[cCodTipoDoc]!=""){
							        $sql.="AND Tra_M_Tramite.cCodTipoDoc='$_GET[cCodTipoDoc]' ";
							  }
							  if($_GET[cNombre]!=""){
							   $sql.="AND Tra_M_Remitente.cNombre LIKE '%$_GET[cNombre]%' ";
							  }
							   if($_GET[cNomRemite]!=""){
   								$sql.="AND Tra_M_Tramite.cNomRemite LIKE '%$_GET[cNomRemite]%' ";
  								}
								if($_GET[iCodOficina]!=""){
							   $sql.="AND Tra_M_Tramite_Movimientos.iCodOficinaDerivar='$_GET[iCodOficina]'   ";
							  }
							  if($_GET[cNroDocumento]!=""){
							   $sql.="AND Tra_M_Tramite.cNroDocumento LIKE '%$_GET[cNroDocumento]%' ";
							  }
								 $sql.= " ORDER BY Tra_M_Tramite.fFecRegistro DESC";	   
							   $rs=mssql_query($sql,$cnx);
							   $Num = MsSQL_num_rows($rs);
							 //  echo $sql;
							   if($Num!=0){
						?>
                        	   
							<table style="width: 1000px; border: solid 0px black;">
							<tr>
							<td style="text-align:left;width:1000px"><br><span style="font-size: 15px; font-weight: bold"><?=$RsOfis[cNomOficina]?></span></td>
							</tr>
							</table>
						
							<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="center">
							<thead>
								<tr>
									<th style="width: 120px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N&ordm; Documento</th>
									<th style="width: 130px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N&ordm; Referencia</th>
									<th style="width: 150px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Institución</th>
									<th style="width: 150px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Fecha Derivo</th>
									<th style="width: 300px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Asunto</th>
									<th style="width: 150px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Firma / Sello</th>
								</tr>
							</thead>
							<tbody>
						<?	
							 while ($Rs=MsSQL_fetch_array($rs)){
							 ?>
							 <tr>
						      <td style="width:120px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;vertical-align:top"><?=$Rs[cCodificacion]?></td>
						      <td style="width:130px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase;vertical-align:top"><?=$Rs[cNroDocumento]?></td>
						      <td style="width:150px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase;vertical-align:top">
						      	<?
						      	$sqlRemi="SELECT * FROM Tra_M_Remitente WHERE iCodRemitente='$Rs[iCodRemitente]'";
												$rsRemi=mssql_query($sqlRemi,$cnx);
												$RsRemi=MsSQL_fetch_array($rsRemi);
												echo "<div>".$RsRemi[cNombre]."</div>";
													if($Rs[cNomRemite]!=""){
														if($RsRemi[cTipoPersona]==1){ echo "<div style=\"color:#408080\">Personal Natural:</div>"; }
													}
								echo "<div style=\"text-transform:uppercase\">".$Rs[cNomRemite]."</div>";
      							if($Rs[nFlgTipoDoc]==4){
      									echo "<div>ANEXO</div>";
      							}
						      	?>
						      </td>
						      <td style="width:150px;text-align:center;border: solid 1px #6F6F6F;font-size:10px;vertical-align:top">
						      	<?
						      	if($Rs[nFlgEnvio]==1){
						      		$sqlM="select TOP 1 * from Tra_M_Tramite_Movimientos WHERE iCodTramite='$Rs[iCodTramite]'";
      								$rsM=mssql_query($sqlM,$cnx);
	    								$RsM=MsSQL_fetch_array($rsM);
						      		echo date("d-m-Y G:i:s", strtotime($RsM[fFecDerivar]));//date("d-m-Y h:i A", strtotime($RsM[fFecDerivar]));
						      	}
						      	?>
						      </td>
						      <td style="width:300px;text-align:justify; border: solid 1px #6F6F6F;font-size:10px;vertical-align:top">
							   <? echo $Rs[cAsunto];
							  if($Rs[iCodTupa]!=""){
    						  $sqlTup="SELECT * FROM Tra_M_Tupa WHERE iCodTupa='$Rs[iCodTupa]'";
      							$rsTup=mssql_query($sqlTup,$cnx);
      							$RsTup=MsSQL_fetch_array($rsTup);
								?>
       							 <br>
     							 <? echo $RsTup["cNomTupa"];
							  }?></td>
						      <td style="width:150px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase">&nbsp;</td> 
						  </tr>
						  <? }?>
						  </tbody>
                           </table>
							<? 
							   }
							}
								   }
								
						?>	  	
							
							  
						
</page>

<?
//*************************************


	$content = ob_get_clean();  set_time_limit(0);     ini_set('memory_limit', '640M');

	// conversion HTML => PDF
	require_once(dirname(__FILE__).'/html2pdf/html2pdf.class.php');
	try
	{
		$html2pdf = new HTML2PDF('L','A4', 'es', false, 'UTF-8', 3);
		$html2pdf->pdf->SetDisplayMode('fullpage');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output('exemple03.pdf');
	}
	catch(HTML2PDF_exception $e) { echo $e; }
?>   
         		
