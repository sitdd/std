<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: iu_actualiza_grupo_tramite.php
SISTEMA: SISTEMA  DE TR�MITE DOCUMENTARIO DIGITAL
OBJETIVO: Mantenimiento de la Tabla Maestra de Grupos de Tramite DOCUMENTARIO DIGITAL para el Perfil Administrador
          -> Actualizar Registro de Grupos de Tramite DOCUMENTARIO DIGITAL
PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver      Autor             Fecha        Descripci�n
------------------------------------------------------------------------
1.0   APCI       03/08/2018   Creaci�n del programa.
 
------------------------------------------------------------------------
*****************************************************************************************/
session_start();
If($_SESSION['CODIGO_TRABAJADOR']!=""){
include_once("../conexion/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
<script>
function validar(f) {
 var error = "Por favor, antes de crear complete:\n\n";
 var a = "";
  if (f.txtindicador.value == "") {
  a += " Ingrese Descripci�n de Indicador";
  alert(error + a);
 }
   
 return (a == "");
 
}
</script>
</head>
<body>

	<?include("includes/menu.php");?>



<!--Main layout-->
 <main class="mx-lg-5">
     <div class="container-fluid">
          <!--Grid row-->
         <div class="row wow fadeIn">
              <!--Grid column-->
             <div class="col-md-12 mb-12">
                  <!--Card-->
                 <div class="card">
                      <!-- Card header -->
                     <div class="card-header text-center ">
                         >>
                     </div>
                      <!--Card content-->
                     <div class="card-body">

<div class="AreaTitulo">Maestra Grupos de Tr&aacute;mite</div>

<?
require_once("../cAccesoBaseDato_SITD/ad_busqueda.php");
?>

<form action="../cLogicaNegocio_SITD/ln_actualiza_grupo_tramite.php" onSubmit="return validar(this)" method="post"  name="form1">
<input name="txtcod_grupo" type="hidden" id="txtcod_grupo" value="<? echo $Rs[iCodGrupoTramite]; ?>">

            <fieldset id="tfa_DatosPersonales" class="fieldset"  >
            <legend class="legend">Datos de Grupo </legend>
        <table border="0">
           <tr>
              <td width="114"></td>
              <td >Descripci&oacute;n :</td>
              <input name="txtgrupo" type="text" id="txtgrupo"  maxlength="30" value="<? echo trim($Rs[cDesGrupoTramite]); ?>" size="40" class="FormPropertReg form-control">
              </td>
           </tr>
           <tr>
              <td height="45" colspan="3" align="center">
              <button class="btn btn-primary"  type="submit" id="Actualizar Indicador" onMouseOver="this.style.cursor='hand'"> <b>Actualizar</b> <img src="images/page_refresh.png" width="17" height="17" border="0"> </button>
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<button class="btn btn-primary" type="button" onclick="window.open('iu_grupo_tramite.php', '_self');" onMouseOver="this.style.cursor='hand'"> <b>Cancelar</b> <img src="images/icon_retornar.png" width="17" height="17" border="0"> </button>
           </td>
        </table>
        </fieldset>

</form>  
</td>
		</tr>
		</table>
 
<div>		

<?include("includes/userinfo.php");?>

<?include("includes/pie.php");?>

</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>