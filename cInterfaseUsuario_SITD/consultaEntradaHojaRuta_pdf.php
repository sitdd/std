<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: consultaEntradaGeneral_pdf.php
SISTEMA: SISTEMA  DE TRÁMITE DOCUMENTARIO DIGITAL
OBJETIVO: Reporte General en PDF
PROPIETARIO: AGENCIA PERUANA DE COOPERACIÓN INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripción
------------------------------------------------------------------------
1.0   APCI    05/09/2018      Creación del programa.
------------------------------------------------------------------------
*****************************************************************************************/
session_start();
ob_start();
//*************************************

include_once("../conexion/conexion.php");
$fDesde=date("Ymd H:i", strtotime($_GET[fDesde]));
$fHasta=date("Ymd H:i", strtotime($_GET[fHasta]));

$sql="SELECT TOP 20 *,Tra_M_Tramite.cObservaciones AS Observaciones   FROM Tra_M_Tramite ";
  if($_GET[iCodOficina]!=""){
  	$sql.=" LEFT OUTER JOIN Tra_M_Tramite_Movimientos ON Tra_M_Tramite.iCodTramite=Tra_M_Tramite_Movimientos.iCodTramite ";
  }
  if($_GET[cNombre]!=""){
  $sql.=" LEFT OUTER JOIN Tra_M_Remitente ON Tra_M_Tramite.iCodRemitente=Tra_M_Remitente.iCodRemitente ";
	}
  $sql.=" WHERE (Tra_M_Tramite.nFlgTipoDoc=1 OR Tra_M_Tramite.nFlgTipoDoc=4)  ";
  if($_GET[fDesde]!="" AND $_GET[fHasta]==""){
  	$sql.=" AND Tra_M_Tramite.fFecRegistro>'$fDesde' ";
  }
  if($_GET[fDesde]=="" AND $_GET[fHasta]!=""){
  	$sql.=" AND Tra_M_Tramite.fFecRegistro<='$fHasta' ";
  }
  if($_GET[fDesde]!="" && $_GET[fHasta]!=""){
  //$sql.=" AND Tra_M_Tramite.fFecRegistro BETWEEN  '$fDesde' and '$fHasta' ";
  $sql.=" AND Tra_M_Tramite.fFecRegistro>'$fDesde' and Tra_M_Tramite.fFecRegistro<'$fHasta' ";
  }
	if($_GET[cCodificacion]!=""){
     $sql.="AND Tra_M_Tramite.cCodificacion='$_GET[cCodificacion]' ";
  }
  if($_GET[cNroDocumento]!=""){
     $sql.="AND Tra_M_Tramite.cNroDocumento='$_GET[cNroDocumento]' ";
  }
	if($_GET[cReferencia]!=""){
     $sql.="AND Tra_M_Tramite.cReferencia LIKE '%$_GET[cReferencia]%' ";
  }
  if($_GET[cAsunto]!=""){
     $sql.="AND Tra_M_Tramite.cAsunto LIKE '%$_GET[cAsunto]%' ";
  }
	if($_GET[iCodTupa]!=""){
     $sql.="AND Tra_M_Tramite.iCodTupa='$_GET[iCodTupa]' ";
  }
	if($_GET[cCodTipoDoc]!=""){
        $sql.="AND Tra_M_Tramite.cCodTipoDoc='$_GET[cCodTipoDoc]' ";
  }
  if($_GET[cNombre]!=""){
   $sql.="AND Tra_M_Remitente.cNombre LIKE '%$_GET[cNombre]%' ";
  }
	if($_GET[iCodOficina]!=""){
   $sql.="AND Tra_M_Tramite_Movimientos.iCodOficinaOrigen='$_GET[iCodOficina]' ";
  }
$sql.= " ORDER BY Tra_M_Tramite.iCodTramite DESC";	   
$rs=mssql_query($sql,$cnx);
while ($Rs=MsSQL_fetch_array($rs)){
?>
<page backtop="15mm" backbottom="10mm" backleft="10mm" backright="10mm">
	<table style="width:400px;border:solid 0px black;">
	<tr>
	<td style="text-align:left;	width: 100%">
					<img style="width:300px" src="images/pdf_pcm.jpg" alt="Logo">
	</td>
	</tr>
	</table>
		
	<table style="width:400px;border: solid 0px #585858; border-collapse: collapse" align="left">
	<tr>
	<td style="width:400px;text-align:left; border: solid 0px #585858;font-family:Times">
	Datos Principales
	</td>
	</tr>
	</table>
	<table border="1" style="width:400px;border: solid 1px #585858; border-collapse: collapse" align="left">
	<tr>
  <td style="width: 10%; text-align: center; border: solid 1px #585858;">
  		<table style="width:400px;border: solid 0px #585858; border-collapse: collapse">
  		<tr>
  		<td style="width:130px;height:10px;text-align:left; border: solid 0px #585858;font-size:12px;font-family:Times">Nro Registro:</td>
  		<td style="width:5px;text-align:left; border: solid 0px #585858;font-size:12px">:</td>
  		<td style="width:265px;text-align:left; border: solid 0px #585858;font-size:12px;font-family:Times"><b><?=$Rs[cCodificacion]?></b></td>
  		</tr>
  		
  		<tr>
  		<td style="width:130px;height:10px;text-align:left; border: solid 0px #585858;font-size:12px;font-family:Times">Fecha/H de Registro:</td>
  		<td style="width:5px;text-align:left; border: solid 0px #585858;font-size:12px">:</td>
  		<td style="width:265px;text-align:left; border: solid 0px #585858;font-size:12px;font-family:Times"><b>
  			<?
  			$PrintMes=array("","ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SET","OCT","NOV","DIC");
  			$fechaMes=date("m", strtotime($Rs[fFecRegistro]));
    		$fechaMesEntero = intval($fechaMes);  
    		echo date("d", strtotime($Rs[fFecRegistro]));
    		echo "-".$PrintMes[$fechaMesEntero]."-";
  			echo date("Y h:i:s", strtotime($Rs[fFecRegistro]));
  			?>
  			</b>
  		</td>
  		</tr>
  		
  		<tr>
  		<td style="width:130px;height:10px;text-align:left; border: solid 0px #585858;font-size:12px;font-family:Times">Area Origen:</td>
  		<td style="width:5px;text-align:left; border: solid 0px #585858;font-size:12px">:</td>
  		<td style="width:265px;text-align:left; border: solid 0px #585858;font-size:12px;font-family:Times"><b>
  				<?
  				$sqlOfi1="SELECT * FROM Tra_M_Trabajadores,Tra_M_Oficinas WHERE Tra_M_Trabajadores.iCodOficina=Tra_M_Oficinas.iCodOficina AND Tra_M_Trabajadores.iCodTrabajador='$Rs[iCodTrabajadorRegistro]'";
					$rsOfi1=mssql_query($sqlOfi1,$cnx);
					$RsOfi1=MsSQL_fetch_array($rsOfi1);
					echo $RsOfi1[cNomOficina];
  				?>	
  		</b></td>
  		</tr>
  		
  		<tr>
  		<td style="width:130px;height:10px;text-align:left; border: solid 0px #585858;font-size:12px;font-family:Times">Fecha/H Derivo:</td>
  		<td style="width:5px;text-align:left; border: solid 0px #585858;font-size:12px">:</td>
  		<td style="width:265px;text-align:left; border: solid 0px #585858;font-size:12px;font-family:Times"><b>
  				<?
  				$sqlM1="SELECT TOP 1 * FROM Tra_M_Tramite_Movimientos WHERE iCodTramite='$Rs[iCodTramite]' AND cFlgTipoMovimiento=1 ORDER BY iCodMovimiento ASC";
					$rsM1=mssql_query($sqlM1,$cnx);
					$RsM1=MsSQL_fetch_array($rsM1);
  				$fechaMes=date("m", strtotime($RsM1[fFecDerivar]));
    			$fechaMesEntero = intval($fechaMes);  
    			echo date("d", strtotime($RsM1[fFecDerivar]));
    			echo "-".$PrintMes[$fechaMesEntero]."-";
  				echo date("Y h:i:s", strtotime($RsM1[fFecDerivar]));
  				?>
  		</b></td>
  		</tr>
  		
  		<tr>
  		<td style="width:130px;height:10px;text-align:left; border: solid 0px #585858;font-size:12px;font-family:Times">Nro de Referencia:</td>
  		<td style="width:5px;text-align:left; border: solid 0px #585858;font-size:12px">:</td>
  		<td style="width:265px;text-align:left; border: solid 0px #585858;font-size:12px;text-transform:uppercase;font-family:Times"><b>
		<? echo $Rs[cNroDocumento];?></b></td>
  		</tr>
  		
  		<tr>
  		<td style="width:130px;height:10px;text-align:left; border: solid 0px #585858;font-size:12px;font-family:Times; vertical-align:bottom">Institución:</td>
  		<td style="width:5px;text-align:left; border: solid 0px #585858;font-size:12px; vertical-align:bottom">:</td>
  		<td style="width:265px;text-align:left; border: solid 0px #585858;font-size:12px;font-family:Times"><b>
				<? 
			   $sqlRemi="SELECT * FROM Tra_M_Remitente WHERE iCodRemitente='$Rs[iCodRemitente]'";
			   $rsRemi=mssql_query($sqlRemi,$cnx);
			   $RsRemi=MsSQL_fetch_array($rsRemi);
			   echo $RsRemi[cNombre];
			  ?>
  			</b>
  		</td>
  		</tr>
  		
  		<tr>
  		<td style="width:130px;height:10px;text-align:left; border: solid 0px #585858;font-size:12px;font-family:Times">Remitente:</td>
  		<td style="width:5px;text-align:left; border: solid 0px #585858;font-size:12px">:</td>
  		<td style="width:265px;text-align:left; border: solid 0px #585858;font-size:12px;text-transform:uppercase;font-family:Times"><b><? echo $Rs[cNomRemite];?></b></td>
  		</tr>
  		
			<tr>
  		<td style="width:130px;height:10px;text-align:left; border: solid 0px #585858;font-size:12px;font-family:Times">Tipo Documento:</td>
  		<td style="width:5px;text-align:left; border: solid 0px #585858;font-size:12px">:</td>
  		<td style="width:265px;text-align:left; border: solid 0px #585858;font-size:12px;font-family:Times"><b>
				<?
					$sqlTipDoc="SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc=$Rs[cCodTipoDoc]";
			    $rsTipDoc=mssql_query($sqlTipDoc,$cnx);
			    $RsTipDoc=MsSQL_fetch_array($rsTipDoc);
			    echo $RsTipDoc[cDescTipoDoc];
				?>
  			</b>
  		</td>
  		</tr>		        		
  		</table>
	</td>
  </tr>
	</table>
	
	<table style="width:410px;border: solid 0px #585858; border-collapse: collapse" align="left">
	<tr>
	<td style="width:410px;height:20px;text-align:left; vertical-align:bottom; border: solid 0px #585858;font-family:Times">
	Asunto
	</td>
	</tr>
	</table>

	<table border="1" style="width:410px;border: solid 1px #585858; border-collapse: collapse" align="left">
	<tr>
  <td style="width: 410px; text-align:left; border: solid 1px #585858;font-family:Times"><?=$Rs[cAsunto];?></td>
  </tr>
	</table>

	<br>

	<table border="1" style="width:400px;border: solid 1px #585858; border-collapse: collapse" align="left">
	<tr>
  <td style="width:10px; text-align: left; border: solid 1px #585858;"></td>
  <td style="width:88px; text-align: left; border: solid 1px #585858;font-size:10px;font-family:Times">Destino</td>
	<td style="width:20px; text-align: left; border: solid 1px #585858;font-size:10px;font-family:Times">Ind</td>
  <td style="width:80px; text-align: left; border: solid 1px #585858;font-size:10px;font-family:Times">Fecha Trans</td>
  <td style="width:23px; text-align: left; border: solid 1px #585858;font-size:10px;font-family:Times">Fls</td>
  <td style="width:40px; text-align: left; border: solid 1px #585858;font-size:10px;font-family:Times">V.B.</td>
  <td style="width:88px; text-align: left; border: solid 1px #585858;font-size:10px;font-family:Times">C.Recep</td>
  </tr>

	<?
	$sqlM="SELECT TOP 1 * FROM Tra_M_Tramite_Movimientos WHERE iCodTramite='$Rs[iCodTramite]' AND cFlgTipoMovimiento=1 ORDER BY iCodMovimiento ASC";
	$rsM=mssql_query($sqlM,$cnx);
	$RsM=MsSQL_fetch_array($rsM);
	?>
  <tr>
  <td style="width:10px;height:20px; text-align: center; border: solid 1px #585858;font-size:10px;font-family:Times">1</td>
  <td style="width:88px;height:20px; text-align: left; border: solid 1px #585858;font-size:10px;font-family:Times">
  	<?
  	$sqlOfiD="SELECT * FROM Tra_M_Oficinas WHERE iCodOficina='$RsM[iCodOficinaDerivar]'";
		$rsOfiD=mssql_query($sqlOfiD,$cnx);
		$RsOfiD=MsSQL_fetch_array($rsOfiD);
		echo $RsOfiD[cSiglaOficina];
  	?>
	</td>
	
	<td style="width:20px;height:20px; text-align:left; border: solid 1px #585858;font-size:10px;font-family:Times">
  	<?
		$sqlIndic="SELECT * FROM Tra_M_Indicaciones WHERE iCodIndicacion='$RsM[iCodIndicacionDerivar]'";
    $rsIndic=mssql_query($sqlIndic,$cnx);
    $RsIndic=MsSQL_fetch_array($rsIndic);
				echo substr($RsIndic["cIndicacion"],0,2);
    mssql_free_result($rsIndic);
		?>
	</td>
	
  <td style="width:80px;height:20px; text-align:left; border: solid 1px #585858;font-size:10px;font-family:Times">
  	<?
  	$fechaMes=date("m", strtotime($RsM[fFecDerivar]));
    $fechaMesEntero = intval($fechaMes);  
    echo date("d", strtotime($RsM[fFecDerivar]));
    echo "-".$PrintMes[$fechaMesEntero]."-";
  	echo date("Y", strtotime($RsM[fFecDerivar]));
  	?>
	</td>

  <td style="width:20px;height:23px; text-align: center; border: solid 1px #585858;font-size:10px;font-family:Times">
  	1
	</td>
  <td style="width:40px;height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="width:88px;height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  </tr>
  
  <tr>
  <td style="height:20px; text-align: center; border: solid 1px #585858;font-size:10px;font-family:Times">2</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
	</tr>

  <tr>
  <td style="height:20px; text-align: center; border: solid 1px #585858;font-size:10px;font-family:Times">3</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
	</tr>  

  <tr>
  <td style="height:20px; text-align: center; border: solid 1px #585858;font-size:10px;font-family:Times">4</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
	</tr>
	
  <tr>
  <td style="height:20px; text-align: center; border: solid 1px #585858;font-size:10px;font-family:Times">5</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
	</tr> 	
	</table>
	
	<table style="width:410px;border: solid 0px #585858; border-collapse: collapse" align="left">
	<tr>
	<td style="width:410px;height:20px;text-align:left; vertical-align:bottom; border: solid 0px #585858;font-family:Times">
	Obserbaciones
	</td>
	</tr>
	</table>
	
	<table border="0" align="left">
	<tr>
  <td style="width:420px; text-align:left; border: solid 0px #585858;font-family:Times"><?=$Rs[Observaciones]?></td>
  </tr>
	</table>
	................................................................................................................<br>
	................................................................................................................<br>
	................................................................................................................<br>
	
	<table style="width:410px;border: solid 0px #585858; border-collapse: collapse" align="left">
	<tr>
	<td style="width:410px;height:30px;text-align:left;vertical-align:bottom; border: solid 0px #585858;font-family:Times">
	Referencias:
	</td>
	</tr>
	</table>
	
	<table style="width:410px;border: solid 0px #585858; border-collapse: collapse" align="left">
	<tr>
	<td style="width:410px;height:40px;text-align:left;vertical-align:bottom; border: solid 0px #585858;font-family:Times">
	Indicaciones:
	</td>
	</tr>
	</table>

	<table style="width:400px;border: solid 0px #585858; border-collapse: collapse">
  		<tr>
  		<td style="width:140px;height:8px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">01.ACCION NECESARIA</td>
  		<td style="width:140px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">02.ESTUDIO E INFORME</td>
  		<td style="width:120px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">03.CONOCIMIENTO Y FINES</td>
  		</tr>
		<tr>
			<td style="width:140px;height:8px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">04.FORMULAR RESPUESTA</td>
  		<td style="width:140px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">05.POR CORRESPONDERLE</td>
  		<td style="width:120px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">06.TRANSCRIBIR</td>
  		</tr>
		<tr>
			<td style="width:140px;height:8px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">07.PROYECTAR DISPOSITIVO</td>
  		<td style="width:140px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">08.FIRMAR Y/O REVISAR</td>
  		<td style="width:120px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">09.ARCHIVAR</td>
  		</tr>
		<tr>
			<td style="width:140px;height:8px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">10.CONOCIMIENTO Y RESPUESTA</td>
  		<td style="width:140px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">11.PARA COMENTARIOS</td>
  		<td style="width:120px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times"></td>
  		</tr>
 </table>

	
	
</page>
<?
}
//*************************************


	$content = ob_get_clean();  set_time_limit(0);     ini_set('memory_limit', '640M');

	// conversion HTML => PDF
	require_once(dirname(__FILE__).'/html2pdf/html2pdf.class.php');
	try
	{
		$html2pdf = new HTML2PDF('P','A4', 'es', false, 'UTF-8', 3);
		$html2pdf->pdf->SetDisplayMode('fullpage');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output('exemple03.pdf');
	}
	catch(HTML2PDF_exception $e) { echo $e; }
?>   
         		
