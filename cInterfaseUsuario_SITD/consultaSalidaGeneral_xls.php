<?php
session_start();
include_once("../conexion/conexion.php");
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=consultaSalidaGeneral.xls");
	
	$anho = date("Y");
	$datomes = date("m");
	$datomes = $datomes*1;
	$datodia = date("d");
	$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre");
	
	echo "<table width=780 border=0><tr><td align=center colspan=9>";
	echo "<H3>REPORTE - DOCUMENTOS DE SALIDA</H3>";
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=right colspan=9>";
	echo "SITD, ".$datodia." ".$meses[$datomes].' del '.$anho;
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=left colspan=9>";
	$sqllog="select cNombresTrabajador, cApellidosTrabajador from tra_m_trabajadores where iCodTrabajador='$_SESSION[CODIGO_TRABAJADOR]' "; 
	$rslog=mssql_query($sqllog,$cnx);
	$Rslog=MsSQL_fetch_array($rslog);
	echo "GENERADO POR : ".$Rslog[cNombresTrabajador]." ".$Rslog[cApellidosTrabajador];
	echo " ";
?>	
	<table style="width: 780px;border: solid 1px #5544DD; border-collapse: collapse" align="center">
     <thead>
      <tr>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Fecha</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Oficina Origen</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Tipo de Documento</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Asunto</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Observaciones</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Solicitado por</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Nro Referencia</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Destino</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Requiere Respuesta</th>
      </tr>
	 </thead>
     <tbody>
<?	
   if($fecini!=''){$fecini=date("Ymd", strtotime($fecini));}
    if($fecfin!=''){
    $fecfin=date("Y-m-d", strtotime($fecfin));
	function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    $date_r = getdate(strtotime($date));
    $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
    return $date_result;
				}
	$fecfin=dateadd($fecfin,1,0,0,0,0,0); // + 1 dia
	}
 
	 $sql="SP_CONSULTA_SALIDA_GENERAL '$fecini', '$fecfin','$_GET[RespuestaSI]', '$_GET[RespuestaNO]', '%$_GET[cCodificacion]%', '%$_GET[cAsunto]%', '%$_GET[cObservaciones]%', '$_GET[cCodTipoDoc]','%$_GET[cNombre]%', '%$_GET[cNomRemite]%', '$_GET[Respuesta]','$_GET[iCodOficina]','$campo','$orden' ";     
   $rs=mssql_query($sql,$cnx);
   $total = MsSQL_num_rows($rs);
   //echo $sql;
   while ($Rs=MsSQL_fetch_array($rs)){
?>
  <tr>
	<td style="width:780px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
	 <?
       echo "<div style=color:#727272;font-size:10px>".date("d-m-Y h:i A", strtotime($Rs[fFecRegistro]))."</div>";
     ?>	</td>
     <td style="width:780px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><?=$Rs[cNomOficina]?></td>
    <td style="width:780px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
	 <?
	   echo "<div style=color:#808080;text-transform:uppercase>".$Rs[cDescTipoDoc]." ".$Rs[cCodificacion]."</div>";
     ?> </td>
    <td style="width:780px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><?=$Rs[cAsunto]?></td>
    <td style="width:780px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><?=$Rs[cObservaciones]?></td>
    <td style="width:780px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><? echo $Rs[cNombresTrabajador]." ".$Rs[cApellidosTrabajador];?></td>
    <td style="width:780px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
	<? 
		$sqlReferencia= "SELECT [iCodTramite],[iCodTramiteRef],[cReferencia] FROM [Tra_M_Tramite_Referencias] where iCodTramite = '".$Rs["iCodTramite"]."'";
		$rsReferencia=mssql_query($sqlReferencia,$cnx);
		$RsReferencia=MsSQL_fetch_array($rsReferencia);
		$sqlDocOri="SELECT [nFlgTipoDoc] ,[cCodTipoDoc] FROM [Tra_M_Tramite] where iCodTramite = '".$RsReferencia["iCodTramiteRef"]."'";
		$rsDocOri=mssql_query($sqlDocOri,$cnx);
		$RsDocOri=MsSQL_fetch_array($rsDocOri);
		if($RsDocOri["nFlgTipoDoc"]!=1){ 
			$sqlTipoDoc=" SP_TIPO_DOCUMENTO_LISTA_AR '".$RsDocOri[cCodTipoDoc]."'";	$rsTipoDoc=mssql_query($sqlTipoDoc,$cnx);	$RsTipoDoc=MsSQL_fetch_array($rsTipoDoc);
			echo "<div style=text-transform:uppercase>".$RsTipoDoc["cDescTipoDoc"]."  ".$RsReferencia["cReferencia"]."</div>";			
			}
		else{echo "<div style=text-transform:uppercase>".$RsReferencia["cReferencia"]."</div>";}		
		//echo "<div style=text-transform:uppercase>".$Rs[cReferencia]."</div>";
	?></td>
    <td style="width:780px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
	 <?
	   if($Rs[iCodRemitente]!=0){
	    echo  $Rs[cNombre];
	   }
	   else if($Rs[iCodRemitente]==0){
	    echo "MULTIPLE"; 
       }
	   else if($Rs[iCodRemitente]==""){
	    echo "NO ASIGNADO"; 
	   }
	  ?> </td>
    <td style="width:780px;border: solid 1px #6F6F6F;font-size:10px;">
	<? 
	if($Rs[nFlgRpta]==1 && $Rs[cRptaOK]!="" )
		   { echo "<div align='center' style='color:#0154AF'>SI / RECIBIDA: ".$Rs[cRptaOK]."</div>";
		   } 
		 else if($Rs[nFlgRpta]==1 && $Rs[cRptaOK]=="" )
		   { echo "<div align='center' style='color:#950000'>SI / PENDIENTE</div>";
		     } 
		 else if($Rs[nFlgRpta]==0 && $Rs[cRptaOK]=="" )
		   { echo "<div align='center' >NO / ------</div>"; }  
    
    
     ?> </td>
  </tr>
<? }?>
	  </tbody>
    </table>  