<?php
session_start();
include_once("../conexion/conexion.php");
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=consultaEntradaExterna.xls");
    
$anho    = date("Y");
$datomes = date("m");
$datomes = $datomes*1;
$datodia = date("d");
$meses   = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre");

echo "<table width=780 border=0><tr><td align=center colspan=8>";
echo "<H3>REPORTE - DOCUMENTOS ENTRADAS EXTERNAS</H3>";
echo " ";

echo "<table width=780 border=0><tr><td align=right colspan=8>";
echo "SITD, ".$datodia." ".$meses[$datomes].' del '.$anho;
echo " ";

echo "<table width=780 border=0><tr><td align=left colspan=8>";
$sqllog = "SELECT cNombresTrabajador, cApellidosTrabajador FROM tra_m_trabajadores 
				   WHERE iCodTrabajador='$_SESSION[CODIGO_TRABAJADOR]' "; 
$rslog  = mssql_query($sqllog,$cnx);
$Rslog  = mssql_fetch_array($rslog);
echo "GENERADO POR : ".$Rslog[cNombresTrabajador]." ".$Rslog[cApellidosTrabajador];
echo " ";

if ($fecini != ''){
	$fecini = date("Ymd", strtotime($fecini));
}
if ($fecfin != ''){
	$fecfin = date("Y-m-d", strtotime($fecfin));
	function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
		$date_r = getdate(strtotime($date));
		$date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),($date_r["year"]+$yy)));
		return $date_result;
	}
	$fecfin = dateadd($fecfin,1,0,0,0,0,0); // + 1 dia
}
?>
<table style="width: 1000px; border: solid 0px black;">
	<tr>
		<td style="text-align:left;width:1000px" colspan="6"><br>&nbsp;<br>
			<span style="font-size: 15px; font-weight: bold"><?=$RsOfis[cNomOficina]?></span>
		</td>
	</tr>
</table>

<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="center">
<thead>
	<tr>
		<th style="width: 120px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N&ordm; Documento</th>
		<th style="width: 130px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N&ordm; Referencia</th>
		<th style="width: 150px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Destino</th>
		<th style="width: 150px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Fecha Derivo</th>
		<th style="width: 300px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Asunto</th>
		<th style="width: 300px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Destino Original</th>
		<th style="width: 300px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Destino Copia</th>
		<th style="width: 300px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Estado</th>
		<th style="width: 300px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Oficio Respuesta</th>
		<th style="width: 300px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Fecha</th>
		<th style="width: 300px; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Oficina</th>
	</tr>
</thead>
<tbody>
	<?php
		$sql = " SP_CONSULTA_DOC_ENTRADA_EXTERNO '$fecini','$fecfin','%$_GET[cCodificacion]%','%$_GET[cReferencia]%','%$_GET[cAsunto]%','$_GET[cCodTipoDoc]','%$_GET[cNombre]%','%$_GET[cNomRemite]%','%$_GET[cNroDocumento]%','$campo','$orden',$_SESSION[CODIGO_TRABAJADOR]";
		$rs = mssql_query($sql,$cnx);
		while ($Rs = mssql_fetch_array($rs)){
	?>
 	<tr>
    <td style="width:120px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;vertical-align:top">
    	<?php echo $Rs[cCodificacion]; ?>
    </td>

    <td style="width:130px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase;vertical-align:top">
    	<?php echo $Rs[cNroDocumento]; ?>    		
    </td>

  	<td style="width:150px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase;vertical-align:top">
  		<?php
				echo "<div style=\"text-transform:uppercase\">".$Rs[cNomRemite]."</div>";
		  ?>
  	</td>

    <td style="width:150px;text-align:center;border: solid 1px #6F6F6F;font-size:10px;vertical-align:top">
    	<?php
    		if ($Rs[nFlgEnvio] == 1){
    			$sqlM = "SELECT TOP 1 * FROM Tra_M_Tramite_Movimientos WHERE iCodTramite='$Rs[iCodTramite]' ORDER BY iCodMovimiento ASC";
					$rsM  = mssql_query($sqlM,$cnx);
					$RsM  = mssql_fetch_array($rsM);
    			echo date("d-m-Y G:i:s", strtotime($RsM[fFecDerivar]));//date("d-m-Y G:i", strtotime($RsM[fFecDerivar]));
    		}
    	?>
    </td>

    <td style="width:300px;text-align:justify; border: solid 1px #6F6F6F;font-size:10px;vertical-align:top">
    	<?php
				echo $Rs[cAsunto];
			?>
		</td>
  	
  	<td style="width:150px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase">
  		<?php
  			$sqlMDestino = "SELECT a.iCodOficinaDerivar,a.iCodOficinaOrigen FROM Tra_M_Tramite_Movimientos a 
  														WHERE a.iCodTramite='$Rs[iCodTramite]' 
															AND a.fFecDerivar = (SELECT MAX(e.fFecDerivar) FROM Tra_M_Tramite_Movimientos e 
																									 WHERE e.iCodTramite=a.iCodTramite AND e.cFlgTipoMovimiento!=5 AND e.cFlgTipoMovimiento!=4)
															AND a.iCodMovimiento = (SELECT MAX(w.iCodMovimiento) FROM Tra_M_Tramite_Movimientos w  
																											WHERE w.iCodTramite=a.iCodTramite AND w.cFlgTipoMovimiento!=5 AND w.cFlgTipoMovimiento!=4) ";
				$rsMDestino = mssql_query($sqlMDestino,$cnx);
				$RsMDestino = mssql_fetch_array($rsMDestino);
    		$sqlSigDes  = "SP_OFICINA_LISTA_AR '$RsMDestino[iCodOficinaDerivar]'";
				$rsSigDes   = mssql_query($sqlSigDes,$cnx);
				$RsSigDes   = mssql_fetch_array($rsSigDes);
				echo $RsSigDes["cNomOficina"];
			?>
  	</td> 
  
  	<td style="width:150px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase">
  		<?php
  			$sqlMDestino2 = "SELECT b.iCodOficinaDerivar from Tra_M_Tramite_Movimientos b 
  											 WHERE b.iCodTramite='$Rs[iCodTramite]' 
															AND b.fFecDerivar = (SELECT MAX(e.fFecDerivar) FROM Tra_M_Tramite_Movimientos e 
																									 WHERE e.iCodTramite=b.iCodTramite AND e.cFlgTipoMovimiento!=5 AND e.cFlgTipoMovimiento=4 AND 
																									 			  iCodOficinaDerivar!='$RsMDestino[iCodOficinaDerivar]' AND iCodOficinaOrigen='$RsMDestino[iCodOficinaOrigen]')
															AND b.iCodMovimiento = (SELECT MAX(w.iCodMovimiento) FROM Tra_M_Tramite_Movimientos w  
																											WHERE w.iCodTramite=b.iCodTramite AND w.cFlgTipoMovimiento!=5 AND w.cFlgTipoMovimiento=4 AND 
																														iCodOficinaDerivar!='$RsMDestino[iCodOficinaDerivar]' AND iCodOficinaOrigen='$RsMDestino[iCodOficinaOrigen]') ";
				$rsMDestino2 = mssql_query($sqlMDestino2,$cnx);
				$RsMDestino2 = mssql_fetch_array($rsMDestino2);
    		$sqlSigDes2  = "SP_OFICINA_LISTA_AR '$RsMDestino2[iCodOficinaDerivar]'";
				$rsSigDes2   = mssql_query($sqlSigDes2,$cnx);
				$RsSigDes2   = mssql_fetch_array($rsSigDes2);
				echo $RsSigDes2["cNomOficina"];
  		?>
  	</td> 
  	
  	<td style="width:150px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase">
	  	<?php
				switch ($Rs["nFlgEstado"]){
					case 1:
						echo "Pendiente";
						break;
					case 2:
						echo "En Proceso";
						break;
					case 3:
						echo "Finalizado";
						break;
				}
			?>	
 		</td> 	
  	
  	<td style="width:150px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase">
  		<?php
 				$sqlRespuesta = "SELECT iCodTramiteDerivar,fFecDerivar,iCodOficinaOrigen,cReferenciaDerivar,cCodTipoDocDerivar 
 												 FROM Tra_M_Tramite_Movimientos
 												 WHERE (iCodTramite='$Rs[iCodTramite]' OR iCodTramiteRel='$Rs[iCodTramite]') AND (cFlgTipoMovimiento=5)
 												 ORDER BY iCodMovimiento DESC";
  			$rsRespuesta = mssql_query($sqlRespuesta,$cnx);
  			$numRes = mssql_num_rows($rsRespuesta);
  			if ($numRes > 0){
  				$RsRespuesta = mssql_fetch_array($rsRespuesta);
  				$sqlTpDcRespuesta = "SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$RsRespuesta[cCodTipoDocDerivar]'";
  				$rsTpDcRespuesta  = mssql_query($sqlTpDcRespuesta,$cnx);
  				$RsTpDcRespuesta  = mssql_fetch_array($rsTpDcRespuesta);
  				echo $RsTpDcRespuesta[cDescTipoDoc]." ".$RsRespuesta[cReferenciaDerivar];
  			}else{
					if($Rs[nFlgEstado] == 3){
						echo "Culminacion sin Respuesta";
					}
  			}
  		?>
  	</td> 
  	
  	<td style="width:150px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase">
  		<?php
  			if ($numRes > 0){
  				echo date("d-m-Y G:i", strtotime($RsRespuesta[fFecDerivar]));
  			}else{
					if ($Rs[nFlgEstado] == 3){
						$sqlFinTxt = "SELECT * FROM Tra_M_Tramite_Movimientos 
												   WHERE nEstadoMovimiento=5 AND iCodTramite='$Rs[iCodTramite]' ORDER BY iCodMovimiento DESC";
						$rsFinTxt = mssql_query($sqlFinTxt,$cnx);
						$RsFinTxt = mssql_fetch_array($rsFinTxt);
            echo "<div style=color:#0154AF>".date("d-m-Y G:i:s", strtotime($RsFinTxt[fFecFinalizar]))/*date("d-m-Y", strtotime($RsFinTxt[fFecFinalizar]))*/."</div>";
					}
  			}
  		?>
 		</td> 
  	
  	<td style="width:150px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase">
  		<?php
  			if ($numRes > 0){
					if ($Rs[nFlgEstado] != 3){
	 					$sqlResO = "SP_OFICINA_LISTA_AR '$RsRespuesta[iCodOficinaDerivar]'";
						$rsResO  = mssql_query($sqlResO,$cnx);
						$RsResO  = mssql_fetch_array($rsResO);
						echo $RsResO["cNomOficina"];
					}else{
  					$sqlResO = "SP_OFICINA_LISTA_AR '$RsRespuesta[iCodOficinaOrigen]'";
						$rsResO  = mssql_query($sqlResO,$cnx);
						$RsResO  = mssql_fetch_array($rsResO);
						echo $RsResO["cNomOficina"];	
					}
				}							
  		?>
  	</td>	
</tr>
<?}?>
</tbody>
</table>  
