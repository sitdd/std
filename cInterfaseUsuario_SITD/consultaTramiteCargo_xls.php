<?php
	include_once("../conexion/conexion.php");
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=consultaTramiteCargo.xls");
	$anho    = date("Y");
	$datomes = date("m");
	$datomes = $datomes*1;
	$datodia = date("d");
	$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre");
	
	echo "<table width=780 border=0><tr><td align=center colspan=12>";
	echo "<H3>REPORTE - DOCUMENTOS DE SALIDAS MULTIPLES</H3>";
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=right colspan=12>";
	echo "SITD, ".$datodia." ".$meses[$datomes].' del '.$anho;
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=left colspan=7>";
	$sqllog = "SELECT cNombresTrabajador, cApellidosTrabajador FROM tra_m_trabajadores WHERE iCodTrabajador = '$traRep' "; 
	$rslog  = mssql_query($sqllog,$cnx);
	$Rslog  = mssql_fetch_array($rslog);
	echo "GENERADO POR : ".$Rslog[cNombresTrabajador]." ".$Rslog[cApellidosTrabajador];
	echo " ";
	?>
	<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="center">
		<thead>
    	<tr>
      	<th colspan="9" style="text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">DOCUMENTOS ENVIADOS</th>
        <th colspan="4" style="text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">DEVOLUCION DE CARGOS</th>
      </tr>

      <tr>
	      <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Fecha de Envio</th>
	      <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Envio</th>
	      <th style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N� Pedido de Servicio</th>
				<th style="width: 7%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Area Usuaria</th>
	      <th style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N� Guia de Servicio</th>
	      <th style="width: 12%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Tipo de Documento</th>
	      <th style="width: 13%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N� de Documento</th>
	      <th style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Destinatario</th>
	      <th style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Direccion</th>
	      <th style="width: 7%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N� Guia</th>
	      <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Fecha Devolucion</th>
	      <th style="width: 15%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Observaciones</th>
	      <th style="width: 15%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Estado</th>
	    </tr>
		</thead>
	<tbody>
	
	<?php
		////////////
		// if (trim($_REQUEST[fHasta]) == ""){
  //     $fecfin = date("d-m-Y");
  //   }else{ 
  //     $fecfin = $_REQUEST[fHasta]; 
  //   }
  //   if (trim($_REQUEST[fDesde]) == ""){
  //     $fecini = date("01-m-Y");
  //   }else{ 
  //     $fecini = $_REQUEST[fDesde]; 
  //   }
    ////////////77
		if ($_REQUEST[fDesde] != ''){
			$fDesde = date("Ymd", strtotime($_REQUEST[fDesde]));
		}
   	
   	if ($_REQUEST[fHasta] != ''){
    	$fHasta = date("Y-m-d", strtotime($_REQUEST[fHasta]));
			
			function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
	  		$date_r = getdate(strtotime($date));
	  		$date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
	  		return $date_result;
	 		}
			$fHasta = dateadd($fHasta,1,0,0,0,0,0); // + 1 dia
		}
		$sql.= " SP_CONSULTA_TRAMITE_CARGO '$fDesde', '$fHasta','$_REQUEST[ChxfRespuesta]', '$_REQUEST[fEntrega]','%$_REQUEST[cCodificacion]%', '%$_REQUEST[cNombre]%', '%$_REQUEST[cDireccion]%', '$_REQUEST[cCodTipoDoc]', '$_REQUEST[cOrdenServicio]', '$_REQUEST[cFlgUrgente]','$_REQUEST[iCodTrabajadorEnvio]', '$_REQUEST[cFlgLocal]', '$_REQUEST[cFlgNacional]','$_REQUEST[cFlgInternacional]','$_REQUEST[iCodOficina]' ,'$_REQUEST[cFlgEstado]', '$_REQUEST[cCodDepartamento]' ,'$_REQUEST[cCodProvincia]' ,'$_REQUEST[cCodDistrito]','$campo','$orden'  ";
		//echo $sql; 
		$rs = mssql_query($sql,$cnx);
		
		while ($Rs = mssql_fetch_array($rs)){
	?>
	
	<tr>
		<td style="width:8%;text-align:center;border: solid 1px #6F6F6F;font-size:10px;">
			<? if($Rs[fEntrega]!="") echo date("d-m-Y", strtotime($Rs[fEntrega]))?>
		</td>
   	<td style="width:10%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase;">
   		<?php 
   			if ($Rs[cFlgEnvio] == 1){
   				echo "LOCAL";
   			}elseif ($Rs[cFlgEnvio]==2) {
   				echo "NACIONAL";
   			}elseif ($Rs[cFlgEnvio]==3){
   					echo "INTERNACIONAL";
   			}else{
   				echo "";
   			}
   		?>
   	</td>
		<td style="width:10%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase;">
			<?=$Rs[cOrdenServicio]?>
		</td>
		<td style="width:7%;text-align:center;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase;">
			<?=$Rs[cSiglaOficina]?>
		</td>
		<td style="width:10%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
			<?=$Rs[cNumGuiaServicio]?>
		</td>
    <td style="width:12%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
    	<?=$Rs[cDescTipoDoc]?>
    </td>
    <td style="width:13%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
    	<?=$Rs[cCodificacion]?>
    </td>
    <td style="width:13%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
    	<?=$Rs[cNombre]?>
    </td>
		<td style="width:10%;text-align:left; border: solid 1px #6F6F6F;font-size:10px;vertical-align:top">
			<?php 
				if (isset($Rs['CODIGO_PAIS'])) {
	        $sqlPais = "SELECT nombrePais FROM Tra_U_Pais WHERE codPais = ".$Rs['CODIGO_PAIS'];
	        $rsPais  = mssql_query($sqlPais,$cnx);
	        $RsPais  = mssql_fetch_array($rsPais);
	        echo $RsPais['nombrePais']." - ";
	      }else{
	      	echo $Rs[cDireccion]; echo "<br>"; 
					if ($Rs[cNomDepartamento] != ""){
						echo $Rs[cNomDepartamento];
					} 
					if ($Rs[cNomProvincia]!=""){ 
						echo " - ".$Rs[cNomProvincia];
					} 
					if ($Rs[cNomDistrito]!=""){
					 echo " - ".$Rs[cNomDistrito];
					}
	      }
			?>
		</td>
		<td style="width:7%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase">
			<?=$Rs[cNumGuia]?>
		</td> 
		<td style="width:8%;text-align:center;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase">
			<? if($Rs[fRespuesta]!="") echo date("d-m-Y", strtotime($Rs[fRespuesta]))?>
		</td> 
    <td style="width:15%;text-align:justify;border: solid 1px #6F6F6F;font-size:10px;text-transform:uppercase">
    	<?=$Rs[cObservaciones]?>
    </td> 
    <td style="width:13%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
	    <? 
		   	if($Rs[cFlgEstado]==1 or $Rs[cFlgEstado]==2 ){ 
		   	if($Rs[cFlgEstado]==1){ echo "<div style='color:#FF0000'>NOTIFICADO</div>";} else if($Rs[cFlgEstado]==2){ echo "<div style='color:#FF0000'>DEVUELTO</div>";}
		   	}else{
			  	if($Rs[cFlgEstado]==3){echo  "<div style='color:#FF0000'>PENDIENTE</div>";
			  	}else{
		  		echo "";
			  	}
		  	}
		 ?>
		</td>
	</tr>
	<?}?>
</tbody>
</table>