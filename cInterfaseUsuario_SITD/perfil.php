<?php
session_start();
if($_SESSION['CODIGO_TRABAJADOR']!=""){
?>
    <!DOCTYPE html>
    <html lang="es">
    <head>

        <?include("includes/head.php");?>
        <script type="text/javascript" language="javascript" src="includes/lytebox.js"></script>
        <link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
        <link type="text/css" rel="stylesheet" href="css/dhtmlgoodies_calendar.css" media="screen"/>
        <script type="text/javascript" src="scripts/dhtmlgoodies_calendar.js"></script>

    </head>
    <body>
    <?include("includes/menu.php");?>
    <?
    require_once("../conexion/conexion.php");
    $sql= "select * from Tra_M_Trabajadores where iCodTrabajador='$_SESSION[CODIGO_TRABAJADOR]'";
    $rs=mssql_query($sql,$cnx);
    $Rs=MsSQL_fetch_array($rs);
    ?>

    <style>
        .img-profile-setting{
            border-radius: 100%;
            display: block;
            width: 90%;
        }
        .md-form{
            margin-bottom: 0!important;
        }
        .noEditable .md-form input{
            border-bottom: none!important;
        }
        @media (min-width: 576px) {
            .img-profile-setting{
                width: 65%;
            }
        }
        @media (min-width: 768px) {
            .img-profile-setting{
                width: 80%;
            }
        }
        @media (min-width: 992px) {
            div.card-header{
                font-size: 1.3rem!important;
            }
            .img-profile-setting{
                width: 90%;
            }
        }
        @media (min-width: 1200px) {
            .img-profile-setting{
                width: 70%;
            }
        }
    </style>

    <!--Main layout-->
    <main class="mx-lg-5">
        <div class="container-fluid">
            <!--Card-->
            <div class="card mb-5">
                <!-- Card header -->
                <div class="card-header text-center ">
                    CONFIGURACIÓN DE PERFIL
                </div>
                <!--Card content-->
                <div class="card-body px-4">

                    <div class="row justify-content-center px-0 px-md-2 px-lg-4">

                        <div class="col-12 col-md-5 col-xl-4">
                            <div class="card mb-4">
                                <div class="card-header">
                                    Foto de Perfil
                                </div>

                                <div id="div1" ondrop="drop(event)" ondragover="allowDrop(event)">
                                    <img class="img-profile-setting mx-auto my-4 my-lg-5 my-xl-4 " src="images/foto.jpg" ondragstart="drag(event)" alt="<?php echo $Rs['cNombresTrabajador'];?>">
                                </div>

                                <div class="row justify-content-center mb-3">
                                    <div class="col">
                                        <button class="btn botenviar"  type="button" id="escogerFoto" onclick="" >Buscar</button>
                                        <div class="btn btn-primary btn-sm float-left">
                                            <span>File</span>
                                            <input type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate"  name="fotoupload" type="text">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-12 col-md-7 col-xl-8">
                            <div class="card mb-4">
                                <div class="card-header">
                                    Datos del Usuario
                                </div>

                                <form class="card-body px-3 px-xl-5" method="post" action="../cAccesoBaseDato_SITD/ad_trabajadores_data.php" aria-label="DatosUsuario" name="datos">
                                            <input type="hidden" name="opcion" value="2">
                                            <div class="row noEditable">
                                                <div class="col-12 col-sm-2 col-md-5 col-lg-2">
                                                    <div class="md-form">
                                                        <input type="text" id="usuario" name="usuario" class="form-control" value="<?php echo $Rs['cUsuario'];?>" disabled>
                                                        <label for="usuario">Usuario</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 col-md-7 col-lg-5">
                                                    <div class="md-form">
                                                        <input type="text" id="correo" name='correo' class="form-control" value="<?php echo $Rs['cMailTrabajador'];?>" disabled>
                                                        <label for="correo">Correo</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 col-md-12 col-lg-5">
                                                    <div class="md-form">
                                                        <input type="text" id="ultimoacceso" name='ultimoacceso' class="form-control" value="<?php echo $Rs['fUltimoAcceso'];?>" disabled>
                                                        <label for="ultimoacceso">Última conexión:</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 col-sm-4">
                                                    <div class="md-form">
                                                        <input type="text" id="nombres" name='nombres' class="form-control editable" value="<?php echo $Rs['cNombresTrabajador'];?>" disabled>
                                                        <label for="nombres">Nombres</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-4">
                                                    <div class="md-form">
                                                        <input type="text" id="apellidos" name='apellidos' class="form-control editable" value="<?php echo $Rs['cApellidosTrabajador'];?>" disabled>
                                                        <label for="apellidos">Apellidos</label>
                                                    </div>
                                                </div>

                                                <?php
                                                $TipDocIden = $Rs['cTipoDocIdentidad'];
                                                $sqlTipodoc = "select * from Tra_M_Doc_Identidad where cTipoDocIdentidad =$TipDocIden";
                                                $rsIden=mssql_query($sqlTipodoc,$cnx);
                                                $RsIden=MsSQL_fetch_array($rsIden);
                                                ?>
                                                <div class="col-12 col-sm-4">
                                                    <div class="md-form">
                                                        <input type="text" id="documento" name='documento' class="form-control editable" value="<?php echo $Rs['cNumDocIdentidad'];?>" disabled>
                                                        <label for="documento"><?php echo $RsIden['cDescDocIdentidad'];?></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="md-form">
                                                        <input type="text" id="direccion" name='direccion' class="form-control editable" value="<?php echo $Rs['cDireccionTrabajador'];?>" disabled>
                                                        <label for="direccion">Dirección</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="md-form">
                                                        <input type="text" id="telefono1" name='telefono1' class="form-control editable" value="<?php echo $Rs['cTlfTrabajador1'];?>" disabled>
                                                        <label for="telefono1">Teléfono 1</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="md-form">
                                                        <input type="text" id="telefono2" name='telefono2' class="form-control editable" value="<?php echo $Rs['cTlfTrabajador2'];?>" disabled>
                                                        <label for="telefono2">Teléfono 2</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mt-4 justify-content-center">
                                                <div class="col-">
                                                    <button class="btn botenviar" type="button" id="actualiza" onclick="editar()" >Actualizar</button>
                                                </div>
                                            </div>
                                </form>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    Perfiles por Oficina
                                </div>
                                <div class="card-body px-2">
                                    <div class="row justify-content-center">
                                        <div class="col-12 col-xl-7">
                                            <table class="table table-responsive table-hover my-3">
                                                <thead class="text-center">
                                                <tr>
                                                    <th>N°</th>
                                                    <th>PERFIL</th>
                                                    <th>OFICINA</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $sqlConsultaPerfiles ="select (select cDescPerfil from Tra_M_Perfil where iCodPerfil=p.iCodPerfil) as nPerfil,
                                                    (select cNomOficina from Tra_M_Oficinas where iCodOficina=p.iCodOficina) as nOficina 
                                                    from Tra_M_Perfil_Ususario p where iCodTrabajador='".$_SESSION['CODIGO_TRABAJADOR']."'";
                                                    $rsConsultaPerfiles=mssql_query($sqlConsultaPerfiles,$cnx);
                                                    $i=1;
                                                    while($RsConsultaPerfiles=MsSQL_fetch_array($rsConsultaPerfiles)){
                                                        echo utf8_encode("<tr>
                                                                <td>".$i."</td>  
                                                                <td>".$RsConsultaPerfiles['nPerfil']."</td>
                                                                <td>".$RsConsultaPerfiles['nOficina']."</td>
                                                              </tr>");
                                                        $i++;
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?include("includes/userinfo.php");?>
    <?include("includes/pie.php");?>

    <!--Actualizar datos de perfil -->
    <script>
        function editar(){
            var elementos= document.getElementsByClassName("editable");
            var i=0;
            while (i < elementos.length){
                elementos[i].removeAttribute("disabled");
                elementos[i].style.background = "#0b30bb2e";
                elementos[i].style.borderRadius = "10px";
                i++;
            }
            var boton = document.getElementById("actualiza");
            boton.innerText = "Guardar";
            boton.setAttribute("onclick","loguear()");
        }

        function loguear(){
            document.datos.submit();
        }

    </script>

    <!--Drag and Drop-->
    <script>
        function allowDrop(ev) {
            ev.preventDefault();
        }

        function drag(ev) {
            ev.dataTransfer.setData("text", ev.target.id);
        }

        function drop(ev) {
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text");
            ev.target.appendChild(document.getElementById(data));
        }
    </script>

    </body>
    </html>

    <?
}else{
    header("Location: ../index.php?alter=5");}
?>