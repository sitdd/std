<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: consultaInternoOficina_xls.php
SISTEMA: SISTEMA   DE TR�MITE DOCUMENTARIO DIGITAL
OBJETIVO: Reporte general en EXCEL de los Documentos Internos por Oficina
PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripci�n
------------------------------------------------------------------------
1.0   Larry Ortiz        05/09/2018      Creaci�n del programa.
------------------------------------------------------------------------
*****************************************************************************************/
session_start();
include_once("../conexion/conexion.php");
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=consultaInternoOficina.xls");
	
	$anho = date("Y");
	$datomes = date("m");
	$datomes = $datomes*1;
	$datodia = date("d");
	$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre");
	
	echo "<table width=780 border=0><tr><td align=center colspan=6>";
	echo "<H3>REPORTE - DOCUMENTOS INTERNOS DE OFICINA</H3>";
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=right colspan=6>";
	echo "SITD, ".$datodia." ".$meses[$datomes].' del '.$anho;
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=left colspan=7>";
	$sqllog="select cNombresTrabajador, cApellidosTrabajador from tra_m_trabajadores where iCodTrabajador='$_SESSION[CODIGO_TRABAJADOR]' "; 
	$rslog=mssql_query($sqllog,$cnx);
	$Rslog=MsSQL_fetch_array($rslog);
	echo "GENERADO POR : ".$Rslog[cNombresTrabajador]." ".$Rslog[cApellidosTrabajador];
	echo " ";
?>	
	<table style="width: 780px;border: solid 1px #5544DD; border-collapse: collapse" align="center">
     <thead>
      <tr>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Fecha</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Hora</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Registrado por</th>	
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Tipo Documento</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Numero de Doc.</th>	
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Responsable</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Asunto</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Observaciones</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Oficina Destino</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Oficina Destino (Responsable)</th>
       <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Referencia</th>
      </tr>
	 </thead>
     <tbody>
<?	
	if ($fecini!=''){$fecini=date("Ymd", strtotime($fecini));}
   	if( $fecfin!=''){
    $fecfin=date("Y-m-d", strtotime($fecfin));
	function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    $date_r = getdate(strtotime($date));
    $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
    return $date_result;
				}
	$fecfin=dateadd($fecfin,1,0,0,0,0,0); // + 1 dia
	}
 	
	$sql.= " SP_CONSULTA_INTERNO_OFICINA '$fecini', '$fecfin',  '$_GET[SI]', '$_GET[NO]',  '%$_GET[cCodificacion]%', '%$_GET[cAsunto]%',  '%$_GET[cObservaciones]%', '$_GET[cCodTipoDoc]' ,'$_GET[iCodOficina]','$session', '$campo', '$orden' ";
	$rs=mssql_query($sql,$cnx);
	//echo $sql;
  while ($Rs=MsSQL_fetch_array($rs)){
?>
    <tr>
    <td style="width: 8%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
	 <? echo "<div style=color:#727272>".date("d-m-Y G:i:s", strtotime($Rs[fFecRegistro]))/*date("d-m-Y", strtotime($Rs[fFecRegistro]))*/."</div>"; ?> </td>
    <td style="width: 8%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"> 	
     <?   /*echo "<div style=color:#727272;font-size:10px>".date("h:i A", strtotime($Rs[fFecRegistro]))."</div>";
	 */?>	</td>
    <td style="width: 8%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
    <? echo $Rs[cNombresTrabajador]." ".$Rs[cApellidosTrabajador];?>
    
    <td style="width: 8%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><? echo $Rs[cDescTipoDoc]; ?> </td> 
    <td style="width: 8%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><? echo "<div style=color:#727272>".$Rs[cCodificacion]."</div>";?> </td>
    <td style="width: 8%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
	<? $sqlTra="SELECT cApellidosTrabajador,cNombresTrabajador FROM Tra_M_Trabajadores WHERE iCodTrabajador='$Jefe'";
			$rsTra=mssql_query($sqlTra,$cnx);
			$RsTra=MsSQL_fetch_array($rsTra);
			echo "<div style=color:#808080;>".$RsTra[cNombresTrabajador]." ".$RsTra[cApellidosTrabajador]."</div>";	 ?> </td>
    <td style="width: 8%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><? echo $Rs[cAsunto];?> </td>
    <td style="width: 8%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><? echo $Rs[cObservaciones];?> </td>   
   
   <td style="width:780px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
	 <?  
    $sqlDes= " SELECT  Tra_M_Tramite.iCodTramite,cNombresTrabajador,cApellidosTrabajador,cNomOficina FROM Tra_M_Tramite "; 
   $sqlDes.= " LEFT OUTER JOIN Tra_M_Tramite_Movimientos on Tra_M_Tramite.icodtramite=Tra_M_Tramite_Movimientos.icodtramite ";
   $sqlDes.= " LEFT OUTER JOIN Tra_M_Trabajadores on Tra_M_Tramite_Movimientos.iCodTrabajadorDerivar=Tra_M_Trabajadores.iCodTrabajador, Tra_M_Oficinas ";
   $sqlDes.= " WHERE Tra_M_Oficinas.iCodOficina=Tra_M_Tramite_Movimientos.iCodOficinaDerivar AND Tra_M_Tramite.nFlgTipoDoc=2 AND Tra_M_Tramite.nFlgClaseDoc=1 ";
   $sqlDes.= " AND Tra_M_Tramite.iCodTramite='$Rs[iCodTramite]' ";
   $rsDes=mssql_query($sqlDes,$cnx);
   $numDes=MsSQL_num_rows($rsDes);
    $RsDes=MsSQL_fetch_array($rsDes); 
  	   if($numDes>1){echo "MULTIPLE";   }
	   else{ 
       echo $RsDes[cNomOficina]; 
	   }
	?></td>	
    <td style="width:780px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
    <?  
  	   if($numDes<=1)
	   {echo  $RsDes[cNombresTrabajador]." ".$RsDes[cApellidosTrabajador]; 
		}
	?>
    </td>		
    <td style="width: 8%;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><? echo $Rs[cReferencia];?> </td>
   </tr>
<? }?>
	  </tbody>
    </table>  	   