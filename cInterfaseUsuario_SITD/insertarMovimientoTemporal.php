<?php 
session_start();
include_once("../conexion/conexion.php");
$sqlAdd = "INSERT INTO Tra_M_Tramite_Temporal (iCodOficina,iCodTrabajador,iCodIndicacion,cPrioridad,cCodSession) 
           VALUES ('$_POST[iCodOficinaMov]','$_POST[iCodTrabajadorMov]','$_POST[iCodIndicacionMov]','$_POST[cPrioridad]','$_SESSION[cCodOfi]')"; 
$rs = mssql_query($sqlAdd,$cnx); 

$sqlMovs = "SELECT Tra_M_Tramite_Temporal.iCodTemp,
                   Tra_M_Trabajadores.encargado,
                   Tra_M_Oficinas.cNomOficina,
                   Tra_M_Trabajadores.cNombresTrabajador,
                   Tra_M_Trabajadores.cApellidosTrabajador,
                   Tra_M_Indicaciones.cIndicacion,
                   Tra_M_Tramite_Temporal.cPrioridad 
            FROM Tra_M_Tramite_Temporal 
            INNER JOIN Tra_M_Oficinas ON Tra_M_Oficinas.iCodOficina = Tra_M_Tramite_Temporal.iCodOficina 
            INNER JOIN Tra_M_Trabajadores ON Tra_M_Trabajadores.iCodTrabajador = Tra_M_Tramite_Temporal.iCodTrabajador 
            INNER JOIN Tra_M_Indicaciones ON Tra_M_Indicaciones.iCodIndicacion = Tra_M_Tramite_Temporal.iCodIndicacion 
            WHERE Tra_M_Tramite_Temporal.cCodSession = '$_SESSION[cCodOfi]' 
            ORDER BY Tra_M_Tramite_Temporal.iCodTemp ASC"; 
$rsMovs = mssql_query($sqlMovs,$cnx);
$result = array();
while ($tramiteTemporal = mssql_fetch_assoc($rsMovs)){
	$result[]= $tramiteTemporal;
}
echo json_encode($result);
?>