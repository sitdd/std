<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: iu_nuevo_corre_int.php
SISTEMA: SISTEMA  DE TRÁMITE DOCUMENTARIO DIGITAL
OBJETIVO: Mantenimiento de la Tabla Maestra Correlativos para el Perfil Administrador
          -> Crear Registro de correlativo interno

*****************************************************************************************/
session_start();
If($_SESSION['CODIGO_TRABAJADOR']!=""){
include_once("../conexion/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />

</head>
<body>
<?include("includes/menu.php");?>
<!--Main layout-->
<main class="mx-lg-5">
    <div class="container-fluid">
       <!--Grid row-->
        <div class="row wow fadeIn">
            <!--Grid column-->
            <div class="col-md-12 mb-12">
                <!--Card-->
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header text-center "> CORRELATIVO INTERNO</div>
                    <!--Card content-->
                    <div class="card-body px-4 px-sm-5">

                        <?php
                            require_once("../cAccesoBaseDato_SITD/ad_busqueda.php");
                         ?>
                        <form action="../cLogicaNegocio_SITD/ln_actualiza_correlativo.php" onSubmit="return validar(this)" method="post"  name="frmCorrelativo">
                        <input type="hidden" name="opcion" value="2">
                            <div class="form-row justify-content-center">

                                <div class="col-lg-8">
                                    <label>Oficina:</label>
                                    <select name="iCodOficina" class="FormPropertReg mdb-select colorful-select dropdown-primary" onchange="releer()"
                                            searchable="Buscar aqui..">
                                        <option value="">Seleccione:</option>
                                        <?php
                                        $sqlOfi=" SP_OFICINA_LISTA_COMBO ";
                                        $rsOfi=mssql_query($sqlOfi,$cnx);
                                        while ($RsOfi=MsSQL_fetch_array($rsOfi)){
                                            if($RsOfi["iCodOficina"]==$_REQUEST[iCodOficina]){
                                                $selecClas="selected";
                                            }Else{
                                                $selecClas="";
                                            }
                                            echo utf8_encode("<option value=".$RsOfi["iCodOficina"]." ".$selecClas.">".$RsOfi["cNomOficina"]."</option>");
                                        }
                                        mssql_free_result($rsOfi);
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-8 col-lg-5">
                                    <label for="">Tipo de Documento:</label>
                                    <select name="cCodTipoDoc" class="FormPropertReg mdb-select colorful-select dropdown-primary" >
                                        <option value="">Seleccione:</option>
                                        <?
                                        include_once("../conexion/conexion.php");
                                        $sqlTipo="SP_TIPO_DOCUMENTO_LISTA_CORRELATIVO $_REQUEST[iCodOficina]";
                                        $rsTipo=mssql_query($sqlTipo,$cnx);
                                        while ($RsTipo=MsSQL_fetch_array($rsTipo)){
                                            if($RsTipo["cCodTipoDoc"]==$_GET[cCodTipoDoc]){
                                                $selecTipo="selected";
                                            }Else{
                                                $selecTipo="";
                                            }
                                            echo utf8_encode("<option value=".$RsTipo["cCodTipoDoc"]." ".$selecTipo.">".$RsTipo["cDescTipoDoc"]."</option>");
                                        }
                                        mssql_free_result($rsTipo);
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-4 col-lg-3">
                                    <label>A&ntilde;o:</label>
                                   <select name="nNumAno"  class="FormPropertReg mdb-select colorful-select dropdown-primary" id="iCodUbicacion">
                                        <option value="2018" selected="selected">2018</option>
                                        <option value="2019">2019</option>
                                        <option value="2020">2020</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row justify-content-center mt-2">
                                <button class="btn botenviar"  type="submit" id="Insert Oficina"
                                        onMouseOver="this.style.cursor='hand'">
                                    <b>Crear</b>
                                </button>
                                <button class="btn botenviar" type="button"
                                        onclick="window.open('iu_correlativo_interno.php?iCodOficina=<?=$_GET[iCodOficina]?>', '_self');"
                                        onMouseOver="this.style.cursor='hand'">
                                    <b>Cancelar</b>
                                </button>
                            </div>
                        </form>

                    </div>
                 </div>
            </div>
        </div>
    </div>
</main>

<?include("includes/userinfo.php");?>
<?include("includes/pie.php");?>
    <script>
        $(document).ready(function() {
            $('.mdb-select').material_select();

        });
        function validar(f) {
            var error = "Por favor, antes de crear complete:\n\n";
            var a = "";
            if (f.iCodOficina.value == "") {
                a += " Ingrese una Oficina";
                alert(error + a);
            }
            else if (f.cCodTipoDoc.value == "") {
                a += " Ingrese un Documento";
                alert(error + a);
            }
            return (a == "");
        }
        function releer(){
            document.frmCorrelativo.action="<?=$_SERVER['PHP_SELF']?>#area";
            document.frmCorrelativo.submit();
        }
    </script>
</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>