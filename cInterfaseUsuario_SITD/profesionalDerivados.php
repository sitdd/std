<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: profesionalDerivados.php
SISTEMA: SISTEMA  DE TRÁMITE DOCUMENTARIO DIGITAL
OBJETIVO: Pantalla formulario parar Derivar un pendiente.
PROPIETARIO: AGENCIA PERUANA DE COOPERACIÓN INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripción
------------------------------------------------------------------------
1.0   APCI    05/09/2018      Creación del programa.
------------------------------------------------------------------------
*****************************************************************************************/
session_start();
If($_SESSION['CODIGO_TRABAJADOR']!=""){
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
<link type="text/css" rel="stylesheet" href="css/dhtmlgoodies_calendar.css" media="screen"/>
<style>
        .wrapper {
            width: auto%;
            position: absolute;
            z-index: 100;
            height: auto;
        }
        #sidebar {
            display: none;
        }
        #tablaResutado{
            width: 100%;
        }
        #sidebar.active {
            display: flex;
            width: 95%;
        }
        #sidebar label{
            font-size: 0.8rem!important;
            margin-bottom: 0!important;
        }
        .info-end ,.page-link{
            font-size: 0.8rem!important;
        }
        @media (min-width: 576px) {
            #sidebar.active {
                width: 80%;
            }
        }
        @media (min-width: 768px) {
            #sidebar.active {
                width: 60%;
            }
        }
        @media (min-width: 992px) {
            #sidebar.active {
                width: 60%;
            }
            #sidebarCollapse{
                margin-left: -35px!important;
            }
            .info-end ,.page-link{
                font-size: 1rem!important;
            }
        }
        @media (min-width: 1200px) {
            #sidebar.active {
                width: 38%;
            }
        }

        .dropdown-content li>a, .dropdown-content li>span {
            font-size: 0.8rem!important;
        }
        .select-wrapper .search-wrap {
            padding-top: 0rem!important;
            margin: 0 0.2rem!important;
        }
        .select-wrapper input.select-dropdown {
            font-size: 0.9rem!important;
        }
        .md-form{
            margin-bottom: 0.5rem!important;
        }

    </style>
</head>
<body>
<?include("includes/menu.php");?>

<!--Main layout-->
 <main class="mx-lg-5">
     <div class="container-fluid">
          <!--Grid row-->
         <div class="row wow fadeIn">
              <!--Grid column-->
             <div class="col-md-12 mb-12">
                  <!--Card-->
                 <div class="card">
                      <!-- Card header -->
                     <div class="card-header text-center ">
                         Bandeja >> Derivados
                     </div>
                      <!--Card content-->
                     <div class="card-body d-flex px-4 px-lg-5">
                         <div class="wrapper">
                             <nav class="navbar-expand py-0">
                                 <button type="button" id="sidebarCollapse" class="botenviar float-left" style="padding: 0rem 8px!important; border: none; margin-left: -18px; border-radius: 10px;">
                                     <i class="fas fa-align-right"></i>
                                 </button>
                             </nav>
                             <!-- Sidebar -->
                             <nav id="sidebar" class="py-0">
                                 <div class="card">
                                     <div class="card-header">Criterios de Búsqueda</div>
                                     <div class="card-body">
                                         <form name="frmConsulta" method="GET">
                                             <tr>
                                                 <td width="110" >Documentos:</td>
                                                 <td width="390" align="left"><input type="checkbox" name="Entrada" value="1" <?if($_GET[Entrada]==1) echo "checked"?> onclick="activaEntrada();">Entrada  &nbsp;&nbsp;&nbsp;<input type="checkbox" name="Interno" value="1" <?if($_GET[Interno]==1) echo "checked"?> onclick="activaInterno();">Internos</td>
                                                 <td width="110" >Desde:</td>
                                                 <td align="left">

                                                 <td><input type="text" readonly name="fDesde" value="<?=$_GET[fDesde]?>" style="width:75px" class="FormPropertReg form-control"></td><td><div class="boton" style="width:24px;height:20px"><a href="javascript:;" onclick="displayCalendar(document.forms[0].fDesde,'dd-mm-yyyy',this,false)"><img src="images/icon_calendar.png" width="22" height="20" border="0"></a></div></td>
                                                 <td width="20"></td>
                                                 <td >Hasta:&nbsp;<input type="text" readonly name="fHasta" value="<?=$_GET[fHasta]?>" style="width:75px" class="FormPropertReg form-control"></td><td><div class="boton" style="width:24px;height:20px"><a href="javascript:;" onclick="displayCalendar(document.forms[0].fHasta,'dd-mm-yyyy',this,false)"><img src="images/icon_calendar.png" width="22" height="20" border="0"></a></div></td>
                                             </tr></table>
                                             </td>
                                             </tr>
                                             <tr>
                                                 <td width="110" >N&ordm; Documento:</td>
                                                 <td width="390" align="left"><input type="txt" name="cCodificacion" value="<?=$_GET[cCodificacion]?>" size="28" class="FormPropertReg form-control"></td>
                                                 <td width="110" >Asunto:</td>
                                                 <td align="left"><input type="txt" name="cAsunto" value="<?=$_GET[cAsunto]?>" size="65" class="FormPropertReg form-control">
                                                 </td>
                                             </tr>
                                             <tr>
                                                 <td width="110" >Tipo Documento:</td>
                                                 <td width="390" align="left">
                                                     <select name="cCodTipoDoc" class="FormPropertReg form-control" style="width:180px" />
                                                     <option value="">Seleccione:</option>
                                                     <?
                                                     include_once("../conexion/conexion.php");
                                                     $sqlTipo="SELECT * FROM Tra_M_Tipo_Documento ";
                                                     $sqlTipo.="ORDER BY cDescTipoDoc ASC";
                                                     $rsTipo=mssql_query($sqlTipo,$cnx);
                                                     while ($RsTipo=MsSQL_fetch_array($rsTipo)){
                                                         if($RsTipo["cCodTipoDoc"]==$_GET[cCodTipoDoc]){
                                                             $selecTipo="selected";
                                                         }Else{
                                                             $selecTipo="";
                                                         }
                                                         echo "<option value=".$RsTipo["cCodTipoDoc"]." ".$selecTipo.">".$RsTipo["cDescTipoDoc"]."</option>";
                                                     }
                                                     mssql_free_result($rsTipo);
                                                     ?>
                                                     </select>							</td>
                                                 <td width="110" >Tema:</td>
                                                 <td align="left" class="CellFormRegOnly">
                                                     <select name="iCodTema" style="width:300px;" class="FormPropertReg form-control">
                                                         <option value="">Seleccione:</option>
                                                         <?
                                                         $sqlTem="SELECT * FROM Tra_M_Temas WHERE  iCodOficina = '$_SESSION[iCodOficinaLogin]' ";
                                                         $sqlTem .= "ORDER BY cDesTema ASC";
                                                         $rsTem=mssql_query($sqlTem,$cnx);
                                                         while ($RsTem=MsSQL_fetch_array($rsTem)){
                                                             if($RsTem[iCodTema]==$_GET[iCodTema]){
                                                                 $selecTem="selected";
                                                             }Else{
                                                                 $selecTem="";
                                                             }
                                                             echo "<option value=\"".$RsTem["iCodTema"]."\" ".$selecTem.">".$RsTem["cDesTema"]." ".$RsTem["cNombresTrabajador"]."</option>";
                                                         }
                                                         mssql_free_result($rsTem);
                                                         ?>
                                                     </select>
                                                 </td>
                                             </tr>
                                             <tr>
                                                 <td >Oficina Destino:</td>
                                                 <td align="left"><select name="iCodOficinaDes" class="FormPropertReg form-control" style="width:360px"  >
                                                         <option value="">Seleccione:</option>
                                                         <?
                                                         $sqlOfi="SP_OFICINA_LISTA_COMBO ";
                                                         $rsOfi=mssql_query($sqlOfi,$cnx);
                                                         while ($RsOfi=MsSQL_fetch_array($rsOfi)){
                                                             if($RsOfi["iCodOficina"]==$_GET[iCodOficinaDes]){
                                                                 $selecClas="selected";
                                                             }Else{
                                                                 $selecClas="";
                                                             }
                                                             echo "<option value=".$RsOfi["iCodOficina"]." ".$selecClas.">".$RsOfi["cNomOficina"]."</option>";
                                                         }
                                                         mssql_free_result($rsOfi);
                                                         ?>
                                                     </select></td>
                                                 <td >&nbsp;</td>
                                                 <td align="left" class="CellFormRegOnly">&nbsp;</td>
                                             </tr>

                                             <tr>
                                                 <td colspan="4" align="right">
                                                     <button class="btn btn-primary" onclick="Buscar();" onMouseOver="this.style.cursor='hand'"> <b>Buscar</b> <img src="images/icon_buscar.png" width="17" height="17" border="0"> </button>
                                                     &nbsp;
                                                     <button class="btn btn-primary" onclick="window.open('<?=$PHP_SELF?>', '_self');" onMouseOver="this.style.cursor='hand'"> <b>Restablecer</b> <img src="images/icon_clear.png" width="17" height="17" border="0"> </button>
                                                     &nbsp;
                                                     <button class="btn btn-primary" onclick="window.open('pendientesDerivadosExcel.php?fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&Entrada=<?=$_GET[Entrada]?>&Interno=<?=$_GET[Interno]?>&cCodificacion=<?=$_GET[cCodificacion]?>&cAsunto=<?=$_GET[cAsunto]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&iCodTrabajadorFinalizar=<?=$_GET[iCodTrabajadorFinalizar]?>&iCodTema=<?=$_GET[iCodTema]?>&EstadoMov=<?=$_GET[EstadoMov]?>&iCodOficina=<?=$_SESSION[iCodOficinaLogin]?>', '_self');" onMouseOver="this.style.cursor='hand'"> <b>a Excel</b> <img src="images/icon_excel.png" width="17" height="17" border="0"> </button>
                                                     &nbsp;
                                                     <button class="btn btn-primary" onclick="window.open('pendientesDerivadosPdf.php?fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&Entrada=<?=$_GET[Entrada]?>&Interno=<?=$_GET[Interno]?>&Anexo=<?=$_GET[Anexo]?>&cCodificacion=<?=$_GET[cCodificacion]?>&cAsunto=<?=$_GET[cAsunto]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&iCodTrabajadorResponsable=<?=$_GET[iCodTrabajadorResponsable]?>&iCodTrabajadorDelegado=<?=$_GET[iCodTrabajadorDelegado]?>&iCodTema=<?=$_GET[iCodTema]?>&EstadoMov=<?=$_GET[EstadoMov]?>&Aceptado=<?=$_GET[Aceptado]?>&SAceptado=<?=$_GET[SAceptado]?>', '_blank');" onMouseOver="this.style.cursor='hand'"> <b>a Pdf</b> <img src="images/icon_pdf.png" width="17" height="17" border="0"> </button>
                                                 </td>
                                             </tr>
                                         </form>
                                     </div>
                                 </div>
                             </nav>
                         </div>
                         <div class="row">
                             <div class="col-lg-12">
                                 <div class="table-responsive">
                                     <table class="table">
                                         <thead>
                                            <tr>
                                                <td class="headColumnas"><a href="<?=$_SERVER['PHP_SELF']?>?campo=Codigo&orden=<?=$cambio?>&cCodificacion=<?=$_GET[cCodificacion]?>"  style=" text-decoration:<?if($campo=="Codigo"){ echo "underline"; }Else{ echo "none";}?>">N&ordm; TRÁMITE</a></td>
                                                <td class="headColumnas"><a href="<?=$_SERVER['PHP_SELF']?>?campo=Documento&orden=<?=$cambio?>&cCodificacion=<?=$_GET[cCodificacion]?>"  style=" text-decoration:<?if($campo=="Documento"){ echo "underline"; }Else{ echo "none";}?>">Tipo de Documento</a></td>
                                                <td class="headColumnas"><a href="<?=$_SERVER['PHP_SELF']?>?campo=Tra_M_Tramite.cCodificacion&orden=<?=$cambio?>&cCodificacion=<?=$_GET[cCodificacion]?>"  style=" text-decoration:<?if($campo=="Tra_M_Tramite.cCodificacion"){ echo "underline"; }Else{ echo "none";}?>">Nombre / Razón Social</a></td>
                                                <td class="headColumnas"><a href="<?=$_SERVER['PHP_SELF']?>?campo=Asunto&orden=<?=$cambio?>&cCodificacion=<?=$_GET[cCodificacion]?>"  style=" text-decoration:<?if($campo=="Asunto"){ echo "underline"; }Else{ echo "none";}?>">Asunto / Procedimiento TUPA</a></td>
                                                <td class="headColumnas">Derivado</td>
                                                <td class="headColumnas"><a href="<?=$_SERVER['PHP_SELF']?>?campo=Tra_M_Tramite.cCodificacion&orden=<?=$cambio?>&cCodificacion=<?=$_GET[cCodificacion]?>"  style=" text-decoration:<?if($campo=="Tra_M_Tramite.cCodificacion"){ echo "underline"; }Else{ echo "none";}?>">Derivado A:</a></td>
                                                <td class="headColumnas">Edit</td>
                                            </tr>
                                         </thead>
                                         <tbody>
	                                            <?php
                                                         if($_GET[fDesde]!=""){ $fDesde=date("Ymd", strtotime($_GET[fDesde])); }
                                                        if($_GET[fHasta]!=""){
                                                        $fHasta=date("d-m-Y", strtotime($_GET[fHasta]));
                                                        function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
                                                        $date_r = getdate(strtotime($date));
                                                        $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
                                                        return $date_result;
                                                                    }
                                                        $fHasta=dateadd($fHasta,1,0,0,0,0,0); // + 1 dia
                                                        }
                                                        include_once("../conexion/conexion.php");

                                                        function paginar($actual, $total, $por_pagina, $enlace, $maxpags=0) {
                                                      $total_paginas = ceil($total/$por_pagina);
                                                    $anterior = $actual - 1;
                                                    $posterior = $actual + 1;
                                                    $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
                                                    $maximo = $maxpags ? min($total_paginas, $actual+floor($maxpags/2)): $total_paginas;
                                                    if ($actual>1)
                                                    $texto = "<a href=\"$enlace$anterior\"><<</a> ";
                                                    else
                                                    $texto = "<b><<</b> ";
                                                    if ($minimo!=1) $texto.= "... ";
                                                    for ($i=$minimo; $i<$actual; $i++)
                                                    $texto .= "<a href=\"$enlace$i\">$i</a> ";
                                                    $texto .= "<b>$actual</b> ";
                                                    for ($i=$actual+1; $i<=$maximo; $i++)
                                                    $texto .= "<a href=\"$enlace$i\">$i</a> ";
                                                    if ($maximo!=$total_paginas) $texto.= "... ";
                                                    if ($actual<$total_paginas)
                                                    $texto .= "<a href=\"$enlace$posterior\"><<</a>";
                                                    else
                                                    $texto .= "<b><<</b>";
                                                    return $texto;
                                                    }


                                                    if (!isset($pag)) $pag = 1; // Por defecto, pagina 1
                                                    $tampag = 15;
                                                    $reg1 = ($pag-1) * $tampag;

                                                    // ordenamiento
                                                    if($_GET[campo]==""){
                                                        $campo="Derivado";
                                                    }Else{
                                                        $campo=$_GET[campo];
                                                    }

                                                    if($_GET[orden]==""){
                                                        $orden="DESC";
                                                    }Else{
                                                        $orden=$_GET[orden];
                                                    }

                                                    //invertir orden
                                                    if($orden=="ASC") $cambio="DESC";
                                                    if($orden=="DESC") $cambio="ASC";

                                                            $sqlTra.= " SP_BANDEJA_DERIVADOS_PROFESIONAL '$_GET[Entrada]','$_GET[Interno]','$fDesde','$fHasta','%$_GET[cCodificacion]%','%$_GET[cAsunto]%','$_SESSION[iCodOficinaLogin]','$_SESSION[CODIGO_TRABAJADOR]','$_GET[cCodTipoDoc]','$_GET[iCodTema]' ,'$_GET[iCodOficinaDes]','$campo','$orden'";
                                                            $rsTra=mssql_query($sqlTra,$cnx);
                                                             // echo $sqlTra;
                                                            $total = MsSQL_num_rows($rsTra);
                                                            $numrows=MsSQL_num_rows($rsTra);
                                                    if($numrows==0){
                                                            echo "NO SE ENCONTRARON REGISTROS<br>";
                                                    }else{
                                                    ///////////////////////////////////////////////////////
                                                            for ($i=$reg1; $i<min($reg1+$tampag, $total); $i++) {
                                                            mssql_data_seek($rsTra, $i);
                                                            $RsTra=MsSQL_fetch_array($rsTra);
                                                    ///////////////////////////////////////////////////////
                                                            //while ($RsTra=MsSQL_fetch_array($rsTra)){
                                                                    if ($color == "#DDEDFF"){
                                                                            $color = "#F9F9F9";
                                                                        }else{
                                                                            $color = "#DDEDFF";
                                                                        }
                                                                        if ($color == ""){
                                                                            $color = "#F9F9F9";
                                                                        }
                                                            ?>
                                                            <tr bgcolor="<?=$color?>" onMouseOver="this.style.backgroundColor='#BFDEFF';" OnMouseOut="this.style.backgroundColor='<?=$color?>'">
                                                            <td width="95" valign="top" align="left">
                                                             <? if($RsTra[nFlgTipoDoc]==1){?>
                                                                    <a href="registroDetalles.php?iCodTramite=<?=$RsTra[iCodTramite]?>"  rel="lyteframe" title="Detalle del TRÁMITE"
                                                                       rev="width: 970px; height: 550px; scrolling: auto; border:no">
                                                                        <?=$RsTra[cCodificacion]?>
                                                                    </a>
                                                            <? }
                                                                if($RsTra[nFlgTipoDoc]==2){
                                                                 echo "INTERNO";}
                                                                 if($RsTra[nFlgTipoDoc]==3){
                                                                 echo "SALIDA";}
                                                                if($RsTra[nFlgTipoDoc]==4){
                                                              ?>
                                                                    <a href="registroDetalles.php?iCodTramite=<?=$RsTra[iCodTramiteRel]?>"  rel="lyteframe" title="Detalle del TRÁMITE"
                                                                       rev="width: 970px; height: 550px; scrolling: auto; border:no"><?=$RsTra[cCodificacion]?></a>
                                                            <?}?>
                                                            <?
                                                            $date = date_create($RsTra[fFecRegistro]);
                                                            $fFecRegistro = date_format($date, 'd-m-Y H:i:s');
                                                            echo "<div style=color:#727272>".$fFecRegistro."</div>";
                                                          //echo "<div style=color:#727272;font-size:10px>".date("G:i", strtotime($RsTra[fFecRegistro]))."</div>";
                                                          if($RsTra[cFlgTipoMovimiento]==4){
                                                             echo "<div style=color:#FF0000;font-size:12px>Copia</div>";
                                                            }

                                                          ?>
                                                            </td>
                                                             <td width="95" valign="top" align="left">
                                                            <?

                                                              if($RsTra[nFlgTipoDoc]==1  ){
                                                                    echo $RsTra[Documento];
                                                              echo "<div style=color:#808080;text-transform:uppercase>".$RsTra[cNroDocumento]."</div>";
                                                              }else{
                                                                    //echo $RsTra[iCodTramite]."-";
                                                                    $sqlTrm="SELECT * FROM Tra_M_Tramite WHERE iCodTramite='$RsTra[iCodTramite]'";
                                                                    $rsTrm=mssql_query($sqlTrm,$cnx);
                                                                    $RsTrm=MsSQL_fetch_array($rsTrm);
                                                                        $sqlTpDcM="SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$RsTrm[cCodTipoDoc]'";
                                                                        $rsTpDcM=mssql_query($sqlTpDcM,$cnx);
                                                                        $RsTpDcM=MsSQL_fetch_array($rsTpDcM);
                                                                            echo $RsTpDcM[cDescTipoDoc];
                                                                    echo "<br>";
                                                                    echo "<a style=\"color:#0067CE\" href=\"registroOficinaDetalles.php?iCodTramite=".$RsTra[iCodTramite]."\" rel=\"lyteframe\" title=\"Detalle del TRÁMITE\" rev=\"width: 970px; height: 450px; scrolling: auto; border:no\">";
                                                                        echo $RsTra[cCodificacion];
                                                                        echo "</a>";
                                                              }
                                                              ?></td>
                                                            <td width="188" align="left" valign="top">
                                                                <?
                                                              $rsRem=mssql_query("SELECT * FROM Tra_M_Remitente WHERE iCodRemitente='$RsTra[iCodRemitente]'",$cnx);
                                                              $RsRem=MsSQL_fetch_array($rsRem);
                                                              echo $RsRem["cNombre"];
                                                                        mssql_free_result($rsRem);
                                                                ?>
                                                            </td>
                                                            <td align="left" valign="top"><?=$RsTra[cAsunto]?></td>
                                                            <td width="100" align="left" valign="top">
                                                                <?
                                                                $date = date_create($RsTra[fFecDerivar]);
                                                                $fFecDerivar = date_format($date, 'd-m-Y H:i:s');
                                                                echo $RsTra[cAsuntoDerivar];?>

                                                                <div style="color:#0154AF"><?=$fFecDerivar;?></div>
                                                                <!--<div style="color:#0154AF;font-size:10px"><?=date("G:i", strtotime($RsTra[fFecDerivar]));?></div>	-->
                                                            </td>
                                                            <td width="100" valign="top" align="left">
                                                                <?
                                                              $rsOfic=mssql_query("SELECT * FROM Tra_M_Oficinas WHERE iCodOficina='$RsTra[iCodOficinaDerivar]'",$cnx);
                                                              $RsOfic=MsSQL_fetch_array($rsOfic);
                                                              echo "<a href=javascript:; title=\"".$RsOfic["cNomOficina"]."\">".$RsOfic["cSiglaOficina"]."</a>";
                                                                        mssql_free_result($rsOfic);
                                                            if($RsTra[fFecRecepcion]==""){
                                                                        echo "<div style=color:#ff0000>sin aceptar</div>";
                                                                }Else{
                                                                $date = date_create($RsTra[fFecRecepcion]);
                                                                        echo "<div style=color:#0154AF>".date_format($date, 'd-m-Y')."</div>";
                                                                        echo "<div style=color:#0154AF;font-size:10px>".date_format($date, 'G:i')."</div>";
                                                                }

                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?
                                                                $sqlChk="SELECT TOP 1 Tra_M_Tramite.iCodTramite, Tra_M_Tramite_Movimientos.iCodTramite, Tra_M_Tramite_Movimientos.iCodOficinaOrigen, Tra_M_Tramite_Movimientos.iCodOficinaDerivar, Tra_M_Tramite.nFlgEnvio, Tra_M_Tramite_Movimientos.nFlgTipoDoc, Tra_M_Tramite.cCodificacion, Tra_M_Tramite_Movimientos.iCodMovimiento FROM Tra_M_Tramite ,Tra_M_Tramite_Movimientos ";
                                                                        $sqlChk.="WHERE Tra_M_Tramite.iCodTramite=Tra_M_Tramite_Movimientos.iCodTramite ";
                                                                $sqlChk.="AND Tra_M_Tramite_Movimientos.iCodOficinaOrigen='$_SESSION[iCodOficinaLogin]' ";
                                                                $sqlChk.="AND Tra_M_Tramite_Movimientos.iCodOficinaDerivar!='$_SESSION[iCodOficinaLogin]' ";
                                                                $sqlChk.="AND Tra_M_Tramite.nFlgEnvio=1 ";
                                                                        $sqlChk.="AND nEstadoMovimiento!=2 ";
                                                                        $sqlChk.="AND Tra_M_Tramite_Movimientos.nFlgTipoDoc!=3 ";
                                                                        $sqlChk.="AND Tra_M_Tramite.cCodificacion='$RsTra[cCodificacion]' ";
                                                                $sqlChk.="ORDER BY iCodMovimiento ASC";
                                                                $rsChk=mssql_query($sqlChk,$cnx);
                                                              $RsChk=MsSQL_fetch_array($rsChk);
                                                              if($RsChk[iCodMovimiento]==$RsTra[iCodMovimiento]){
                                                                ?>
                                                                <a href="profesionalDerivadosEdit.php?iCodMovimientoDerivar=<?=$RsTra[iCodMovimiento]?>"><i class="fas fa-edit"></i>
                                                                <?}?>
                                                            </td>
                                                            </tr>
                                                            <?
                                                            }
                                                            }
                                                            mssql_free_result($rsTra);
                                                                    ?>
                                         </tbody>
                                     </table>
                                 </div>
                             </div>
                             <div class="col-lg-12">
			                        <? echo paginar($pag, $total, $tampag, "profesionalDerivados.php?fDesde=".$_GET[fDesde]."&fHasta=".$_GET[fHasta]."&cCodificacion=".$_GET[cCodificacion]."&cAsunto=".$_GET[cAsunto]."&iCodTema=".$_GET[iCodTema]."&cCodTipoDoc=".$_GET[cCodTipoDoc]."&Entrada=".$_GET[Entrada]."&Interno=".$_GET[Interno]."&pag="); ?>
                             </div>
                         </div>
					</div>
                 </div>
             </div>
         </div>
     </div>
 </main>
 <?php include("includes/userinfo.php"); ?>

<?include("includes/pie.php");?>
<script type="text/javascript" language="javascript" src="includes/lytebox.js"></script>
<script type="text/javascript" src="scripts/dhtmlgoodies_calendar.js"></script>
<script Language="JavaScript">
    $('.datepicker').pickadate({
        monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
        format: 'dd-mm-yyyy',
        formatSubmit: 'dd-mm-yyyy',
    });
    $('.mdb-select').material_select();
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
        function Buscar()
        {
            document.frmConsulta.action="<?=$PHP_SELF?>";
            document.frmConsulta.submit();
        }


        //--></script>
</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>