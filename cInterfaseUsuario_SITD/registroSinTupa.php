<?php
session_start();
if($_SESSION['CODIGO_TRABAJADOR']!=""){
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
<link type="text/css" rel="stylesheet" href="css/dhtmlgoodies_calendar.css" media="screen">
    <style>
        legend.scheduler-border {
            width:inherit; /* Or auto */
            padding:0 10px; /* To give a bit of padding on the left and right */
            border-bottom:none;
        }
    </style>
</head>
<body onload="mueveReloj()">

<?include("includes/menu.php");?>

<!--Main layout--> 
<main class="mx-lg-5">     
    <div class="container-fluid">          
        <!--Grid row-->         
        <div class="row wow fadeIn">              
            <!--Grid column-->             
            <div class="col-md-12 mb-12">                  
                <!--Card-->                 
                <div class="card">                     
                    <!-- Card header -->                     
                    <div class="card-header text-center ">
                        Registro de entrada sin TUPA
                    </div>                      
                    <!--Card content-->                     
                    <div class="card-body">
                         <form  name="frmRegistro" method="POST" enctype="multipart/form-data" >
                                        <input type="hidden" name="opcion" value="1">
                                        <input type="hidden" name="sal" value="1">
                                        <input type="hidden" name="nFlgClaseDoc" value="2">
                                        <input type="hidden" name="nFlgEnvio" value="<?if($_POST[ActivarDestino]==1) echo "1"?>">
                             Datos del Documento
                             <hr>
                             <div class="form-row">
                                <div class="col-md-3">
                                    <div class="md-form input-group">
                                                <label for="docu">N&ordm; del Documento:</label>
                                                <input type="text" name="cNroDocumento" class="form-control FormPropertReg"
                                                    <?=$fondo1?> value="<? echo stripslashes($_POST[cNroDocumento]);?>"   id="cNroDocumento"
                                                       required>
                                                <div class="input-group-append">
                                                    <input name="button" type="button" class="btn btn-primary" value="ChkDoc." onclick="releer();" ><?=$eti?>
                                                </div>
                                            </div>
                                            <div class="invalid-feedback">
                                                <?php
                                                if(trim($_POST[cNroDocumento])!=""){
                                                    $sqlChek = "SELECT cNroDocumento FROM Tra_M_tramite WHERE cNroDocumento = '$_POST[cNroDocumento]'";
                                                    $rsChek  = mssql_query($sqlChek,$cnx);
                                                    $numChek = mssql_num_rows($rsChek);
                                                    if ($numChek>0) {
                                                        $fondo1 = "style=background-color:#FF3333;color:#000"; $eti="<span class='FormCellRequisito'>El número ingresado ya existe</span>";
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                                <label><strong>Tipo de Documento:</strong></label>
                                                <select name="cCodTipoDoc" class="FormPropertReg mdb-select colorful-select dropdown-primary"   searchable="Buscar aqui..">
                                                    <option value="">Seleccione:</option>
                                                    <?php
                                                    include_once("../conexion/conexion.php");
                                                    $sqlTipo = "SELECT * FROM Tra_M_Tipo_Documento WHERE nFlgEntrada = 1";
                                                    $sqlTipo.="ORDER BY cDescTipoDoc ASC  ";
                                                    $rsTipo  = mssql_query($sqlTipo,$cnx);
                                                    while ($RsTipo = mssql_fetch_array($rsTipo)){
                                                        if ($RsTipo['cCodTipoDoc'] == $_POST['cCodTipoDoc']){
                                                            $selecTipo="selected";
                                                        }else{
                                                            $selecTipo = "";
                                                        }
                                                        echo utf8_encode("<option value=".$RsTipo["cCodTipoDoc"]." ".$selecTipo.">".$RsTipo["cDescTipoDoc"]."</option>");
                                                    }
                                                    mssql_free_result($rsTipo);
                                                    ?>
                                                </select>&nbsp;
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="md-form">
                                                <input placeholder="dd-mm-aaaa" value="<?php echo $_POST[fechaDocumento]; ?>" type="text"
                                                       id="date-picker-example" name="fechaDocumento" class="FormPropertReg form-control datepicker">
                                                <label for="date-picker-example">Fecha del Documento:</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 ">
                                            <label for="FormPropertReg"><strong>Fecha  Registro:</strong></label>
                                            <input type="text" name="reloj" id="FormPropertReg" class="FormPropertReg form-control"  onfocus="window.document.frmRegistro.reloj.blur()">
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="md-form">
                                                <input type="text" style="text-transform:uppercase" name="cNomRemite" class="form-control FormPropertReg"
                                                    value="<?=$_POST[cNomRemite]?>"   id="cNomRemite"
                                                       required>
                                                <label for="text2">Remite:</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="md-form">
                                         <textarea type="text" style="text-transform:uppercase" name="cAsunto" id="cAsunto"
                                                   class="md-textarea md-textarea-auto form-control FormPropertReg" rows="1"><?=stripslashes($_POST[cAsunto])?></textarea>
                                                <label for="cAsunto">Asunto:</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="md-form">
                                         <textarea type="text" style="text-transform:uppercase" name="cObservaciones" id="cObservaciones"
                                                   class="md-textarea md-textarea-auto form-control FormPropertReg" rows="1"><?=stripslashes($_POST[cObservaciones])?></textarea>
                                                <label for="cObservaciones">Observaciones:</label>
                                            </div>
                                        </div>
                                    </div>
                             Remitente
                             <hr>
                             <div class="form-row">
                                 <div class="col-lg-3">
                                   <div class="md-form input-group">
                                      <input id="cNombreRemitente" type="search" name="cNombreRemitente" class="FormPropertReg form-control" value="<?=$_POST[cNombreRemitente]?>"  readonly>
                                      <label for="cNombreRemitente">Remitente / Instituci&oacute;n:</label>
                                      <div class="input-group-append">
                                         <a class="btn btn-primary waves-effect m-0" href="javascript:;"
                                              onClick="window.open('registroRemitentesLs.php','popuppage','width=745,height=360,toolbar=0,statusbar=1,resizable=0,scrollbars=yes,top=100,left=100');">
                                              <i class="fas fa-search"></i>
                                          </a>
                                          <a class="btn btn-primary waves-effect m-0" style="text-decoration:none" href="javascript:;"
                                             onClick="window.open('registroRemitentesNw.php','popuppage','width=590,height=450,toolbar=0,statusbar=1,resizable=0,scrollbars=yes,top=100,left=100');">
                                             <i class="far fa-plus-square"></i>
                                          </a>
                                      </div>
                                  </div>
                                     <input id="iCodRemitente" name="iCodRemitente" type="hidden" value="<?=$_POST[iCodRemitente]?>">
                                     <input id="Remitente" name="Remitente" type="hidden" value="<?=$_POST[iCodRemitente]?>">
                                 </div>
                                 <div class="col-lg-2">
                                     <div class="md-form">
                                         <input type="hidden" readonly="readonly" name="cReferencia" value="<?if($_GET[clear]==""){ echo trim($Rs[cReferencia]); }else{ echo trim($_POST[cReferencia]);}?>" class="FormPropertReg form-control" style="width:140px;text-transform:uppercase" />
                                         <input type="hidden" name="iCodTramiteRef" value="<?=$_REQUEST[iCodTramiteRef]?>"  />
                                         <a class="btn btn-primary waves-effect m-0 " style=" text-decoration:none" href="javascript:;" onClick="Buscar();">A&ntilde;adir Referencia</a>
                                     </div>
                                 </div>
                                 <?php
                                 $sqlRefs = "SELECT * FROM Tra_M_Tramite_Referencias WHERE cCodSession='$_SESSION[cCodRef]'";
                                 // echo $sqlRefs;
                                 $rsRefs = mssql_query($sqlRefs,$cnx);
                                 while ($RsRefs = mssql_fetch_array($rsRefs)){      ?>
                                     <span style="background-color:#EAEAEA;"><?=$RsRefs[cReferencia]?>
                                         <a href="registroData.php?iCodReferencia=<?=$RsRefs[iCodReferencia]?>&opcion=19&iCodTramite=<?=$_GET[iCodTramite]?>&sal=1&URI=<?=$_GET[URI]?>&radioSeleccion=<?=$_POST[radioSeleccion]?>&cNombreRemitente=<?=$_POST[cNombreRemitente]?>&iCodTrabajadorResponsable=<?=$_POST[iCodTrabajadorResponsable]?>&iCodOficinaResponsable=<?=$_POST[iCodOficinaResponsable]?>&cNroDocumento=<?=$_POST[cNroDocumento]?>&cNomRemite=<?=$_POST[cNomRemite]?>&ActivarDestino=<?=$_POST[ActivarDestino]?>&iCodRemitente=<?=$_POST[iCodRemitente]?>&Remitente=<?=$_POST[Remitente]?>&cCodTipoDoc=<?=$_POST[cCodTipoDoc]?>&cAsunto=<?=$_POST[cAsunto]?>&cObservaciones=<?=$_POST[cObservaciones]?>&nNumFolio=<?=$_POST[nNumFolio]?>&nFlgEnvio=<?=$_POST[nFlgEnvio]?>&cSiglaAutor=<?=$_POST[cSiglaAutor]?>&archivoFisico=<?=$_POST[archivoFisico]?>">
                                                        <img src="images/icon_del.png" border="0" width="13" height="13">
                                                    </a>
                                             </span>&nbsp;
                                     <?php
                                 }
                                 ?>
                                 <div class="col-lg-2">
                                     <label><strong>Oficina:</strong></label>
                                     <select name="iCodOficinaResponsable" class="FormPropertReg mdb-select colorful-select dropdown-primary" searchable="Buscar aqui.."
                                             onChange="loadResponsables(this.value);"
                                             >
                                         <option value="">Seleccione:</option>
                                         <?php

                                         $sqlDep2 = "SELECT * FROM Tra_M_Oficinas 
                                                                            WHERE iFlgEstado != 0 
                                                                                        
                                                                            ORDER BY cNomOficina ASC"; //AND iCodOficina != $iCodOficinaVirtual
                                         //$sqlDep2 = " SP_OFICINA_LISTA_COMBO ";
                                         $rsDep2  = mssql_query($sqlDep2,$cnx);
                                         while ($RsDep2 = mssql_fetch_array($rsDep2)){
                                             if ($RsDep2['iCodOficina'] == $_POST['iCodOficinaResponsable']){
                                                 $selecOfi = "selected";
                                             }else{
                                                 $selecOfi = "";
                                             }
                                             echo utf8_encode("<option value=".$RsDep2['iCodOficina']." ".$selecOfi.">".trim($RsDep2['cNomOficina'])." | ".trim($RsDep2["cSiglaOficina"])."</option>");
                                         }
                                         mssql_free_result($rsDep2);
                                         ?>
                                     </select>&nbsp;
                                 </div>
                                 <div class="col-lg-2">
                                     <label><strong>Responsable:</strong></label>
                                     <select name="iCodTrabajadorResponsable" id="responsable"
                                             class="FormPropertReg colorful-select dropdown-primary">
                                     </select>
                                 </div>
                                 <div class="col-lg-2">
                                     <label><strong>Indicaci&oacute;n:</strong></label>
                                     <select name="iCodIndicacion" class="FormPropertReg mdb-select colorful-select dropdown-primary"
                                             searchable="Buscar aqui.."
                                     >
                                         <option value="">Seleccione:</option>
                                         <?
                                         $sqlIndic="SELECT * FROM Tra_M_Indicaciones ";
                                         $sqlIndic .= "ORDER BY cIndicacion ASC";
                                         $rsIndic=mssql_query($sqlIndic,$cnx);
                                         while ($RsIndic=MsSQL_fetch_array($rsIndic)){
                                             if($RsIndic[iCodIndicacion]==$_POST[iCodIndicacion] OR $RsIndic[iCodIndicacion]==3){
                                                 $selecIndi="selected";
                                             }Else{
                                                 $selecIndi="";
                                             }
                                             echo utf8_encode("<option value=".$RsIndic["iCodIndicacion"]." ".$selecIndi.">".$RsIndic["cIndicacion"]."</option>");
                                         }
                                         mssql_free_result($rsIndic);
                                         ?>
                                     </select>
                                 </div>

                             </div>
                             Archivo
                             <hr>
                             <div class="form-row">
                                 <div class="col-lg-1">
                                     <div class="md-form">
                                         <label >Folios:</label>
                                         <input type="number" min=1 name="nNumFolio" value="<? if($_POST[nNumFolio]==""){echo 1;} else { echo $_POST[nNumFolio];}?>"
                                                class="FormPropertReg form-control"  />
                                     </div>
                                 </div>
                                 <div class="col-lg-2">
                                     <div class="form-check">
                                         <input type="checkbox" name="ActivarDestino" class="form-check-input" id="materialChecked2" >
                                         <label class="form-check-label" for="materialChecked2">Derivar inmediatamente:</label>
                                     </div>
                                 </div>
                                 <div class="col-lg-3">
                                     <div class="md-form">
                                         <textarea type="text" style="text-transform:uppercase" name="archivoFisico" id="archivoFisico"
                                                   class="md-textarea md-textarea-auto form-control FormPropertReg" rows="1"><?php echo trim($_POST['archivoFisico']); ?></textarea>
                                         <label for="archivoFisico">Archivo F&iacute;sico:</label>
                                     </div>
                                 </div>
                                 <div class="col-md-3">
                                     <?php
                                     if ($_FILES['fileUpLoadDigital']['name'] != null){
                                         $_SESSION['ArchivoPDF']=$_FILES['fileUpLoadDigital']['tmp_name']."/". $_FILES['fileUpLoadDigital']['name'];
                                         echo $_FILES['fileUpLoadDigital']['name'];
                                     }
                                     ?>
                                     <div class="file-field">
                                         <div class="btn btn-primary btn-sm float-left">
                                             <span>File</span>
                                             <input type="file">
                                         </div>
                                         <div class="file-path-wrapper">
                                             <input class="file-path validate"  name="fileUpLoadDigital" type="text" placeholder="Elegir Archivo">
                                         </div>
                                     </div>

                                 </div>
                                   <input name="button" type="button" class="btn btn-primary" value="Registrar" onclick="Registrar();">

                             </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include("includes/userinfo.php"); ?>
<?php include("includes/pie.php"); ?>
    <script Language="JavaScript">

        function activaDestino(){
            if (document.frmRegistro.nFlgEnvio.value == 1){
                document.frmRegistro.nFlgEnvio.value="";
            }else{
                document.frmRegistro.nFlgEnvio.value=1;
            }
            return false;
        }

        function mueveReloj(){
            momentoActual = new Date()
            anho = momentoActual.getFullYear()
            mes = (momentoActual.getMonth())+1
            dia = momentoActual.getDate()
            hora = momentoActual.getHours()
            minuto = momentoActual.getMinutes()
            segundo = momentoActual.getSeconds()
            if((mes>=0)&&(mes<=9)){ mes="0"+mes; }
            if((dia>=0)&&(dia<=9)){ dia="0"+dia; }
            if((hora>=0)&&(hora<=9)){ hora="0"+hora; }
            if((minuto>=0)&&(minuto<=9)){ minuto="0"+minuto; }
            if ((segundo>=0)&&(segundo<=9)){ segundo="0"+segundo; }
            horaImprimible = dia + "-" + mes + "-" + anho + " " + hora + ":" + minuto + ":" + segundo
            document.frmRegistro.reloj.value=horaImprimible
            setTimeout("mueveReloj()",1000)
        }



        function activaDerivar(){
            document.frmRegistro.action="<?=$_SERVER['PHP_SELF']?>";
            document.frmRegistro.submit();
            return false;
        }

        function releer(){
            document.frmRegistro.action="<?=$_SERVER['PHP_SELF']?>#area";
            document.frmRegistro.submit();
        }


        var miPopup
        function Buscar(){
            miPopup=window.open('registroBuscarDocEnt.php','popuppage','width=745,height=360,toolbar=0,status=0,resizable=0,scrollbars=yes,top=100,left=100');
        }

        function AddReferencia(){
            document.frmRegistro.opcion.value=21;
            document.frmRegistro.action="registroData.php";
            document.frmRegistro.submit();
        }

        function Registrar()
        {
            if (document.frmRegistro.cCodTipoDoc.value.length == "")
            {
                alert("Seleccione Tipo de Documento");
                document.frmRegistro.cCodTipoDoc.focus();
                return (false);
            }
            if (document.frmRegistro.cNroDocumento.value.length == "")
            {
                alert("Ingrese Número del Documento");
                document.frmRegistro.cNroDocumento.focus();
                return (false);
            }
            if (document.frmRegistro.iCodRemitente.value.length == "")
            {
                alert("Seleccione Remitente");
                return (false);
            }
            if(document.frmRegistro.fechaDocumento.value.length==""){
                alert("Seleccione una fecha de documento");
                return (false);
            }

            //  if (document.frmRegistro.nFlgEnvio.value==1)
            if (document.frmRegistro.nFlgEnvio.value=="")
            {
                if (document.frmRegistro.iCodOficinaResponsable.value.length == "")
                {
                    document.frmRegistro.nFlgEnvio.value=1;
                }
            }

            document.frmRegistro.action="registroData.php";
            document.frmRegistro.submit();
        }
        /*
        function go() {
            alert("dd");
             w = new ActiveXObject("WScript.Shell");
            //w.run("c:\\envioSMS.jar", 1, true);
            w.run("c:\\refirma\\1.1.0\\ReFirma-1.1.0.jar", 1, true);//G:\refirma\1.1.0\ReFirma-1.1.0.jar
            return true;
        }
        */
        //--></script>
    <script type="text/javascript" language="javascript" src="includes/lytebox.js"></script>
    <script type="text/javascript" src="scripts/dhtmlgoodies_calendar.js"></script>
<script>
    // Data Picker Initialization
    $('.datepicker').pickadate({
        monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
        format: 'dd-mm-yyyy',
        formatSubmit: 'dd-mm-yyyy',
    });
    $('.mdb-select').material_select();
	function loadResponsables(value)
	{
        $("#responsable > option").remove(); 
        $('#responsable').append('<option value="">Cargando Datos...</option>'); 
		
        var parametros = {
					"iCodOficinaResponsable" : value
		   };
		var dominio = document.domain;
		        
	    $.ajax({
	        type: 'POST',
	        url: 'loadResponsable.php', 
	        data: parametros, 
	        dataType: 'json',
	        success: function(list){
                $('#responsable').append('<option value="">Cargando Datos...</option>');
                $("#responsable > option").remove(); 

                console.log(list);
                var opt = $('<option />'); 
                //opt.text('Seleccione un responsable');
                $('#responsable').append(opt);
                $.each(list,function(index,value) 
                {
                    //var opt = $('<option />'); 
                    opt.val(value.iCodTrabajador);
                    opt.text(value.cNombresTrabajador+" "+value.cApellidosTrabajador);
                    $('#responsable').append(opt);
                });
                $('#responsable').material_select();
	        },
	        error: function(e){
	        	console.log(e);
	            alert('Error Processing your Request!!');
	        }
	    });
        
	}


</script>

</body>
</html>

<?php
}else{
   header("Location: ../index.php?alter=5");
}
?>