<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: consultaAnexos_pdf.php
SISTEMA: SISTEMA  DE TRÁMITE DOCUMENTARIO DIGITAL
OBJETIVO: Reporte General en PDF de los Documentos de Anexo
PROPIETARIO: AGENCIA PERUANA DE COOPERACIÓN INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripción
------------------------------------------------------------------------
1.0   APCI    05/09/2018      Creación del programa.
------------------------------------------------------------------------
*****************************************************************************************/
session_start();
ob_start();
//*************************************
?>
<page backtop="25mm" backbottom="10mm" backleft="10mm" backright="10mm">
	<page_header>
		<table style="width: 100%; border: solid 0px black;">
			<tr>
				<td style="text-align: center;	width: 100%">
					<img style="width: 93%;" src="images/pdf_head.jpg" alt="Logo">
				</td>
			</tr>
		</table>
	</page_header>
	<page_footer>
		<table style="width: 100%; border: solid 0px black;">
			<tr>
				<td style="text-align: right;	width: 100%">p�gina [[page_cu]]/[[page_nb]]</td>
			</tr>
		</table>
	</page_footer>
	
	
	<table style="width: 100%; border: solid 0px black;">
	<tr>
	<td style="text-align: left;	width: 50%"><span style="font-size: 15px; font-weight: bold">Reporte Entradas General</span></td>
	<td style="text-align: right;	width: 50%"><span style="font-size: 15px; font-weight: bold"><?=date("d-m-Y")?></span></td>
	</tr>
	</table>
	<br>
	<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="center">
		<thead>
			<tr>
				<th style="width: 10%; text-align: center; border: solid 1px #2D96FF; background: #E1F0FF">N&ordm; Doc.</th>
				<th style="width: 20%; text-align: center; border: solid 1px #2D96FF; background: #E1F0FF">N&ordm; de Referencia</th>
				<th style="width: 20%; text-align: center; border: solid 1px #2D96FF; background: #E1F0FF">Remitente</th>
				<th style="width: 10%; text-align: center; border: solid 1px #2D96FF; background: #E1F0FF">Fecha de Derivado</th>
				<th style="width: 40%; text-align: center; border: solid 1px #2D96FF; background: #E1F0FF">Asunto</th>

			</tr>
		</thead>
		<tbody>
				<?
				require_once("../conexion/conexion.php");
    $fDesde=date("Ymd", strtotime($_GET[fDesde]));
	$fHasta=date("Y-m-d", strtotime($_GET[fHasta]));
	function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    $date_r = getdate(strtotime($date));
    $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
    return $date_result;
				}
	$fHasta=dateadd($fHasta,1,0,0,0,0,0); // + 1 dia

        $sql=" SELECT * FROM Tra_M_Tramite,Tra_M_Tramite_Movimientos,Tra_M_Remitente ";
        $sql.=" WHERE Tra_M_Tramite.iCodTramite=Tra_M_Tramite_Movimientos.iCodTramite AND Tra_M_Tramite.iCodRemitente=Tra_M_Remitente.iCodRemitente  ";
       if($_GET[fDesde]!="" AND $_GET[fHasta]==""){
       	$sql.=" AND Tra_M_Tramite_Movimientos.fFecDerivar>'$fDesde' ";
       }
       if($_GET[fDesde]=="" AND $_GET[fHasta]!=""){
       	$sql.=" AND Tra_M_Tramite_Movimientos.fFecDerivar<='$fHasta' ";
       }
       if($_GET[fDesde]!="" && $_GET[fHasta]!=""){
       $sql.=" AND Tra_M_Tramite_Movimientos.fFecDerivar BETWEEN  '$fDesde' and '$fHasta' ";
       }
	   	   
	   if($_GET[cCodificacion]!=""){
        $sql.="AND Tra_M_Tramite.cCodificacion='$_GET[cCodificacion]' ";
       }
	   if($_GET[cNroDocumento]!=""){
        $sql.="AND Tra_M_Tramite.cNroDocumento='$_GET[cNroDocumento]' ";
       }
	   
       if($_GET[cAsunto]!=""){
        $sql.="AND Tra_M_Tramite.cAsunto LIKE '%$_GET[cAsunto]%' ";
       }
	   if($_GET[iCodTupa]!=""){
        $sql.="AND Tra_M_Tramite.iCodTupa='$_GET[iCodTupa]' ";
       }
	   if($_GET[cCodTipoDoc]!=""){
        $sql.="AND Tra_M_Tramite.cCodTipoDoc='$_GET[cCodTipoDoc]' ";
       }
       if($_GET[cNombre]!=""){
        $sql.="AND Tra_M_Remitente.cNombre LIKE '%$_GET[cNombre]%' ";
       }
	    if($_GET[iCodOficina]!=""){
        $sql.="AND Tra_M_Tramite_Movimientos.iCodOficinaOrigen='$_GET[iCodOficina]' ";
       }
	   
	   	   
       $sql.= " ORDER BY Tra_M_Tramite_Movimientos.iCodOficinaOrigen";	   
       $rs=mssql_query($sql,$cnx);
       //echo $sql;
	   
	   while ($Rs=MsSQL_fetch_array($rs)){
	   ?>
	    <tr>
        <td style="width: 10%; text-align: center; border: solid 1px #2D96FF;font-size:10px"><? echo $Rs[cCodificacion];?></td>
        <td style="width: 20%; text-align: center; border: solid 1px #2D96FF;font-size:10px"><? echo $Rs[cNroDocumento];?></td>
        <td style="width: 10%; border: solid 1px #2D96FF;font-size:10px">
		    <? if($Rs[cTipoPersona]=='1'){ 
			   echo "<div style=color:#000000;text-align:center>Persona Natural</div>";
               echo "<div style=color:#0154AF;font-size:10px;text-align:center>".$Rs[cNombre]."</div>";
			   }
			   else{
			   echo "<div style=color:#000000;text-align:center>".$Rs[cNombre]."</div>";
               echo "<div style=color:#0154AF;text-align:center>Representante</div>";}?>
              </td> 
        <td style="width: 10%; border: solid 1px #2D96FF;font-size:10px">
		    <? echo "<div style=color:#0154AF;text-align:center>".date("d-m-Y", strtotime($Rs[fFecDerivar]))."</div>";
               echo "<div style=color:#0154AF;font-size:10px;text-align:center>".date("h:i A", strtotime($Rs[fFecDerivar]))."</div>";?></td>
        <td style="width: 40%; text-align: justify; border: solid 1px #2D96FF;font-size:10px"><? echo $Rs[cAsunto];?></td>
        </tr>
      <?
         }
      ?>
	   	
      </tbody>
	</table>
</page>

<?
//*************************************


	$content = ob_get_clean();  set_time_limit(0);     ini_set('memory_limit', '640M');

	// conversion HTML => PDF
	require_once(dirname(__FILE__).'/html2pdf/html2pdf.class.php');
	try
	{
		$html2pdf = new HTML2PDF('P','A4', 'es', false, 'UTF-8', 3);
		$html2pdf->pdf->SetDisplayMode('fullpage');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output('exemple03.pdf');
	}
	catch(HTML2PDF_exception $e) { echo $e; }
?>   
         		
