<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: iu_actualiza_indicador.php
SISTEMA: SISTEMA  DE TR�MITE DOCUMENTARIO DIGITAL
OBJETIVO: Mantenimiento de la Tabla Maestra de Indicadores para el Perfil Administrador
          -> Actualizar Registro de Indicadores
PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver      Autor             Fecha        Descripci�n
------------------------------------------------------------------------
1.0   APCI       03/08/2018   Creaci�n del programa.
 
------------------------------------------------------------------------
*****************************************************************************************/
session_start();
If($_SESSION['CODIGO_TRABAJADOR']!=""){
include_once("../conexion/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
<script>
function validar(f) {
 var error = "Por favor, antes de crear complete:\n\n";
 var a = "";
  if (f.txtindicador.value == "") {
  a += " Ingrese Descripci�n de Indicador";
  alert(error + a);
 }
   
 return (a == "");
 
}
</script>
</head>
<body>


	<?include("includes/menu.php");?>



<!--Main layout-->
 <main class="mx-lg-5">
     <div class="container-fluid">
          <!--Grid row-->
         <div class="row wow fadeIn">
              <!--Grid column-->
             <div class="col-md-12 mb-12">
                  <!--Card-->
                 <div class="card">
                      <!-- Card header -->
                     <div class="card-header text-center ">
                         >>
                     </div>
                      <!--Card content-->
                     <div class="card-body">

<div class="AreaTitulo">Maestra Indicaciones</div>

<?
require_once("../cAccesoBaseDato_SITD/ad_busqueda.php");
?>

<form action="../cLogicaNegocio_SITD/ln_actualiza_indicador.php" onSubmit="return validar(this)" method="post"  name="form1">
<input name="iCodIndicacion" type="hidden" id="iCodIndicacion" value="<? echo $Rs[iCodIndicacion]; ?>">
<input type="hidden" name="cIndicacion2" value="<?=$Rs[cIndicacion]?>">

            <fieldset id="tfa_DatosPersonales" class="fieldset"  >
            <legend class="legend">Datos de Indicaci&oacute;n</legend>
        <table border="0">
           <tr>
              <td width="114" height="49"></td>
              <td >Descripcion de Indicaci&oacute;n:</td>
              <td width="352" align="left"><input name="cIndicacion" type="text" id="cIndicacion"  maxlength="30" value="<? echo trim($Rs[cIndicacion]); ?>" size="40" class="FormPropertReg form-control">
              <?if($_GET[cIndicacion]!="") echo "Indicador existente"?></td>
           </tr>
           <tr>
              <td colspan="3" align="center">
              <button class="btn btn-primary"  type="submit" id="Actualizar Indicador" onMouseOver="this.style.cursor='hand'"> <b>Actualizar</b> <img src="images/page_refresh.png" width="17" height="17" border="0"> </button>
             ������
				<button class="btn btn-primary" type="button" onclick="window.open('iu_indicadores.php', '_self');" onMouseOver="this.style.cursor='hand'"> <b>Cancelar</b> <img src="images/icon_retornar.png" width="17" height="17" border="0"> </button>           </td>
        </table>
        </fieldset>

</form>  
</td>
		</tr>
		</table>
 
<div>		

<?include("includes/userinfo.php");?>

<?include("includes/pie.php");?>

</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>