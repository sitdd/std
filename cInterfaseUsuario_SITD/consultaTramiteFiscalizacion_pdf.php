<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: consultaTramiteFiscalizacion_pdf.php
SISTEMA: SISTEMA   DE TRÁMITE DOCUMENTARIO DIGITAL
OBJETIVO: Reporte General en PDF de los Documentos Internos Generales
PROPIETARIO: AGENCIA PERUANA DE COOPERACIÓN INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripción
------------------------------------------------------------------------
1.0  Larry Ortiz         05/09/2018      Creación del programa.
------------------------------------------------------------------------
*****************************************************************************************/
session_start();
ob_start();
//*************************************
include_once("../conexion/conexion.php");
?>
<page backtop="25mm" backbottom="15mm" backleft="10mm" backright="10mm">
	<page_header>
		<br>
		<table style="width: 1000px; border: solid 0px black;">
			<tr>
				<td style="text-align:left;	width: 20px"></td>
				<td style="text-align:left;	width: 980px">
					<img style="width: 220px" src="images/cab.jpg" alt="Logo">
				</td>
			</tr>
		</table>
        <br><br>
	</page_header>
	<page_footer>
		<table style="width: 100%; border: solid 0px black;">
			<tr>
                <td style="text-align: center;	width: 40%">
				<? 
				   $sqllog="select cNombresTrabajador, cApellidosTrabajador from tra_m_trabajadores where iCodTrabajador='$_SESSION[CODIGO_TRABAJADOR]' "; 
				   $rslog=mssql_query($sqllog,$cnx);
				   $Rslog=MsSQL_fetch_array($rslog);
				   echo $Rslog[cNombresTrabajador]." ".$Rslog[cApellidosTrabajador];
				?></td>
				<td style="text-align: right;	width: 60%">p�gina [[page_cu]]/[[page_nb]]</td>
			</tr>
		</table>
        <br>
        <br>
	</page_footer>
	
	
	<table style="width: 100%; border: solid 0px black;">
	<tr>
	<td style="text-align: left;	width: 50%"><span style="font-size: 15px; font-weight: bold">REPORTE - DOCUMENTOS TUPA DE FISCALIZACION</span></td>
	<td style="text-align: right;	width: 50%"><span style="font-size: 15px; font-weight: bold"><?=date("d-m-Y")?></span></td>
	</tr>
	</table>
	<br>
    <?
	//////////////
	 $totalreg=$total;
	 echo  $totalreg;
	 ////////////
	 $sqltupa=" SP_TUPA_LISTA_COMBO ";
		$rstupa=mssql_query($sqltupa,$cnx);
		
		$numrowstupa=MsSQL_num_rows($rstupa);
		
		//echo  $numrowstupa; echo "<br>";
		for ($i=1;$i<=$numrowstupa;$i++){
		   
		    while($Rstupa=MsSQL_fetch_array($rstupa)){
			  $sqltra=" SP_CONSULTA_FISCALIZACION_TUPA_LISTA '$fDesde', '$fHasta', '".$Rstupa[iCodTupa]."' ";
			  $rstra=mssql_query($sqltra,$cnx);
			  $registro_por_tupa=MsSQL_num_rows($rstra);
			  
			  if($registro_por_tupa<=500){
			    $x=ceil(0.1*$registro_por_tupa);
			  }
			  if($registro_por_tupa>500 and $registro_por_tupa<=2500 ){
			    $x=50;
			  }
			  if($registro_por_tupa>2500){
			    $x=ceil(sqrt($registro_por_tupa));
			  }
			 // echo $registro_por_tupa;  echo "-".$Rstupa[iCodTupa]." - "; echo $x;echo "<br>";
			$sql=" 
			 	SELECT TOP (".$x." ) 
				iCodTramite,				cCodificacion,		fFecRegistro,		fFecFinalizado,
				Tra_M_Tupa.iCodOficina,		cNomOFicina,		cNomTupa,nSilencio,	nDias,
				DATEDIFF(DAY, fFecRegistro, GETDATE()) as Proceso ,
				DATEDIFF(DAY, fFecRegistro, fFecFinalizado) as Proceso2 ,nFlgEstado 
    			FROM 
			Tra_M_Tramite 
			LEFT OUTER JOIN Tra_M_Tupa ON Tra_M_Tramite.iCodTupa=Tra_M_Tupa.iCodTupa 
			LEFT OUTER JOIN Tra_M_Oficinas ON Tra_M_Oficinas.iCodOficina=Tra_M_Tupa.iCodOficina 
    			WHERE 
			Tra_M_Tramite.nFlgTipoDoc=1 
			AND Tra_M_Tramite.iCodTupa= ".$Rstupa[iCodTupa]." and ( Tra_M_Tramite.iCodTupa!=1 and Tra_M_Tramite.iCodTupa!=5 and Tra_M_Tramite.iCodTupa!=15 and Tra_M_Tramite.iCodTupa!=17 and Tra_M_Tramite.iCodTupa!=18 )
			AND Tra_M_Tramite.iCodTupa IS NOT NULL 
			AND Tra_M_Tramite.iCodPaquete IS NULL 
			AND (Tra_M_Tramite.fFecRegistro  BETWEEN '".$fDesde."' AND '".$fHasta."') 
			AND (".$chain; 
    	    $rs=mssql_query($sql,$cnx);
	  //echo $sql;
	  
	  
	  if($registro_por_tupa!=0){
	  
	echo "SUBTOTAL DE REGISTROS PROCESADOS : ".$registro_por_tupa; 
	echo "<br>";
 	echo "SUBTOTAL DE REGISTROS SELECCIONADOS: ".$x;
	$totalproces=$totalproces+$x;
	$totalselect=$totalselect+$registro_por_tupa;
	
?>	
	<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="center">
		<thead>
			<tr>
            	<th style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Nro Documento</th>
      <th style="width: 14%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Fecha de Registro</th>
      <th style="width: 16%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Oficina</th>
      <th style="width: 20%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Procedimiento TUPA</th>
      <th style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N� de Dias Programados</th>
      <th style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">N� de Dias Ejecutados</th>                
      <th style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Estado</th>
      <th style="width: 10%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Resultado</th>
         	</tr>
		</thead>
		<tbody>
        <?
        while ($Rs=MsSQL_fetch_array($rs)){
        		if ($color == "#DDEDFF"){
			  			$color = "#F9F9F9";
	    			}else{
			  			$color = "#DDEDFF";
	    			}
	    			if ($color == ""){
			  			$color = "#F9F9F9";
	    			}	
		?>
	<td style="width:780px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><?=$Rs[cCodificacion]?></td>
    <td style="width:780px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
	 <?	
		echo "<div style=color:#0154AF>".date("d-m-Y", strtotime($Rs[fFecRegistro]))."</div>";
      	echo "<div style=color:#0154AF;font-size:10px>".date("G:i", strtotime($Rs[fFecRegistro]))."</div>";
	 ?> </td>
    <td style="width:780px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><?=$Rs[cNomOFicina]?></td>
    <td style="width:780px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><?=$Rs[cNomTupa]?></td>
    <td style="width:780px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;"><?=$Rs[nDias]?></td>
    <td style="width:780px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
	<? 
	        if($Rs[nFlgEstado]==1){	echo $Rs[Proceso]; }
		  	else if($Rs[nFlgEstado]==2){ echo $Rs[Proceso]; }
			else if($Rs[nFlgEstado]==3){ 
				$sqlFin="  SP_CONSULTA_FISCALIZACION_FIN '$Rs[iCodTramite]'";
				$rsFin=mssql_query($sqlFin,$cnx);
				$RsFin=MsSQL_fetch_array($rsFin);
				$fFecFinalizar=date("Ymd",strtotime($RsFin[fFecFinalizar]));
				$sqlDate=" SP_CONSULTA_FISCALIZACION_REPORTE '$Rs[iCodTramite]','$fFecFinalizar'";
				$rsDate=mssql_query($sqlDate,$cnx);
				$RsDate=MsSQL_fetch_array($rsDate);
					echo $RsDate[proceso2]; } 
			 } 
	?></td>    
    <td style="width:780px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
				 <?
                    if($Rs[nFlgEstado]==1){
					echo "<div style='color:#005E2F'>PENDIENTE</div>";
					}
					else if($Rs[nFlgEstado]==2){
					echo "<div style='color:#0154AF'>EN PROCESO</div>";
					}
					else if($Rs[nFlgEstado]==3){
					echo "FINALIZADO";
						$sqlFinTxt="SELECT * FROM Tra_M_Tramite_Movimientos WHERE nEstadoMovimiento=5 AND iCodTramite='$Rs[iCodTramite]'";
			            $rsFinTxt=mssql_query($sqlFinTxt,$cnx);
			            $RsFinTxt=MsSQL_fetch_array($rsFinTxt);
			            echo "<div style=color:#0154AF>".date("d-m-Y", strtotime($RsFinTxt[fFecFinalizar]))."</div>";
						echo "<div style=color:#0154AF;font-size:10px>".date("G:i", strtotime($RsFinTxt[fFecFinalizar]))."</div>";	
					}
				?></td>
    <td style="width:780px;text-align:left;border: solid 1px #6F6F6F;font-size:10px;">
   		<?
    		if($Rs[Proceso] > $Rs[nDias] and $Rs[nSilencio]==1 and $Rs[nFlgEstado]!=3){ 
	  		  echo "<div style='color:#950000'>VENCIDO</div>"; 
					echo "<div style='color:#950000'>SAP</div>";
				}
				else if($Rs[Proceso] > $Rs[nDias] and $Rs[nSilencio]==0 and $Rs[nFlgEstado]!=3){
					echo "<div style='color:#950000'>VENCIDO</div>"; 
					echo "<div style='color:#950000'>SAN</div>"; 
				}
				else if($RsDate[proceso2] > $Rs[nDias] and $Rs[nSilencio]==1  and $Rs[nFlgEstado]==3){ 
	  		  echo "<div style='color:#950000'>VENCIDO</div>"; 
					echo "<div style='color:#950000'>SAP</div>";
				}
				else if($RsDate[proceso2] > $Rs[nDias] and $Rs[nSilencio]==0  and $Rs[nFlgEstado]==3){
					echo "<div style='color:#950000'>VENCIDO</div>"; 
					echo "<div style='color:#950000'>SAN</div>"; 
				}
				?> </td>    
  </tr>
<? }?>
	  </tbody>
    </table>  
 <?
		    }
	else {
			$totalproces==0;
			$totalselect==0;	
	}
		  }  
		}
	 echo "TOTAL DE REGISTROS SELECCIONADOS: ".$totalselect;
	 echo "<br>";
	 echo "TOTAL DE REGISTROS PROCESADOS : ".$totalproces;	  
?>   
</page>

<?
//*************************************


	$content = ob_get_clean();  set_time_limit(0);     ini_set('memory_limit', '640M');

	// conversion HTML => PDF
	require_once(dirname(__FILE__).'/html2pdf/html2pdf.class.php');
	try
	{
		$html2pdf = new HTML2PDF('P','A4', 'es', false, 'UTF-8', 3);
		$html2pdf->pdf->SetDisplayMode('fullpage');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output('exemple03.pdf');
	}
	catch(HTML2PDF_exception $e) { echo $e; }
?>   
