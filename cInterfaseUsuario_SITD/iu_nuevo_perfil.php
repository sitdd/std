<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: iu_nuevo_perfil.php
SISTEMA: SISTEMA  DE TR�MITE DOCUMENTARIO DIGITAL
OBJETIVO: Mantenimiento de la Tabla Maestra de Perfiles para el Perfil Administrador
          -> Crear Registro de Perfil
PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver      Autor             Fecha        Descripci�n
------------------------------------------------------------------------
1.0   APCI       03/08/2018   Creaci�n del programa.
 
------------------------------------------------------------------------
*****************************************************************************************/
session_start();
If($_SESSION['CODIGO_TRABAJADOR']!=""){
include_once("../conexion/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />

</head>
<body>
<?include("includes/menu.php");?>
<!--Main layout-->
<main class="mx-lg-5">
    <div class="container-fluid">

        <!--Grid row-->
        <div class="row wow fadeIn justify-content-center">

            <!--Grid column-->
            <div class="col-11 col-sm-8 col-md-6 col-xl-4">

                <!--Card-->
                <div class="card">

                    <!-- Card header -->
                    <div class="card-header text-center ">CREAR NUEVO PERFIL</div>

                    <!--Card content-->
                    <div class="card-body">
                        <form action="../cLogicaNegocio_SITD/ln_nuevo_perfil.php"  onSubmit="return validar(this)" method="post"  name="form1">
                            <div class="form-row justify-content-center">
                                <div class="col-12">
                                    <div class="md-form">
                                        <input type="text" id="cDescPerfil" name="cDescPerfil" class="FormPropertReg form-control" value="<?=$_GET[cDescPerfil]?>" >
                                        <?if($_GET[cDescPerfil]!="") echo "Perfil existente"?>
                                        <label for="cDescPerfil">Nombre del Perfil</label>
                                    </div>
                                </div>

                                <div class="col- mx-3 mb-3">
                                    <button class="botenviar"  type="submit" id="Insert Perfil" onMouseOver="this.style.cursor='hand'">Crear</button>
                                </div>
                                <div class="col- mx-3">
                                    <button class="botenviar" type="button" onclick="window.open('iu_perfil.php', '_self');" onMouseOver="this.style.cursor='hand'">Cancelar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--/.Card-->

                </div>
                <!--Grid column-->
            </div>
            <!--Grid column-->
        </div>
        <!--Grid column-->
</main>
<!--Main layout-->
<?include("includes/userinfo.php");?>


<?include("includes/pie.php");?>
<script>
    function validar(f) {
        var error = "Por favor, antes de crear complete:\n\n";
        var a = "";
        if (f.txtperfil.value == "") {
            a += " Ingrese N�mero de Perfil";
            alert(error + a);
        }
        else if (f.textdescricion_perfil.value == "") {
            a += " Ingrese Descripcion de perfil";
            alert(error + a);
        }

        return (a == "");

    }
</script>
</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>

