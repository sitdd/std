<?
session_start();
ob_start();
/**************************************************************************************
NOMBRE DEL PROGRAMA: pendientesFinalizadosExcel.php
SISTEMA: SISTEMA  DE TRÁMITE DOCUMENTARIO DIGITAL
OBJETIVO: Reporte en EXCEL a partir de una lista de pendientes.
PROPIETARIO: AGENCIA PERUANA DE COOPERACIÓN INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripción
------------------------------------------------------------------------
1.0   APCI    05/09/2018      Creación del programa.
------------------------------------------------------------------------
*****************************************************************************************/
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=ReporteFinalizados.xls");
	
	$anho = date("Y");
	$datomes = date("m");
	$datomes = $datomes*1;
	$datodia = date("d");
	$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre");
	
	echo "<table width=780 border=0><tr><td align=center colspan=7>";
	echo "<H3>REPORTE - FINALIZADOS </H3>";
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=right colspan=7>";
	echo "SITD, ".$datodia." ".$meses[$datomes].' del '.$anho;
	echo " ";
	
	echo "<table width=780 border=1><tr>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Dia</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Mes</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Ano</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Tipo</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Trabajador de Registro</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Tipo Documento</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> N&ordm; Tramite</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Responsable</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Delegado</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Trab. Finaliza</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Asunto</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Nombre / Razon Social</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Remitente</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Documento</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Derivado</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Recepcion</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Delegado</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Finalizado</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Comentario de Finalzado</td>";
	echo "</tr>";	
	
	include_once("../conexion/conexion.php");
		if($_GET[fDesde]!=""){ $fDesde=date("Ymd", strtotime($_GET[fDesde])); }
			    if($_GET[fHasta]!=""){
				$fHasta=date("Y-m-d", strtotime($_GET[fHasta]));

				function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    				  $date_r = getdate(strtotime($date));
    				  $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),($date_r["year"]+$yy)));
    				  return $date_result;
				}
				$fHasta=dateadd($fHasta,1,0,0,0,0,0); // + 1 dia
				}

$sqlTra.= " SP_BANDEJA_FINALIZADOS '$campo' ,'$_GET[Entrada]','$_GET[Interno]','$fDesde','$fHasta', '$_GET[cCodificacion]', '$_GET[cAsunto]', '$_GET[iCodOficina]','$_GET[cCodTipoDoc]','$_GET[iCodTema]' ,   '$orden' ";
  $rsTra=mssql_query($sqlTra,$cnx);
  while ($RsTra=MsSQL_fetch_array($rsTra)){
  	echo "<tr>";
		echo "<td valign=top>";
		//echo date("d", strtotime($RsTra[fFecDocumento]));
		echo date("d", strtotime(substr($RsTra[fFecDocumento], 0, -6)));
		echo "</td>";
		
		echo "<td valign=top>";
		//echo date("m", strtotime($RsTra[fFecDocumento]));
		echo date("m", strtotime(substr($RsTra[fFecDocumento], 0, -6)));
		echo "</td>";
		
		echo "<td valign=top>";
		//echo date("Y", strtotime($RsTra[fFecDocumento]));
		echo date("Y", strtotime(substr($RsTra[fFecDocumento], 0, -6)));
		echo "</td>";
		
		echo "<td valign=top>";
		if($RsTra[nFlgTipoDoc]==1)
		{ echo "Entrada";
		}
		else
		{
		  echo "Interno";	
		}
		echo "</td>";
		
		echo "<td valign=top>";
		 $rsReg=mssql_query("SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='".$RsTra[iCodTrabajadorRegistro]."'",$cnx);
          $RsReg=MsSQL_fetch_array($rsReg);
          echo $RsReg["cApellidosTrabajador"].", ".$RsReg["cNombresTrabajador"];
			mssql_free_result($rsReg);		
		echo "</td>";
		
		echo "<td valign=top>".$RsTra[cDescTipoDoc]."</td>";
		
		echo "<td valign=top align=left>".$RsTra[cCodificacion]."</td>";
		
		echo "<td valign=top>";
          $rsResp=mssql_query("SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsTra[iCodTrabajadorDerivar]'",$cnx);
          $RsResp=MsSQL_fetch_array($rsResp);
          echo trim($RsResp["cApellidosTrabajador"]).", ".$RsResp["cNombresTrabajador"];
					mssql_free_result($rsResp);
		echo "</td>";
		
		echo "<td valign=top>";
		
  					$rsDelg=mssql_query("SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsTra[iCodTrabajadorDelegado]'",$cnx);
          	$RsDelg=MsSQL_fetch_array($rsDelg);
          	echo trim($RsDelg["cApellidosTrabajador"]).", ".$RsDelg["cNombresTrabajador"];
						mssql_free_result($rsDelg);
				
		echo "</td>";	
		echo "<td valign=top>";
		
  					$rsFin=mssql_query("SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsTra[iCodTrabajadorFinalizar]'",$cnx);
          	$RsFin=MsSQL_fetch_array($rsFin);
          	echo trim($RsFin["cApellidosTrabajador"]).", ".$RsFin["cNombresTrabajador"];
						mssql_free_result($rsFin);
				
		echo "</td>";		
		
		echo "<td valign=top>".$RsTra[cAsunto]."</td>";
		echo "<td valign=top>";
          $rsRem=mssql_query("SELECT * FROM Tra_M_Remitente WHERE iCodRemitente='$RsTra[iCodRemitente]'",$cnx);
          $RsRem=MsSQL_fetch_array($rsRem);
          echo $RsRem["cNombre"];
					mssql_free_result($rsRem);
		echo "</td>";
		echo "<td valign=top>".$RsTra[cNomRemite]."</td>";
		echo "<td valign=top><br>";
		//echo date("d-m-Y G:i", strtotime($RsTra[fFecDocumento]));
		echo date("d-m-Y G:i:s", strtotime(substr($RsTra[fFecDocumento], 0, -6)));
		echo "</td>";
		echo "<td valign=top><br>";
		//echo date("d-m-Y G:i", strtotime($RsTra[fFecDerivar]));
		echo date("d-m-Y G:i:s", strtotime(substr($RsTra[fFecDerivar], 0, -6)));
		echo "</td>";
		echo "<td valign=top>";
        	if($RsTra[fFecRecepcion]==""){
        			echo "<div style=color:#ff0000;text-align:center>sin aceptar</div>";
        	}Else{
        			//echo date("d-m-Y G:i", strtotime($RsTra[fFecRecepcion]));
					echo date("d-m-Y G:i:s", strtotime(substr($RsTra[fFecRecepcion], 0, -6)));
        	}
		echo "</td>";
		echo "<td valign=top>";
        	if($RsTra[fFecDelegado]==""){
        			echo "";
        	}Else{
        			//echo date("d-m-Y G:i", strtotime($RsTra[fFecDelegado]));
					echo date("d-m-Y G:i:s", strtotime(substr($RsTra[fFecDelegado], 0, -6)));
        	}
		echo "</td>";
		echo "<td valign=top>";
           			//echo date("d-m-Y G:i", strtotime($RsTra[fFecFinalizar]));
					echo date("d-m-Y G:i:s", strtotime(substr($RsTra[fFecFinalizar], 0, -6)));
		echo "</td>";		
		echo "<td valign=top>".$RsTra[cObservacionesFinalizar]."</td>";
		
		echo "</tr>";
}
		echo "</table>"
?>