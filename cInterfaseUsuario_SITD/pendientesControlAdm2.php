<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: PendienteData.php
SISTEMA: SISTEMA  DE TR�MITE DOCUMENTARIO DIGITAL
OBJETIVO: Reporte en EXCEL a partir de una lista de pendientes.
PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripci�n
------------------------------------------------------------------------
1.0   APCI    05/09/2018      Creaci�n del programa.
------------------------------------------------------------------------
*****************************************************************************************/
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=ReportePendientes.xls");
	
	$anho = date("Y");
	$datomes = date("m");
	$datomes = $datomes*1;
	$datodia = date("d");
	$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre");
	
	echo "<table width=780 border=0><tr><td align=center colspan=7>";
	echo "<H3>REPORTE - PENDIENTES</H3>";
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=right colspan=7>";
	echo "SITD, ".$datodia." ".$meses[$datomes].' del '.$anho;
	echo " ";
	
	echo "<table width=780 border=1><tr>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Tipo</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Trabajador de Registro</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Tipo Documento</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> N&ordm; Doc.</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Oficina Destino</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Responsable</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Delegado</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Asunto</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Institucion</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Remitente</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Documento</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Derivado</td>";
	
	echo "</tr>";	
	
	include_once("../conexion/conexion.php");
		if($_GET[fDesde]!=""){ $fDesde=date("Y-m-d", strtotime($_GET[fDesde])); }
			    if($_GET[fHasta]!=""){
				$fHasta=date("Y-m-d", strtotime($_GET[fHasta]));

				function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    				  $date_r = getdate(strtotime($date));
    				  $date_result = date("Y-m-d", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),($date_r["year"]+$yy)));
    				  return $date_result;
				}
				$fHasta=dateadd($fHasta,1,0,0,0,0,0); // + 1 dia
				}
	$sqlTra="SELECT * FROM Tra_M_Tramite_Movimientos		z 
WHERE Z.iCodTramite IN 
(		 
		 SELECT  iCodTramite
		 FROM [db_pcm_gob_pe_std].[dbo].[Tra_M_Tramite_Movimientos] a
		 WHERE 
				a.iCodOficinaOrigen = 303
			AND a.fFecDerivar > '2012-07-23 00:00:00.000' and a.fFecDerivar <= '2013-04-05 00:00:00.000'
			--AND a.nEstadoMovimiento!=5
			AND a.iCodTramite IN 
		 (	SELECT  d.iCodTramite 
			FROM Tra_M_Tramite_Movimientos d  
			WHERE 
				d.iCodOficinaDerivar =	303 
			AND d.nEstadoMovimiento= 2 
			AND d.fFecDerivar	>	'2012-07-23 00:00:00.000' and d.fFecDerivar <= '2013-04-05 00:00:00.000'
		  )
			AND a.iCodMovimiento IN 
			(	SELECT  b.iCodMovimiento FROM Tra_M_Tramite_Movimientos b 
				WHERE /*iCodOficinaDerivar!=12 and */  
			--	b.nEstadoMovimiento!=5  
			--AND cFlgTipoMovimiento!=5 AND 
			b.iCodTramite=a.iCodTramite 
			AND fFecMovimiento = 
			   (	SELECT MAX(fFecMovimiento) 
					FROM Tra_M_Tramite_Movimientos e 
					WHERE e.iCodTramite=b.iCodTramite 
				--		AND  b.nEstadoMovimiento!=5
				)
			)
)
	AND z.fFecMovimiento	= 
			   (	SELECT MAX(w.fFecMovimiento) 
					FROM Tra_M_Tramite_Movimientos w 
					WHERE w.iCodTramite=z.iCodTramite 
					--	AND  z.nEstadoMovimiento!=5
				)
	AND z.iCodOficinaDerivar != 303	";
   $rsTra=mssql_query($sqlTra,$cnx);
  while ($RsTra=MsSQL_fetch_array($rsTra)){
  	echo "<tr>";			
		echo "<td valign=top>";
		if($RsTra[nFlgTipoDoc]==1)
		{ echo "Entrada";
		}
		else
		{
		  echo "Interno";	
		}
		echo "</td>";
		
		echo "<td valign=top>";
		 $rsReg=mssql_query("SELECT cApellidosTrabajador,cNombresTrabajador FROM Tra_M_Trabajadores WHERE iCodTrabajador='".$RsTra[iCodTrabajadorRegistro]."'",$cnx);
          $RsReg=MsSQL_fetch_array($rsReg);
          echo $RsReg["cApellidosTrabajador"].", ".$RsReg["cNombresTrabajador"];
			mssql_free_result($rsReg);		
		echo "</td>";
		
			$rsDoc=mssql_query("SELECT cCodTipoDoc,cCodificacion,cAsunto,iCodRemitente,cNomRemite,fFecDocumento FROM Tra_M_Tramite WHERE iCodTramite='".$RsTra[iCodTramite]."'",$cnx);
			$RsDoc=MsSQL_fetch_array($rsDoc);          
		echo "<td valign=top>";
		$rsTip=mssql_query("SELECT cDescTipoDoc FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$RsDoc[cCodTipoDoc]'",$cnx);
          $RsTip=MsSQL_fetch_array($rsTip);
          echo $RsTip["cDescTipoDoc"];
					mssql_free_result($rsTip);
		echo "</td>";
		
		echo "<td valign=top>".$RsDoc[cCodificacion]."</td>";
		
		echo "<td valign=top>";
           $sqlSig="SP_OFICINA_LISTA_AR '$RsTra[iCodOficinaDerivar]'";
			  $rsSig=mssql_query($sqlSig,$cnx);
              $RsSig=MsSQL_fetch_array($rsSig);
                echo $RsSig["cNomOficina"];
		echo "</td>";
		
		echo "<td valign=top>";
          $rsResp=mssql_query("SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsTra[iCodTrabajadorDerivar]'",$cnx);
          $RsResp=MsSQL_fetch_array($rsResp);
          echo trim($RsResp["cApellidosTrabajador"]).", ".$RsResp["cNombresTrabajador"];
					mssql_free_result($rsResp);
		echo "</td>";
		
		echo "<td valign=top>";
		
  					$rsDelg=mssql_query("SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsTra[iCodTrabajadorDelegado]'",$cnx);
          	$RsDelg=MsSQL_fetch_array($rsDelg);
          	echo trim($RsDelg["cApellidosTrabajador"]).", ".$RsDelg["cNombresTrabajador"];
						mssql_free_result($rsDelg);
				
		echo "</td>";		
		
		echo "<td valign=top>".$RsDoc[cAsunto]."</td>";
		echo "<td valign=top>";
          $rsRem=mssql_query("SELECT cNombre FROM Tra_M_Remitente WHERE iCodRemitente='$RsDoc[iCodRemitente]'",$cnx);
          $RsRem=MsSQL_fetch_array($rsRem);
          echo $RsRem["cNombre"];
					mssql_free_result($rsRem);
		echo "</td>";
		echo "<td valign=top>".$RsDoc[cNomRemite]."</td>";
		echo "<td valign=top><br>";
		echo date("d-m-Y G:i", strtotime($RsDoc[fFecDocumento]));		
		echo "</td>";
		echo "<td valign=top><br>";
		echo date("d-m-Y G:i", strtotime($RsTra[fFecDerivar]));		
		echo "</td>";
		
		
		
		
		
		echo "</tr>";
}
		echo "</table>"
?>