<?php
session_start();
if ($_SESSION['CODIGO_TRABAJADOR'] != ""){
    include_once("../conexion/conexion.php");
    $pag = $_GET['pag'];
    ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <?include("includes/head.php");?>
    </head>
    <body  >

    <?include("includes/menu.php");?>

    <!--Main layout-->
    <main class="mx-lg-5">
        <div class="container-fluid">          <!--Grid row-->
            <div class="row wow fadeIn">              <!--Grid column-->
                <div class="col-md-12 mb-12">                  <!--Card-->
                    <div class="card">                      <!-- Card header -->
                        <div class="card-header text-center ">  Mantenimiento >> M. OFICINAS       </div>                      <!--Card content-->
                        <div class="card-body">
                              <form name="form1" method="GET" action="<?=$PHP_SELF?>">
                                              Oficina: <input name="cNomOficina" class="FormPropertReg" type="text" value="<?=$_GET[cNomOficina]?>" size="40" />
                                              Sigla de Oficina: <input name="cSiglaOficina" class="FormPropertReg"s type="text" value="<?=$_GET[cSiglaOficina]?>" />
                                              Estado: <select name="iFlgEstado"  class="FormPropertReg mdb-select colorful-select dropdown-primary"
                                                                                    searchable="Buscar aqui.." id="iFlgEstado">
                                                          <option value="%" >Seleccione:</option>
                                                          <option value="0" <? if($_GET[iFlgEstado]=="0"){echo selected;} ?>>Inactivo</option>
                                                          <option value="1" <? if($_GET[iFlgEstado]=="1"){echo selected;} ?>>Activo</option>
                                                     </select>
                                  <button class="btn btn-primary" onclick="Buscar();" onMouseOver="this.style.cursor='hand'">
                                      <b>Buscar</b> <img src="images/icon_buscar.png" width="17" height="17" border="0">
                                  </button>
                                    <button class="btn btn-primary" onclick="window.open('<?=$PHP_SELF?>', '_self');" onMouseOver="this.style.cursor='hand'">
                                        <b>Restablecer</b> <img src="images/icon_clear.png" width="17" height="17" border="0">
                                    </button>

                                  <button class="btn btn-primary" onClick="window.open('iu_oficinas_xls.php?cNomOficina=<?=$_GET[cNomOficina]?>&cSiglaOficina=<?=$_GET[cSiglaOficina]?>&cTipoUbicacion=<?=$_GET[cTipoUbicacion]?>&iFlgEstado=<?=$_GET[iFlgEstado]?>&orden=<?=$orden?>&campo=<?=$campo?>&traRep=<?=$_SESSION['CODIGO_TRABAJADOR']?>', '_blank');" onMouseOver="this.style.cursor='hand'">
                                      <b>a Excel</b> <img src="images/icon_excel.png" width="17" height="17" border="0">
                                  </button>
                                  <button class="btn btn-primary" onClick="window.open('iu_oficinas_pdf.php?cNomOficina=<?=$_GET[cNomOficina]?>&cSiglaOficina=<?=$_GET[cSiglaOficina]?>&cTipoUbicacion=<?=$_GET[cTipoUbicacion]?>&iFlgEstado=<?=$_GET[iFlgEstado]?>&orden=<?=$orden?>&campo=<?=$campo?>', '_blank');" onMouseOver="this.style.cursor='hand'">
                                      <b>a Pdf</b> <img src="images/icon_pdf.png" width="17" height="17" border="0">
                                  </button>
                                  <a href='iu_nueva_oficina.php' class="btn btn-primary"><b>Nueva Oficina</b></a>
                               </form>

                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="table table-hover" border="1" align="center">

                                    <?
                                    function paginar($actual, $total, $por_pagina, $enlace, $maxpags=0) {
                                        $total_paginas = ceil($total/$por_pagina);
                                        $anterior = $actual - 1;
                                        $posterior = $actual + 1;
                                        $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
                                        $maximo = $maxpags ? min($total_paginas, $actual+floor($maxpags/2)): $total_paginas;
                                        if ($actual>1)

                                    $texto = "<nav aria-label=\"Page navigation example\"><ul class=\"pagination justify-content-end\">
                                               <li class=\"page-item\"><a class=\"page-link\" href=\"$enlace$anterior\">Anterior</a></li> ";
                                    else
                                    $texto = "<nav aria-label=\"Page navigation example\"><ul class=\"pagination justify-content-end\">
                                              <li class=\"page-item disabled\"><a class=\"page-link\" href=\"#\">Anterior</a></li> ";

                                    if ($minimo!=1) $texto.= "... ";

                                    for ($i=$minimo; $i<$actual; $i++)
                                        $texto .= "  <li class=\"page-item\"><a class=\"page-link\" href=\"$enlace$i\">$i</a></li> ";

                                    $texto .= "<li class=\"page-item active\">
                                                 <a class=\"page-link\" href=\"#\">$actual<span class=\"sr-only\">(current)</span></a></li>";

                                    for ($i=$actual+1; $i<=$maximo; $i++)
                                        $texto .= "<li class=\"page-item\"><a class=\"page-link\" href=\"$enlace$i\">$i</a></li> ";

                                    if ($maximo!=$total_paginas) $texto.= "... ";

                                    if ($actual<$total_paginas)
                                        $texto .= "<li class=\"page-item\"><a class=\"page-link\" href=\"$enlace$posterior\">Siguiente</a></li></ul></nav>";
                                    else
                                        $texto .= "<li class=\"page-item disabled\"><a class=\"page-link\" href=\"$enlace$posterior\">Siguiente</a></li></ul></nav>";
                                    return $texto;
                                    }


                                    if (!isset($pag)) $pag = 1; // Por defecto, pagina 1
                                    $tampag = 5;
                                    $reg1 = ($pag-1) * $tampag;

                                    // ordenamiento
                                    if($_GET[campo]==""){
                                        $campo="Oficina";
                                    }Else{
                                        $campo=$_GET[campo];
                                    }

                                    if($_GET[orden]==""){
                                        $orden="ASC";
                                    }Else{
                                        $orden=$_GET[orden];
                                    }

                                    //invertir orden
                                    if($orden=="DESC") $cambio="ASC";
                                    if($orden=="ASC") $cambio="DESC";


                                    $sql=" SP_OFICINA_LISTA '%$_GET[cNomOficina]%','%$_GET[cSiglaOficina]%','$_GET[iFlgEstado]' ,'".$orden."' , '".$campo."' ";
                                    //print_r("XXX==>"+$sql);

                                    $rs=mssql_query($sql,$cnx);
                                    ///////////
                                    $total = mssql_num_rows($rs);

                                    ?>
                                    <tr>
                                        <td width="473" class="headCellColum">
                                            <a style=" text-decoration:<?if($campo=="Oficina"){ echo "underline"; }else{ echo "none";}?>"
                                               href="<?=$_SERVER['PHP_SELF']?>?campo=Oficina&orden=<?=$cambio?>&cNomOficina=<?=$_GET[cNomOficina]?>&cSiglaOficina=<?=$_GET[cSiglaOficina]?>&cTipoUbicacion=<?=$_GET[cTipoUbicacion]?>&iFlgEstado=<?=$_GET[iFlgEstado]?>">Oficina
                                            </a>
                                        </td>
                                        <td width="80" class="headCellColum">
                                            <a style=" text-decoration:<?if($campo=="Sigla"){ echo "underline"; }else{ echo "none";}?>"
                                               href="<?=$_SERVER['PHP_SELF']?>?campo=Sigla&orden=<?=$cambio?>&cNomOficina=<?=$_GET[cNomOficina]?>&cSiglaOficina=<?=$_GET[cSiglaOficina]?>&cTipoUbicacion=<?=$_GET[cTipoUbicacion]?>&iFlgEstado=<?=$_GET[iFlgEstado]?>">Sigla</a>
                                        </td>
                                        <td width="112" class="headCellColum">Opciones</td>
                                    </tr>
                                    <?
                                    $numrows=MsSQL_num_rows($rs);
                                    if($numrows==0){
                                        echo "NO SE ENCONTRARON REGISTROS<br>";
                                        echo "TOTAL DE REGISTROS : ".$numrows;
                                    }else{
                                        echo "<div align=\"center\">TOTAL DE REGISTROS : $numrows  </div><br>";

///	//////
                                        for ($i=$reg1; $i<min($reg1+$tampag, $total); $i++) {
                                            mssql_data_seek($rs, $i);
                                            $Rs = mssql_fetch_array($rs);
//////////////////
//while ($Rs=MsSQL_fetch_array($rs)){
                                            if ($color == "#CEE7FF"){
                                                $color = "#F9F9F9";
                                            }else{
                                                $color = "#CEE7FF";
                                            }
                                            if ($color == ""){
                                                $color = "#F9F9F9";
                                            }
                                            ?>
                                            <tr bgcolor="<?=$color?>">
                                                <td align="left"><? echo utf8_encode($Rs[cNomOficina]);?></td>
                                                <td align="left"><? echo $Rs[cSiglaOficina];?></td>
                                                <!--  <td align="left">       <? echo $Rs[cNomUbicacion];?>       </td> -->
                                                <td>

                                                    <a href="../cLogicaNegocio_SITD/ln_elimina_oficina.php?id=<? echo $Rs[iCodOficina];?>&cNomOficina=<?=$_GET[cNomOficina]?>&cSiglaOficina=<?=$_GET[cSiglaOficina]?>&cTipoUbicacion=<?=$_GET[cTipoUbicacion]?>&iFlgEstado=<?=$_GET[iFlgEstado]?>&pag=<?=$pag?>" onClick='return ConfirmarBorrado();' ><i class="far fa-trash-alt"></i></a>
                                                    <a href="../cInterfaseUsuario_SITD/iu_actualiza_oficina.php?cod=<? echo $Rs[iCodOficina];?>&sw=3&se=<?=$Rs[iCodUbicacion]?>&cNomOficina=<?=$_GET[cNomOficina]?>&cSiglaOficina=<?=$_GET[cSiglaOficina]?>&cTipoUbicacion=<?=$_GET[cTipoUbicacion]?>&iFlgEstado=<?=$_GET[iFlgEstado]?>&pag=<?=$pag?>"><i class="fas fa-edit"></i></a></td>
                                            </tr>

                                            <?
                                        }
                                    }
                                    ?>
                                </table>
                                <? echo paginar($pag, $total, $tampag, "iu_oficinas.php?cNomOficina=".$_GET[cNomOficina]."&cSiglaOficina=".$_GET[cSiglaOficina]."&cTipoUbicacion=".$_GET[cTipoUbicacion]."&pag=");?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>


<?include("includes/userinfo.php");?>

<?include("includes/pie.php");?>

    <script>
        $('.mdb-select').material_select();
        function ConfirmarBorrado()
        {
            if (confirm("Esta seguro de eliminar el registro?")){
                return true;
            }else{
                return false;
            }
        }

        function Buscar()
        {
            document.form1.action="<?=$PHP_SELF?>";
            document.form1.submit();
        }

    </script>

    </body>
    </html>

    <?
}Else{
    header("Location: ../index.php?alter=5");
}
?>