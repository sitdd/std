<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
If($_SESSION['CODIGO_TRABAJADOR']!=""){
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
<style>
        .wrapper {
            width: auto%;
            position: absolute;
            z-index: 100;
            height: auto;
        }
        #sidebar {
            display: none;
        }
        #tablaResutado{
            width: 100%;
        }
        #sidebar.active {
            display: flex;
            width: 95%;
        }
        #sidebar label{
            font-size: 0.8rem!important;
            margin-bottom: 0!important;
        }
        .info-end ,.page-link{
            font-size: 0.8rem!important;
        }
        @media (min-width: 576px) {
            #sidebar.active {
                width: 80%;
            }
        }
        @media (min-width: 768px) {
            #sidebar.active {
                width: 60%;
            }
        }
        @media (min-width: 992px) {
            #sidebar.active {
                width: 60%;
            }
            #sidebarCollapse{
                margin-left: -35px!important;
            }
            .info-end ,.page-link{
                font-size: 1rem!important;
            }
        }
        @media (min-width: 1200px) {
            #sidebar.active {
                width: 38%;
            }
        }

        .dropdown-content li>a, .dropdown-content li>span {
            font-size: 0.8rem!important;
        }
        .select-wrapper .search-wrap {
            padding-top: 0rem!important;
            margin: 0 0.2rem!important;
        }
        .select-wrapper input.select-dropdown {
            font-size: 0.9rem!important;
        }
        .md-form{
            margin-bottom: 0.5rem!important;
        }

    </style>
</head>
<body>
<?include("includes/menu.php");?>

<!--Main layout-->
<main class="mx-lg-5">
     <div class="container-fluid">
          <!--Grid row-->
         <div class="row wow fadeIn">
              <!--Grid column-->
             <div class="col-md-12 mb-12">
                  <!--Card-->
                 <div class="card">
                      <!-- Card header -->
                     <div class="card-header text-center ">
                         Bandeja >> Finalizados
                     </div>
                      <!--Card content-->
                     <div class="card-body  d-flex px-4 px-lg-5">
                         <div class="wrapper">
                             <nav class="navbar-expand py-0">
                                 <button type="button" id="sidebarCollapse" class="botenviar float-left" style="padding: 0rem 8px!important; border: none; margin-left: -18px; border-radius: 10px;">
                                     <i class="fas fa-align-right"></i>
                                 </button>
                             </nav>
                             <!-- Sidebar -->
                             <nav id="sidebar" class="py-0">
                                 <div class="card">
                                     <div class="card-header">Criterios de Búsqueda</div>
                                     <div class="card-body">
                                         <form name="frmConsulta" method="GET">
                                             Documentos:
                                             <input type="checkbox" name="Entrada" value="1" <?if($_GET[Entrada]==1) echo "checked"?> onclick="activaEntrada();">
                                             Entrada  &nbsp;&nbsp;&nbsp;
                                             <input type="checkbox" name="Interno" value="2" <?if($_GET[Interno]==2) echo "checked"?> onclick="activaInterno();">Internos
                                             Desde:
                                             <input type="text" readonly name="fDesde" value="<?=$_GET[fDesde]?>" style="width:75px" class="FormPropertReg form-control">
                                             Hasta:&nbsp;
                                             <input type="text" readonly name="fHasta" value="<?=$_GET[fHasta]?>" style="width:75px" class="FormPropertReg form-control">
                                             N&ordm; Documento:
                                             <input type="txt" name="cCodificacion" value="<?=$_GET[cCodificacion]?>" size="28" class="FormPropertReg form-control">
                                             Asunto:
                                             <input type="txt" name="cAsunto" value="<?=$_GET[cAsunto]?>" size="65" class="FormPropertReg form-control">
                                             Tipo Documento:
                                             <select name="cCodTipoDoc" class="FormPropertReg form-control" style="width:180px" />
                                                     <option value="">Seleccione:</option>
                                                     <?
                                                     include_once("../conexion/conexion.php");
                                                     $sqlTipo="SELECT * FROM Tra_M_Tipo_Documento ";
                                                     $sqlTipo.="ORDER BY cDescTipoDoc ASC";
                                                     $rsTipo=mssql_query($sqlTipo,$cnx);
                                                     while ($RsTipo=MsSQL_fetch_array($rsTipo)){
                                                         if($RsTipo["cCodTipoDoc"]==$_GET[cCodTipoDoc]){
                                                             $selecTipo="selected";
                                                         }Else{
                                                             $selecTipo="";
                                                         }
                                                         echo "<option value=".$RsTipo["cCodTipoDoc"]." ".$selecTipo.">".$RsTipo["cDescTipoDoc"]."</option>";
                                                     }
                                                     mssql_free_result($rsTipo);
                                                     ?>
                                             </select>
                                             Tema:
                                             <select name="iCodTema" style="width:300px;" class="FormPropertReg form-control">
                                                 <option value="">Seleccione:</option>
                                                         <?
                                                         $sqlTem="SELECT * FROM Tra_M_Temas WHERE  iCodOficina = '$_SESSION[iCodOficinaLogin]' ";
                                                         $sqlTem .= "ORDER BY cDesTema ASC";
                                                         $rsTem=mssql_query($sqlTem,$cnx);
                                                         while ($RsTem=MsSQL_fetch_array($rsTem)){
                                                             if($RsTem[iCodTema]==$_GET[iCodTema]){
                                                                 $selecTem="selected";
                                                             }Else{
                                                                 $selecTem="";
                                                             }
                                                             echo "<option value=\"".$RsTem["iCodTema"]."\" ".$selecTem.">".$RsTem["cDesTema"]." ".$RsTem["cNombresTrabajador"]."</option>";
                                                         }
                                                         mssql_free_result($rsTem);
                                                         ?>
                                             </select>
                                             <button class="btn btn-primary" onclick="Buscar();" onMouseOver="this.style.cursor='hand'"> <b>Buscar</b> <img src="images/icon_buscar.png" width="17" height="17" border="0"> </button>
                                             <button class="btn btn-primary" onclick="window.open('<?=$PHP_SELF?>', '_self');" onMouseOver="this.style.cursor='hand'"> <b>Restablecer</b> <img src="images/icon_clear.png" width="17" height="17" border="0"> </button>
                                                     &nbsp;
                                                     <? // end table por fieldset
                                                     // ordenamiento
                                                     if($_GET[campo]==""){	$campo="op1";}Else{$campo=$_GET[campo];}
                                                     if($_GET[orden]==""){	$orden="DESC";}Else{	$orden=$_GET[orden];}
                                                     //invertir orden
                                                     if($orden=="ASC") $cambio="DESC";
                                                     if($orden=="DESC") $cambio="ASC";
                                                     ?>
                                                     <button class="btn btn-primary" onclick="window.open('pendientesFinalizadosExcel.php?fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&Entrada=<?=$_GET[Entrada]?>&Interno=<?=$_GET[Interno]?>&cCodificacion=<?=$_GET[cCodificacion]?>&cAsunto=<?=$_GET[cAsunto]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&iCodTrabajadorFinalizar=<?=$_GET[iCodTrabajadorFinalizar]?>&iCodTema=<?=$_GET[iCodTema]?>&EstadoMov=<?=$_GET[EstadoMov]?>&iCodOficina=<?=$_SESSION[iCodOficinaLogin]?>', '_blank');" onMouseOver="this.style.cursor='hand'"> <b>a Excel</b> <img src="images/icon_excel.png" width="17" height="17" border="0"> </button>
                                                     &nbsp;
                                                     <button class="btn btn-primary" onclick="window.open('pendientesFinalizadosPdf.php?fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&Entrada=<?=$_GET[Entrada]?>&Interno=<?=$_GET[Interno]?>&Anexo=<?=$_GET[Anexo]?>&cCodificacion=<?=$_GET[cCodificacion]?>&cAsunto=<?=$_GET[cAsunto]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&iCodTrabajadorResponsable=<?=$_GET[iCodTrabajadorResponsable]?>&iCodTrabajadorDelegado=<?=$_GET[iCodTrabajadorDelegado]?>&iCodTema=<?=$_GET[iCodTema]?>&EstadoMov=<?=$_GET[EstadoMov]?>&Aceptado=<?=$_GET[Aceptado]?>&SAceptado=<?=$_GET[SAceptado]?>', '_blank');" onMouseOver="this.style.cursor='hand'"> <b>a Pdf</b> <img src="images/icon_pdf.png" width="17" height="17" border="0"> </button>
                                         </form>
                                     </div>
                                 </div>
                             </nav>
                         </div>
                         <div class="row">
                         <div class="col-lg-12">
                             <div class="table-responsive">
                                 <table class="table table-sm">
                                     <thead>
                                     <tr>
                                         <th><a href="<?=$_SERVER['PHP_SELF']?>?campo=op2&orden=<?=$cambio?>&cCodificacion=<?=$_GET[cCodificacion]?>"  style=" text-decoration:<?if($campo=="op2"){ echo "underline"; }Else{ echo "none";}?>">
                                                 N&ordm; Trámite</a>
                                         </th>
                                         <th><a href="<?=$_SERVER['PHP_SELF']?>?campo=op3&orden=<?=$cambio?>&cCodificacion=<?=$_GET[cCodificacion]?>"  style=" text-decoration:<?if($campo=="op3"){ echo "underline"; }Else{ echo "none";}?>">
                                                 Tipo de Documento</a>
                                         </th>
                                         <th>Nombre / Razón Social</th>
                                         <th><a href="<?=$_SERVER['PHP_SELF']?>?campo=op4&orden=<?=$cambio?>&cCodificacion=<?=$_GET[cCodificacion]?>"  style=" text-decoration:<?if($campo=="op4"){ echo "underline"; }Else{ echo "none";}?>">Asunto / Procedimiento TUPA</a></th>
                                         <th>Derivado</th>
                                         <th><a href="<?=$_SERVER['PHP_SELF']?>?campo=op5&orden=<?=$cambio?>&cCodificacion=<?=$_GET[cCodificacion]?>"  style=" text-decoration:<?if($campo=="op5.cCodificacion"){ echo "underline"; }Else{ echo "none";}?>">
                                                 Recepción</a></th>
                                         <th>Respuesta del Delegado:</th>
                                         <th><a href="<?=$_SERVER['PHP_SELF']?>?campo=op1&orden=<?=$cambio?>&cCodificacion=<?=$_GET[cCodificacion]?>"  style=" text-decoration:<?if($campo=="op1"){ echo "underline"; }Else{ echo "none";}?>">Finalizado:</a></th>
                                         <th>Revertir:</th>
                                     </tr>
                                     </thead>
                                     <tbody>
                                     <?php
                                     if($_GET[fDesde]!=""){ $fDesde=date("Ymd", strtotime($_GET[fDesde])); }
                                     if($_GET[fHasta]!=""  ){
                                         // $fDesde=date("Ymd", strtotime($_GET[fDesde]));
                                         $fHasta=date("d-m-Y", strtotime($_GET[fHasta]));
                                         function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
                                             $date_r = getdate(strtotime($date));
                                             $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
                                             return $date_result;
                                         }
                                         $fHasta=dateadd($fHasta,1,0,0,0,0,0); // + 1 dia
                                     }
                                     include_once("../conexion/conexion.php");
                                     function paginar($actual, $total, $por_pagina, $enlace, $maxpags=0) {
                                         $total_paginas = ceil($total/$por_pagina);
                                         $anterior = $actual - 1;
                                         $posterior = $actual + 1;
                                         $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
                                         $maximo = $maxpags ? min($total_paginas, $actual+floor($maxpags/2)): $total_paginas;
                                         if ($actual>1)
                                             $texto = "<a href=\"$enlace$anterior\">«</a> ";
                                         else
                                             $texto = "<b><<</b> ";
                                         if ($minimo!=1) $texto.= "... ";
                                         for ($i=$minimo; $i<$actual; $i++)
                                             $texto .= "<a href=\"$enlace$i\">$i</a> ";
                                         $texto .= "<b>$actual</b> ";
                                         for ($i=$actual+1; $i<=$maximo; $i++)
                                             $texto .= "<a href=\"$enlace$i\">$i</a> ";
                                         if ($maximo!=$total_paginas) $texto.= "... ";
                                         if ($actual<$total_paginas)
                                             $texto .= "<a href=\"$enlace$posterior\">>></a>";
                                         else
                                             $texto .= "<b>>></b>";
                                         return $texto;
                                     }
                                     if (!isset($pag)) $pag = 1; // Por defecto, pagina 1
                                     $tampag = 15;
                                     $reg1 = ($pag-1) * $tampag;
                                     $sqlTra.= " SP_BANDEJA_FINALIZADOS '$campo' ,'$_GET[Entrada]','$_GET[Interno]','$fDesde','$fHasta', '$_GET[cCodificacion]', '$_GET[cAsunto]', '$_SESSION[iCodOficinaLogin]','$_GET[cCodTipoDoc]','$_GET[iCodTema]' ,   '$orden' ";
                                     //	echo $sqlTra."<br>";
                                     $rsTra=mssql_query($sqlTra,$cnx);
                                     //
                                     $total = MsSQL_num_rows($rsTra);
                                     $numrows=MsSQL_num_rows($rsTra);
                                     if($numrows==0){
                                         echo "NO SE ENCONTRARON REGISTROS<br>";
                                     }else{

                                         //////////////////////////////////////////////////////
                                         for ($i=$reg1; $i<min($reg1+$tampag, $total); $i++) {
                                             mssql_data_seek($rsTra, $i);
                                             $RsTra=MsSQL_fetch_array($rsTra);
                                             ///////////////////////////////////////////////////////
                                             //while ($RsTra=MsSQL_fetch_array($rsTra)){
                                             if ($color == "#DDEDFF"){
                                                 $color = "#F9F9F9";
                                             }else{
                                                 $color = "#DDEDFF";
                                             }
                                             if ($color == ""){
                                                 $color = "#F9F9F9";
                                             }
                                             ?>
                                             <tr bgcolor="<?=$color?>" onMouseOver="this.style.backgroundColor='#BFDEFF';" OnMouseOut="this.style.backgroundColor='<?=$color?>'">
                                                 <td >
                                                     <? if($RsTra[nFlgTipoDoc]==1){?>
                                                         <a href="registroDetalles.php?iCodTramite=<?=$RsTra[iCodTramite]?>"  rel="lyteframe" title="Detalle del Trámite" rev="width: 970px; height: 550px; scrolling: auto; border:no"><?=$RsTra[cCodificacion]?></a>
                                                     <? }
                                                     if($RsTra[nFlgTipoDoc]==2){
                                                         echo "INTERNO";}
                                                     if($RsTra[nFlgTipoDoc]==3){
                                                         echo "SALIDA";}
                                                     if($RsTra[nFlgTipoDoc]==4){
                                                         ?>
                                                         <a href="registroDetalles.php?iCodTramite=<?=$RsTra[iCodTramiteRel]?>"  rel="lyteframe" title="Detalle del Trámite" rev="width: 970px; height: 550px; scrolling: auto; border:no"><?=$RsTra[cCodificacion]?></a>
                                                     <?}?>
                                                     <?
                                                     echo "<div style=color:#727272>".date("d-m-Y", strtotime($RsTra[fFecRegistro]))."</div>";
                                                     echo "<div style=color:#727272;font-size:10px>".date("G:i", strtotime($RsTra[fFecRegistro]))."</div>";
                                                     //echo "<div style=color:#727272>".$RsTra[fFecRegistro]."</div>";
                                                     //echo "<div style=color:#727272;font-size:10px>".date("G:i", strtotime($RsTra[fFecRegistro]))."</div>";
                                                     if($RsTra[cFlgTipoMovimiento]==4){
                                                         echo "<div style=color:#FF0000;font-size:12px>Copia</div>";
                                                     }
                                                     ?>
                                                     <br>
                                                     <b>
                                                         <?php
                                                         if(ltrim(rtrim($RsTra[cPrioridadDerivar]))=="Alta"){
                                                             echo "<font color='#ff0000'>Alta</font>";
                                                         }elseif(ltrim(rtrim($RsTra[cPrioridadDerivar]))=="Media"){
                                                             echo "<font color='#07b52f'>Media</font>";
                                                         }elseif(ltrim(rtrim($RsTra[cPrioridadDerivar]))=="Baja"){
                                                             echo "<font color='#aecc05'>Baja</font>";
                                                         }
                                                         ?>
                                                     </b>
                                                 </td>
                                                 <td>
                                                     <?
                                                     if($RsTra[nFlgTipoDoc]==1){
                                                         $rsTDoc=mssql_query("SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$RsTra[cCodTipoDoc]'",$cnx);
                                                         $RsTDoc=MsSQL_fetch_array($rsTDoc);
                                                         echo $RsTDoc["cDescTipoDoc"];
                                                         mssql_free_result($rsTDoc);
                                                         echo "<div style=color:#808080;text-transform:uppercase>".$RsTra[cNroDocumento]."</div>";
                                                     }
                                                     else {
                                                         $rsTDoc=mssql_query("SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$RsTra[cCodTipoDoc]'",$cnx);
                                                         $RsTDoc=MsSQL_fetch_array($rsTDoc);
                                                         echo $RsTDoc["cDescTipoDoc"];
                                                         mssql_free_result($rsTDoc);
                                                         echo "<br>";
                                                         echo "<a style=\"color:#0067CE\" href=\"registroOficinaDetalles.php?iCodTramite=".$RsTra[iCodTramite]."\" rel=\"lyteframe\" title=\"Detalle del Trámite\" rev=\"width: 970px; height: 450px; scrolling: auto; border:no\">";
                                                         echo $RsTra[cCodificacion];
                                                         echo "</a>";
                                                     }
                                                     ?></td>
                                                 <td>
                                                     <?
                                                     $rsRem=mssql_query("SELECT * FROM Tra_M_Remitente WHERE iCodRemitente='$RsTra[iCodRemitente]'",$cnx);
                                                     $RsRem=MsSQL_fetch_array($rsRem);
                                                     echo $RsRem["cNombre"];
                                                     mssql_free_result($rsRem);
                                                     ?>
                                                 </td>
                                                 <td><?=$RsTra[cAsunto]?></td>
                                                 <td>
                                                     <div><?=date("d-m-Y", strtotime($RsTra[fFecDerivar]));?></div>
                                                     <div style="font-size:10px"><?=date("G:i", strtotime($RsTra[fFecDerivar]));?></div>
                                                 </td>
                                                 <td>
                                                     <?
                                                     if($RsTra[fFecRecepcion]==""){
                                                         echo "<div style=color:#ff0000>sin aceptar</div>";
                                                     }Else{
                                                         echo "<div style=color:#0154AF>".date("d-m-Y", strtotime($RsTra[fFecRecepcion]))."</div>";
                                                         echo "<div style=color:#0154AF;font-size:10px>".date("G:i", strtotime($RsTra[fFecRecepcion]))."</div>";
                                                     }
                                                     ?>
                                                 </td>
                                                 <td>
                                                     <?
                                                     if($RsTra[iCodTrabajadorDelegado]!=""){
                                                         $rsDelg=mssql_query("SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsTra[iCodTrabajadorDelegado]'",$cnx);
                                                         $RsDelg=MsSQL_fetch_array($rsDelg);
                                                         echo "<div style=color:#005B2E;font-size:12px>".$RsDelg["cApellidosTrabajador"]." ".$RsDelg["cNombresTrabajador"]."</div>";
                                                         mssql_free_result($rsDelg);
                                                         echo "<div style=color:#0154AF>".date("d-m-Y", strtotime($RsTra[fFecDelegado]))."</div>";
                                                         echo "<div style=color:#0154AF;font-size:10px>".date("G:i", strtotime($RsTra[fFecDelegado]))."</div>";
                                                     }
                                                     if($RsTra[iCodTrabajadorResponder]!=""){ //respondido
                                                         echo "<a style=\"color:#0067CE\" href=\"pendientesControlVerRpta.php?iCodMovimiento=".$RsTra[iCodMovimiento]."\" rel=\"lyteframe\" title=\"Detalle Respuesta\" rev=\"width: 480px; height: 270px; scrolling: auto; border:no\">RESPONDIDO</a>";
                                                     }
                                                     ?>
                                                 </td>
                                                 <td>

                                                     <?=$RsTra[cObservacionesFinalizar]?>
                                                     <br>
                                                     <?
                                                     $rsFina=mssql_query("SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsTra[iCodTrabajadorFinalizar]'",$cnx);
                                                     $RsFina=MsSQL_fetch_array($rsFina);
                                                     echo "<div style=color:#0154AF>".$RsFina["cApellidosTrabajador"]." ".$RsFina["cNombresTrabajador"]."</div>";
                                                     mssql_free_result($rsFina);
                                                     echo "<div style=color:#727272>".date("d-m-Y", strtotime($RsTra[fFecFinalizar]))."</div>";
                                                     echo "<div style=color:#727272;font-size:10px>".date("G:i", strtotime($RsTra[fFecFinalizar]))."</div>";
                                                     ?>
                                                 </td>
                                                 <td>
                                                     <a href="pendientesData.php?iCodMovimiento=<?=$RsTra[iCodMovimiento]?>&opcion=11">
                                                         <img src="images/icon_retornar.png" width="16" height="16" border="0" alt="Revertir Estado de Documento">
                                                     </a>
                                                     <?php echo "<a style=\"color:#0067CE\" href=\"registroObsFinalizar.php?iCodMovimiento=".$RsTra[iCodMovimiento]."\" rel=\"lyteframe\" title=\"Modificar Observacion\" rev=\"width: 410px; height: 280px; scrolling: no; border:no\"><img src='images/icon_avance.png' width='16' height='16' border='0'></a>";
                                                     ?>
                                                     <a href="registroTemaSelect.php?iCodTramite=<?=$RsTra[iCodTramite]?>&iCodOficinaRegistro=<?=$_SESSION[iCodOficinaLogin]?>"  rel="lyteframe" title="Vincular Tema" rev="width: 600px; height: 400px; scrolling: auto; border:no">
                                                         <img src="images/page_add.png" width="22" height="20" border="0">
                                                     </a>
                                                     <?php
                                                     if($RsTra[flg_libreblanco]=='' or $RsTra[flg_libreblanco]=='0'){
                                                         ?>
                                                         <a title="Libro Blanco" href="flg_libroblanco.php?opcion=1&iCodTramite=<?=$RsTra[iCodTramite]?>&page=2">
                                                             <img src="images/notebook_0.png" border='0' alt="Libro Blanco">
                                                         </a>
                                                         <?php
                                                     }else{
                                                         ?>
                                                         <a title="Libro Blanco" href="flg_libroblanco.php?opcion=0&iCodTramite=<?=$RsTra[iCodTramite]?>&page=2">
                                                             <img src="images/notebook_1.png" border='0' alt="Libro Blanco">
                                                         </a>
                                                         <?php
                                                     }
                                                     ?>
                                                 </td>
                                             </tr>

                                             <?
                                         }
                                     }
                                     mssql_free_result($rsTra);
                                     ?>
                                     </tbody>
                                 </table>
                             </div>
                         </div>
                         <div class="col-lg-12">
                             <div class="form-row">
                                 <?php echo "TOTAL DE REGISTROS : ".$numrows; ?> <br>
                                 <? echo paginar($pag, $total, $tampag, "pendientesFinalizados.php?fDesde=".$_GET[fDesde]."&fHasta=".$_GET[fHasta]."&cCodificacion=".$_GET[cCodificacion]."&cAsunto=".$_GET[cAsunto]."&iCodTema=".$_GET[iCodTema]."&cCodTipoDoc=".$_GET[cCodTipoDoc]."&Entrada=".$_GET[Entrada]."&Interno=".$_GET[Interno]."&pag="); ?>
                             </div>
                         </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </main>
<?php include("includes/userinfo.php"); ?>
<?include("includes/pie.php");?>
<script type="text/javascript" language="javascript" src="includes/lytebox.js"></script>
<script Language="JavaScript">
    $(document).ready(function() {
        $('.datepicker').pickadate({
            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
            format: 'dd-mm-yyyy',
            formatSubmit: 'dd-mm-yyyy',
        });
        $('.mdb-select').material_select();

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });


    });
    function Buscar()
    {
        document.frmConsulta.action="<?=$PHP_SELF?>";
        document.frmConsulta.submit();
    }

    //--></script>
</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>