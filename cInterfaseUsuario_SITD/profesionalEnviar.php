<?php
session_start();
if($_SESSION['CODIGO_TRABAJADOR']!=""){
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
<link type="text/css" rel="stylesheet" href="css/dhtmlgoodies_calendar.css" media="screen"/>

</head>
<body>
<?include("includes/menu.php");?>

<!--Main layout-->
<main class="mx-lg-5">
    <div class="container-fluid">
        <!--Grid row-->
        <div class="row wow fadeIn">
            <!--Grid column-->
            <div class="col-md-12 mb-12">
                <!--Card-->
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header text-center ">Enviar Documento</div>
                    <!--Card content-->
                    <div class="card-body">
						<form name="frmConsulta" method="POST">
							<input type="hidden" name="opcion" value="6">
							<input type="hidden" name="iCodMovimiento" value="<?=$_GET[iCodMovimientoAccion]?>">
							Enviar a:
							<select name="iCodTrabajadorEnviar" class="FormPropertReg mdb-select colorful-select dropdown-primary" searchable="Buscar aqui..">
									<option value="">Seleccione:</option>
									<?php
									include_once("../conexion/conexion.php");
									$sqlCodPeril = "SELECT iCodPerfil FROM Tra_M_Perfil_Ususario WHERE iCodTrabajador = ".$_SESSION['CODIGO_TRABAJADOR'];
									$rsCodPerfil = mssql_query($sqlCodPeril,$cnx);
									$RsCodPerfil = mssql_fetch_array($rsCodPerfil);

									$sqlTrb = "SELECT TMT.iCodTrabajador AS iCodTrabajador,
																		TMT.cApellidosTrabajador AS cApellidosTrabajador, 
																		TMT.cNombresTrabajador AS cNombresTrabajador  
														 FROM Tra_M_Perfil_Ususario TMU 
														 INNER JOIN Tra_M_Trabajadores TMT ON TMU.iCodTrabajador = TMT.iCodTrabajador
														 WHERE TMU.iCodOficina = '$_SESSION[iCodOficinaLogin]' AND 
														 			 TMU.iCodPerfil = '$RsCodPerfil[iCodPerfil]'";
									$rsTrb=mssql_query($sqlTrb,$cnx);
									while ($RsTrb=MsSQL_fetch_array($rsTrb)){
									  echo utf8_encode("<option value=\"".$RsTrb["iCodTrabajador"]."\">".$RsTrb["cApellidosTrabajador"]." ".utf8_decode($RsTrb["cNombresTrabajador"])."</option>");
									}
									mssql_free_result($rsTrb);
									?>
							</select>
							Observaciones:
                            <textarea name="cObservacionesEnviar" style="width:340px;height:55px" class="FormPropertReg form-control"></textarea>
							<button class="btn btn-primary" onclick="Delegar();" onMouseOver="this.style.cursor='hand'">
                                <b>Enviar</b>
                                <img src="images/icon_delegar.png" width="17" height="17" border="0">
                            </button>
							&nbsp;&nbsp;
							<button class="btn btn-primary" onclick="window.open('profesionalPendientes.php', '_self');" onMouseOver="this.style.cursor='hand'">
                                <b>Cancelar</b> <img src="images/icon_retornar.png" width="17" height="17" border="0">
                            </button>

						</form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include("includes/userinfo.php"); ?>

<?include("includes/pie.php");?>

<script type="text/javascript" language="javascript" src="includes/lytebox.js"></script>
<script type="text/javascript" src="scripts/dhtmlgoodies_calendar.js"></script>
<script Language="JavaScript">
    $('.mdb-select').material_select();
    function Delegar()
    {
        document.frmConsulta.action="profesionalData.php";
        document.frmConsulta.submit();
    }
    //--></script>
</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>