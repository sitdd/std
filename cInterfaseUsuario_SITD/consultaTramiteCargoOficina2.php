<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: consultaTramiteCargoOficina.php
SISTEMA: SISTEMA  DE TRÁMITE DOCUMENTARIO DIGITAL
OBJETIVO: Consulta de los Documentos de Cargo
PROPIETARIO: AGENCIA PERUANA DE COOPERACIÓN INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver      Autor             Fecha        Descripción
------------------------------------------------------------------------
1.0   APCI       03/08/2018   Creación del programa.
 
------------------------------------------------------------------------
*****************************************************************************************/
?>
<?
session_start();
If($_SESSION['CODIGO_TRABAJADOR']!=""){
include_once("../conexion/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<script type="text/javascript" language="javascript" src="includes/lytebox.js"></script>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
<link type="text/css" rel="stylesheet" href="css/dhtmlgoodies_calendar.css" media="screen"/>
<script type="text/javascript" src="scripts/dhtmlgoodies_calendar.js"></script>
<script Language="JavaScript">
function getXMLHTTP() { //fuction to return the xml http object
		var xmlhttp=false;	
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e)	{		
			try{			
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		 	
		return xmlhttp;
    }
	
	function getState(departamentoId) {		
		
		var strURL="iu_provincia.php?departamento="+departamentoId;
		var req = getXMLHTTP();
		
		if (req) {
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('statediv').innerHTML=req.responseText;						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}		
	}
	function getCity(departamentoId,provinciaId) {		
		var strURL="iu_distrito.php?departamento="+departamentoId+"&provincia="+provinciaId;
		var req = getXMLHTTP();
		
		if (req) {
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('citydiv').innerHTML=req.responseText;						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}
				
	}







function funcion(bol, frm, chkbox) { 
for (var i=0;i < frmConsultaTramiteCargo.elements[chkbox].length;i++) { // Dentro de todos los elementos, seleccionamos lo que tengan el mismo nombre que el seleccionado
elemento = frmConsultaTramiteCargo.elements[chkbox][i]; // Ahora es bidimensional
elemento.checked = (bol) ? true : false;
} 
}

function Buscar()
{
  document.frmConsultaTramiteCargoOficina.action="<?=$PHP_SELF?>";
  document.frmConsultaTramiteCargoOficina.submit();
}


//--></script>
</head>
<body>

	<?include("includes/menu.php");?>



<!--Main layout-->
 <main class="mx-lg-5">
     <div class="container-fluid">
          <!--Grid row-->
         <div class="row wow fadeIn">
              <!--Grid column-->
             <div class="col-md-12 mb-12">
                  <!--Card-->
                 <div class="card">
                      <!-- Card header -->
                     <div class="card-header text-center ">
                         >>
                     </div>
                      <!--Card content-->
                     <div class="card-body">

<div class="AreaTitulo">Control de Cargos</div>




							<form name="frmConsultaTramiteCargoOficina" method="GET" action="consultaTramiteCargoOficina.php"  >
						<tr>
							<td width="106" >N&ordm; Documento:</td>
							<td width="343" align="left"><input type="txt" name="cCodificacion" value="<?=$_GET[cCodificacion]?>" size="28" class="FormPropertReg form-control"></td>
							<td width="122" >Desde:</td>
							<td width="390" align="left">

									<td><input type="text" readonly name="fDesde" value="<?=$_GET[fDesde]?>" style="width:75px" class="FormPropertReg form-control"></td><td><div class="boton" style="width:24px;height:20px"><a href="javascript:;" onclick="displayCalendar(document.forms[0].fDesde,'dd-mm-yyyy',this,false)"><img src="images/icon_calendar.png" width="22" height="20" border="0"></a></div></td>
									<td width="20"></td>
									<td >Hasta:&nbsp;<input type="text" readonly name="fHasta" value="<?=$_GET[fHasta]?>" style="width:75px" class="FormPropertReg form-control"></td><td><div class="boton" style="width:24px;height:20px"><a href="javascript:;" onclick="displayCalendar(document.forms[0].fHasta,'dd-mm-yyyy',this,false)"><img src="images/icon_calendar.png" width="22" height="20" border="0"></a></div></td>
									</tr></table>							</td>
						</tr>
						<tr>
							<td width="106" >Tipo Documento:</td>
							<td width="343" align="left"><select name="cCodTipoDoc" class="FormPropertReg form-control" style="width:180px" />
									<option value="">Seleccione:</option>
									<?
									$sqlTipo="SELECT * FROM Tra_M_Tipo_Documento WHERE nFlgSalida=1";
									$sqlTipo.="ORDER BY cDescTipoDoc ASC";
          				$rsTipo=mssql_query($sqlTipo,$cnx);
          				while ($RsTipo=MsSQL_fetch_array($rsTipo)){
          					if($RsTipo["cCodTipoDoc"]==$_GET[cCodTipoDoc]){
          						$selecTipo="selected";
          					}Else{
          						$selecTipo="";
          					}
          				echo "<option value=".$RsTipo["cCodTipoDoc"]." ".$selecTipo.">".$RsTipo["cDescTipoDoc"]."</option>";
          				}
          				mssql_free_result($rsTipo);
									?>
									</select></td>
							<td width="122" >Razon Social:</td>
							<td align="left"><input type="txt" name="cNombre" value="<?=$_GET[cNombre]?>" size="65" class="FormPropertReg form-control">							</td>
						</tr>
						<tr>
							<td width="106" >Departamento:</td>
							<td width="343" align="left">
                        <?   
						     				$sqlDep="select * from Tra_U_Departamento order by cCodDepartamento "; 
                        $rsDep=mssql_query($sqlDep,$cnx);
                        ?>
                            <select name="cCodDepartamento" onchange="getState(this.value)" style="width:236px"><option>Seleccione:</option>
                       <?   while ($RsDep=MsSQL_fetch_array($rsDep)){
			                	if($RsDep["cCodDepartamento"]==$Rs[cDepartamento]){
          		            $selecDep="selected";
          	                }else{
          		            $selecDep="";
          	                 }
                            echo "<option value='$RsDep[cCodDepartamento]' >".$RsDep[cNomDepartamento]."</option>";
                             }
                        mssql_free_result($rsDep);				
                        ?>
                   </select>
                            </td>
							<td width="122" >Direccion:</td>
							<td align="left" class="CellFormRegOnly"><input type="txt" name="cDireccion" value="<?=$_GET[cDireccion]?>" size="65" class="FormPropertReg form-control">						  </td>
						</tr>
						<tr>
						  <td  >Provincia:</td>
						  <td  align="left"> 
                           <p id="statediv">
                           <select  name="cCodProvincia" <? if($_POST[cCodDepartamento]=="") echo "disabled"?> style="width:236px">
                            <option>Seleccione:</option>
                           </select>
                          </td>
						  <td  >Orden de Servicio:</td>
						  <td align="left" ><span class="CellFormRegOnly">
						    <input type="txt" name="cOrdenServicio" value="<?=$_GET[cOrdenServicio]?>" size="65" class="FormPropertReg form-control" />
						  </span></td>
						  </tr>
						<tr>
							<td >Distrito:</td>
                            <td align="left" > 
                            <p id="citydiv"> 
                           <select  name="cCodDistrito" <? if($_POST[cCodProvincia]=="") echo "disabled"?> style="width:236px">
                            <option>Seleccione:</option>       
                           </select>
                            </td>
                            <td  >Urgente:</td>
                            <td align="left" ><input type="checkbox" name="cFlgUrgente" value="1" <?if($_GET[cFlgUrgente]==1) echo "checked"?> />
                            <label for="checkbox"></label></td>
						</tr>
						<tr>
                         
							<td colspan="2" align="left">
                                                      </td>
                          <td colspan="2" align="right">
							<button class="btn btn-primary" onclick="Buscar();" onMouseOver="this.style.cursor='hand'"> <b>Buscar</b> <img src="images/icon_buscar.png" width="17" height="17" border="0"> </button>
							�
                           <button class="btn btn-primary" onclick="window.open('<?=$PHP_SELF?>', '_self');" onMouseOver="this.style.cursor='hand'"> <b>Restablecer</b> <img src="images/icon_clear.png" width="17" height="17" border="0"> </button>
                     �<? // ordenamiento
                        if($_GET[campo]==""){ $campo="Tra_M_Doc_Salidas_Multiples.iCodAuto"; }Else{$campo=$_GET[campo];}
                        if($_GET[orden]==""){ $orden="DESC"; }Else{ $orden=$_GET[orden]; }					    
					  ?>
							<button class="btn btn-primary" onclick="window.open('consultaTramiteCargoOficina_xls.php?fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodificacion=<?=$_GET[cCodificacion]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cFlgUrgente=<?=$_GET[cFlgUrgente]?>&iCodTrabajadorEnvio=<?=$_GET[iCodTrabajadorEnvio]?>&cNombre=<?=$_GET[cNombre]?>&cDireccion=<?=$_GET[cDireccion]?>&cOrdenServicio=<?=$_GET[cOrdenServicio]?>&ChxfRespuesta=<?=$_GET[ChxfRespuesta]?>&traRep=<?=$_SESSION['CODIGO_TRABAJADOR']?>&orden=<?=$orden?>&campo=<?=$campo?>', '_self');" onMouseOver="this.style.cursor='hand'"> <b>a Excel</b> <img src="images/icon_excel.png" width="17" height="17" border="0"> </button>
							�
							<button class="btn btn-primary" onclick="window.open('consultaTramiteCargoOficina_pdf.php?fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodificacion=<?=$_GET[cCodificacion]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cFlgUrgente=<?=$_GET[cFlgUrgente]?>&iCodTrabajadorEnvio=<?=$_GET[iCodTrabajadorEnvio]?>&ChxfRespuesta=<?=$_GET[ChxfRespuesta]?>&cNombre=<?=$_GET[cNombre]?>&cDireccion=<?=$_GET[cDireccion]?>&cOrdenServicio=<?=$_GET[cOrdenServicio]?>&orden=<?=$orden?>&campo=<?=$campo?>', '_blank');" onMouseOver="this.style.cursor='hand'"> <b>a Pdf</b> <img src="images/icon_pdf.png" width="17" height="17" border="0"> </button>							</td>
						</tr>
							
						</table>
			</fieldset>




<?
function paginar($actual, $total, $por_pagina, $enlace, $maxpags=0) {
$total_paginas = ceil($total/$por_pagina);
$anterior = $actual - 1;
$posterior = $actual + 1;
$minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
$maximo = $maxpags ? min($total_paginas, $actual+floor($maxpags/2)): $total_paginas;
if ($actual>1)
$texto = "<a href=\"$enlace$anterior\">�</a> ";
else
$texto = "<b>�</b> ";
if ($minimo!=1) $texto.= "... ";
for ($i=$minimo; $i<$actual; $i++)
$texto .= "<a href=\"$enlace$i\">$i</a> ";
$texto .= "<b>$actual</b> ";
for ($i=$actual+1; $i<=$maximo; $i++)
$texto .= "<a href=\"$enlace$i\">$i</a> ";
if ($maximo!=$total_paginas) $texto.= "... ";
if ($actual<$total_paginas)
$texto .= "<a href=\"$enlace$posterior\">�</a>";
else
$texto .= "<b>�</b>";
return $texto;
}


if (!isset($pag)) $pag = 1; // Por defecto, pagina 1
$tampag = 40;
$reg1 = ($pag-1) * $tampag;


//invertir orden
if($orden=="ASC") $cambio="DESC";
if($orden=="DESC") $cambio="ASC";
	
    $fDesde=date("Ymd", strtotime($_GET[fDesde]));
	$fHasta=date("Y-m-d", strtotime($_GET[fHasta]));
	function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    $date_r = getdate(strtotime($date));
    $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
    return $date_result;
				}
	$fHasta=dateadd($fHasta,1,0,0,0,0,0); // + 1 dia
	
/*
   	$sql="SELECT iCodAuto,cOrdenServicio,cDescTipoDoc,iCodTrabajadorEnvio,cNombresTrabajador,cApellidosTrabajador,fRespuesta,fEntrega,cFlgEstado,cRecibido,
          Tra_M_Doc_Salidas_Multiples.cObservaciones,Tra_M_Doc_Salidas_Multiples.iCodTramite,cNombre,Tra_M_Tramite.cCodTipoDoc,fFecRegistro,
          Tra_M_Doc_Salidas_Multiples.iCodRemitente,Tra_M_Doc_Salidas_Multiples.cCodificacion,Tra_M_Doc_Salidas_Multiples.cAsunto,Tra_M_Remitente.cDireccion ,
          cNomDepartamento,cNomProvincia ,cFlgEnvio ";
    $sql.="    FROM  Tra_M_Doc_Salidas_Multiples INNER JOIN Tra_M_Remitente ON Tra_M_Doc_Salidas_Multiples.iCodRemitente=Tra_M_Remitente.iCodRemitente
          LEFT JOIN Tra_U_Departamento ON Tra_M_Remitente.cDepartamento=Tra_U_Departamento.cCodDepartamento   
          LEFT  JOIN Tra_U_Provincia ON Tra_M_Remitente.cDepartamento + Tra_M_Remitente.cProvincia=  Tra_U_Provincia.cCodDepartamento+Tra_U_Provincia.cCodProvincia
          LEFT JOIN Tra_M_Trabajadores ON Tra_M_Doc_Salidas_Multiples.iCodTrabajadorEnvio=Tra_M_Trabajadores.iCodTrabajador ,Tra_M_Tramite,Tra_M_Tipo_Documento ";
    $sql.="  WHERE Tra_M_Tramite.iCodTramite=Tra_M_Doc_Salidas_Multiples.iCodTramite AND Tra_M_Tramite.cCodTipoDoc=Tra_M_Tipo_Documento.cCodTipoDoc ";
*/ 
   $sql=" SELECT TOP 500 iCodAuto,cOrdenServicio,cSiglaOficina,cDescTipoDoc,iCodTrabajadorEnvio,cNombresTrabajador,cApellidosTrabajador,fRespuesta,fEntrega,cFlgEnvio,cFlgEstado,cRecibido,cNomOficina,
           Tra_M_Doc_Salidas_Multiples.cObservaciones,Tra_M_Doc_Salidas_Multiples.iCodTramite,cNombre,Tra_M_Tramite.cCodTipoDoc,fFecRegistro,
           Tra_M_Doc_Salidas_Multiples.iCodRemitente,Tra_M_Doc_Salidas_Multiples.cCodificacion,Tra_M_Doc_Salidas_Multiples.cAsunto,Tra_M_Remitente.cDireccion ,
           cNomDepartamento,cNomProvincia ,cNomDistrito,cFlgEnvio, cNumGuia,cNumGuiaServicio ";
    $sql.=" FROM  Tra_M_Doc_Salidas_Multiples INNER JOIN Tra_M_Remitente ON Tra_M_Doc_Salidas_Multiples.iCodRemitente=Tra_M_Remitente.iCodRemitente
            LEFT JOIN Tra_U_Departamento ON Tra_M_Remitente.cDepartamento=Tra_U_Departamento.cCodDepartamento   
            LEFT  JOIN Tra_U_Provincia ON Tra_M_Remitente.cDepartamento=  Tra_U_Provincia.cCodDepartamento and Tra_M_Remitente.cProvincia=  Tra_U_Provincia.cCodProvincia
            LEFT  JOIN Tra_U_Distrito ON Tra_M_Remitente.cDepartamento=  Tra_U_Distrito.cCodDepartamento and Tra_M_Remitente.cProvincia=  Tra_U_Distrito.cCodProvincia and   
			Tra_M_Remitente.cDistrito=Tra_U_Distrito.cCodDistrito
            LEFT OUTER JOIN Tra_M_Trabajadores ON Tra_M_Doc_Salidas_Multiples.iCodTrabajadorEnvio=Tra_M_Trabajadores.iCodTrabajador 
			INNER JOIN Tra_M_Tramite ON  Tra_M_Tramite.iCodTramite=Tra_M_Doc_Salidas_Multiples.iCodTramite
			 LEFT OUTER JOIN Tra_m_oficinas on Tra_M_Tramite.iCodOficinaRegistro=Tra_m_oficinas.iCodOficina 
			,Tra_M_Tipo_Documento ";
	$sql.="  WHERE  Tra_M_Tramite.cCodTipoDoc=Tra_M_Tipo_Documento.cCodTipoDoc  ";  
   
    if($_GET[fDesde]!="" AND $_GET[fHasta]=="" AND $_GET[ChxfRespuesta]!="1"){
  	$sql.=" AND fEntrega >'$fDesde' ";
    }
    if($_GET[fDesde]=="" AND $_GET[fHasta]!="" AND $_GET[ChxfRespuesta]!="1"){
  	$sql.=" AND fEntrega <='$fHasta' ";
    }
    if($_GET[fDesde]!="" AND $_GET[fHasta]!="" AND $_GET[ChxfRespuesta]!="1"){
  	$sql.=" AND fEntrega BETWEEN '$fDesde' AND '$fHasta2' ";
    }	
	
	if($_GET[fDesde]!="" AND $_GET[fHasta]=="" AND $_GET[ChxfRespuesta]=="1"){
  	$sql.=" AND fRespuesta >'$fDesde' ";
    }
    if($_GET[fDesde]=="" AND $_GET[fHasta]!="" AND $_GET[ChxfRespuesta]=="1"){
  	$sql.=" AND fRespuesta <='$fHasta' ";
    }
    if($_GET[fDesde]!="" AND $_GET[fHasta]!="" AND $_GET[ChxfRespuesta]=="1"){
  	$sql.=" AND fRespuesta BETWEEN '$fDesde' AND '$fHasta2' ";
    }	

    if($_GET[cCodificacion]!=""){
    $sql.="AND Tra_M_Tramite.cCodificacion LIKE '%$_GET[cCodificacion]%' ";
    }
	if($_GET[cNombre]!=""){
    $sql.="AND Tra_M_Remitente.cNombre LIKE '%$_GET[cNombre]%' ";
    }
	if($_GET[cDireccion]!=""){
    $sql.="AND Tra_M_Remitente.cDireccion LIKE '%$_GET[cDireccion]%' ";
    }
	if($_GET[cCodTipoDoc]!=""){
    $sql.="AND Tra_M_Tramite.cCodTipoDoc='$_GET[cCodTipoDoc]' ";
    }	
	if($_GET[cOrdenServicio]!=""){
    $sql.="AND Tra_M_Doc_Salidas_Multiples.cOrdenServicio LIKE '%'+'$_GET[cOrdenServicio]'+'%' ";
    }	  
	if($_GET[cFlgUrgente]!=""){
	$sql.="AND Tra_M_Doc_Salidas_Multiples.cFlgUrgente='$_GET[cFlgUrgente]' ";
	}
	if($_GET[iCodTrabajadorEnvio]!=""){
	$sql.="AND iCodTrabajadorEnvio='$_GET[iCodTrabajadorEnvio]' ";
	}
	/*if($_GET[cCodDepartamento]!=""){
	$sql.="AND Tra_M_Remitente.cDepartamento='$_GET[cCodDepartamento]' ";
	}
	if($_GET[cCodProvincia]!=""){
	$sql.="AND Tra_M_Remitente.cProvincia='$_GET[cCodProvincia]' ";
	}
	if($_GET[cCodDistrito]!=""){
	$sql.="AND Tra_M_Remitente.cDistrito='$_GET[cCodDistrito]' ";
	}*/
	$sql.= " ORDER BY $campo $orden ";	
    
    $rs=mssql_query($sql,$cnx);
	$total = MsSQL_num_rows($rs);
   //echo $sql;
?>
<form name="frmCargo" method="GET" action="consultaTramiteCargoOficina.php"  >
<br>
<table width="1000" border="0" cellpadding="3" cellspacing="3" align="center">
<tr>
    <td width="98" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=cDescTipoDoc&orden=<?=$cambio?>&cDescTipoDoc=<?=$_GET[cDescTipoDoc]?>&fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodificacion=<?=$_GET[cCodificacion]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cFlgUrgente=<?=$_GET[cFlgUrgente]?>&iCodTrabajadorEnvio=<?=$_GET[iCodTrabajadorEnvio]?>&cNombre=<?=$_GET[cNombre]?>&cDireccion=<?=$_GET[cDireccion]?>&cOrdenServicio=<?=$_GET[cOrdenServicio]?>&ChxfRespuesta=<?=$_GET[ChxfRespuesta]?>"  style=" text-decoration:<?if($campo=="cDescTipoDoc"){ echo "underline"; }Else{ echo "none";}?>">Tipo de Documento</a></td>
    <td width="98" class="headCellColum">Oficina de Origen</td>
	<td width="98" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=Tra_M_Tramite.cCodificacion&orden=<?=$cambio?>&cCodificacion=<?=$_GET[cCodificacion]?>&fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cFlgUrgente=<?=$_GET[cFlgUrgente]?>&iCodTrabajadorEnvio=<?=$_GET[iCodTrabajadorEnvio]?>&cNombre=<?=$_GET[cNombre]?>&cDireccion=<?=$_GET[cDireccion]?>&cOrdenServicio=<?=$_GET[cOrdenServicio]?>&ChxfRespuesta=<?=$_GET[ChxfRespuesta]?>"  style=" text-decoration:<?if($campo=="Tra_M_Tramite.cCodificacion"){ echo "underline"; }Else{ echo "none";}?>">Nro Documento</a></td>
	<td width="142" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=cNombre&orden=<?=$cambio?>&cNombre=<?=$_GET[cNombre]?>&fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodificacion=<?=$_GET[cCodificacion]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cFlgUrgente=<?=$_GET[cFlgUrgente]?>&iCodTrabajadorEnvio=<?=$_GET[iCodTrabajadorEnvio]?>&cDireccion=<?=$_GET[cDireccion]?>&cOrdenServicio=<?=$_GET[cOrdenServicio]?>&ChxfRespuesta=<?=$_GET[ChxfRespuesta]?>"  style=" text-decoration:<?if($campo=="cNombre"){ echo "underline"; }Else{ echo "none";}?>">Destinatario</a></td>
    <td width="84" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=cDireccion&orden=<?=$cambio?>&cNombre=<?=$_GET[cNombre]?>&fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodificacion=<?=$_GET[cCodificacion]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cFlgUrgente=<?=$_GET[cFlgUrgente]?>&iCodTrabajadorEnvio=<?=$_GET[iCodTrabajadorEnvio]?>&cDireccion=<?=$_GET[cDireccion]?>&cOrdenServicio=<?=$_GET[cOrdenServicio]?>&ChxfRespuesta=<?=$_GET[ChxfRespuesta]?>"  style=" text-decoration:<?if($campo=="cDireccion"){ echo "underline"; }Else{ echo "none";}?>">Domicilio</a></td>
	<td width="84" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=cNombresTrabajador&orden=<?=$cambio?>&cNombresTrabajador=<?=$_GET[cNombresTrabajador]?>&fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodificacion=<?=$_GET[cCodificacion]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cFlgUrgente=<?=$_GET[cFlgUrgente]?>&iCodTrabajadorEnvio=<?=$_GET[iCodTrabajadorEnvio]?>&cNombre=<?=$_GET[cNombre]?>&cDireccion=<?=$_GET[cDireccion]?>&cOrdenServicio=<?=$_GET[cOrdenServicio]?>&ChxfRespuesta=<?=$_GET[ChxfRespuesta]?>"  style=" text-decoration:<?if($campo=="cNombresTrabajador"){ echo "underline"; }Else{ echo "none";}?>">Entrega a Motorizado</a></td>
    <td width="183" class="headCellColum"><a href="<?=$_SERVER['PHP_SELF']?>?campo=cFlgEstado&orden=<?=$cambio?>&cFlgEstado=<?=$_GET[cFlgEstado]?>&fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&cCodificacion=<?=$_GET[cCodificacion]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&cFlgUrgente=<?=$_GET[cFlgUrgente]?>&iCodTrabajadorEnvio=<?=$_GET[iCodTrabajadorEnvio]?>&cNombre=<?=$_GET[cNombre]?>&cDireccion=<?=$_GET[cDireccion]?>&cOrdenServicio=<?=$_GET[cOrdenServicio]?>&ChxfRespuesta=<?=$_GET[ChxfRespuesta]?>"  style=" text-decoration:<?if($campo=="cFlgEstado"){ echo "underline"; }Else{ echo "none";}?>">Estado Cargo</a></td>
	</tr>
<?
if( fDesde=="" && fHasta=="" && cCodificacion=="" && cCodTipoDoc=="" && cFlgUrgente=="" && iCodTrabajadorEnvio=="" && cNombre=="" && cDireccion=="" && cOrdenServicio=="" && ChxfRespuesta=="" ){
$sqlcargo=" SELECT  iCodAuto FROM  Tra_M_Doc_Salidas_Multiples ";  
$rscargo=mssql_query($sqlcargo,$cnx);
$numrows = MsSQL_num_rows($rscargo);
}
else {  
$numrows=MsSQL_num_rows($rs);
}
if($numrows==0){ 
		echo "NO SE ENCONTRARON REGISTROS<br>";
		echo "TOTAL DE REGISTROS : ".$numrows;
}else{
        echo "TOTAL DE REGISTROS : ".$numrows;
for ($i=$reg1; $i<min($reg1+$tampag, $total); $i++) {
	for ($h=0;$h<count($_POST[iCodAuto]);$h++){
      	$iCodAuto= $_POST[iCodAuto];
		if($Rs[iCodAuto]==$iCodAuto[$h]){
   			$Checkear="checked";
		}
	}
mssql_data_seek($rs, $i);
$Rs=MsSQL_fetch_array($rs);

//while ($Rs=MsSQL_fetch_array($rs)){
        		if ($color == "#DDEDFF"){
			  			$color = "#F9F9F9";
	    			}else{
			  			$color = "#DDEDFF";
	    			}
	    			if ($color == ""){
			  			$color = "#F9F9F9";
	    			}	
?>

 <tr bgcolor="<?=$color?>" onMouseOver="this.style.backgroundColor='#BFDEFF'" OnMouseOut="this.style.backgroundColor='<?=$color?>'" >
     <td valign="middle" align="left"><a href="iu_detalle_documento.php?codmov=<?=$Rs[cDescTipoDoc]?>"  rel="lyteframe" title="Detalle del Documento" rev="width: 970px; height: 550px; scrolling: auto; border:no"><?	echo $Rs[cDescTipoDoc];?> </a>
    <?
      echo "<div style=color:#727272>".date("d-m-Y", strtotime($Rs[fFecRegistro]))."</div>";
      echo "<div style=color:#727272;font-size:10px>".date("h:i A", strtotime($Rs[fFecRegistro]))."</div>";
      ?>  </td> 
      <td valign="middle" align="left"><?=$Rs[cNomOficina]?></td>
    <td valign="middle" align="left"><a href="iu_detalle_documento.php?codmov=<?=$Rs[cCodificacion]?>"  rel="lyteframe" title="Detalle del Documento" rev="width: 970px; height: 550px; scrolling: auto; border:no"><?	echo $Rs[cCodificacion];?> </a>    </td>
    <td valign="middle" align="left"><?=$Rs[cNombre]?>    	</td>
       <td align="left" valign="middle">
    			<? echo $Rs[cDireccion];
				  if($Rs[cNomDepartamento]!=""){ echo " - ".$Rs[cNomDepartamento];}
				  if($Rs[cNomProvincia]!=""){ echo " - ".$Rs[cNomProvincia];}?>
                </td>
    <td valign="middle" align="left"><? echo $Rs[cNombresTrabajador]." ".$Rs[cApellidosTrabajador];?>
    
    <? if($Rs[fEntrega]!=''){
      echo "<div style=color:#727272>".date("d-m-Y", strtotime($Rs[fEntrega]))."</div>";
      echo "<div style=color:#727272;font-size:10px>".date("h:i A", strtotime($Rs[fEntrega]))."</div>";
	  }
	  if($Rs[cOrdenServicio]!=''){
      echo "<div style=color:#03F>".$Rs[cOrdenServicio]."</div>";
      }?> </td>
    <td valign="middle" align="left">
	<? if($Rs[cFlgEstado]!=""){ 
	   if($Rs[cFlgEstado]==1){ $estado= "<div style='color:#FF0000'>NOTIFICADO.</div>";} else if($Rs[cFlgEstado]==2){$estado= "<div style='color:#FF0000'>DEVUELTO.</div>";}
	   $estado_cargo= "".$estado." El ".date("d-m-Y", strtotime($Rs[fRespuesta])).". Recibido por: ".$Rs[cRecibido]." , con Observacion: ".$Rs[cObservaciones]."";
	   echo $estado_cargo;
	  }
	  else{
	  echo "";
	  }
	 ?>	  </td>
</tr>
 

<?
}
}
?> 
<tr>
		<td colspan="7" align="center">
        <? echo paginar($pag, $total, $tampag, "consultaTramiteCargoOficina.php?fDesde=".$_GET[fDesde]."&fHasta=".$_GET[fHasta]."&cCodificacion=".$_GET[cCodificacion]."&cCodTipoDoc=".$_GET[cCodTipoDoc]."&cFlgUrgente=".$_GET[cFlgUrgente]."&iCodTrabajadorEnvio=".$_GET[iCodTrabajadorEnvio]."&cNombre=".$_GET[cNombre]."&cDireccion=".$_GET[cDireccion]."&cOrdenServicio=".$_GET[cOrdenServicio]."&ChxfRespuesta=".$_GET[ChxfRespuesta]."&pag=");?>          </td>
		</tr>
</table>
  </form> 
    </td>
	  </tr>
		</table>  
					</div>
                 </div>
             </div>
         </div>
     </div>
 </main>


<?include("includes/userinfo.php");?> <?include("includes/pie.php");?>


<map name="Map" id="Map"><area shape="rect" coords="1,4,19,15" href="#" /></map>
<map name="Map2" id="Map2"><area shape="rect" coords="0,5,15,13" href="#" /></map></body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>