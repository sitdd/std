<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: consultaAnexos_xls.php
SISTEMA: SISTEMA  DE TR�MITE DOCUMENTARIO DIGITAL
OBJETIVO: Reporte general en EXCEL de los Documentos Anexos
PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripci�n
------------------------------------------------------------------------
1.0   APCI    05/09/2018      Creaci�n del programa.
------------------------------------------------------------------------
*****************************************************************************************/
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=consultaEntradaGeneral.xls");
	
	$anho = date("Y");
	$datomes = date("m");
	$datomes = $datomes*1;
	$datodia = date("d");
	$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre");
	
	echo "<table width=780 border=0><tr><td align=center colspan=5>";
	echo "<H3>REPORTE - DOCUMENTOS DE ENTRADA</H3>";
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=right colspan=5>";
	echo "SITD, ".$datodia." ".$meses[$datomes].' del '.$anho;
	echo " ";
	
	echo "<table width=780 border=1><tr>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> N&ordm; Doc.</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> N&ordm; de Referencia</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Remitente</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Fecha de Derivado</td>";
	echo "<td bgcolor=#0000CC><font color=#ffffff> Asunto</td>";
	echo "</tr>";	

require_once("../conexion/conexion.php");
 $fDesde=date("Ymd", strtotime($_GET[fDesde]));
	$fHasta=date("Y-m-d", strtotime($_GET[fHasta]));
	function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    $date_r = getdate(strtotime($date));
    $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
    return $date_result;
				}
	$fHasta=dateadd($fHasta,1,0,0,0,0,0); // + 1 dia

        $sql=" SELECT * FROM Tra_M_Tramite,Tra_M_Tramite_Movimientos,Tra_M_Remitente ";
        $sql.=" WHERE Tra_M_Tramite.iCodTramite=Tra_M_Tramite_Movimientos.iCodTramite AND Tra_M_Tramite.iCodRemitente=Tra_M_Remitente.iCodRemitente  ";
       if($_GET[fDesde]!="" AND $_GET[fHasta]==""){
       	$sql.=" AND Tra_M_Tramite_Movimientos.fFecDerivar>'$fDesde' ";
       }
       if($_GET[fDesde]=="" AND $_GET[fHasta]!=""){
       	$sql.=" AND Tra_M_Tramite_Movimientos.fFecDerivar<='$fHasta' ";
       }
       if($_GET[fDesde]!="" && $_GET[fHasta]!=""){
       $sql.=" AND Tra_M_Tramite_Movimientos.fFecDerivar BETWEEN  '$fDesde' and '$fHasta' ";
       }
	   	   
	   if($_GET[cCodificacion]!=""){
        $sql.="AND Tra_M_Tramite.cCodificacion='$_GET[cCodificacion]' ";
       }
	   if($_GET[cNroDocumento]!=""){
        $sql.="AND Tra_M_Tramite.cNroDocumento='$_GET[cNroDocumento]' ";
       }
	   
       if($_GET[cAsunto]!=""){
        $sql.="AND Tra_M_Tramite.cAsunto LIKE '%$_GET[cAsunto]%' ";
       }
	   if($_GET[iCodTupa]!=""){
        $sql.="AND Tra_M_Tramite.iCodTupa='$_GET[iCodTupa]' ";
       }
	   if($_GET[cCodTipoDoc]!=""){
        $sql.="AND Tra_M_Tramite.cCodTipoDoc='$_GET[cCodTipoDoc]' ";
       }
       if($_GET[cNombre]!=""){
        $sql.="AND Tra_M_Remitente.cNombre LIKE '%$_GET[cNombre]%' ";
       }
	    if($_GET[iCodOficina]!=""){
        $sql.="AND Tra_M_Tramite_Movimientos.iCodOficinaOrigen='$_GET[iCodOficina]' ";
       }
	   
	   $sql.= " ORDER BY Tra_M_Tramite_Movimientos.iCodOficinaOrigen";	   
       $rs=mssql_query($sql,$cnx);
       //echo $sql;

       while ($Rs=MsSQL_fetch_array($rs)){
	   echo "<tr>";
       echo "<td valign=top>".$Rs[cCodificacion]."</td>";
       echo "<td valign=top>".$Rs[cNroDocumento]."</td>";
       echo "<td valign=top>";
	           if($Rs[cTipoPersona]=='1'){ 
			   echo "<div style=color:#000000;text-align:center>Persona Natural</div>";
               echo "<div style=color:#0154AF;font-size:10px;text-align:center>".$Rs[cNombre]."</div>";
			   }
			   else{
			   echo "<div style=color:#000000;text-align:center>".$Rs[cNombre]."</div>";
               echo "<div style=color:#0154AF;text-align:center>Representante</div>";}
	   echo	 "</td>";
       echo "<td valign=top>";
	         echo "<div style=color:#0154AF;text-align:center>".date("d-m-Y", strtotime($Rs[fFecDerivar]))."</div>";
             echo "<div style=color:#0154AF;font-size:10px;text-align:center>".date("h:i A", strtotime($Rs[fFecDerivar]))."</div>";
	   echo "</td>";
       echo "<td valign=top>".$Rs[cAsunto]."</td>";
       echo "</tr>";
       }
       echo "</table>";
?>