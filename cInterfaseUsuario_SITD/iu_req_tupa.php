<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: iu_req_tupa.php
SISTEMA: SISTEMA  DE TRÁMITE DOCUMENTARIO DIGITAL
OBJETIVO: Administrar Tabla Maestra de Requerimientos de Tupa para el Perfil Administrador 
PROPIETARIO: AGENCIA PERUANA DE COOPERACIÓN INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver      Autor             Fecha        Descripción
------------------------------------------------------------------------
1.0   APCI       03/08/2018   Creación del programa.
 
------------------------------------------------------------------------
*****************************************************************************************/
session_start();
If($_SESSION['CODIGO_TRABAJADOR']!=""){
include_once("../conexion/conexion.php");
$cod = $_GET["cod"];
$sw = $_GET["sw"];
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<script>
function ConfirmarBorrado()
{
 if (confirm("Esta seguro de eliminar el registro?")){
  return true; 
 }else{ 
  return false; 
 }
}
</script>
</head>
<body>

	<?include("includes/menu.php");?>



<!--Main layout-->
 <main class="mx-lg-5">
     <div class="container-fluid">
          <!--Grid row-->
         <div class="row wow fadeIn">
              <!--Grid column-->
             <div class="col-md-12 mb-12">
                  <!--Card-->
                 <div class="card">
                      <!-- Card header -->
                     <div class="card-header text-center ">
                         >>
                     </div>
                      <!--Card content-->
                     <div class="card-body">

<div class="AreaTitulo">Maestra Requisitos de Tupa</div>

<form name="form1" method="GET" action="iu_req_tupa.php">
 <input type="hidden" name="cod" value="<? echo $_GET[cod];?>">
<table width="796" border="0" align="center">
  <tr>
  <td>
  <button class="btn btn-primary" type="button" onclick="window.open('iu_tupa.php', '_self');" onMouseOver="this.style.cursor='hand'"> <b>Regresar</b> <img src="images/icon_retornar.png" width="17" height="17" border="0"> </button>
  </td>
    <td width="844" colspan="2" align="right">   
    <button class="btn btn-primary" onClick="window.open('iu_req_tupa_xls.php?cod=<?=$_GET[cod]?>&traRep=<?=$_SESSION['CODIGO_TRABAJADOR']?>', '_blank');" onMouseOver="this.style.cursor='hand'"> <b>a Excel</b> <img src="images/icon_excel.png" width="17" height="17" border="0"> </button>
		�	
    <button class="btn btn-primary" onClick="window.open('iu_req_tupa_pdf.php?cod=<?=$_GET[cod]?>', '_blank');" onMouseOver="this.style.cursor='hand'"> <b>a Pdf</b> <img src="images/icon_pdf.png" width="17" height="17" border="0"> </button>
    </td>
  </tr>
</table>
</form>

<table width="800" border="1" align="center">
  <input type="hidden" name="iCodTupaRequisito" value="<?=$Rs[iCodTupaRequisito];?>">
	<input type="hidden" name="iCodTupa" value="<? echo $Rs[iCodTupa];?>">
   
    <input type="hidden" name="cod" value="<? if($_GET[cod]!=""){echo $_GET[cod];} else {echo $_GET[cod];}?>">
<?
function paginar($actual, $total, $por_pagina, $enlace, $maxpags=0) {
$total_paginas = ceil($total/$por_pagina);
$anterior = $actual - 1;
$posterior = $actual + 1;
$minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
$maximo = $maxpags ? min($total_paginas, $actual+floor($maxpags/2)): $total_paginas;
if ($actual>1)
$texto = "<a href=\"$enlace$anterior\">�</a> ";
else
$texto = "<b>�</b> ";
if ($minimo!=1) $texto.= "... ";
for ($i=$minimo; $i<$actual; $i++)
$texto .= "<a href=\"$enlace$i\">$i</a> ";
$texto .= "<b>$actual</b> ";
for ($i=$actual+1; $i<=$maximo; $i++)
$texto .= "<a href=\"$enlace$i\">$i</a> ";
if ($maximo!=$total_paginas) $texto.= "... ";
if ($actual<$total_paginas)
$texto .= "<a href=\"$enlace$posterior\">�</a>";
else
$texto .= "<b>�</b>";
return $texto;
}


if (!isset($pag)) $pag = 1; // Por defecto, pagina 1
$tampag = 10;
$reg1 = ($pag-1) * $tampag;

// ordenamiento
if($_GET[campo]==""){
	$campo="Requisito";
}Else{
	$campo=$_GET[campo];
}

if($_GET[orden]==""){
	$orden="ASC";
}Else{
	$orden=$_GET[orden];
}

//invertir orden
if($orden=="ASC") $cambio="DESC";
if($orden=="DESC") $cambio="ASC";
/* $sql= " select * from Tra_M_Tupa_Requisitos ";
$sql.= " where iCodTupaRequisito >0 ";
$sql.= " AND iCodTupa ='$cod'" ;
$sql.= " ORDER BY nNumTupaRequisito ASC"; */
 //echo $sql;
 $sql= " SP_REQUISITO_TUPA_LISTA '$cod' ,'".$orden."' , '".$campo."'  " ;
 $rs=mssql_query($sql,$cnx);
 $total = MsSQL_num_rows($rs);
?>
<tr>
    <td width="573" class="headCellColum">Nombre Requisito</td>
	<td width="126" class="headCellColum">Estado</td>
	<td width="79" class="headCellColum">Opciones</td>
</tr>
<?
$numrows=MsSQL_num_rows($rs);
if($numrows==0){ 
		echo "NO SE ENCONTRARON REGISTROS<br>";
		echo "TOTAL DE REGISTROS : ".$numrows;
}else{
         echo "TOTAL DE REGISTROS : ".$numrows;
	for ($i=$reg1; $i<min($reg1+$tampag, $total); $i++) {
mssql_data_seek($rs, $i);
$Rs=MsSQL_fetch_array($rs);
//while ($Rs=MsSQL_fetch_array($rs)){ 
	if ($color == "#CEE7FF"){
			  $color = "#F9F9F9";
	    		}else{
			  $color = "#CEE7FF";
	    		}
	    		if ($color == ""){
			  $color = "#F9F9F9";
	    		}	
?>
<tr bgcolor="<?=$color?>">
    <td align="left"><? echo $Rs[cNomTupaRequisito];?></td>
    <td><? if ($Rs[nEstadoTupaRequisito]==1){?>
    		<div style="color:#005E2F">Activo</div>
    	<? }Else{?>
    		<div style="color:#950000">Inactivo</div>
    	<? }?>
	</td>
    <td>
    	<a href="../cLogicaNegocio_SITD/ln_elimina_req_tupa.php?id=<? echo $Rs[iCodTupaRequisito];?>&cod=<?=$Rs[iCodTupa];?>" onClick='return ConfirmarBorrado();'"><i class="far fa-trash-alt"></i></a>
	    <a href="../cInterfaseUsuario_SITD/iu_actualiza_req_tupa.php?cod=<? echo $Rs[iCodTupaRequisito];?>&sw=9"><i class="fas fa-edit"></i></a></td>
  </tr>
  
<?
}
}
?>

</table>
<? echo paginar($pag, $total, $tampag, "iu_req_tupa.php?sw=8&cod=".$Rs[iCodTupa]."&pag=");?>
<table width="800" border="0" align="center">
  <tr>
    <td align="right"><a href="../cInterfaseUsuario_SITD/iu_nuevo_req_tupa.php?cod=<?=$_GET[cod]?>&sw=8">Nuevo Requisito</a>
   
</td>
  </tr>





					</div>
                 </div>
             </div>
         </div>
     </div>
 </main>
  <?php include("includes/userinfo.php"); ?> <?php include("includes/pie.php"); ?>

</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>