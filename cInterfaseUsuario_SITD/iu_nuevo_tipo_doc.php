<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: iu_nuevo_tipo_doc.php
SISTEMA: SISTEMA  DE TR�MITE DOCUMENTARIO DIGITAL
OBJETIVO: Mantenimiento de la Tabla Maestra de Tipo de Documentos para el Perfil Administrador
          -> Crear Registro de Tipo de Documento
PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver      Autor             Fecha        Descripci�n
------------------------------------------------------------------------
1.0   APCI       03/08/2018   Creaci�n del programa.
 
------------------------------------------------------------------------
*****************************************************************************************/
session_start();
If($_SESSION['CODIGO_TRABAJADOR']!=""){
include_once("../conexion/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
<script>
function validar(f) {
 var error = "Por favor, antes de crear complete:\n\n";
 var a = "";
  if (f.cDescTipoDoc.value == "") {
  a += " Ingrese el Tipo de Documento";
  alert(error + a);
 }

  
 return (a == "");
 
}
</script>
</head>
<body>

	<?include("includes/menu.php");?>



<!--Main layout-->
 <main class="mx-lg-5">
     <div class="container-fluid">
          <!--Grid row-->
         <div class="row wow fadeIn">
              <!--Grid column-->
             <div class="col-md-12 mb-12">
                  <!--Card-->
                 <div class="card">
                      <!-- Card header -->
                     <div class="card-header text-center ">
                         >>
                     </div>
                      <!--Card content-->
                     <div class="card-body">

<div class="AreaTitulo">Maestra Tipo de Documentos</div>

<?
require_once("../cAccesoBaseDato_SITD/ad_busqueda.php");
?>
<form action="../cLogicaNegocio_SITD/ln_nuevo_tipo_doc.php"  onSubmit="return validar(this)" method="post"  name="form1">

            <fieldset id="tfa_DatosTipoDoc" class="fieldset"  >
            <legend >Datos de Tipo Documento</legend>
        <table border="0">
           <tr>
              <td width="114"></td>
              <td >Tipo de Documento:</td>
              <input name="cDescTipoDoc" class="FormPropertReg form-control" style="text-transform:uppercase" maxlength="70" type="text" id="cDescTipoDoc" value="<?=$_GET[cDescTipoDoc]?>" size="40"/><? if($_GET[cSiglaDoc]!="") echo "El Tipo de Documento Ya Existe"?>              </td>
           </tr>
          <? /* <tr>
              <td width="114"></td>
              <td >Sigla del Documento:</td>
              <input name="cSiglaDoc" class="FormPropertReg form-control" style="text-transform:uppercase" maxlength="20" type="text" size="40" id="cSiglaDoc" value="<?=$_GET[cSiglaDoc]?>"><? if($_GET[cDescTipoDoc]!="") echo "La Sigla Ya Existe"?></td>
           </tr> */ ?>
           <tr>
             <td height="43" colspan="3" align="center">
             <button class="btn btn-primary"  type="submit" id="Insert Tipo Doc" onMouseOver="this.style.cursor='hand'"> <b>Crear</b> <img src="images/page_add.png" width="17" height="17" border="0"> </button>
              ������
              <button class="btn btn-primary"  type="button" onclick="window.open('iu_tipo_doc.php', '_self');" onMouseOver="this.style.cursor='hand'"> <b>Cancelar</b> <img src="images/icon_retornar.png" width="17" height="17" border="0"> </button>    </td>
           </tr>
        </table>
        </fieldset>
     

</form>        
</td>
		</tr>
		</table>

					</div>
                 </div>
             </div>
         </div>
     </div>
 </main>
  <?php include("includes/userinfo.php"); ?> <?php include("includes/pie.php"); ?>

</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>