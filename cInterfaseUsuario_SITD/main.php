<? session_start();
If($_SESSION['CODIGO_TRABAJADOR']!=""){
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?></head>
<body>

<?include("includes/menu.php");?>

<!--Main layout-->
<main class="mx-lg-5">
    <div class="container-fluid">

        <!--Grid row-->
        <div class="row wow fadeIn justify-content-center">

            <!--Grid column-->
            <div class="col-md-4 mb-4">

                <!--Card-->
                <div class="card">

                    <!-- Card header -->
                    <div class="card-header text-center">
                        Expedientes
                    </div>

                    <!--Card content-->
                    <div class="card-body">

                        <canvas id="myChart"></canvas>

                    </div>

                </div>
                <!--/.Card-->

            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-md-4 mb-4">

                <!--Card-->
                <div class="card mb-4">

                    <!-- Card header -->
                    <div class="card-header text-center">
                        Total de expedientes
                    </div>

                    <!--Card content-->
                    <div class="card-body">

                        <canvas id="pieChart"></canvas>

                    </div>

                </div>
                <!--/.Card-->


            </div>
            <!--Grid column-->

        </div>
        <!--Grid row-->

        <!--Grid row-->

        <!--Grid row-->
        <div class="row wow fadeIn">

            <!--Grid column-->
            <div class="col-lg-4 col-md-12 mb-4">

                <!--Card-->
                <div class="card">

                    <!-- Card header -->
                    <div class="card-header text-center">Linea de crecimiento documental semanal</div>

                    <!--Card content-->
                    <div class="card-body">

                        <canvas id="lineChart"></canvas>

                    </div>

                </div>
                <!--/.Card-->

            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-lg-4 col-md-6 mb-4">

                <!--Card-->
                <div class="card">

                    <!-- Card header -->
                    <div class="card-header text-center">Tamaño documental por día</div>

                    <!--Card content-->
                    <div class="card-body">

                        <canvas id="radarChart"></canvas>

                    </div>

                </div>
                <!--/.Card-->

            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-lg-4 col-md-6 mb-4">

                <!--Card-->
                <div class="card">

                    <!-- Card header -->
                    <div class="card-header text-center">Documentos referentes a los dias</div>

                    <!--Card content-->
                    <div class="card-body">

                        <canvas id="doughnutChart"></canvas>

                    </div>

                </div>
                <!--/.Card-->

            </div>
            <!--Grid column-->

        </div>
        <!--Grid row-->

    </div>
</main>
<!--Main layout-->


<?include("includes/userinfo.php");?>
<?include("includes/pie.php");?>

<?php

// --------------------------- Pendientes ----------------------------------
// Punto de control - Jefes = 3
// Punto de control - Asistentes = 19
// Profesional = 4
if($_SESSION['iCodPerfilLogin'] == 3 OR $_SESSION['iCodPerfilLogin'] == 19){
    $sqlBtn1 = "SP_BANDEJA_PENDIENTES  '','','','','','', ";
    $sqlBtn1.= "'','','','','','','','','$_SESSION[iCodOficinaLogin]','Fecha','DESC' ";
    $rsBtn1 = mssql_query($sqlBtn1, $cnx2);
    $total1 = mssql_num_rows($rsBtn1);
}else{
    $sqlBtn1 = "SELECT Tra_M_Tramite.iCodTramite AS Tramite, * FROM Tra_M_Tramite,Tra_M_Tramite_Movimientos 
                WHERE Tra_M_Tramite.iCodTramite = Tra_M_Tramite_Movimientos.iCodTramite 
                AND ((Tra_M_Tramite_Movimientos.iCodOficinaDerivar='$_SESSION[iCodOficinaLogin]' AND ( Tra_M_Tramite_Movimientos.iCodTrabajadorDelegado='$_SESSION[CODIGO_TRABAJADOR]' OR Tra_M_Tramite_Movimientos.iCodTrabajadorDerivar='$_SESSION[CODIGO_TRABAJADOR]')) OR (Tra_M_Tramite_Movimientos.iCodOficinaDerivar='$_SESSION[iCodOficinaLogin]' AND Tra_M_Tramite_Movimientos.iCodTrabajadorEnviar='$_SESSION[CODIGO_TRABAJADOR]') ) And Tra_M_Tramite_Movimientos.nEstadoMovimiento!=2 And Tra_M_Tramite.nFlgEnvio=1 AND (Tra_M_Tramite_Movimientos.nEstadoMovimiento=1 OR Tra_M_Tramite_Movimientos.nEstadoMovimiento=3 OR (Tra_M_Tramite_Movimientos.nEstadoMovimiento=1 AND Tra_M_Tramite_Movimientos.cFlgTipoMovimiento=6)) ORDER BY Tra_M_Tramite_Movimientos.iCodMovimiento DESC ";
    $rsBtn1 = mssql_query($sqlBtn1, $cnx2);
    $total1 = mssql_num_rows($rsBtn1);
}
// --------------------------- Derivados ----------------------------------

$sqlBtn2 = "SP_BANDEJA_DERIVADOS '','','','','%%','%%','$_SESSION[iCodOficinaLogin]','','' ,'','Derivado','DESC'";
$rsBtn2 = mssql_query($sqlBtn2, $cnx2);
$total2 = mssql_num_rows($rsBtn2);

// --------------------------- Finalizados ----------------------------------
// Punto de control - Jefes = 3
// Punto de control - Asistentes = 19
// Profesional = 4
if($_SESSION['iCodPerfilLogin']==3 OR $_SESSION['iCodPerfilLogin'] == 19){
    $sqlBtn3 = "SP_BANDEJA_FINALIZADOS 'op1' ,'','','','', '', '', '$_SESSION[iCodOficinaLogin]','','' , 'DESC' ";
    $rsBtn3 = mssql_query($sqlBtn3, $cnx2);
    $total3 = mssql_num_rows($rsBtn3);
}else{
    $sqlBtn3 = "SELECT Tra_M_Tramite.iCodTramite as Tramite, * FROM Tra_M_Tramite,Tra_M_Tramite_Movimientos WHERE Tra_M_Tramite.iCodTramite=Tra_M_Tramite_Movimientos.iCodTramite AND ((Tra_M_Tramite_Movimientos.iCodOficinaDerivar='$_SESSION[iCodOficinaLogin]' AND Tra_M_Tramite_Movimientos.iCodTrabajadorDelegado='$_SESSION[CODIGO_TRABAJADOR]' ) OR Tra_M_Tramite_Movimientos.iCodTrabajadorEnviar='$_SESSION[CODIGO_TRABAJADOR]' ) AND Tra_M_Tramite_Movimientos.nEstadoMovimiento=5 ORDER BY Tra_M_Tramite.iCodTramite DESC ";
    $rsBtn3 = mssql_query($sqlBtn3, $cnx2);
    $total3 = mssql_num_rows($rsBtn3);
}


// --------------------------- Enviados ----------------------------------
$sqlBtn4 = "SELECT *, c.cFlgTipoMovimiento,c.nEstadoMovimiento, c.fFecDelegadoRecepcion,c.fFecRecepcion FROM Tra_M_Tramite a, Tra_M_Tramite_Trabajadores b, Tra_M_Tramite_Movimientos c WHERE a.iCodTramite = b.iCodTramite AND (b.iCodTrabajadorOrigen='$_SESSION[CODIGO_TRABAJADOR]' AND b.iCodTrabajadorDestino!='$_SESSION[CODIGO_TRABAJADOR]' ) AND b.iCodOficina='$_SESSION[iCodOficinaLogin]' and b.iCodMovimiento =c.iCodMovimiento ORDER BY a.iCodTramite DESC";
$rsBtn4 = mssql_query($sqlBtn4, $cnx2);
$total4 = mssql_num_rows($rsBtn4);


// --------------------------- Respondidos ----------------------------------
$sqlBtn5 = "SELECT Tra_M_Tramite.iCodTramite as Tramite, * FROM Tra_M_Tramite,Tra_M_Tramite_Movimientos WHERE Tra_M_Tramite.iCodTramite=Tra_M_Tramite_Movimientos.iCodTramite AND ((Tra_M_Tramite_Movimientos.iCodOficinaDerivar='$_SESSION[iCodOficinaLogin]' AND Tra_M_Tramite_Movimientos.iCodTrabajadorDelegado='$_SESSION[CODIGO_TRABAJADOR]' ) OR Tra_M_Tramite_Movimientos.iCodTrabajadorEnviar='$_SESSION[CODIGO_TRABAJADOR]' ) AND Tra_M_Tramite_Movimientos.nEstadoMovimiento=4 ORDER BY Tra_M_Tramite.iCodTramite DESC   ";
$rsBtn5 = mssql_query($sqlBtn5, $cnx2);
$total5 = mssql_num_rows($rsBtn5);
?>

<script>

    // Line
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Pendientes", "Derivados", "Finalizados", "Enviados", "Respondidos"],
            datasets: [{
                label: 'Numero de expedientes',
                data: [<? echo $total1 ?> ,<? echo $total2 ?>, <? echo $total3 ?>, <? echo $total4 ?>, <? echo $total5 ?>],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    //pie
    var ctxP = document.getElementById("pieChart").getContext('2d');
    var myPieChart = new Chart(ctxP, {
        type: 'pie',
        data: {
            labels: ["Pendientes", "Derivados", "Finalizados", "Enviados", "Respondidos"],
            datasets: [
                {
                    data: [<? echo $total1 ?> ,<? echo $total2 ?>, <? echo $total3 ?>, <? echo $total4 ?>, <? echo $total5 ?>],
                    backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
                    hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
                }
            ]
        },
        options: {
            responsive: true
        }
    });


    //line
    var ctxL = document.getElementById("lineChart").getContext('2d');
    var myLineChart = new Chart(ctxL, {
        type: 'line',
        data: {
            labels: ["Pendientes", "Derivados", "Finalizados", "Enviados", "Respondidos"],
            datasets: [
                {
                    label: "Semana pasada",
                    fillColor: "#F7464A",
                    strokeColor: "#F7464A",
                    pointColor: "#F7464A",
                    pointStrokeColor: "#F7464A",
                    pointHighlightFill: "#F7464A",
                    pointHighlightStroke: "#F7464A",
                    data: [<? echo $total1 ?> ,<? echo $total2 ?>, <? echo $total3 ?>, <? echo $total4 ?>, <? echo $total5 ?>]
                },
            ]
        },
        options: {
            responsive: true
        }
    });


    //radar
    var ctxR = document.getElementById("radarChart").getContext('2d');
    var myRadarChart = new Chart(ctxR, {
        type: 'radar',
        data: {
            labels: ["Pendientes", "Derivados", "Finalizados", "Enviados", "Respondidos"],
            datasets: [
                {
                    label: "Semana pasada",
                    fillColor: "#F7464A",
                    strokeColor: "#F7464A",
                    pointColor: "#F7464A",
                    pointStrokeColor: "#F7464A",
                    pointHighlightFill: "#F7464A",
                    pointHighlightStroke: "#F7464A",
                    data: [<? echo $total1 ?> ,<? echo $total2 ?>, <? echo $total3 ?>, <? echo $total4 ?>, <? echo $total5 ?>]
                },
            ]
        },
        options: {
            responsive: true
        }
    });

    //doughnut
    var ctxD = document.getElementById("doughnutChart").getContext('2d');
    var myLineChart = new Chart(ctxD, {
        type: 'doughnut',
        data: {
            labels: ["Pendientes", "Derivados", "Finalizados", "Enviados", "Respondidos"],
            datasets: [
                {
                    data: [<? echo $total1 ?> ,<? echo $total2 ?>, <? echo $total3 ?>, <? echo $total4 ?>, <? echo $total5 ?>],
                    backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
                    hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
                }
            ]
        },
        options: {
            responsive: true
        }
    });
</script>

</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>