<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: registroHojaRuta_pdf.php
SISTEMA: SISTEMA  DE TRÁMITE DOCUMENTARIO DIGITAL
OBJETIVO: Reporte General en PDF
PROPIETARIO: AGENCIA PERUANA DE COOPERACIÓN INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripción
------------------------------------------------------------------------
1.0   APCI    05/09/2018      Creación del programa.
------------------------------------------------------------------------
*****************************************************************************************/
session_start();
ob_start();
//*************************************

include_once("../conexion/conexion.php");
//echo $sql;
$sql="SELECT *,Tra_M_Tramite.cObservaciones AS Observaciones FROM Tra_M_Tramite ";
$sql.=" LEFT OUTER JOIN Tra_M_Tramite_Movimientos ON Tra_M_Tramite.iCodTramite=Tra_M_Tramite_Movimientos.iCodTramite ";
$sql.=" LEFT OUTER JOIN Tra_M_Remitente ON Tra_M_Tramite.iCodRemitente=Tra_M_Remitente.iCodRemitente ";
$sql.=" WHERE Tra_M_Tramite_Movimientos.iCodTramite='$_GET[iCodTramite]'   ";
$rs=mssql_query($sql,$cnx);
while($Rs=MsSQL_fetch_array($rs)){
?>
<page backtop="15mm" backbottom="10mm" backleft="10mm" backright="10mm">
	<table style="width:100%;border:solid 0px black;">
	<tr>
	<td style="text-align:left;	width: 100%">
					<img style="width:300px" src="images/pdf_pcm.jpg" alt="Logo">
	</td>
	</tr>
	</table>
		
	<table style="width:100%;border: solid 0px #585858; border-collapse: collapse" align="left">
	<tr>
	<td style="width:100%;text-align:left; border: solid 0px #585858;font-family:Times">
	Datos Principales
	</td>
	</tr>
	</table>
	<table border=1 style="width:100%;border: solid 1px #585858; border-collapse: collapse" align="left">
	<tr>
  <td style="width: 100%; text-align: center; border: solid 1px #585858;">
  		<table style="width:90%;border: solid 0px #585858; border-collapse: collapse">
  		<tr>
  		<td style="width:23%;height:10px;text-align:left; border: solid 0px #585858;font-size:14px;font-family:Times">Nro Registro</td>
  		<td style="width:2%;text-align:left; border: solid 0px #585858;font-size:14px">:</td>
  		<td style="width:65%;text-align:left; border: solid 0px #585858;font-size:14px;font-family:Times"><b><?=$Rs[cCodificacion]?></b></td>
  		</tr>
  		
  		<tr>
  		<td style="width:23%;height:10px;text-align:left; border: solid 0px #585858;font-size:14px;font-family:Times">Fecha/H de Registro</td>
  		<td style="width:2%;text-align:left; border: solid 0px #585858;font-size:14px">:</td>
  		<td style="width:65%;text-align:left; border: solid 0px #585858;font-size:14px;font-family:Times"><b>
				<?
  			$PrintMes=array("","ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SET","OCT","NOV","DIC");
  			$fechaMes=date("m", strtotime($Rs[fFecRegistro]));
    		$fechaMesEntero = intval($fechaMes);  
    		echo date("d", strtotime($Rs[fFecRegistro]));
    		echo "-".$PrintMes[$fechaMesEntero]."-";
  			echo date("Y h:i:s", strtotime($Rs[fFecRegistro]));
  			?>
  			</b>
  		</td>
  		</tr>
  		
  		<tr>
  		<td style="width:23%;height:10px;text-align:left; border: solid 0px #585858;font-size:14px;font-family:Times">Area Origen</td>
  		<td style="width:2%;text-align:left; border: solid 0px #585858;font-size:14px">:</td>
  		<td style="width:65%;text-align:left; border: solid 0px #585858;font-size:14px;font-family:Times"><b>
  				<?
  				$sqlOfi1="SELECT * FROM Tra_M_Trabajadores,Tra_M_Oficinas WHERE Tra_M_Trabajadores.iCodOficina=Tra_M_Oficinas.iCodOficina AND Tra_M_Trabajadores.iCodTrabajador='$Rs[iCodTrabajadorRegistro]'";
					$rsOfi1=mssql_query($sqlOfi1,$cnx);
					$RsOfi1=MsSQL_fetch_array($rsOfi1);
					echo $RsOfi1[cNomOficina];
  				?>	
  		</b></td>
  		</tr>
  		
  		<?
  		
  		?>
  		
  		<tr>
  		<td style="width:23%;height:10px;text-align:left; border: solid 0px #585858;font-size:14px;font-family:Times">Fecha/H Derivo</td>
  		<td style="width:2%;text-align:left; border: solid 0px #585858;font-size:14px">:</td>
  		<td style="width:65%;text-align:left; border: solid 0px #585858;font-size:14px;font-family:Times"><b>
  				<?
  				if($Rs[fFecDerivar]!=""){
  					$fechaMes=date("m", strtotime($Rs[fFecDerivar]));
    				$fechaMesEntero = intval($fechaMes);  
    				echo date("d", strtotime($Rs[fFecDerivar]));
    				echo "-".$PrintMes[$fechaMesEntero]."-";
  					echo date("Y h:i:s", strtotime($Rs[fFecDerivar]));
  				}
				
  				?>
  		</b></td>
  		</tr>
  		
  		<tr>
  		<td style="width:23%;height:10px;text-align:left; border: solid 0px #585858;font-size:14px;font-family:Times">Nro Doc. Principal</td>
  		<td style="width:2%;text-align:left; border: solid 0px #585858;font-size:14px">:</td>
  		<td style="width:65%;text-align:left; border: solid 0px #585858;font-size:14px;text-transform:uppercase;font-family:Times"><b>
		<?
	$rsTraDe=mssql_query("SELECT iCodTramite FROM Tra_M_Tramite_Movimientos WHERE iCodTramiteDerivar ='$_GET[iCodTramite]' and cFlgTipoMovimiento!=5 And cFlgTipoMovimiento=4 ",$cnx);
	$RsTraDe=mssql_fetch_array($rsTraDe);
	$rsTrax=mssql_query("SELECT nFlgTipoDoc,cCodificacion FROM Tra_M_Tramite WHERE iCodTramite ='$RsTraDe[iCodTramite]' ",$cnx);
	$RsTrax=mssql_fetch_array($rsTrax);
		echo trim($RsTrax[cCodificacion]);?>                     
        </b></td>
  		</tr>
        <tr>
  		<td style="width:23%;height:10px;text-align:left; border: solid 0px #585858;font-size:14px;font-family:Times">Nro de Referencia</td>
  		<td style="width:2%;text-align:left; border: solid 0px #585858;font-size:14px">:</td>
  		<td style="width:65%;text-align:left; border: solid 0px #585858;font-size:14px;text-transform:uppercase;font-family:Times"><b>
		<?
	$rsRef=mssql_query("SELECT cReferencia FROM Tra_M_Tramite_Referencias WHERE iCodTramite ='$_GET[iCodTramite]' And iCodTipo=2 ",$cnx);
	$RsRef=mssql_fetch_array($rsRef);
		echo " ".$RsRef[cReferencia];?>                      
        </b></td>
  		</tr>
  		
			<tr>
  		<td style="width:23%;height:10px;text-align:left; border: solid 0px #585858;font-size:14px;font-family:Times">Tipo Documento</td>
  		<td style="width:2%;text-align:left; border: solid 0px #585858;font-size:14px">:</td>
  		<td style="width:65%;text-align:left; border: solid 0px #585858;font-size:14px;font-family:Times"><b>
				<?
					$sqlTipDoc="SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc=$Rs[cCodTipoDoc]";
			    $rsTipDoc=mssql_query($sqlTipDoc,$cnx);
			    $RsTipDoc=MsSQL_fetch_array($rsTipDoc);
			    echo $RsTipDoc[cDescTipoDoc];
				?>
  			</b>
  		</td>
  		</tr>		        		
  		</table>
	</td>
  </tr>
	</table>
	
	<table style="width:100%;border: solid 0px #585858; border-collapse: collapse" align="left">
	<tr>
	<td style="width:100%;height:20px;text-align:left; vertical-align:bottom; border: solid 0px #585858;font-family:Times">
	Asunto
	</td>
	</tr>
	</table>

	<table border="1" style="width:100%;border: solid 1px #585858; border-collapse: collapse" align="left">
	<tr>
  <td style="width: 100%; text-align:left; border: solid 1px #585858;font-family:Times">
   <?  echo $Rs[cAsunto];
     if($Rs[iCodTupa]!=""){
    		$sqlTup="SELECT * FROM Tra_M_Tupa WHERE iCodTupa='$Rs[iCodTupa]'";
      	$rsTup=mssql_query($sqlTup,$cnx);
      	$RsTup=MsSQL_fetch_array($rsTup);
	 echo "<br>";	
	 echo $RsTup[cNomTupa]; 
	 }?></td>
  </tr>
	</table>

	<table style="width:100%;border: solid 0px #585858; border-collapse: collapse" align="left">
	<tr>
	<td style="width:100%;height:20px;text-align:left; vertical-align:bottom; border: solid 0px #585858;font-family:Times">&nbsp;
	
	</td>
	</tr>
	</table>

	<table border="1" style="width:100%;border: solid 1px #585858; border-collapse: collapse" align="left">
	<tr>
 <td style="width:3%; text-align: left; border: solid 1px #585858;"></td>
  <td style="width:7%; text-align: left; border: solid 1px #585858;font-size:12px;font-family:Times">Origen</td>
  <td style="width:7%; text-align: left; border: solid 1px #585858;font-size:12px;font-family:Times">Destino</td>
  <td style="width:5%; text-align: left; border: solid 1px #585858;font-size:12px;font-family:Times">Ind</td>
  <td style="width:14%; text-align: left; border: solid 1px #585858;font-size:12px;font-family:Times">Fecha Derivo   / <br />
    Fecha Aceptado</td>
  <td style="width:18%; text-align: left; border: solid 1px #585858;font-size:12px;font-family:Times">N&uacute;mero de Documento</td>
  <td style="width:7%; text-align: left; border: solid 1px #585858;font-size:12px;font-family:Times">Fls</td>
  <td style="width:8%; text-align: left; border: solid 1px #585858;font-size:12px;font-family:Times">V.B.</td>
  <td style="width:19%; text-align: left; border: solid 1px #585858;font-size:12px;font-family:Times">Observaciones.</td>
  <td style="width:12%; text-align: left; border: solid 1px #585858;font-size:12px;font-family:Times">C.Recep</td>
   </tr>

  <tr>
  <td style="height:40px; text-align: center; border: solid 1px #585858;font-size:10px;font-family:Times">1</td>
  <td style="height:20px; text-align: left; border: solid 1px #585858;font-size:10px;font-family:Times">
  	<?
  	$sqlOfiD="SELECT * FROM Tra_M_Oficinas WHERE iCodOficina='$Rs[iCodOficinaOrigen]'";
		$rsOfiD=mssql_query($sqlOfiD,$cnx);
		$RsOfiD=MsSQL_fetch_array($rsOfiD);
		echo $RsOfiD[cSiglaOficina];
  	?>
	</td>
   <td style="height:20px; text-align: left; border: solid 1px #585858;font-size:10px;font-family:Times">
  	<?
  	$sqlOfiD="SELECT * FROM Tra_M_Oficinas WHERE iCodOficina='$Rs[iCodOficinaDerivar]'";
		$rsOfiD=mssql_query($sqlOfiD,$cnx);
		$RsOfiD=MsSQL_fetch_array($rsOfiD);
		echo $RsOfiD[cSiglaOficina];
		if($Rs[cFlgTipoMovimiento]==4){echo "<br/>copia";} if($Rs[cFlgTipoMovimiento]==5){echo "<br/>referencia";}
  	?>
	</td>
	
	<td style="height:20px; text-align: left; border: solid 1px #585858;font-size:10px;font-family:Times">
		<?
		$sqlIndic="SELECT * FROM Tra_M_Indicaciones WHERE iCodIndicacion='$Rs[iCodIndicacionDerivar]'";
    $rsIndic=mssql_query($sqlIndic,$cnx);
    $RsIndic=MsSQL_fetch_array($rsIndic);
				echo substr($RsIndic["cIndicacion"],0,2);
    mssql_free_result($rsIndic);
		?>
	</td>
	
  <td style="height:20px; text-align: left; border: solid 1px #585858;font-size:10px;font-family:Times">
  	<?
	if($Rs[fFecDerivar]!=""){
  	$fechaMes=date("m", strtotime($Rs[fFecDerivar]));
    $fechaMesEntero = intval($fechaMes);  
    echo date("d", strtotime($Rs[fFecDerivar]));
    echo "-".$PrintMes[$fechaMesEntero]."-";
  	echo date("Y", strtotime($Rs[fFecDerivar]));
	}
	echo "<br/>";
	if($Rs[fFecRecepcion]!=""){
  	$fechaMes2=date("m", strtotime($Rs[fFecRecepcion]));
    $fechaMesEntero2 = intval($fechaMes2);  
    echo date("d", strtotime($Rs[fFecRecepcion]));
    echo "-".$PrintMes[$fechaMesEntero2]."-";
  	echo date("Y", strtotime($Rs[fFecRecepcion]));
	}
	?>
	</td>
<td style="height:20px; text-align: center; border: solid 1px #585858;font-size:10px;font-family:Times">
  	<? 
	echo $RsTipDoc[cDescTipoDoc];echo "<br>";
	echo $Rs[cCodificacion];?>
	</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;font-size:10px;font-family:Times">
  	<? echo $Rs[nNumFolio]; ?>
	</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
   <td style="height:20px; text-align: justify; vertical-align:top; border: solid 1px #585858;font-size:10px;font-family:Times">
      <? 
  	$val= strlen(ltrim(rtrim($Rs[Observaciones]))); if($val>84){$con="...";}else{$con="";}
  	echo substr($Rs[Observaciones], 0, 28)."<br/>";
	echo substr($Rs[Observaciones], 28, 28)."<br/>";
	echo substr($Rs[Observaciones], 56, 28)."<br/>";
	echo substr($Rs[Observaciones], 84, 28).$con;?>
      </td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
   </tr>
  
  <tr>
  <td style="height:40px; text-align: center; border: solid 1px #585858;font-size:10px;font-family:Times">2</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  	</tr>

  <tr>
  <td style="height:40px; text-align: center; border: solid 1px #585858;font-size:10px;font-family:Times">3</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
 	</tr>  

  <tr>
  <td style="height:40px; text-align: center; border: solid 1px #585858;font-size:10px;font-family:Times">4</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
 	</tr>
	
  <tr>
  <td style="height:40px; text-align: center; border: solid 1px #585858;font-size:10px;font-family:Times">5</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
	</tr> 	
     <tr>
  <td style="height:40px; text-align: center; border: solid 1px #585858;font-size:10px;font-family:Times">6</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
 	</tr> 	
     <tr>
  <td style="height:40px; text-align: center; border: solid 1px #585858;font-size:10px;font-family:Times">7</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
 	</tr> 	
     <tr>
  <td style="height:40px; text-align: center; border: solid 1px #585858;font-size:10px;font-family:Times">8</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
  <td style="height:20px; text-align: center; border: solid 1px #585858;">&nbsp;</td>
	</tr> 	
   </table>
	
	<table style="width:100%;border: solid 0px #585858; border-collapse: collapse" align="left">
	<tr>
	<td style="width:100%;height:20px;text-align:left;vertical-align:bottom; border: solid 0px #585858;font-family:Times">
	Observaciones:
	</td>
	</tr>
	</table>
	
	<table border="0" align="left">
	<tr>
  <td style="width:100%; text-align:left; border: solid 0px #585858;font-family:Times"><?=$Rs[Observaciones]?></td>
  </tr>
	</table>
	<br>
	<br>
	<br>	
	<table style="width:100%;border: solid 0px #585858; border-collapse: collapse" align="left">
	<tr>
	<td style="width:100%;height:30px;text-align:left;vertical-align:bottom; border: solid 0px #585858;font-family:Times">
	Referencias del Doc. Principal:
	</td>
	</tr>
	<tr>
	<td>
	<? $sqlRefs="SELECT * FROM Tra_M_Tramite_Referencias WHERE iCodTramite='".$_GET[iCodTramite]."'";
         $rsRefs=mssql_query($sqlRefs,$cnx);
         while ($RsRefs=MsSQL_fetch_array($rsRefs)){
		echo $RsRefs[cReferencia];echo " <br> ";
		}?> 
	</td>
	</tr>
	
	</table>
	
	<table style="width:100%;border: solid 0px #585858; border-collapse: collapse" align="left">
	<tr>
	<td style="width:100%;height:40px;text-align:left;vertical-align:bottom; border: solid 0px #585858;font-family:Times">
	Indicaciones:
	</td>
	</tr>
	</table>

	<table style="width:400px;border: solid 0px #585858; border-collapse: collapse" align="center">
  		<tr>
  		<td style="width:140px;height:8px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">01.ACCION NECESARIA</td>
  		<td style="width:140px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">02.ESTUDIO E INFORME</td>
  		<td style="width:120px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">03.CONOCIMIENTO Y FINES</td>
  		</tr>
		<tr>
			<td style="width:140px;height:8px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">04.FORMULAR RESPUESTA</td>
  		<td style="width:140px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">05.POR CORRESPONDERLE</td>
  		<td style="width:120px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">06.TRANSCRIBIR</td>
  		</tr>
		<tr>
			<td style="width:140px;height:8px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">07.PROYECTAR DISPOSITIVO</td>
  		<td style="width:140px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">08.FIRMAR Y/O REVISAR</td>
  		<td style="width:120px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">09.ARCHIVAR</td>
  		</tr>
		<tr>
			<td style="width:140px;height:8px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">10.CONOCIMIENTO Y RESPUESTA</td>
  		<td style="width:140px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times">11.PARA COMENTARIOS</td>
  		<td style="width:120px;text-align:left; border: solid 0px #585858;font-size:8px;font-family:Times"></td>
  		</tr>
 </table>

	
	
</page>
<?
}

//*************************************


	$content = ob_get_clean();  set_time_limit(0);     ini_set('memory_limit', '640M');

	// conversion HTML => PDF
	require_once(dirname(__FILE__).'/html2pdf/html2pdf.class.php');
	try
	{
		$html2pdf = new HTML2PDF('P','A4', 'es', false, 'UTF-8', 3);
		$html2pdf->pdf->SetDisplayMode('fullpage');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output('exemple03.pdf');
	}
	catch(HTML2PDF_exception $e) { echo $e; }
?>   
         		
