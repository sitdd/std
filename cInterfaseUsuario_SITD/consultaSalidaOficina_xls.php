<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: consultaSalidaOficina_xls.php
SISTEMA: SISTEMA   DE TR�MITE DOCUMENTARIO DIGITAL
OBJETIVO: Reporte general en EXCEL
PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripci�n
------------------------------------------------------------------------
1.0   APCI    05/09/2018      Creaci�n del programa.
------------------------------------------------------------------------
*****************************************************************************************/
include_once("../conexion/conexion.php");
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=consultaSalidaOficina.xls");
	
	$anho = date("Y");
	$datomes = date("m");
	$datomes = $datomes*1;
	$datodia = date("d");
	$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre");
	
	echo "<table width=780 border=0><tr><td align=center colspan=9>";
	echo "<H3>REPORTE - DOCUMENTOS DE SALIDA POR OFICINA</H3>";
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=right colspan=9>";
	echo "SITD, ".$datodia." ".$meses[$datomes].' del '.$anho;
	echo " ";
	
	echo "<table width=780 border=0><tr><td align=left colspan=9>";
	$sqllog="select cNombresTrabajador, cApellidosTrabajador from tra_m_trabajadores where iCodTrabajador='$traRep' "; 
	$rslog=mssql_query($sqllog,$cnx);
	$Rslog=MsSQL_fetch_array($rslog);
	echo "GENERADO POR : ".$Rslog[cNombresTrabajador]." ".$Rslog[cApellidosTrabajador];
	echo " ";
?>	

	<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="center">
		<thead>
			<tr>
			 <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Fecha</th>
             <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Hora</th>
             <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Expediente /<br />
               Referencia</th>	
			 <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Tipo Documento</th>
			 <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Numero Documento</th>
          	 <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Asunto</th>
			 <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Observaciones</th>
             <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Solicitado por</th>
             
             <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Destino</th>
             <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Cargos</th>
             <th style="width: 8%; text-align: center; border: solid 1px #6F6F6F; background: #D8D8D8">Obs</th>
          </tr>
		</thead>
		<tbody>
				<?
  	if ($fecini!=''){$fecini=date("Ymd", strtotime($fecini));}
   	if( $fecfin!=''){
    $fecfin=date("Y-m-d", strtotime($fecfin));
	function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    $date_r = getdate(strtotime($date));
    $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),(    $date_r["year"]+$yy)));
    return $date_result;
				}
	$fecfin=dateadd($fecfin,1,0,0,0,0,0); // + 1 dia
	}
   
     $sql="SP_CONSULTA_SALIDA_OFICINA_EXCEL '$fecini', '$fecfin','$_GET[RespuestaSI]', '$_GET[RespuestaNO]', '%$_GET[cCodificacion]%', '%$_GET[cAsunto]%', '%$_GET[cObservaciones]%', '$_GET[cCodTipoDoc]','%$_GET[cNombre]%','$_GET[Respuesta]', '$_GET[RegistroPersonal]', '$_GET[RegistroSolicitado]','$session','$campo','$orden' ";  
   $rs=mssql_query($sql,$cnx);
   //echo $sql;
	   
	   while ($Rs=MsSQL_fetch_array($rs)){
	   ?>
	    <tr>
        <td style="width: 8%; border: solid 1px #6F6F6F;font-size:10px">
		    <?  echo "<div style=color:#727272;text-align:center>".date("d-m-Y", strtotime($Rs[fFecRegistro]))." ".date("h:i A", strtotime($Rs[fFecRegistro]))."</div>";?></td> 
        <td style="width: 8%; border: solid 1px #6F6F6F;font-size:10px">        
            <?  echo "<div style=color:#727272;font-size:10px;text-align:center>".date("h:i A", strtotime($Rs[fFecRegistro]))."</div>";
		    ?></td>
         <td style="width: 8%; text-align: left; border: solid 1px #6F6F6F;font-size:10px">
			 <?	echo "<div style=text-transform:uppercase>".$Rs[cReferencia]."</div>"; ?>
         </td> 
        <td style="width: 8%; text-align: left; border: solid 1px #6F6F6F;font-size:10px">
		 <? echo "<div>".$Rs[cDescTipoDoc]."</div>"; 
		 
		  ?></td>
		  <td style="width: 8%; text-align: left; border: solid 1px #6F6F6F;font-size:10px">
		 <?  echo "<div style=color:#808080;text-transform:uppercase>".$Rs[cCodificacion]."</div>"
		  ?></td>
         
        <td style="width: 8%; text-align: left; border: solid 1px #6F6F6F;font-size:10px"><? echo $Rs[cAsunto];?></td>
        <td style="width: 8%; text-align: left; border: solid 1px #6F6F6F;font-size:10px"><? echo $Rs[cObservaciones];?></td>
        <td style="width: 8%; text-align: left; border: solid 1px #6F6F6F;font-size:10px"><? echo $Rs[cNombresTrabajador]." ".$Rs[cApellidosTrabajador];?></td>
        
        <td style="width: 8%; text-align: center; border: solid 1px #6F6F6F;font-size:10px">
         <? 
	    	if($Rs[iCodRemitente]!=0){ echo  $Rs[cNombre]; }
  	        else if($Rs[iCodRemitente]==0){ echo "MULTIPLE"; }
  	        else if($Rs[iCodRemitente]==""){ echo "NO ASIGNADO"; }
	    ?></td> 
        <td style="width: 8%;  border: solid 1px #6F6F6F;font-size:10px">
		    
          </td>
          <td style="width: 8%;  border: solid 1px #6F6F6F;font-size:10px">
		   <? echo $Rs[cRptaOK];?> 
          </td> 
        </tr>
      <?
         }
      ?>
	   	
      </tbody>
	</table>