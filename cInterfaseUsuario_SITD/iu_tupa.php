<?
/**************************************************************************************
NOMBRE DEL PROGRAMA: iu_tupa.php
SISTEMA: SISTEMA  DE TR�MITE DOCUMENTARIO DIGITAL
OBJETIVO: Administrar la Tabla Maestra de Tupa para el Perfil Administrador
PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver      Autor             Fecha        Descripci�n
------------------------------------------------------------------------
1.0   APCI       03/08/2018   Creaci�n del programa.
 
------------------------------------------------------------------------
*****************************************************************************************/
session_start();
$pag = $_GET['pag'];

If($_SESSION['CODIGO_TRABAJADOR']!=""){
include_once("../conexion/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />

</head>

<body>

<?include("includes/menu.php");?>
<!--Main layout-->
<main class="mx-lg-5">
    <div class="container-fluid">

        <!--Grid row-->
        <div class="row wow fadeIn">

            <!--Grid column-->
            <div class="col-md-12 mb-12">

                <!--Card-->
                <div class="card">

                    <!-- Card header -->
                    <div class="card-header text-center ">
                        Mantenimiento >> TUPA
                    </div>

                    <!--Card content-->
                    <div class="card-body">
                        <form name="form1" method="GET" action="iu_tupa.php">
                             <fieldset>
                                 <legend>Criterios de B&uacute;squeda</legend>
                                  <strong>Clase de Procedimiento:</strong>
                                      <? //Consulta para rellenar el combo Oficina
                                           $sqlTup="SP_CLASE_TUPA_LISTA_COMBO ";
                                           $rsTup=mssql_query($sqlTup,$cnx);
                                        ?>
                                  <select name="iCodTupaClase" class="FormPropertReg mdb-select colorful-select dropdown-primary"
                                          searchable="Buscar aqui.." id="TupaClase"  />
                                       <option value="">Seleccione:</option>
                                            <?  while ($RsTup=MsSQL_fetch_array($rsTup)){
                                                                        if($RsTup["iCodTupaClase"]==$_REQUEST[iCodTupaClase]){
                                                                            $selecClas="selected";
                                                                        }Else{
                                                                            $selecClas="";
                                                                        }
                                                                        echo "<option value=".$RsTup["iCodTupaClase"]." ".$selecClas.">".$RsTup["cNomTupaClase"]."</option>";
                                                                    }
                                                                    mssql_free_result($rsTup);
                                             ?>
                                  </select>

                                 <strong>Nombre de Procedimiento Tupa:</strong>
                                 <input name="cNomTupa" class="FormPropertReg form-control" type="text" value="<?=$_REQUEST[cNomTupa]?>" />

                                 <strong>Estado:</strong>
                                 <select name="txtestado"  class="FormPropertReg form-control" id="txtestado">
                                      <option value="" selected>Seleccione:</option>
                                                                        <?
                                                                        if ($_REQUEST[txtestado]==1){
                                                                            echo "<OPTION value=1 selected>Activo</OPTION> ";
                                                                        }
                                                                        else{
                                                                            echo "<OPTION value=1>Activo</OPTION> ";
                                                                        }
                                                                        if ($_REQUEST[txtestado]==2){
                                                                            echo "<OPTION value=2 selected>Inactivo</OPTION> ";
                                                                        }
                                                                        else{
                                                                            echo "<OPTION value=2>Inactivo</OPTION> ";
                                                                        }
                                                                        ?>
                                 </select>

                                 <button class="btn btn-primary" type="submit" name="Submit" onMouseOver="this.style.cursor='hand'">
                                     <b>Buscar</b>&nbsp;<img src="images/icon_buscar.png" width="17" height="17" border="0">
                                 </button>
                                 <button class="btn btn-primary"  name="Restablecer" onClick="window.open('<?=$PHP_SELF?>', '_self');" onMouseOver="this.style.cursor='hand'">
                                     <b>Restablecer</b>&nbsp;&nbsp;<img src="images/icon_clear.png" width="17" height="17" border="0">
                                 </button>
                                 <button class="btn btn-primary" onClick="window.open('iu_tupa_xls.php?cNomTupa=<?=$_GET[cNomTupa]?>&iCodTupaClase=<?=$_GET[iCodTupaClase]?>&txtestado=<?=$_GET[txtestado]?>&orden=<?=$orden?>&campo=<?=$campo?>&traRep=<?=$_SESSION['CODIGO_TRABAJADOR']?>', '_blank');" onMouseOver="this.style.cursor='hand'">
                                     <b>a Excel</b>&nbsp;<img src="images/icon_excel.png" width="17" height="17" border="0">
                                 </button>
                                 <button class="btn btn-primary" onClick="window.open('iu_tupa_pdf.php?cNomTupa=<?=$_GET[cNomTupa]?>&iCodTupaClase=<?=$_GET[iCodTupaClase]?>&txtestado=<?=$_GET[txtestado]?>&orden=<?=$orden?>&campo=<?=$campo?>', '_blank');" onMouseOver="this.style.cursor='hand'">
                                     <b>a Pdf</b>&nbsp;&nbsp;<img src="images/icon_pdf.png" width="17" height="17" border="0">
                                 </button>
                                 <a class="btn btn-default" href='iu_nuevo_tupa.php'>Nuevo Tupa</a>

                             </fieldset>
                         </form>
                        <hr>
                        <table class="table">
                          <?
                                        function paginar($actual, $total, $por_pagina, $enlace, $maxpags=0) {
                                            $total_paginas = ceil($total/$por_pagina);
                                            $anterior = $actual - 1;
                                            $posterior = $actual + 1;
                                            $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
                                            $maximo = $maxpags ? min($total_paginas, $actual+floor($maxpags/2)): $total_paginas;
                                            if ($actual>1)
                                                $texto = "<nav aria-label=\"Page navigation example\">  <ul class=\"pagination justify-content-end\">
                                              <li class=\"page-item\"><a class=\"page-link\" href=\"$enlace$anterior\">Anterior</a></li> ";
                                            else
                                                $texto = "<nav aria-label=\"Page navigation example\">  <ul class=\"pagination justify-content-end\">
                                              <li class=\"page-item disabled\"><a class=\"page-link\" href=\"#\">Anterior</a></li> ";
                                            if ($minimo!=1) $texto.= "... ";
                                            for ($i=$minimo; $i<$actual; $i++)
                                                $texto .= "  <li class=\"page-item\"><a class=\"page-link\" href=\"$enlace$i\">$i</a></li> ";
                                            $texto .= "<li class=\"page-item active\">
      <a class=\"page-link\" href=\"#\">$actual<span class=\"sr-only\">(current)</span></a>
    </li>";
                                            for ($i=$actual+1; $i<=$maximo; $i++)
                                                $texto .= "<li class=\"page-item\"><a class=\"page-link\" href=\"$enlace$i\">$i</a></li> ";
                                            if ($maximo!=$total_paginas) $texto.= "... ";
                                            if ($actual<$total_paginas)
                                                $texto .= "<li class=\"page-item\"><a class=\"page-link\" href=\"$enlace$posterior\">Siguiente</a></li></ul></nav>";
                                            else
                                                $texto .= "<li class=\"page-item disabled\"><a class=\"page-link\" href=\"$enlace$posterior\">Siguiente</a></li></ul></nav>";
                                            return $texto;
                                        }


                                        if (!isset($pag)) $pag = 1; // Por defecto, pagina 1
                                        $tampag = 10;
                                        $reg1 = ($pag-1) * $tampag;


                                        // ordenamiento
                                        if($_GET[campo]==""){
                                            $campo="Nombre";
                                        }Else{
                                            $campo=$_GET[campo];
                                        }

                                        if($_GET[orden]==""){
                                            $orden="ASC";
                                        }Else{
                                            $orden=$_GET[orden];
                                        }

                                        //invertir orden
                                        if($orden=="DESC") $cambio="ASC";
                                        if($orden=="ASC") $cambio="DESC";

                                        /*
                                        $sql="select * from Tra_M_Tupa ";
                                        $sql.=" WHERE iCodTupa>0 ";
                                        if($_GET[iCodTupaClase]!=""){
                                        $sql.=" AND iCodTupaClase='$_GET[iCodTupaClase]' ";
                                        }
                                        if($_GET[cNomTupa]!=""){
                                        $sql.=" AND cNomTupa like '%$_GET[cNomTupa]%' ";
                                        }
                                        if($_GET['txtestado']!=""){
                                        $sql.=" AND nEstado='".$_GET['txtestado']."'";
                                                }
                                        $sql.="ORDER BY iCodTupa ASC"; */
                                        $sql="SP_TUPA_LISTA '$_GET[iCodTupaClase]','%$_GET[cNomTupa]%','$_GET[txtestado]' ,'".$orden."' , '".$campo."'  ";
                                        $rs=mssql_query($sql,$cnx);
                                        $total = MsSQL_num_rows($rs);
                                        //echo $sql;
                                        ?>
                                        <tr>
                                            <td width="63" class="headCellColum"><a style=" text-decoration:<?if($campo=="Clase"){ echo "underline"; }Else{ echo "none";}?>" href="<?=$_SERVER['PHP_SELF']?>?campo=Clase&orden=<?=$cambio?>&cNomOficina=<?=$_GET[cNomOficina]?>">Clase</a></td>
                                            <td width="79" class="headCellColum">N&ordm;  de Requisitos</td>
                                            <td width="317" class="headCellColum"><a style=" text-decoration:<?if($campo=="Nombre"){ echo "underline"; }Else{ echo "none";}?>" href="<?=$_SERVER['PHP_SELF']?>?campo=Nombre&orden=<?=$cambio?>&cNomTupa=<?=$_GET[cNomTupa]?>">Tupa</a></td>
                                            <td width="74" class="headCellColum"><a style=" text-decoration:<?if($campo=="Silencio"){ echo "underline"; }Else{ echo "none";}?>" href="<?=$_SERVER['PHP_SELF']?>?campo=Silencio&orden=<?=$cambio?>">Silencio Adm.</a></td>
                                            <td width="84" class="headCellColum"><a style=" text-decoration:<?if($campo=="Dias"){ echo "underline"; }Else{ echo "none";}?>" href="<?=$_SERVER['PHP_SELF']?>?campo=Dias&orden=<?=$cambio?>">Dias de Tupa</a></td>
                                            <td width="67" class="headCellColum"><a style=" text-decoration:<?if($campo=="Estado"){ echo "underline"; }Else{ echo "none";}?>" href="<?=$_SERVER['PHP_SELF']?>?campo=Estado&orden=<?=$cambio?>&cNomOficina=<?=$_GET[cNomOficina]?>">Estado</a></td>
                                            <td width="70" class="headCellColum">Opciones</td>
                                            <td width="70" class="headCellColum">Op. Flujo</td>
                                        </tr>
                                        <?
                                        $numrows=MsSQL_num_rows($rs);
                                        if($numrows==0){
                                            echo "NO SE ENCONTRARON REGISTROS<br>";
                                            echo "TOTAL DE REGISTROS : ".$numrows;
                                        }else{
                                            echo "<div align=\"center\">TOTAL DE REGISTROS : $numrows  </div><br>";
                                            for ($i=$reg1; $i<min($reg1+$tampag, $total); $i++) {
                                                mssql_data_seek($rs, $i);
                                                $Rs=MsSQL_fetch_array($rs);
                                                                            //while ($Rs=MsSQL_fetch_array($rs)){
                                                if ($color == "#CEE7FF"){
                                                    $color = "#F9F9F9";
                                                }else{
                                                    $color = "#CEE7FF";
                                                }
                                                if ($color == ""){
                                                    $color = "#F9F9F9";
                                                }
                                                ?>
                                                <tr bgcolor="<?=$color?>">
                                                    <td><?
                                                        $sqlTuc="SELECT * FROM Tra_M_Tupa_Clase WHERE iCodTupaClase='$Rs[iCodTupaClase]'";
                                                        $rsTuc=mssql_query($sqlTuc,$cnx);
                                                        $RsTuc=MsSQL_fetch_array($rsTuc);
                                                        echo $RsTuc[cNomTupaClase];
                                                        ?></td>
                                                    <td align="center"><?
                                                        $sqlReq="SELECT * FROM Tra_M_Tupa_Requisitos WHERE iCodTupa='$Rs[iCodTupa]'";
                                                        $rsReq=mssql_query($sqlReq,$cnx);
                                                        $RsReq=MsSQL_num_rows($rsReq);
                                                        echo $RsReq;
                                                        ?></td>
                                                    <td align="left"><a href="../cInterfaseUsuario_SITD/iu_req_tupa.php?cod=<? echo $Rs[iCodTupa];?>&sw=8"><? echo utf8_encode($Rs[cNomTupa]);?></a></td>
                                                    <td align="center"><? if ($Rs[nSilencio]==1){
                                                            echo "SAP";
                                                        }
                                                        else if ($Rs[nSilencio]==0){
                                                            echo "SAN";
                                                        }
                                                        ?></td>
                                                    <td align="center"><? echo $Rs[nDias];?></td>
                                                    <td align="center"><? if($Rs[nEstado]==1){?>
                                                            <div style="color:#005E2F">Activo</div>
                                                        <? }Else{?>
                                                            <div style="color:#950000">Inactivo</div>
                                                        <? }?></td>
                                                    <td>


                                                            <a href="../cInterfaseUsuario_SITD/iu_actualiza_tupa.php?cod=<? echo $Rs[iCodTupa];?>&sw=7&s1=<?=$Rs[iCodTupaClase]?>&s2=<?=$Rs[iCodOficina]?>&iCodTupaClase=<?=$_REQUEST[iCodTupaClase]?>&cNomTupa=<?=$_REQUEST[cNomTupa]?>&txtestado=<?=$_REQUEST[txtestado]?>&pag=<?=$pag?>"><img src="images/icon_edit.png" width="16" height="16" alt="Actualizar Doc Tupa" border="0"></a>
                                                            <? if($RsReq==0){?>
                                                                <a href="../cLogicaNegocio_SITD/ln_elimina_tupa.php?id=<? echo $Rs[iCodTupa];?>&iCodTupaClase=<?=$_REQUEST[iCodTupaClase]?>&cNomTupa=<?=$_REQUEST[cNomTupa]?>&txtestado=<?=$_REQUEST[txtestado]?>&pag=<?=$pag?>" onClick='return ConfirmarBorrado();'"><img src="images/icon_del.png" width="16" height="16" alt="Eliminar Doc Tupa" border="0"></a>
                                                            <? }
                                                            else if($RsReq > 0){
                                                                ?>
                                                                <img src="images/icon_del_off.png" width="16" alt="No se puede Eliminar Doc Tupa" height="16" border="0">
                                                            <? }?>
                                                    </td>
                                                    <td>
                                                            <a href="registroDetalleFlujoTupa.php?iCodTupa=<?=$Rs[iCodTupa]?>" rel="lyteframe" title="Detalles Flujo Tupa" rev="width: 880px; height: 300px; scrolling: auto; border:no"><img src="images/icon_view.png" alt="Ver detalles del remitente" width="16" height="16" border="0"></a>
                                                            <a href="../cInterfaseUsuario_SITD/registroWorkFlow.php?cod=<? echo $Rs[iCodTupa];?>&sw=7&s1=<?=$Rs[iCodTupaClase]?>&s2=<?=$Rs[iCodOficina]?>"><img src="images/workflow.png" width="28" height="20" alt="Generar Flujo de Documentos Tupa" border="0"></a>
                                                    </td>
                                                </tr>

                                                <?
                                            }
                                        }
                                        ?>
                        </table>
                        <? echo paginar($pag, $total, $tampag, "iu_tupa.php?iCodTupaClase=".$_GET[iCodTupaClase]."&cNomTupa=".$_GET[cNomTupa]."&txtestado=".$_GET[txtestado]."&pag=");?>


                    </div>

                </div>
                <!--/.Card-->

            </div>
            <!--Grid column-->
        </div>
        <!--Grid column-->
    </div>
    <!--Grid column-->
</main>
<!--Main layout-->


<?include("includes/userinfo.php");?>


<?include("includes/pie.php");?>
    <script>
        function ConfirmarBorrado()
        {
            if (confirm("Esta seguro de eliminar el registro?")){
                return true;
            }else{
                return false;
            }
        }
        $(document).ready(function() {
            $('.mdb-select').material_select();

        });
    </script>
    <script type="text/javascript" language="javascript" src="includes/lytebox.js"></script>
</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>