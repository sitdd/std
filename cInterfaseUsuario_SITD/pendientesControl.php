<?php
session_start();
date_default_timezone_set('America/Lima');
if($_SESSION['CODIGO_TRABAJADOR']!=""){

    function cifras($num){
        if(strlen($num)==1){
            return "0".$num;
        }else{
            return $num;
        }
    }

    function fecha($fecha){
        $a=explode(" ",$fecha);
        $b=$a[0];

        $aa=explode("-",$b);
        $bb=cifras($aa[0])."-".cifras($aa[1])."-".$aa[2];
        return $bb;
    }
    ?>
    <!DOCTYPE html>
    <html lang="es">
    <head>
        <?php include("includes/head.php"); ?>
        <link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />
        <link type="text/css" rel="stylesheet" href="css/dhtmlgoodies_calendar.css" media="screen"/>
        <style>
            .wrapper {
                width: auto%;
                position: absolute;
                z-index: 100;
                height: auto;
            }
            #sidebar {
                display: none;
            }
            #tablaResutado{
                width: 100%;
            }
            #sidebar.active {
                display: flex;
                width: 95%;
            }
            #sidebar label{
                font-size: 0.8rem!important;
                margin-bottom: 0!important;
            }
            .info-end ,.page-link{
                font-size: 0.8rem!important;
            }
            @media (min-width: 576px) {
                #sidebar.active {
                    width: 80%;
                }
            }
            @media (min-width: 768px) {
                #sidebar.active {
                    width: 60%;
                }
            }
            @media (min-width: 992px) {
                #sidebar.active {
                    width: 60%;
                }
                #sidebarCollapse{
                    margin-left: -35px!important;
                }
                .info-end ,.page-link{
                    font-size: 1rem!important;
                }
            }
            @media (min-width: 1200px) {
                #sidebar.active {
                    width: 38%;
                }
            }

            .dropdown-content li>a, .dropdown-content li>span {
                font-size: 0.8rem!important;
            }
            .select-wrapper .search-wrap {
                padding-top: 0rem!important;
                margin: 0 0.2rem!important;
            }
            .select-wrapper input.select-dropdown {
                font-size: 0.9rem!important;
            }
            .md-form{
                margin-bottom: 0.5rem!important;
            }

        </style>
    </head>
    <body>

    <?include("includes/menu.php");?>

    <!--Main layout-->
    <main class="mx-lg-5">
        <div class="container-fluid">
            <!--Grid row-->
            <div class="row wow fadeIn">
                <!--Grid column-->
                <div class="col-md-12 mb-12">
                    <!--Card-->
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header text-center "> Bandeja de pendientes</div>
                        <!--Card content-->
                        <div class="card-body d-flex px-4 px-lg-5">
                            <div class="wrapper">
                                <nav class="navbar-expand py-0">
                                    <button type="button" id="sidebarCollapse" class="botenviar float-left" style="padding: 0rem 8px!important; border: none; margin-left: -18px; border-radius: 10px;">
                                        <i class="fas fa-align-right"></i>
                                    </button>
                                </nav>
                                <!-- Sidebar -->
                                <nav id="sidebar" class="py-0">
                                    <div class="card">
                                        <div class="card-header">Criterios de Búsqueda</div>
                                        <div class="card-body">
                                            <form name="frmConsulta" method="GET">
                                                <div class="form-row">
                                                    <div class="col-md-12">
                                                        Documentos:
                                                        <div class="form-check form-check-inline">
                                                            <input type="checkbox" name="Entrada" value="1" class="form-check-input" id="Entrada" <?if($_GET[Entrada]==1) echo "checked"?>  >
                                                            <label class="form-check-label" for="Entrada">Entrada</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input type="checkbox" name="Interno" value="1" class="form-check-input" id="Internos" <?if($_GET[Interno]==1) echo "checked"?>  >
                                                            <label class="form-check-label" for="Internos">Internos</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input type="checkbox" name="Anexo" value="1" class="form-check-input" id="Anexos" <?if($_GET[Anexo]==1) echo "checked"?>  >
                                                            <label class="form-check-label" for="Anexos">Anexos</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="md-form">
                                                            <input placeholder="dd-mm-aaaa" value="<?=$_GET[fDesde]?>" type="text"
                                                                   id="fDesde" name="fDesde" class="FormPropertReg form-control datepicker">
                                                            <label for="fDesde">Desde:</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="md-form">
                                                            <input placeholder="dd-mm-aaaa" value="<?=$_GET[fHasta]?>" type="text"
                                                                   id="fHasta" name="fHasta" class="FormPropertReg form-control datepicker">
                                                            <label for="fHasta">Hasta:</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="md-form">
                                                            <input type="text" id="cCodificacion" name="cCodificacion" value="<?=$_GET[cCodificacion]?>" size="28" class="FormPropertReg form-control">
                                                            <label for="cCodificacion">N&ordm; Documento:</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="md-form">
                                                            <input type="text" id="cAsunto" name="cAsunto" value="<?=$_GET[cAsunto]?>" size="65" class="FormPropertReg form-control">
                                                            <label for="cAsunto">Asunto:</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="">Tipo Documento:</label>
                                                        <select name="cCodTipoDoc" class="FormPropertReg mdb-select colorful-select dropdown-primary"
                                                                searchable="Buscar aqui..">
                                                            <option value="">Seleccione:</option>
                                                            <?
                                                            include_once("../conexion/conexion.php");
                                                            $sqlTipo="SELECT * FROM Tra_M_Tipo_Documento ";
                                                            $sqlTipo.="ORDER BY cDescTipoDoc ASC";
                                                            $rsTipo=mssql_query($sqlTipo,$cnx);
                                                            while ($RsTipo=MsSQL_fetch_array($rsTipo)){
                                                                if($RsTipo["cCodTipoDoc"]==$_GET[cCodTipoDoc]){
                                                                    $selecTipo="selected";
                                                                }Else{
                                                                    $selecTipo="";
                                                                }
                                                                echo  utf8_encode("<option value=".$RsTipo["cCodTipoDoc"]." ".$selecTipo.">".$RsTipo["cDescTipoDoc"]."</option>");
                                                            }
                                                            mssql_free_result($rsTipo);
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="">Responsable:</label>
                                                        <select name="iCodTrabajadorResponsable" class="FormPropertReg mdb-select colorful-select dropdown-primary"
                                                                searchable="Buscar aqui..">
                                                            <option value="">Seleccione:</option>
                                                            <?php
                                                            $sqlTrbR="SELECT * FROM Tra_M_Trabajadores ";
                                                            $sqlTrbR.="WHERE iCodOficina = '$_SESSION[iCodOficinaLogin]' ";
                                                            $sqlTrbR .= "ORDER BY cApellidosTrabajador ASC";
                                                            $rsTrbR=mssql_query($sqlTrbR,$cnx);
                                                            while ($RsTrbR=MsSQL_fetch_array($rsTrbR)){
                                                                if($RsTrbR[iCodTrabajador]==$_GET[iCodTrabajadorResponsable]){
                                                                    $selecTrabR="selected";
                                                                }Else{
                                                                    $selecTrabR="";
                                                                }
                                                                echo utf8_encode("<option value=\"".$RsTrbR["iCodTrabajador"]."\" ".$selecTrabR.">".$RsTrbR["cApellidosTrabajador"]." ".$RsTrbR["cNombresTrabajador"]."</option>");
                                                            }
                                                            mssql_free_result($rsTrbR);
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="">Tema:</label>
                                                        <select name="iCodTema" class="FormPropertReg mdb-select colorful-select dropdown-primary"
                                                                searchable="Buscar aqui..">
                                                            <option value="">Seleccione:</option>
                                                            <?php
                                                            $sqlTem="SELECT * FROM Tra_M_Temas WHERE  iCodOficina = '$_SESSION[iCodOficinaLogin]'    ";
                                                            $sqlTem .= "ORDER BY cDesTema ASC";
                                                            $rsTem=mssql_query($sqlTem,$cnx);
                                                            while ($RsTem=MsSQL_fetch_array($rsTem)){
                                                                if($RsTem[iCodTema]==$_GET[iCodTema]){
                                                                    $selecTem="selected";
                                                                }Else{
                                                                    $selecTem="";
                                                                }
                                                                echo "<option value=\"".$RsTem["iCodTema"]."\" ".$selecTem.">".$RsTem["cDesTema"]." ".$RsTem["cNombresTrabajador"]."</option>";
                                                            }
                                                            mssql_free_result($rsTem);
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="">Delegado:</label>
                                                        <select name="iCodTrabajadorDelegado" class="FormPropertReg mdb-select colorful-select dropdown-primary"
                                                                searchable="Buscar aqui..">
                                                            <option value="">Seleccione:</option>
                                                            <?
                                                            $sqlTrb="SELECT * FROM Tra_M_Trabajadores ";
                                                            $sqlTrb.="WHERE iCodOficina = '$_SESSION[iCodOficinaLogin]' And nFlgEstado=1   ";
                                                            $sqlTrb .= "ORDER BY cApellidosTrabajador ASC";
                                                            $rsTrb=mssql_query($sqlTrb,$cnx);
                                                            while ($RsTrb=MsSQL_fetch_array($rsTrb)){
                                                                if($RsTrb[iCodTrabajador]==$_GET[iCodTrabajadorDelegado]){
                                                                    $selecTrab="selected";
                                                                }Else{
                                                                    $selecTrab="";
                                                                }
                                                                echo utf8_encode("<option value=\"".$RsTrb["iCodTrabajador"]."\" ".$selecTrab.">".$RsTrb["cApellidosTrabajador"]." ".$RsTrb["cNombresTrabajador"]."</option>");
                                                            }
                                                            mssql_free_result($rsTrb);
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-12">
                                                        Estado:
                                                        <div class="form-check form-check-inline">
                                                            <input type="checkbox" name="Aceptado" value="1" class="form-check-input" id="Aceptado" <?if($_GET[Aceptado]==1) echo "checked"?>  >
                                                            <label class="form-check-label" for="Aceptado">Aceptado</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input type="checkbox" name="SAceptado" value="1" class="form-check-input" id="SAceptado" <?if($_GET[SAceptado]==1) echo "checked"?>  >
                                                            <label class="form-check-label" for="SAceptado">Sin Aceptar</label>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <button class="btn btn-outline-primary waves-effect btn-sm" onclick="Buscar();"
                                                            onMouseOver="this.style.cursor='hand'">
                                                        <b>Buscar</b>
                                                        <img src="images/icon_buscar.png" width="17" height="17" border="0">
                                                    </button>
                                                    <button class="btn btn-outline-default waves-effect btn-sm" onclick="window.open('<?=$PHP_SELF?>', '_self');" onMouseOver="this.style.cursor='hand'"> <b>Restablecer</b> <img src="images/icon_clear.png" width="17" height="17" border="0"> </button>
                                                    <button class="btn btn-outline-info waves-effect btn-sm" onclick="window.open('pendientesControlExcel.php?', '_self'); return false;" onMouseOver="this.style.cursor='hand'"> <b>a Excel</b> <img src="images/icon_excel.png" width="17" height="17" border="0"> </button>
                                                    <button class="btn btn-outline-primary waves-effect btn-sm" onclick="window.open('pendientesControlPdf.php?fDesde=<?=$_GET[fDesde]?>&fHasta=<?=$_GET[fHasta]?>&Entrada=<?=$_GET[Entrada]?>&Interno=<?=$_GET[Interno]?>&Anexo=<?=$_GET[Anexo]?>&cCodificacion=<?=$_GET[cCodificacion]?>&cAsunto=<?=$_GET[cAsunto]?>&cCodTipoDoc=<?=$_GET[cCodTipoDoc]?>&iCodTrabajadorResponsable=<?=$_GET[iCodTrabajadorResponsable]?>&iCodTrabajadorDelegado=<?=$_GET[iCodTrabajadorDelegado]?>&iCodTema=<?=$_GET[iCodTema]?>&EstadoMov=<?=$_GET[EstadoMov]?>&Aceptado=<?=$_GET[Aceptado]?>&SAceptado=<?=$_GET[SAceptado]?>', '_blank');" onMouseOver="this.style.cursor='hand'"> <b>a Pdf</b> <img src="images/icon_pdf.png" width="17" height="17" border="0"> </button>
                                                </div>

                                            </form>

                                        </div>
                                    </div>
                                </nav>
                            </div>

                            <form name="formulario">
                                <input type="hidden" name="opcion" value="">
                                <input type="hidden" name="Entradax" value="<?php echo $_GET['Entrada'];?>"  />
                                <input type="hidden" name="Internox" value="<?php echo $_GET['Interno'];?>"  />
                                <input type="hidden" name="Anexox" value="<?php echo $_GET['Anexo'];?>"  />
                                <input type="hidden" name="fDesdex" value="<?php echo $_GET['fDesde'];?>"  />
                                <input type="hidden" name="fHastax" value="<?php echo $_GET['fHasta'];?>"  />
                                <input type="hidden" name="cCodificacionx" value="<?php echo $_GET['cCodificacion'];?>"  />
                                <input type="hidden" name="cAsuntox" value="<?php echo $_GET['cAsunto'];?>"  />
                                <input type="hidden" name="cCodTipoDocx" value="<?php echo $_GET['cCodTipoDoc'];?>"  />
                                <input type="hidden" name="iCodTrabajadorDelegadox" value="<?php echo $_GET['iCodTrabajadorDelegado'];?>"  />
                                <input type="hidden" name="iCodTemax" value="<?php echo $_GET['iCodTema'];?>"  />
                                <input type="hidden" name="Aceptadox" value="<?php echo $_GET['Aceptado'];?>"  />
                                <input type="hidden" name="SAceptadox" value="<?php echo $_GET['SAceptado'];?>"  />
                                <input type="hidden" name="pagx" value="<?php echo $_GET['pag'];?>"  />
                                <?php
                                if($_GET[fDesde]!=""){ $fDesde=date("Ymd", strtotime($_GET[fDesde])); }
                                if($_GET[fHasta]!=""){
                                    $fHasta=date("Y-m-d", strtotime($_GET[fHasta]));

                                    function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
                                        $date_r = getdate(strtotime($date));
                                        $date_result = date("Ymd", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),($date_r["year"]+$yy)));
                                        return $date_result;
                                    }
                                    $fHasta=dateadd($fHasta,1,0,0,0,0,0); // + 1 dia
                                }
                                function paginar($actual, $total, $por_pagina, $enlace, $maxpags=0) {
                                    $total_paginas = ceil($total/$por_pagina);
                                    $anterior = $actual - 1;
                                    $posterior = $actual + 1;
                                    $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
                                    $maximo = $maxpags ? min($total_paginas, $actual+floor($maxpags/2)): $total_paginas;
                                    if ($actual>1)
                                        $texto = "<a href=\"$enlace$anterior\"><</a> ";
                                    else
                                        $texto = "<b><</b> ";
                                    if ($minimo!=1) $texto.= "... ";
                                    for ($i=$minimo; $i<$actual; $i++)
                                    {
                                        $enlace_new=$enlace.$i;
                                        $texto .= "<a href=\"$enlace_new\">$i</a> ";
                                    }

                                    $texto .= "<b>$actual</b> ";
                                    for ($i=$actual+1; $i<=$maximo; $i++){
                                        $enlace_new=$enlace.$i;
                                        $texto .= "<a href=\"$enlace_new\">$i</a> ";
                                    }

                                    if ($maximo!=$total_paginas) $texto.= "... ";
                                    if ($actual<$total_paginas){
                                        $enlace_new=$enlace.$posterior;

                                        $texto .= "<a href=\"$enlace_new\">></a>";}
                                    else
                                        $texto .= "<b>></b>";
                                    return $texto;
                                }

                                $pag=$_GET['pag'];
                                if (!isset($pag)) $pag = 1; // Por defecto, pagina 1
                                $tampag = 20;
                                $reg1 = ($pag-1) * $tampag;

                                // ordenamiento
                                if($_GET[campo]==""){
                                    $campo="Fecha";
                                }Else{
                                    $campo=$_GET[campo];
                                }

                                if($_GET[orden]==""){
                                    $orden="DESC";
                                }Else{
                                    $orden=$_GET[orden];
                                }

                                //invertir orden
                                if($orden=="ASC") $cambio="DESC";
                                if($orden=="DESC") $cambio="ASC";


                                $sqlTra = " SP_BANDEJA_PENDIENTES '$fDesde','$fHasta','$_GET[Entrada]','$_GET[Interno]','$_GET[Anexo]','%$_GET[cCodificacion]%', ";
                                $sqlTra.= "'%$_GET[cAsunto]%','$_GET[cCodTipoDoc]','$_GET[iCodTrabajadorResponsable]','$_GET[iCodTrabajadorDelegado]','$_GET[iCodTema]','$_GET[EstadoMov]','$_GET[Aceptado]','$_GET[SAceptado]','$_SESSION[iCodOficinaLogin]','$campo','$orden' ";

                                $rsTra = mssql_query($sqlTra,$cnx);
                                $total = mssql_num_rows($rsTra);
                                ?>

                                <div class="row-form ">
                                    <div class="col-md-12 row-inline">
                                        <button class="FormBotonAccion btn btn-primary btn-rounded btn-sm" name="OpAceptar" disabled onclick="activaAceptar();"
                                                onMouseOver="this.style.cursor='hand'" style="FILTER:alpha(opacity=50)">
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td><img src="images/icon_aceptar.png" width="17" height="17" border="0"></td>
                                                    <td>&nbsp;Aceptar</td>
                                                </tr>
                                            </table>
                                        </button>
                                        <span style="font-size:18px">&#124;&nbsp;</span>
                                        <button class="FormBotonAccion btn btn-primary btn-rounded btn-sm" name="OpDerivar" disabled onclick="activaDerivar();"
                                                onMouseOver="this.style.cursor='hand'" style="FILTER:alpha(opacity=50)">
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td><img src="images/icon_derivar.png" width="17" height="17" border="0"></td>
                                                    <td>&nbsp;Derivar</td>
                                                </tr>
                                            </table>
                                        </button>
                                        <span style="font-size:18px">&#124;&nbsp;</span>
                                        <button class="FormBotonAccion btn btn-primary btn-rounded btn-sm" name="OpDelegar" disabled onclick="activaDelegar();"
                                                onMouseOver="this.style.cursor='hand'" style="FILTER:alpha(opacity=50)">
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td><img src="images/icon_delegar.png" width="17" height="17" border="0"></td>
                                                    <td>&nbsp;Delegar</td>
                                                </tr>
                                            </table>
                                        </button>
                                        <span style="font-size:18px">&#124;&nbsp;</span>
                                        <button class="FormBotonAccion btn btn-primary btn-rounded btn-sm" name="OpFinalizar" disabled onclick="activaFinalizar();"
                                                onMouseOver="this.style.cursor='hand'" style="FILTER:alpha(opacity=50)">
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td><img src="images/icon_finalizar.png" width="17" height="17" border="0"></td>
                                                    <td>&nbsp;Finalizar</td>
                                                </tr>
                                            </table>
                                        </button>
                                        <span style="font-size:18px">&#124;&nbsp;</span>
                                        <button class="FormBotonAccion btn btn-primary btn-rounded btn-sm" name="OpAvance" disabled onclick="activaAvance();"
                                                onMouseOver="this.style.cursor='hand'" style="FILTER:alpha(opacity=50)">
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td><img src="images/icon_avance.png" width="17" height="17" border="0"></td>
                                                    <td>&nbsp;Avance</td>
                                                </tr>
                                            </table>
                                        </button>
                                    </div>
                                    <hr>
                                    <div class="table-responsive">
                                        <table class="table table-sm">
                                            <thead>
                                            <tr>
                                                <th class="headColumnas"></th>
                                                <th class="headColumnas">
                                                    <a href="<?=$_SERVER['PHP_SELF']?>?campo=Codigo&orden=<?=$cambio?>&Tra_M_Tramite.cCodificacion=<?=$_GET[cCodificacion]?>"  style=" text-decoration:<?if($campo=="Codigo"){ echo "underline"; }Else{ echo "none";}?>">
                                                        N&ordm; Trámite</a>
                                                </th>
                                                <th class="headColumnas">
                                                    <a href="<?=$_SERVER['PHP_SELF']?>?campo=Documento&orden=<?=$cambio?>&nFlgTipoDoc=<?=$_GET[nFlgTipoDoc]?>"  style=" text-decoration:<?if($campo=="Documento"){ echo "underline"; }Else{ echo "none";}?>">Tipo de Documento
                                                    </a>
                                                </th>
                                                <th class="headColumnas"><a href="<?=$_SERVER['PHP_SELF']?>?campo=cNombre&orden=<?=$cambio?>&cNombre=<?=$_GET[cNombre]?>"  style=" text-decoration:<?if($campo=="cNombre"){ echo "underline"; }Else{ echo "none";}?>">
                                                        Nombre / Razón Social</a>
                                                </th>
                                                <th class="headColumnas">
                                                    <a href="<?=$_SERVER['PHP_SELF']?>?campo=Asunto&orden=<?=$cambio?>&cAsunto=<?=$_GET[cAsunto]?>"  style=" text-decoration:<?if($campo=="Asunto"){ echo "underline"; }Else{ echo "none";}?>">Asunto / Procedimiento TUPA</a>
                                                </th>
                                                <th class="headColumnas">Derivado Por</th>
                                                <th class="headColumnas">
                                                    <a href="<?=$_SERVER['PHP_SELF']?>?campo=Recepcion&orden=<?=$cambio?>&fFecRecepcion=<?=$_GET[fFecRecepcion]?>"  style=" text-decoration:<?if($campo=="Recepcion"){ echo "underline"; }Else{ echo "none";}?>">Recepción</a>
                                                </th>
                                                <th class="headColumnas">
                                                    <a href="<?=$_SERVER['PHP_SELF']?>?campo=Trabajador&orden=<?=$cambio?>&iCodTrabajadorDerivar=<?=$_GET[iCodTrabajadorDerivar]?>"  style=" text-decoration:<?if($campo=="Trabajador"){ echo "underline"; }Else{ echo "none";}?>">
                                                        Responsable/ Delegado</a>
                                                </th>
                                                <th class="headColumnas">
                                                    <a href="<?=$_SERVER['PHP_SELF']?>?campo=Estado&orden=<?=$cambio?>&cCodificacion=<?=$_GET[cCodificacion]?>"  style=" text-decoration:<?if($campo=="Estado"){ echo "underline"; }Else{ echo "none";}?>">Estado / Avance</a>
                                                </th>
                                                <th class="headColumnas">Opción</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $numrows=MsSQL_num_rows($rsTra);
                                            if($numrows==0){
                                                echo "NO SE ENCONTRARON REGISTROS<br>";
                                            }else{
                                                while ($RsTra=MsSQL_fetch_array($rsTra)){
                                                    if ($color == "#DDEDFF"){
                                                        $color = "#F9F9F9";
                                                    }else{
                                                        $color = "#DDEDFF";
                                                    }
                                                    if ($color == ""){
                                                        $color = "#F9F9F9";
                                                    }
                                                    ?>
                                                    <tr bgcolor="<?=$color?>" onMouseOver="this.style.backgroundColor='#BFDEFF'"
                                                        OnMouseOut="this.style.backgroundColor='<?=$color?>'" >
                                                        <td>
                                                            <? if($RsTra[fFecRecepcion]!=""){?>
                                                                <div class="form-check">
                                                                    <input type="checkbox" name="MovimientoAccion[]" id="ma<?=$RsTra[iCodMovimiento]?>" value="<?=$RsTra[iCodMovimiento]?>" class="form-check-input" onclick="activaOpciones4();" >
                                                                    <label class="form-check-label" for="ma<?=$RsTra[iCodMovimiento]?>"></label>
                                                                </div>
                                                            <? }?>
                                                        </td>
                                                        <td >
                                                            <?if($RsTra[nFlgTipoDoc]==1){?>
                                                                <a href="registroDetalles.php?iCodTramite=<?=$RsTra[iCodTramite]?>&iCodMovimiento=<?=$RsTra[iCodMovimiento]?>&nFlgEstado=<?=$RsTra[nFlgEstado]?>" rel="lyteframe" title="Detalle del Tr?mite" rev="width: 970px; height: 550px; scrolling: auto; border:no"><?=$RsTra[cCodificacion]?></a>
                                                            <?}
                                                            if ($RsTra[nFlgTipoDoc]==2){
                                                                echo "INTERNO ".$RsTra['cCodificacionI'];}
                                                            if($RsTra[nFlgTipoDoc]==3){
                                                                echo "SALIDA";}
                                                            if($RsTra[nFlgTipoDoc]==4){
                                                                ?>
                                                                <a href="registroDetalles.php?iCodTramite=<?=$RsTra[iCodTramiteRel]?>&iCodMovimiento=<?=$RsTra[iCodMovimiento]?>&nFlgEstado=<?=$RsTra[nFlgEstado]?>" rel="lyteframe" title="Detalle del Tr?mite" rev="width: 970px; height: 550px; scrolling: auto; border:no"><?=$RsTra[cCodificacion]?></a>
                                                            <?}?>
                                                            <br>
                                                            <b>
                                                                <?php
                                                                if(ltrim(rtrim($RsTra[cPrioridadDerivar]))=="Alta"){
                                                                    echo "<font color='#ff0000'>Alta</font>";
                                                                }elseif(ltrim(rtrim($RsTra[cPrioridadDerivar]))=="Media"){
                                                                    echo "<font color='#07b52f'>Media</font>";
                                                                }elseif(ltrim(rtrim($RsTra[cPrioridadDerivar]))=="Baja"){
                                                                    echo "<font color='#aecc05'>Baja</font>";
                                                                }
                                                                ?>
                                                            </b>
                                                            <?php
                                                            $date = date_create(strtotime($RsTra[fFecRegistro]));
                                                            //$date = date_create($RsTra[fFecRegistro]);
                                                            $fFecRegistro = date_format($date, 'd-m-Y H:i:s');

                                                            echo "<div style=color:#727272>".$fFecRegistro."</div>";
                                                            echo "<div style=color:#727272;font-size:10px>".$horaRegistro."</div>";
                                                            if($RsTra[cFlgTipoMovimiento]==4){
                                                                echo "<div style=color:#FF0000;font-size:12px>Copia</div>";
                                                            }
                                                            $sqlTrax = "SELECT * FROM Tra_M_Tramite TRA
                                    INNER JOIN Tra_M_Trabajadores TRAB ON TRA.iCodTrabajadorRegistro = TRAB.iCodTrabajador
                                    WHERE TRA.iCodTramite = ".$RsTra[iCodTramite];
                                                            //$sqlTrax="SELECT cApellidosTrabajador,cNombresTrabajador FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsTra[iCodTrabajadorRegistro]'";
                                                            $rsTrax=mssql_query($sqlTrax,$cnx);
                                                            $RsTrax=MsSQL_fetch_array($rsTrax);
                                                            echo utf8_encode("<div style=color:#808080;>".$RsTrax[cNombresTrabajador]." ".$RsTrax[cApellidosTrabajador]."</div>");
                                                            if ($RsTrax['ES_EXTERNO'] == 1) {
                                                                echo "<div style=color:#FF00FF;>Usuario Web</div>";
                                                            }
                                                            ?>
                                                        </td>
                                                        <td >
                                                            <? echo utf8_encode($RsTra[cDescTipoDoc]);
                                                            if($RsTra[nFlgTipoDoc]==1 ){
                                                                echo utf8_encode("<div style=color:#808080;text-transform:uppercase>".$RsTra[cNroDocumento]."</div>");
                                                            }
                                                            else if($RsTra[nFlgTipoDoc]==2 ){
                                                                echo "<br>";
                                                                echo "<a style=\"color:#0067CE\" href=\"registroOficinaDetalles.php?iCodTramite=".$RsTra[iCodTramite]."\" rel=\"lyteframe\" title=\"Detalle del Tr?mite\" rev=\"width: 970px; height: 450px; scrolling: auto; border:no\">";
                                                                echo utf8_encode($RsTra[cCodificacion]);
                                                                echo "</a>";
                                                            }
                                                            else if($RsTra[nFlgTipoDoc]==3 ){
                                                                echo "<br>";
                                                                echo "<a style=\"color:#0067CE\" href=\"registroSalidaDetalles.php?iCodTramite=".$RsTra[iCodTramite]."\" rel=\"lyteframe\" title=\"Detalle del Tr?mite\" rev=\"width: 970px; height: 290px; scrolling: auto; border:no\">";
                                                                echo utf8_encode($RsTra[cCodificacion]);
                                                                echo "</a>";
                                                            }
                                                            ?>
                                                        </td>
                                                        <td >
                                                            <?
                                                            $rsRem=mssql_query("SELECT * FROM Tra_M_Remitente WHERE iCodRemitente='$RsTra[iCodRemitente]'",$cnx);
                                                            $RsRem=MsSQL_fetch_array($rsRem);
                                                            if($RsRem[cTipoPersona]=='1'){
                                                                echo utf8_encode("<div style=color:#000000;>".$RsRem[cNombre]."</div>");
                                                                echo "<div style=color:#0154AF;font-size:10px;text-align:left>DNI: ".$RsRem[nNumDocumento]."</div>";
                                                            }else if ($RsRem[cTipoPersona]=='2') {
                                                                echo utf8_encode("<div style=color:#000000;>".$RsRem[cNombre]."</div>");
                                                                echo utf8_encode("<div style=color:#408080;>".$RsTra[cNomRemite]."</div>");
                                                                echo "<div style=color:#0154AF;font-size:10px;>RUC:".$RsRem[nNumDocumento]."</div>";
                                                            } else {
                                                                echo utf8_encode("<div style=color:#000000;>".$RsRem[cNombre]."</div>");
                                                            }
                                                            mssql_free_result($rsRem);

                                                            if($RsTra[cFlgTipoMovimiento]==3){
                                                                echo utf8_encode("<div style=color:#006600;><b>ANEXO</b></div>");
                                                            }
                                                            ?>
                                                        </td>
                                                        <td >
                                                            <?
                                                            echo $RsTra[cAsunto];
                                                            if($RsTra[iCodTupa]!=""){
                                                                $sqlTup="SELECT * FROM Tra_M_Tupa WHERE iCodTupa='$RsTra[iCodTupa]'";
                                                                $rsTup=mssql_query($sqlTup,$cnx);
                                                                $RsTup=MsSQL_fetch_array($rsTup);
                                                                echo utf8_encode("<div style=color:#0154AF>".$RsTup["cNomTupa"]."</div");
                                                            }
                                                            ?>

                                                        </td>
                                                        <td>
                                                            <?php
                                                            $sqlSig = "SP_OFICINA_LISTA_AR '$RsTra[iCodOficinaOrigen]'";
                                                            $rsSig  = mssql_query($sqlSig,$cnx);
                                                            $RsSig  = mssql_fetch_array($rsSig);
                                                            echo utf8_encode($RsSig["cSiglaOficina"]);
                                                            mssql_free_result($rsSig);
                                                            if($RsTra[iCodTramiteDerivar]!=""){
                                                                $sqlTraD = "SELECT cCodificacion, iCodTramite,cCodTipoDoc FROM tra_M_Tramite 
                                                      WHERE iCodTramite='$RsTra[iCodTramiteDerivar]'";
                                                                $rsTraD  = mssql_query($sqlTraD,$cnx);
                                                                $RsTraD  = mssql_fetch_array($rsTraD);
                                                                $sqlTipDoc1 = "SELECT * FROM Tra_M_Tipo_Documento WHERE cCodTipoDoc='$RsTraD[cCodTipoDoc]'";
                                                                $rsTipDoc1  = mssql_query($sqlTipDoc1,$cnx);
                                                                $RsTipDoc1  = mssql_fetch_array($rsTipDoc1);
                                                                echo utf8_encode("<div style=color:#0154AF;font-size:10px align=left>".$RsTipDoc1[cDescTipoDoc]."</div>");
                                                                echo utf8_encode("<div style=color:#0154AF;font-size:10px align=left>".$RsTraD[cCodificacion]."</div>");
                                                            }
                                                            $sqlIndic = "SELECT * FROM Tra_M_Indicaciones WHERE iCodIndicacion='$RsTra[iCodIndicacionDerivar]'";
                                                            $rsIndic=mssql_query($sqlIndic,$cnx);
                                                            $RsIndic=MsSQL_fetch_array($rsIndic);
                                                            echo utf8_encode("<div style=color:#808080;font-size:10px align=left>".$RsIndic["cIndicacion"]."</div>");
                                                            mssql_free_result($rsIndic);
                                                            ?>

                                                            <?php
                                                            $sqlfecha1 =  "SELECT iCodMovimiento,iCodTramite,iCodOficinaOrigen,fFecRecepcion,iCodOficinaDerivar,iCodTrabajadorDerivar,
                                    cCodTipoDocDerivar,cAsuntoDerivar,cObservacionesDerivar,fFecDerivar,iCodTrabajadorDelegado,fFecDelegado, 
                                    nEstadoMovimiento,cFlgTipoMovimiento,cNumDocumentoDerivar,cReferenciaDerivar,iCodTramiteDerivar 
                                FROM Tra_M_Tramite_Movimientos 
                                WHERE (iCodTramite='$RsTra[iCodTramite]' OR iCodTramiteRel='$RsTra[iCodTramite]') 
                                            AND (cFlgTipoMovimiento=1 OR cFlgTipoMovimiento=3 OR cFlgTipoMovimiento=5) 
                                ORDER BY iCodMovimiento DESC";
                                                            $rsFecha1 = mssql_query($sqlfecha1,$cnx);

                                                            while ($RsFecha1 = mssql_fetch_array($rsFecha1)) {
                                                                if ($_SESSION['iCodOficinaLogin'] == $RsFecha1['iCodOficinaDerivar']) {
                                                                    $fFecDerivar = $RsFecha1['fFecDerivar'];
                                                                }
                                                            }
                                                            if (isset($fFecDerivar)) {
                                                                $datefd = date_create(strtotime($fFecDerivar));
                                                                echo "<div style=color:#0154AF>".date_format($datefd, 'd-m-Y')."</div>";
                                                                echo "<div style=color:#0154AF;font-size:10px>".date_format($datefd, 'G:i')."</div>";
                                                            }else{
                                                                $rsFecha1 = mssql_query($sqlfecha1,$cnx);
                                                                $RsFecha1 = mssql_fetch_array($rsFecha1);
                                                                echo "<div style=color:#0154AF>".date("d-m-Y", strtotime($RsFecha1[fFecDerivar]))."</div>";
                                                                echo "<div style=color:#0154AF;font-size:10px>".date("G:i", strtotime($RsFecha1[fFecDerivar]))."</div>";
                                                            }
                                                            ?>

                                                            <? 	if($RsTra[iCodMovAsociado]!=""){
                                                                $sqlAsoc="SELECT cNumDocumentoDerivar FROM Tra_M_Tramite_Movimientos WHERE iCodMovimiento==$RsTra[iCodMovAsociado] ";
                                                                $rsAsoc=mssql_query($sqlAsoc,$cnx);
                                                                $numAsoc=MsSQL_num_rows($rsAsoc);
                                                                $RsModA=MsSQL_fetch_array($rsAsoc);
                                                                if($numAsoc>0){
                                                                    echo "Doc. Asociado </br>";
                                                                    echo $RsModA[cNumDocumentoDerivar];
                                                                }} ?>

                                                        </td>
                                                        <td >
                                                            <?php
                                                            //$date = date_create(strtotime($RsTra[fFecRegistro]));
                                                            ///
                                                            $sqlfecha =  "SELECT TOP 1 iCodMovimiento, iCodTramite,iCodOficinaOrigen,fFecRecepcion,iCodOficinaDerivar,iCodTrabajadorDerivar,
                                    cCodTipoDocDerivar,cAsuntoDerivar, cObservacionesDerivar, fFecDerivar,iCodTrabajadorDelegado,fFecDelegado, 
                                    nEstadoMovimiento,cFlgTipoMovimiento, cNumDocumentoDerivar,cReferenciaDerivar,iCodTramiteDerivar 
                                FROM Tra_M_Tramite_Movimientos 
                                WHERE (iCodTramite='$RsTra[iCodTramite]' OR iCodTramiteRel='$RsTra[iCodTramite]') 
                                            AND (cFlgTipoMovimiento=1 OR cFlgTipoMovimiento=3 OR cFlgTipoMovimiento=5) 
                                ORDER BY iCodMovimiento DESC";
                                                            $rsFecha = mssql_query($sqlfecha,$cnx);
                                                            $RsFecha = mssql_fetch_array($rsFecha);

                                                            // $date = date_create($RsTra[fFecRegistro]);
                                                            // $fFecRecepcion = date_format($date, 'd-m-Y H:i:s');

                                                            // if($RsTra[fFecRecepcion] == ""){
                                                            // 	echo "<div style=color:#ff0000>sin aceptar</div>";
                                                            // }else{
                                                            // 	echo "<div style=color:#0154AF>aceptado</div>";
                                                            // 	echo "<div style=color:#0154AF>".$fFecDelegadoRecepcion."</div>";
                                                            // }
                                                            //   		$date 				 = date_create($RsFecha['fFecRecepcion']);
                                                            // $fFecRecepcion = date_format($date,'d-m-Y H:i:s');

                                                            if($RsTra[fFecRecepcion] == ""){
                                                                echo "<div style=color:#ff0000>sin aceptar</div>";
                                                            }else{
                                                                // $fechaArray = explode('-',$RsTra[fFecRecepcion]);
                                                                // $elAnio = substr($fechaArray[2],0,4);
                                                                // if ($elAnio == '1900') {
                                                                // 	echo "<div style=color:#0154AF>aceptado</div>";
                                                                // }else{
                                                                // 	echo "<div style=color:#0154AF>aceptado</div>";
                                                                // 	echo "<div style=color:#0154AF>".$RsFecha['fFecRecepcion']."</div>";
                                                                // }
                                                                echo "<div style=color:#0154AF>aceptado</div>";
                                                                echo "<div style=color:#0154AF>".$RsFecha['fFecRecepcion']."</div>";
                                                            }
                                                            ?>

                                                        </td>
                                                        <td>
                                                            <?
                                                            $rsResp=mssql_query("SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsTra[iCodTrabajadorDerivar]'",$cnx);
                                                            $RsResp=MsSQL_fetch_array($rsResp);
                                                            echo utf8_encode($RsResp["cApellidosTrabajador"]." ".$RsResp["cNombresTrabajador"]);
                                                            mssql_free_result($rsResp);

                                                            if($RsTra[iCodTrabajadorDelegado]!=""){
                                                                $sqlDelg="SELECT * FROM Tra_M_Trabajadores WHERE iCodTrabajador='$RsTra[iCodTrabajadorDelegado]' ";
                                                                $rsDelg=mssql_query($sqlDelg,$cnx);
                                                                $RsDelg=MsSQL_fetch_array($rsDelg);
                                                                echo "<div style=color:#005B2E;font-size:12px>".$RsDelg["cApellidosTrabajador"]." ".$RsDelg["cNombresTrabajador"]."</div>";
                                                                mssql_free_result($rsDelg);
                                                                if($RsTra[fFecDelegadoRecepcion]==""){
                                                                    echo "<div style=color:#ff0000>sin aceptar</div>";
                                                                }Else{

                                                                    $sql= "select fFecDelegado from Tra_M_Tramite_Movimientos where iCodTramite='".$RsTra[iCodTramite]."'";
                                                                    $query=mssql_query($sql,$cnx);
                                                                    $rs=mssql_fetch_assoc($query);
                                                                    do{
                                                                        if($rs['fFecDelegado']!=''){
                                                                            $tiempo=$rs['fFecDelegado'];
                                                                        }
                                                                    }while($rs=mssql_fetch_assoc($query));
                                                                    echo "<div style=color:#0154AF>aceptado</div>";
                                                                    //echo "<div style=color:#0154AF>".date("d-m-Y", strtotime($RsTra[fFecDelegadoRecepcion]))."</div>";
                                                                    echo "<div style=color:#0154AF>".$tiempo."</div>";
                                                                    //echo "<div style=color:#0154AF;font-size:10px>".date("G:i", strtotime($RsTra[fFecDelegadoRecepcion]))."</div>";
                                                                }
                                                            }
                                                            if($RsTra[nEstadoMovimiento]==4){ //respondido
                                                                echo "<a style=\"color:#0067CE\" href=\"pendientesControlVerRpta.php?iCodMovimiento=".$RsTra[iCodMovimiento]."\" rel=\"lyteframe\" title=\"Detalle Respuesta\" rev=\"width: 500px; height: 300px; scrolling: auto; border:no\">RESPONDIDO</a>";
                                                            }
                                                            ?>

                                                        </td>
                                                        <td>
                                                            <?
                                                            if($RsTra[fFecRecepcion]==""){
                                                                switch ($RsTra[nEstadoMovimiento]){
                                                                    case 1:
                                                                        echo "Pendiente";
                                                                        break;
                                                                    case 2:
                                                                        echo "Derivado";
                                                                        break;
                                                                    case 3:
                                                                        echo "Asignado";
                                                                        break;
                                                                    case 4:
                                                                        echo "Finalizado";
                                                                        break;
                                                                }
                                                            }else{
                                                                echo "En Proceso";
                                                            }
                                                            $sqlAvan = "SELECT TOP(1) * FROM Tra_M_Tramite_Avance WHERE iCodMovimiento = '$RsTra[iCodMovimiento]' 
                                                  ORDER BY iCodAvance DESC";
                                                            $rsAvan = mssql_query($sqlAvan,$cnx);
                                                            if(mssql_num_rows($rsAvan) > 0){
                                                                $RsAvan = mssql_fetch_array($rsAvan);
                                                                echo "<hr>";
                                                                //echo "<div style=font-size:10px>".$RsAvan[cObservacionesAvance]."</div>";
                                                            }
                                                            ?>
                                                            <a href="listadoDeAvances.php?iCodTramite=<?=$RsTra[iCodTramite]?>&iCodOficinaRegistro=<?=$_SESSION[iCodOficinaLogin]?>&iCodMovimiento=<?=$RsTra[iCodMovimiento]?>" rel="lyteframe" title="Listado de Avances" rev="width: 600px; height: 400px; scrolling: auto; border:no">
                                                                <img src="images/page_add.png" width="22" height="20" border="0">
                                                            </a>
                                                        </td>
                                                        <td width="78" valign="middle">
                                                            <?php
                                                            if($RsTra[cFlgTipoMovimiento] != 4){?>
                                                                <?if($RsTra[fFecRecepcion] == ""){ ?>
                                                                    <div class="form-check">
                                                                        <input type="checkbox" name="iCodMovimiento[]" id="ma2"value="<?=$RsTra[iCodMovimiento]?>"  class="form-check-input" onclick="activaOpciones1();" >
                                                                        <label class="form-check-label" for="ma2"></label>
                                                                    </div>

                                                                <?}else{?>
                                                                    <?if($RsTra[cFlgTipoMovimiento]!=3 ){ ?>
                                                                        <div class="form-check">
                                                                            <input type="radio" class="form-check-input" id="iCodMovimientoAccion<?=$RsTra[iCodMovimiento]?>" value="<?=$RsTra[iCodMovimiento]?>" onclick="activaOpciones2();" name="iCodMovimientoAccion">
                                                                            <label class="form-check-label" for="iCodMovimientoAccion<?=$RsTra[iCodMovimiento]?>"></label>
                                                                        </div>

                                                                    <?}else{ ?>
                                                                        <div class="form-check">
                                                                            <input type="radio" class="form-check-input" id="iCodMovimientoAccion<?=$RsTra[iCodMovimiento]?>" value="<?=$RsTra[iCodMovimiento]?>" onclick="activaOpciones2();" name="iCodMovimientoAccion">
                                                                            <label class="form-check-label" for="iCodMovimientoAccion<?=$RsTra[iCodMovimiento]?>"></label>
                                                                        </div>
                                                                    <?}?>

                                                                    <?if($RsTra[iCodTupa]=="" && $RsTra[nFlgTipoDoc]==1){?>
                                                                        <a href="registroDiasVigencia.php?iCodTramite=<?=$RsTra[iCodTramite]?>"  rel="lyteframe" title="Ingresar Plazo de tramite" rev="width: 440px; height: 180px; scrolling: auto; border:no"><img src="images/icon_calendar.png" width="22" height="20" border="0"></a>
                                                                    <?}?>

                                                                    <?if($RsTra[nFlgTipoDoc]==1 or $RsTra[nFlgTipoDoc]==2){?>
                                                                        <a href="registroTemaSelect.php?iCodTramite=<?=$RsTra[iCodTramite]?>&iCodOficinaRegistro=<?=$_SESSION[iCodOficinaLogin]?>"  rel="lyteframe" title="Vincular Tema" rev="width: 600px; height: 400px; scrolling: auto; border:no"><img src="images/page_add.png" width="22" height="20" border="0"></a>
                                                                    <?}?>
                                                                    <!-- Inicio Max -->
                                                                    <?php
                                                                    if($RsTra[flg_libreblanco] == '' OR $RsTra[flg_libreblanco] == '0'){
                                                                        ?>
                                                                        <a title="Libro Blanco" href="flg_libroblanco.php?opcion=1&iCodTramite=<?=$RsTra[iCodTramite]?>&page=1">
                                                                            <img src="images/notebook_0.png" border='0' alt="Libro Blanco">
                                                                        </a>
                                                                        <?php
                                                                    }else{
                                                                        ?>
                                                                        <a title="Libro Blanco" href="flg_libroblanco.php?opcion=0&iCodTramite=<?=$RsTra[iCodTramite]?>&page=1">
                                                                            <img src="images/notebook_1.png" border='0' alt="Libro Blanco">
                                                                        </a>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <!-- Fin Max -->
                                                                <?}?>
                                                                <?
                                                            }
                                                            else if($RsTra[cFlgTipoMovimiento]==4){
                                                                if($RsTra[fFecRecepcion]==""){ ?>
                                                                    <div class="form-check">
                                                                        <input type="checkbox" name="iCodMovimiento[]"  id="ma<?=$RsTra[iCodMovimiento]?>" value="<?=$RsTra[iCodMovimiento]?>"  class="form-check-input" onclick="activaOpciones1();" >
                                                                        <label class="form-check-label" for="ma<?=$RsTra[iCodMovimiento]?>"></label>
                                                                    </div>
                                                                <? }else{
                                                                    if($RsTra[nFlgTipoDoc]==3){ ?>
                                                                        <div class="form-check">
                                                                            <input type="radio" class="form-check-input" id="iCodMovimientoAccion<?=$RsTra[iCodMovimiento]?>" value="<?=$RsTra[iCodMovimiento]?>" onclick="activaOpciones2();" name="iCodMovimientoAccion">
                                                                            <label class="form-check-label" for="iCodMovimientoAccion<?=$RsTra[iCodMovimiento]?>"></label>
                                                                        </div>
                                                                    <? }else{ ?>
                                                                        <div class="form-check">
                                                                            <input type="radio" class="form-check-input" id="iCodMovimientoAccion<?=$RsTra[iCodMovimiento]?>" value="<?=$RsTra[iCodMovimiento]?>" onclick="activaOpciones2();" name="iCodMovimientoAccion">
                                                                            <label class="form-check-label" for="iCodMovimientoAccion<?=$RsTra[iCodMovimiento]?>"></label>
                                                                        </div>
                                                                        <!-- <input type="radio" name="iCodMovimientoAccion" value="<?=$RsTra[iCodMovimiento]?>" onclick="activaOpciones2();"> -->
                                                                        <a href="registroTemaSelect.php?iCodTramite=<?=$RsTra[iCodTramite]?>&iCodOficinaRegistro=<?=$_SESSION[iCodOficinaLogin]?>"  rel="lyteframe" title="Vincular Tema" rev="width: 600px; height: 400px; scrolling: auto; border:no"><img src="images/page_add.png" width="22" height="20" border="0"></a>
                                                                    <?php		}
                                                                }
                                                            }// fin de cFlgTipoMovimiento = 4
                                                            ?>
                                                            <?php
                                                            $tramitePDF   = mssql_query("select descripcion,* from Tra_M_Tramite where iCodTramite='$RsTra[iCodTramite]'",$cnx);
                                                            $RsTramitePDF = mssql_fetch_object($tramitePDF);

                                                            if (strlen(rtrim(ltrim($RsTramitePDF->descripcion)))>0) {
                                                                ?>
                                                                <hr>

                                                                <a href="ver_digital.php?iCodTramite=<?=$RsTra[iCodTramite]?>" rel="lyteframe" title="Ver Detalle Documento" rev="width: 970px; height: 550px; scrolling: auto; border:no">
                                                                    <img src="images/1471041812_pdf.png" border="0" height="17" width="17">
                                                                </a>

                                                                <a href="pdf_digital.php?iCodTramite=<?=$RsTra[iCodTramite]?>" title="Descargar Detalle Documento" rev="width: 970px; height: 550px; scrolling: auto; border:no" target="_blank">
                                                                    <img src="images/icon_print.png" border="0" height="17" width="17">
                                                                </a>

                                                                <!--a href="pdf_digital.php?iCodTramite=<?php echo $RsTramitePDF->iCodTramite;?>" target="_blank" title="Documento">
                            <img src="images/1471041812_pdf.png" border="0" height="17" width="17">
                        </a-->
                                                            <?php }
                                                            $consulta  = "SELECT * FROM Tra_M_Tramite_Digitales WHERE iCodTramite =  $RsTra[iCodTramite]";
                                                            $resultado = mssql_query($consulta,$cnx);
                                                            $data      = mssql_fetch_assoc($resultado);
                                                            if ($data["cNombreNuevo"]) {
                                                                $a = '../cAlmacenArchivos/&file='.trim($data["cNombreNuevo"]);
                                                                $b = $RsTra[iCodMovimiento];
                                                                $c = $RsTra[nFlgEstado];
                                                                ?>
                                                                <script>
                                                                    function url(a,b,c){
                                                                        var URLactual = window.location;
                                                                        window.open('download.php?direccion='+a+'&iCodMovimiento='+b+'&nFlgEstado='+c, '_blank');
                                                                        setTimeout('document.location.reload()',100)
                                                                    }
                                                                </script>
                                                                <a href="javascript:url('<?php echo $a;?>','<?php echo $b?>','<?php echo $c?>')" title="Documento Complementario">
                                                                    <?php
                                                                    echo "<img src=images/icon_download.png border=0 width=16 height=16 alt=\"".trim($data["cNombreNuevo"])."\">";
                                                                    ?>
                                                                </a>
                                                                <?php
                                                            }
                                                            /*echo "<a title=\"Documento Complementario\" href=\"download.php?direccion=".$nombre_fichero."&iCodMovimiento=".$RsTra[iCodMovimiento]."&nFlgEstado=".$RsTra[nFlgEstado]."\"><img src=images/icon_download.png border=0 width=16 height=16 alt=\"".trim($data["cNombreNuevo"])."\"></a>";*/
                                                            ?>


                                                            <?if($RsTra[iCodTupa]=="" && $RsTra[nFlgTipoDoc]==1){?>
                                                                <hr>
                                                                <?php

                                                                if($RsTra['FechaPlazoFinal']==''){
                                                                    echo "";
                                                                }else if(fecha($RsTra['FechaPlazoFinal'])==date('d-m-Y')){
                                                                    echo "Hoy Vence";
                                                                }else if(fecha($RsTra['FechaPlazoFinal'])>date('d-m-Y')){
                                                                    $fecha = strtotime(date('Y-m-d'));
                                                                    $fecha2 = strtotime(date($RsTra['FechaPlazoFinal']));
                                                                    $min = 60; //60 segundos
                                                                    $hora = 60*$min;
                                                                    $dia = 24*$hora;
                                                                    $mes = date('t')*$dia;
                                                                    $diasfinalies=floor($fecha2/$dia) - floor($fecha/$dia);

                                                                    echo "Quedan ".$diasfinalies." Dia(s)";
                                                                }else if(fecha($RsTra['FechaPlazoFinal'])<date('d-m-Y')){
                                                                    $fecha = strtotime(date('Y-m-d'));
                                                                    $fecha2 = strtotime(date($RsTra['FechaPlazoFinal']));
                                                                    $min = 60; //60 segundos
                                                                    $hora = 60*$min;
                                                                    $dia = 24*$hora;
                                                                    $mes = date('t')*$dia;
                                                                    $diasfinalies=floor($fecha2/$dia) - floor($fecha/$dia);

                                                                    echo "Vencio hace ".abs($diasfinalies)." Dia(s)";
                                                                }
                                                                ?>
                                                                <?php
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <?
                                                }
                                            }
                                            mssql_free_result($rsTra);
                                            ?>
                                            </tbody>


                                        </table>
                                        <?php echo "TOTAL DE REGISTROS : ".$numrows; ?>
                                    </div>

                                </div>
                                <div class="row-form">
                                    <?php echo paginar($_GET['pag'], $total, $tampag, "pendientesControl.php?fDesde=".$_GET[fDesde]."&fHasta=".$_GET[fHasta]."&cCodificacion=".$_GET[cCodificacion]."&cAsunto=".$_GET[cAsunto]."&cCodTipoDoc=".$_GET[cCodTipoDoc]."&iCodTrabajadorResponsable=".$_GET[iCodTrabajadorResponsable]."&iCodTrabajadorDelegado=".$_GET[iCodTrabajadorDelegado]."&Entrada=".$_GET[Entrada]."&Interno=".$_GET[Interno]."&Anexo=".$_GET[Anexo]."&Aceptado=".$_GET[Aceptado]."&SAceptado=".$_GET[SAceptado]."&iCodTema=".$_GET[iCodTema]."&campo=".$campo."&orden=".$orden."&pag="); ?>
                                </div>



                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php include("includes/userinfo.php"); ?>
    <?include("includes/pie.php");?>
    <script type="text/javascript" language="javascript" src="includes/lytebox.js"></script>
    <script type="text/javascript" src="scripts/dhtmlgoodies_calendar.js"></script>
    <script Language="JavaScript">
        $(document).ready(function() {
            $('.datepicker').pickadate({
                monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
                format: 'dd-mm-yyyy',
                formatSubmit: 'dd-mm-yyyy',
            });
            $('.mdb-select').material_select();

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });


        });
        function activaOpciones1(){
            for (j=0; j < document.formulario.elements.length; j++){
                if(document.formulario.elements[j].type == "radio"){
                    document.formulario.elements[j].checked = 0;
                }
            }
            document.formulario.OpAceptar.disabled=false;
            document.formulario.OpDerivar.disabled=true;
            document.formulario.OpDelegar.disabled=true;
            document.formulario.OpFinalizar.disabled=true;
            document.formulario.OpAvance.disabled=true;
            document.formulario.OpAceptar.filters.alpha.opacity=100;
            document.formulario.OpDerivar.filters.alpha.opacity=50;
            document.formulario.OpDelegar.filters.alpha.opacity=50;
            document.formulario.OpFinalizar.filters.alpha.opacity=50;
            document.formulario.OpAvance.filters.alpha.opacity=50;
            return false;
        }

        function activaOpciones2(){
            for (i=0;i<document.formulario.elements.length;i++){
                if(document.formulario.elements[i].type == "checkbox"){
                    document.formulario.elements[i].checked=0;
                }
            }
            document.formulario.OpAceptar.disabled=true;
            document.formulario.OpDerivar.disabled=false;
            document.formulario.OpDelegar.disabled=false;
            document.formulario.OpFinalizar.disabled=false;
            document.formulario.OpAvance.disabled=false;
            document.formulario.OpAceptar.filters.alpha.opacity=50;
            document.formulario.OpDerivar.filters.alpha.opacity=100;
            document.formulario.OpDelegar.filters.alpha.opacity=100;
            document.formulario.OpFinalizar.filters.alpha.opacity=100;
            document.formulario.OpAvance.filters.alpha.opacity=100;
            return false;
        }

        function activaOpciones5(){
            for (i=0;i<document.formulario.elements.length;i++){
                if(document.formulario.elements[i].type == "checkbox"){
                    document.formulario.elements[i].checked=0;
                }
            }
            document.formulario.OpAceptar.disabled=true;
            document.formulario.OpDerivar.disabled=true; // CUANDO ES COPIA Y SE HA ACEPTADO ENTONCES NO SE PUEDE DERIVAR
            document.formulario.OpDelegar.disabled=false;
            document.formulario.OpFinalizar.disabled=false;
            document.formulario.OpAvance.disabled=false;
            document.formulario.OpAceptar.filters.alpha.opacity=50;
            document.formulario.OpDerivar.filters.alpha.opacity=100;
            document.formulario.OpDelegar.filters.alpha.opacity=100;
            document.formulario.OpFinalizar.filters.alpha.opacity=100;
            document.formulario.OpAvance.filters.alpha.opacity=100;
            return false;
        }

        function activaOpciones3(){
            for (i=0;i<document.formulario.elements.length;i++){
                if(document.formulario.elements[i].type == "checkbox"){
                    document.formulario.elements[i].checked=0;
                }
            }
            document.formulario.OpAceptar.disabled=true;
            document.formulario.OpDerivar.disabled=true;
            document.formulario.OpDelegar.disabled=false;
            document.formulario.OpFinalizar.disabled=false;
            document.formulario.OpAvance.disabled=true;
            document.formulario.OpAceptar.filters.alpha.opacity=50;
            document.formulario.OpDerivar.filters.alpha.opacity=50;
            document.formulario.OpDelegar.filters.alpha.opacity=50;
            document.formulario.OpFinalizar.filters.alpha.opacity=100;
            document.formulario.OpAvance.filters.alpha.opacity=50;
            return false;
        }

        function activaOpciones4(){
            for (j=0;j<document.formulario.elements.length;j++){
                if(document.formulario.elements[j].type == "radio"){
                    document.formulario.elements[j].checked=0;
                }
            }
            document.formulario.OpAceptar.disabled=true;
            document.formulario.OpDerivar.disabled=false;
            document.formulario.OpDelegar.disabled=false;
            document.formulario.OpFinalizar.disabled=false;
            document.formulario.OpAvance.disabled=false;
            document.formulario.OpAceptar.filters.alpha.opacity=50;
            document.formulario.OpDerivar.filters.alpha.opacity=100;
            document.formulario.OpDelegar.filters.alpha.opacity=100;
            document.formulario.OpFinalizar.filters.alpha.opacity=100;
            document.formulario.OpAvance.filters.alpha.opacity=100;
            return false;
        }

        function activaAceptar()
        {
            document.formulario.opcion.value=1;
            document.formulario.method="POST";
            document.formulario.action="pendientesData.php";
            document.formulario.submit();
        }

        function activaDerivar()
        {
            document.formulario.OpAceptar.value="";
            document.formulario.OpDerivar.value="";
            document.formulario.OpDelegar.value="";
            document.formulario.OpFinalizar.value="";
            document.formulario.OpAvance.value="";
            document.formulario.opcion.value=1;
            document.formulario.method="POST";
            document.formulario.action="pendientesControlDerivar.php";
            document.formulario.submit();
        }

        function activaDelegar()
        {
            document.formulario.method="POST";
            document.formulario.action="pendientesControlDelegar.php";
            document.formulario.submit();
        }

        function activaFinalizar()
        {
            document.formulario.method="POST";
            document.formulario.action="pendientesControlFinalizar.php";
            document.formulario.submit();
        }

        function activaAvance()
        {
            document.formulario.method="POST";
            document.formulario.action="pendientesControlAvance.php";
            document.formulario.submit();
        }

        function Buscar()
        {
            document.frmConsulta.action="<?=$PHP_SELF?>";
            document.frmConsulta.submit();
        }
    </script>
    </body>
    </html>

    <?
}Else{
    header("Location: ../index.php?alter=5");
}
?>