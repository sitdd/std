<?php session_start();
    include_once("../conexion/conexion.php");
?>
<?php
if($_SESSION['CODIGO_TRABAJADOR']!=""){
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?include("includes/head.php");?>
<link type="text/css" rel="stylesheet" href="includes/lytebox.css" media="screen" />

</head>
<body>
<?include("includes/menu.php");?>
<!--Main layout-->
<main class="mx-lg-5">
    <div class="container-fluid">
        <!--Grid row-->
        <div class="row wow fadeIn justify-content-center">
            <!--Grid column-->
            <div class="col-11 col-sm-10 col-md-8 col-lg-6 col-xl-4">
                <!--Card-->
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header text-center ">  AGREGAR NUEVA OFICINA-PERFIL </div>
                    <!--Card content-->
                    <div class="card-body">
                        <?php
                        $sql= "select * from Tra_M_Trabajadores where iCodTrabajador='$_GET[cod]'";
                        $rs=mssql_query($sql,$cnx);
                        $Rs=MsSQL_fetch_array($rs);
                        ?>
                        <form action="data_nuevo_nivel.php" method="post"  name="form1" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                            <input type="hidden" name="op" value="<?php echo $_GET['op'];?>">
                            <div class="form-row justify-content-center">
                                <div class="col-12">
                                    <label class="select">Oficina</label>
                                    <select name="oficina" class="FormPropertReg mdb-select colorful-select dropdown-primary"   searchable="Buscar aqui..">
                                        <option value="">Seleccione:</option>
                                        <?php
                                            $sqlPer = "SELECT * FROM Tra_M_Oficinas ORDER BY cNomOficina ";
                                            $rsPer  = mssql_query($sqlPer,$cnx);
                                            while ($RsPer = mssql_fetch_array($rsPer)){
                                                echo utf8_encode("<option value=".$RsPer["iCodOficina"].">".$RsPer["cNomOficina"]." | ".$RsPer["cSiglaOficina"]."</option>");
                                            }
                                            mssql_free_result($rsPer);
                                        ?>
                                    </select>
                                </div>
                                <div class="col-12">
                                    <label class="select">Perfil</label>
                                    <select name="perfil" class="FormPropertReg mdb-select colorful-select dropdown-primary"   searchable="Buscar aqui..">
                                        <option value="">Seleccione:</option>
                                        <?php
                                            $sqlPer="select * from Tra_M_Perfil";
                                            $rsPer=mssql_query($sqlPer,$cnx);
                                            while ($RsPer=MsSQL_fetch_array($rsPer)){
                                                echo "<option value=".$RsPer["iCodPerfil"].">".$RsPer["cDescPerfil"]."</option>";
                                            }
                                            mssql_free_result($rsPer);
                                        ?>
                                    </select>
                                </div>
                                <div class="col- my-3">
                                    <input class="botenviar" type="submit" value="Agregar">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php include("includes/userinfo.php"); ?>
<?php include("includes/pie.php"); ?>

  <script type="text/javascript" language="javascript" src="ckeditor/adapters/jquery.js"></script>
    <script>
        function ConfirmarBorrado(){
            if (confirm("Esta seguro de eliminar el registro?")){
                return true;
            }else{
                return false;
            }
        }
        $(document).ready(function() {
            $('.mdb-select').material_select();
        });
    </script>
</body>
</html>

<?
}else{
   header("Location: ../index.php?alter=5");
}
?>
