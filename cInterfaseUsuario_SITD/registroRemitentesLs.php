<? header('Content-Type: text/html; charset=UFT-8');
/**************************************************************************************
NOMBRE DEL PROGRAMA: PendienteData.php
SISTEMA: SISTEMA  DE TRÁMITE DOCUMENTARIO DIGITAL
OBJETIVO: Seleccion remitente
PROPIETARIO: AGENCIA PERUANA DE COOPERACIÓN INTERNACIONAL

 
CONTROL DE VERSIONES:
Ver   Autor                 Fecha          Descripción
------------------------------------------------------------------------
1.0   APCI    12/11/2010      Creación del programa.
------------------------------------------------------------------------
*****************************************************************************************/
session_start();
If($_SESSION['CODIGO_TRABAJADOR']!=""){
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv=Content-Type content=text/html; charset=utf-8>
<title>SITDD</title>

<link type="text/css" rel="stylesheet" href="css/tramite.css" media="screen" />
<SCRIPT LANGUAGE="JavaScript">
<!-- Begin
function sendValue (s,t){
var selvalue1 = s.value;
var selvalue2 = t.value;
window.opener.document.getElementById('cNombreRemitente').value = selvalue1;
window.opener.document.getElementById('iCodRemitente').value = selvalue2;
window.opener.document.getElementById('Remitente').value = selvalue2;
window.close();
}
//  End -->
</script>
</head>
<body>
 
<div class="container">

<h2  align="center">
	Seleccione Remitente:
</h2>
		<table width="100%" border="1" cellpadding="0" cellspacing="3">
			<form method="GET" name="formulario" action="<?=$_SERVER['PHP_SELF']?>">
		<tr>
		<td align="left" colspan="3">
				<table width="100%">
				<tr>
				<td>
							Nombre: <input type="text" name="cNombreBuscar" value="<?=$_GET[cNombreBuscar]?>" style="width:320px">
				</td>
				<td align="right">
							Nro. Documento: <input type="text" name="nNumDocumento" value="<?=$_GET[nNumDocumento]?>" size="10">
							<input type="submit" class="btn btn-primary" name="buscar" value="Buscar">
				</td>
				</tr>
				</table>
		</td>
		</tr>
			</form>
		<tr>
			<td  width="560">NOMBRE REMITENTE &nbsp;&nbsp;/&nbsp;&nbsp; N&ordm; DOCUMENTO</td>
			<td align="center"    width="70">OPCION</td>
		</tr>			
		<?
		include_once("../conexion/conexion.php");
		if($_GET[buscar]==""){
			$sqlRem="SELECT TOP 10 * FROM Tra_M_Remitente ";
		}Else{
			$sqlRem="SELECT TOP 500 * FROM Tra_M_Remitente ";
		}
		$sqlRem.="WHERE cNombre IS NOT NULL ";
		if($_GET[cNombreBuscar]!=""){
		$sqlRem.="AND cNombre LIKE '%$_GET[cNombreBuscar]%' ";
		}
		if($_GET[nNumDocumento]!=""){
		$sqlRem.="AND nNumDocumento='$_GET[nNumDocumento]' ";
		}
    $sqlRem.="ORDER BY cNombre ASC";
    $rsRem=mssql_query($sqlRem,$cnx);
    while ($RsRem=MsSQL_fetch_array($rsRem)){
    if ($color == "#e8f3ff"){
			$color = "#FFFFFF";
	  }else{
			$color = "#e8f3ff";
	  }
	  if ($color == ""){
			$color = "#FFFFFF";
	  }
		?>
    <tr bgcolor="<?=$color?>" onMouseOver="this.style.backgroundColor='#BFDEFF';" onMouseOut="this.style.backgroundColor='<?=$color?>'">
    <td width="560" style="font-family:'Arial Narrow';font-size:11px"><?=$RsRem["cNombre"]?> <?if($RsRem["nNumDocumento"]!="") echo "- <font color=#A96705><b>".trim($RsRem["nNumDocumento"])."</b></font>"?></td>
			<form name="selectform">
    <td width="70">
    	<input name="cNombreRemitente" value="<?=trim($RsRem["cNombre"])?>" type="hidden">
    	<input name="iCodRemitente" value="<?=trim($RsRem[iCodRemitente])?>" type="hidden">
		<input type=button value="seleccione" class="btn btn-primary" style="font-size:8px" onClick="sendValue(this.form.cNombreRemitente,this.form.iCodRemitente);">
    </td>
    	</form>
    </tr>
    <?
    }
    mssql_free_result($rsRem);
		?>
		</table>
<div>		
					</div>
                 </div>
             </div>
         </div>
     </div>
 </main>

</body>
</html>

<?
}Else{
   header("Location: ../index.php?alter=5");
}
?>