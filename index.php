<?php
session_start();
if($_SESSION['CODIGO_TRABAJADOR']){
    header( "Location: cInterfaseUsuario_SITD/main.php" );
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <title>SISTEMA DE TRAMITE DOCUMENTARIO</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    <link href="static/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="static/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="static/css/login.css" rel="stylesheet">
</head>

<body>

<div class="container-fluid h-100">

    <div class="row h-100 justify-content-center align-items-center">

        <div class="col col-sm-8 col-md-6 col-lg-5 col-xl-4">

            <div class="login-container w-75 mx-auto">

                <!--form login -->
                <?php
                switch ($_GET["alter"]) {
                    case 2:
                        $observacion = "<h7><b>SALIDA</b><br>Ha salido correctamente</h7>";
                        $notificacion="
                                <br>
                                <div class='rectangle'>
                                    <div class='notification-text'>
                                        $observacion
                                    </div>
                                </div>
                            ";
                        break;
                    case 3:
                        $observacion = "<h7><b>ERROR</b> - datos vacios<br>Ingrese correctamente</h7>";
                        $notificacion="
                                <br>
                                <div class='rectangle'>
                                    <div class='notification-text'>
                                        $observacion
                                    </div>
                                </div>
                            ";
                        break;
                    case 4:
                        $observacion = "<h7>ERROR... <b>Clave incorrecta</b> o <br>es <b>Cuenta Incorrecta</b></h7>";
                        $notificacion="
                                <br>
                                <div class='rectangle'>
                                    <div class='notification-text'>
                                        $observacion
                                    </div>
                                </div>
                            ";
                        break;
                    case 5:
                        $observacion = "<h7>ERROR... <b>Usuario no autorizado</b></h7>";
                        $notificacion="
                                <br>
                                <div class='rectangle'>
                                    <div class='notification-text'>
                                        $observacion
                                    </div>
                                </div>
                            ";
                        break;
                };
                if (isset($notificacion)){
                ?>
                    <!--Card Danger-->
                    <div class="card pink lighten-2 text-center z-depth-2">
                        <div class="card-body">
                            <p class="white-text mb-0"><?php echo $notificacion;?></p>
                        </div>
                    </div>
                <?php } ?>
                <form class="form-login text-center p-4" method="POST" action="cInterfaseUsuario_SITD/login.php" aria-label="Login" name="Datos">

                    <i class="far fa-user icon" id="iconlogin"></i>

                    <p class="h4 icon">Iniciar Sesión</p>


                    <!--User id-->
                    <div class="md-form text-white text-left">
                        <i class="fas fa-user-circle icon prefix"></i>
                        <input type="text" name="usuario" id="usuario" class="form-control validate text-white">
                        <label class="text-login text-white" for="usuario" data-error="Incorrecto" data-success="Correcto">Usuario o Correo</label>
                    </div>
                    <!--/.User id-->


                    <!--Password-->
                    <div class="md-form text-white text-left">
                        <i class="fa fa-lock icon prefix"></i>
                        <input type="password" id="contrasena" name="contrasena" class="form-control validate text-white mb-0">
                        <label class="text-login text-white" for="contrasena" data-error="Incorrecto" data-success="Correcto">Contraseña</label>
                    </div>
                    <!--/.Password-->


                    <div class="d-flex justify-content-around">
                        <div>
                            <!-- Remember me -->
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="recordar">
                                <label class="custom-control-label text-white" for="recordar">Recordarme</label>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <!-- Sign in button -->
                        <button class="btn  grey darken-4" type="submit" onclick="loguear()">Ingresar</button>
                        <a class="btn btn-link text-center" href="#">
                            ¿Olvidaste tu Contraseña?
                        </a>
                    </div>

                </form>
                <!--/.form login -->
            </div>

        </div>

    </div>


</div>

<script type="text/javascript">
    function loguear() {
        if (document.Datos.usuario.value == "") {
            alert("Ingrese Usuario");
        } else if (document.Datos.contrasena.value == "") {
            alert("Ingrese Clave");
        } else {
            document.Datos.submit();
        }
    }
</script>



<!-- /Start your project here-->

<!-- JQuery -->
<script type="text/javascript" src="cInterfaseUsuario_SITD/js_select/jquery-3.3.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="cInterfaseUsuario_SITD7js_select/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="cInterfaseUsuario_SITD/js_select/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="cInterfaseUsuario_SITD/js_select/mdb.min.js"></script>



</body>
</html>