<?php

/* * ************************************************************************************
  NOMBRE DEL PROGRAMA: ad_actualiza_tipo_doc.php
  SISTEMA: SISTEMA INTEGRAL DE TRÁMITE DOCUMENTARIO
  OBJETIVO: Procesamiento de Información de la Tabla Maestra de Tipo de Documentos
  -> Actualizar Registro de Tipo de Documento
  PROPIETARIO: AGENCIA PERUANA DE COOPERACIÓN INTERNACIONAL
  

  CONTROL DE VERSIONES:
  Ver      Autor             Fecha        Descripción
  ------------------------------------------------------------------------
  1.0   APCI       03/08/2018   Creación del programa.

  ------------------------------------------------------------------------
 * *************************************************************************************** */

$sql2 = " SELECT * FROM Tra_M_Tipo_Documento WHERE cDescTipoDoc='".utf8_decode($_POST[cDescTipoDoc])."' or cDescTipoDoc='".utf8_decode($_POST[cDescTipoDoc2])."' ";

$rs2 = mssql_query($sql2, $cnx);

$registro2 = MsSQL_num_rows($rs2);

if ($registro2 == 1) {

    $sql = "SP_TIPO_DOCUMENTO_UPDATE '".utf8_decode($_POST[cDescTipoDoc])."'  ,'$_POST[cCodTipoDoc]' ";
    $rs = mssql_query($sql, $cnx);
    mssql_close($cnx);
    //header("Location: ../cInterfaseUsuario_SITD/iu_tipo_doc.php?cDescTipoDoc=" . $_POST[cDescTipoDocx] . "&Entrada=" . $_POST[Entradax] . "&Interno=" . $_POST[Internox] . "&Salida=" . $_POST[Salidax] . "&pag=" . $_POST[pagx]);
	header("Location: ../cInterfaseUsuario_SITD/iu_tipo_doc.php?cDescTipoDoc=" . utf8_decode($_POST[cDescTipoDoc]) . "&Entrada=" . $_POST[Entradax] . "&Interno=" . $_POST[Internox] . "&Salida=" . $_POST[Salidax] . "&pag=" . $_POST[pagx]);
} else {
    if ($registro2 > 1) {
        header("Location: ../cInterfaseUsuario_SITD/iu_actualiza_tipo_doc.php?cDescTipoDoc=" . utf8_decode($_POST[cDescTipoDoc]));
    }
}
?>
