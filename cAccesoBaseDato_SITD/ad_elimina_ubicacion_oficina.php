<?php

/* * ************************************************************************************
  NOMBRE DEL PROGRAMA: ad_elimina_oficina.php
  SISTEMA: SISTEMA INTEGRAL DE TR�MITE DOCUMENTARIO
  OBJETIVO: Procesamiento de Informaci�n de la Tabla Maestra de Ubicaci�n de Oficinas
  -> Eliminar Registro de Ubicaci�n de Oficina
  PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL


  CONTROL DE VERSIONES:
  Ver      Autor             Fecha        Descripci�n
  ------------------------------------------------------------------------
  1.0   APCI       03/08/2018   Creaci�n del programa.

  ------------------------------------------------------------------------
 * *************************************************************************************** */
$sql = "SP_UBICACION_OFICINA_DELETE " . $_GET[id];
$rs = mssql_query($sql, $cnx);
header("Location:../cInterfaseUsuario_SITD/iu_ubicacion_oficina.php");
mssql_close($cnx);
?>