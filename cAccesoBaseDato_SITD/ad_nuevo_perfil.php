<?php

/* * ************************************************************************************
  NOMBRE DEL PROGRAMA: ad_nuevo_perfil.php
  SISTEMA: SISTEMA INTEGRAL DE TR�MITE DOCUMENTARIO
  OBJETIVO: Procesamiento de Informaci�n de la Tabla Maestra de Perfiles
  -> Crear Registro de Perfil
  PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL
  

  CONTROL DE VERSIONES:
  Ver      Autor             Fecha        Descripci�n
  ------------------------------------------------------------------------
  1.0   APCI       03/08/2018   Creaci�n del programa.

  ------------------------------------------------------------------------
 * *************************************************************************************** */
$sql = " SELECT * FROM Tra_M_Perfil WHERE cDescPerfil='$_POST[cDescPerfil]' ";

$rs = mssql_query($sql, $cnx);

$registro = MsSQL_num_rows($rs);

if ($registro == 0) {
    $sql = "SP_PERFIL_INSERT '$_POST[cDescPerfil]'";
    $rs = mssql_query($sql, $cnx);
    header("Location: ../cInterfaseUsuario_SITD/iu_perfil.php");
} else {
    if ($registro != 0) {
        header("Location: ../cInterfaseUsuario_SITD/iu_nuevo_perfil.php?cDescPerfil=" . $_POST[cDescPerfil]);
    }
}
?>

