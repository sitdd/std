<?php

/* * ************************************************************************************
  NOMBRE DEL PROGRAMA: ad_nuevo_tipo_doc.php
  SISTEMA: SISTEMA INTEGRAL DE TR�MITE DOCUMENTARIO
  OBJETIVO: Procesamiento de Informaci�n de la Tabla Maestra de Tipo de Documentos
  -> Crear Registro de Tipo de Documento
  PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL


  CONTROL DE VERSIONES:
  Ver      Autor             Fecha        Descripci�n
  ------------------------------------------------------------------------
  1.0   APCI       03/08/2018   Creaci�n del programa.

  ------------------------------------------------------------------------
 * *************************************************************************************** */
//echo $id;
$sql2 = " SELECT * FROM Tra_M_Tipo_Documento WHERE cDescTipoDoc='$_POST[cDescTipoDoc]' ";

$rs2 = mssql_query($sql2, $cnx);

$registro2 = MsSQL_num_rows($rs2);

if ($registro2 == 0) {
    /* $sql= "insert into Tra_M_Tipo_Documento (cDescTipoDoc,cSiglaDoc,nNumCorrelativo,nFlgEntrada,nFlgInterno) 
      VALUES ('$_POST[textdesc_doc]','$_POST[txtsigla_doc]','$_POST[txtnum_corr]','$_POST[txtflgentrada]','$_POST[txtflginterno]')"; */
    $sql = "SP_TIPO_DOCUMENTO_INSERT '$_POST[cDescTipoDoc]','0','0','0','0' ";
    $rs = mssql_query($sql, $cnx);
    header("Location: ../cInterfaseUsuario_SITD/iu_tipo_doc.php");
} else {

    if ($registro2 != 0) {
        header("Location: ../cInterfaseUsuario_SITD/iu_nuevo_tipo_doc.php?cDescTipoDoc=" . $_POST[cDescTipoDoc]);
    }
}
?>

