<?php

/* * ************************************************************************************
  NOMBRE DEL PROGRAMA: ad_nuevo_indicador.php
  SISTEMA: SISTEMA INTEGRAL DE TR�MITE DOCUMENTARIO
  OBJETIVO: Procesamiento de Informaci�n de la Tabla Maestra de Indicadores
  -> Crear Registro de Indicador
  PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL


  CONTROL DE VERSIONES:
  Ver      Autor             Fecha        Descripci�n
  ------------------------------------------------------------------------
  1.0   APCI       03/08/2018   Creaci�n del programa.

  ------------------------------------------------------------------------
 * *************************************************************************************** */
$sql1 = " SELECT * FROM Tra_M_Indicaciones WHERE cIndicacion='$_POST[cIndicacion]' ";

$rs1 = mssql_query($sql1, $cnx);
$registro1 = MsSQL_num_rows($rs1);

if ($registro1 == 0) {
    $sql = "SP_INDICADORES_INSERT '$_POST[cIndicacion]'";
    $rs = mssql_query($sql, $cnx);
    header("Location: ../cInterfaseUsuario_SITD/iu_indicadores.php");
} else {
    header("Location: ../cInterfaseUsuario_SITD/iu_nuevo_indicador.php?cIndicacion=" . $_POST[cIndicacion]);
}
?>

