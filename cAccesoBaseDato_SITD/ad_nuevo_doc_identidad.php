<?php

/* * ************************************************************************************
  NOMBRE DEL PROGRAMA: ad_nuevoa_doc_identidad.php
  SISTEMA: SISTEMA INTEGRAL DE TR�MITE DOCUMENTARIO
  OBJETIVO: Procesamiento de Informaci�n de la Tabla Maestra de Documentos de Identidad
  -> Crear Registro de Documento de Identidad
  PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL
  

  CONTROL DE VERSIONES:
  Ver      Autor             Fecha        Descripci�n
  ------------------------------------------------------------------------
  1.0   APCI       03/08/2018   Creaci�n del programa.

  ------------------------------------------------------------------------
 * *************************************************************************************** */
//echo $id;
$sql1 = " SELECT * FROM Tra_M_Doc_Identidad WHERE cDescDocIdentidad='$_POST[cDescDocIdentidad]' ";
$rs1 = mssql_query($sql1, $cnx);
$registro1 = MsSQL_num_rows($rs1);

if ($registro1 == 0) {
    $sql = "SP_DOC_IDENTIDAD_INSERT '$_POST[cDescDocIdentidad]' ";
    $rs = mssql_query($sql, $cnx);
    header("Location: ../cInterfaseUsuario_SITD/iu_doc_identidad.php");
} else {
    header("Location: ../cInterfaseUsuario_SITD/iu_nuevo_doc_identidad.php?cDescDocIdentidad=" . $_POST[cDescDocIdentidad]);
}
?>