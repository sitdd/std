<?php

/* * ************************************************************************************
  NOMBRE DEL PROGRAMA: ad_elimina_tipo_doc.php
  SISTEMA: SISTEMA INTEGRAL DE TR�MITE DOCUMENTARIO
  OBJETIVO: Procesamiento de Informaci�n de la Tabla Maestra de Tipo de Documentos
  -> Eliminar Registro de Tipo de Documento
  PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL


  CONTROL DE VERSIONES:
  Ver      Autor             Fecha        Descripci�n
  ------------------------------------------------------------------------
  1.0   APCI       03/08/2018   Creaci�n del programa.

  ------------------------------------------------------------------------
 * *************************************************************************************** */
/* $sql= "delete from Tra_M_Tipo_Documento WHERE cCodTipoDoc=".$id; */
$sql = "SP_TIPO_DOCUMENTO_DELETE " . $_GET[id];
$rs = mssql_query($sql, $cnx);
mssql_close($cnx);
header("Location: ../cInterfaseUsuario_SITD/iu_tipo_doc.php?Entrada=" . $Entrada . "&Interno=" . $Interno . "&Salida=" . $Salida . "&cDescTipoDoc=" . $cDescTipoDoc . "&pag=" . $pag);
?>