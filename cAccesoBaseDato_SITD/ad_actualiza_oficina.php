<?php

/* * ************************************************************************************
  NOMBRE DEL PROGRAMA: ad_actualiza_oficina.php
  SISTEMA: SISTEMA INTEGRAL DE TR�MITE DOCUMENTARIO
  OBJETIVO: Procesamiento de Informaci�n de la Tabla Maestra de Oficinas
  -> Actualizar Registro de Oficina
  -> Validar Ingreso de Valores Repetidos
  PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL


  CONTROL DE VERSIONES:
  Ver      Autor             Fecha        Descripci�n
  ------------------------------------------------------------------------
  1.0   APCI       03/08/2018   Creaci�n del programa.

  ------------------------------------------------------------------------
 * *************************************************************************************** */
$sql1 = " SP_OFICINA_LISTA_AR3 '$_POST[cNomOficina]' , '$_POST[cNomOficina2]' ";
$sql2 = " SP_OFICINA_LISTA_AR4 '$_POST[cSiglaOficina]' , '$_POST[cSiglaOficina2]' ";

$rs1 = mssql_query($sql1, $cnx);
$rs2 = mssql_query($sql2, $cnx);

$registro1 = MsSQL_num_rows($rs1);
$registro2 = MsSQL_num_rows($rs2);

if ($registro1 == 1 && $registro2 == 1) {
    $sql = "SP_OFICINA_UPDATE '$_POST[cNomOficina]','$_POST[cSiglaOficina]', '$_POST[iFlgEstado]','$_POST[iCodOficina]'";
    $rs = mssql_query($sql, $cnx);
    mssql_close($cnx);
    header("Location: ../cInterfaseUsuario_SITD/iu_oficinas.php?cNomOficina=" . $_POST['cNomOficinax'] . "&cSiglaOficina=" . $_POST['cSiglaOficinax'] .  "&iFlgEstado=" . $_POST['iFlgEstadox'] . "&pag=" . $_POST['pagx']);
} else {
    if ($registro1 > 1 && $registro2 == 1) {
        header("Location: ../cInterfaseUsuario_SITD/iu_actualiza_oficina.php?cNomOficina=" . $_POST[cNomOficina]);
    }

    if ($registro1 == 1 && $registro2 > 1) {
        header("Location: ../cInterfaseUsuario_SITD/iu_actualiza_oficina.php?cSiglaOficina=" . $_POST[cSiglaOficina]);
    }

    if ($registro1 > 1 && $registro2 > 1) {
        header("Location: ../cInterfaseUsuario_SITD/iu_actualiza_oficina.php?cSiglaOficina=" . $_POST[cSiglaOficina] . "&cNomOficina=" . $_POST[cNomOficina]);
    }
}
mssql_close($cnx);
?>