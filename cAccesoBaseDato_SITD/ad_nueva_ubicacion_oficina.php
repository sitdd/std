<?php

/* * ************************************************************************************
  NOMBRE DEL PROGRAMA: ad_nueva_ubicacion_oficina.php
  SISTEMA: SISTEMA INTEGRAL DE TR�MITE DOCUMENTARIO
  OBJETIVO: Procesamiento de Informaci�n de la Tabla Maestra de Ubicaci�n de Oficinas
  -> Crear Registro de Ubicaci�n de Oficina
  PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL


  CONTROL DE VERSIONES:
  Ver      Autor             Fecha        Descripci�n
  ------------------------------------------------------------------------
  1.0   APCI       03/08/2018   Creaci�n del programa.

  ------------------------------------------------------------------------
 * *************************************************************************************** */
//echo $cod;
$sql1 = " SELECT * FROM Tra_M_Ubicacion_Oficina WHERE cNomUbicacion='$_POST[cNomUbicacion]' ";
$rs1 = mssql_query($sql1, $cnx);
$registro1 = MsSQL_num_rows($rs1);

if ($registro1 == 0) {
    $sql = "SP_UBICACION_OFICINA_INSERT '$_POST[cNomUbicacion]','$_POST[nFlagEstado]' ";
    $rs = mssql_query($sql, $cnx);
    header("Location: ../cInterfaseUsuario_SITD/iu_ubicacion_oficina.php");
} else {
    if ($registro1 != 0) {
        header("Location: ../cInterfaseUsuario_SITD/iu_nueva_ubicacion_oficina.php?cNomUbicacion=" . $_POST[cNomUbicacion]);
    }
}
?>