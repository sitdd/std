<?php

/* * ************************************************************************************
  NOMBRE DEL PROGRAMA: ad_actualiza_flg_tipo_doc.php
  SISTEMA: SISTEMA INTEGRAL DE TR�MITE DOCUMENTARIO
  OBJETIVO: Procesamiento de Informaci�n de la Tabla Maestra de Tipo de Documentos
  -> Actualizar Si es Documento de Entrada,Interno o Salida
  PROPIETARIO: AGENCIA PERUANA DE COOPERACI�N INTERNACIONAL


  CONTROL DE VERSIONES:
  Ver      Autor             Fecha        Descripci�n
  ------------------------------------------------------------------------
  1.0   APCI       03/08/2018   Creaci�n del programa.

  ------------------------------------------------------------------------
 * *************************************************************************************** */

if ($_GET[Entrada] == '1') {
    $sql = "update Tra_M_Tipo_Documento SET nFlgEntrada='0'  where cCodTipoDoc=".$_GET[id];
    $rs = mssql_query($sql, $cnx);
    mssql_close($cnx);
    header("Location: ../cInterfaseUsuario_SITD/iu_tipo_doc.php");
} else if ($_GET[Entrada] == '0') {
    $sql = "update Tra_M_Tipo_Documento SET nFlgEntrada='1'  where cCodTipoDoc=".$_GET[id];
    $rs = mssql_query($sql, $cnx);
    mssql_close($cnx);
    header("Location: ../cInterfaseUsuario_SITD/iu_tipo_doc.php");
}

if ($_GET[Interno] == '1') {
    $sql = "update Tra_M_Tipo_Documento SET nFlgInterno='0' where cCodTipoDoc=".$_GET[id];
    $rs = mssql_query($sql, $cnx);
    mssql_close($cnx);
    header("Location: ../cInterfaseUsuario_SITD/iu_tipo_doc.php");
} else if ($_GET[Interno] == '0') {
    $sql = "update Tra_M_Tipo_Documento SET nFlgInterno='1'  where cCodTipoDoc=".$_GET[id];
    $rs = mssql_query($sql, $cnx);
    mssql_close($cnx);
    header("Location: ../cInterfaseUsuario_SITD/iu_tipo_doc.php");
}

if ($_GET[Salida] == '1') {
    $sql = "update Tra_M_Tipo_Documento SET nFlgSalida='0' where cCodTipoDoc=".$_GET[id];
    $rs = mssql_query($sql, $cnx);
    mssql_close($cnx);
    header("Location: ../cInterfaseUsuario_SITD/iu_tipo_doc.php");
} else if ($_GET[Salida] == '0') {
    $sql = "update Tra_M_Tipo_Documento SET nFlgSalida='1' where cCodTipoDoc=".$_GET[id];
    $rs = mssql_query($sql, $cnx);
    mssql_close($cnx);
    header("Location: ../cInterfaseUsuario_SITD/iu_tipo_doc.php");
}
?>
